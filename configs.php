<?php

define('ROOT',__DIR__.'/');
define('SITE_NAME','پزشکی فردوسی');
define('LOGO','/assets_app/logo2.png');
define('DEFAULT_LANG',['fa','فارسی']);

//در ثابت زیر زبان پیش فرض نباید قرار گیرد.
define('LANGUAGES',[
//    'en'=>['en','انگلیسی'],
//    'ru'=>['ru','روسی'],
]);


define('MENUS',[
    ['منوی اصلی','1'],
    ['منوی فوتر 1','2'],
    ['منوی فوتر 2','3'],
    ['منوی فوتر 3','4'],

]);


define('CAPTCHA',[
//    'login'=>['use','count','difficulty'],

    'login'=>[false,3,1],
    'register'=>[false,3,1],
    'register_verify'=>[false,3,1],
    'forgot'=>[false,3,1],
    'forgot_verify'=>[false,3,1],
]);
