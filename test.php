
r
1.2 MB

r
سلام مهندس، این نمونه سرویس جهت تست توکن و سرویس bi
http://2.180.30.220:22225
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <!--<script src="js/jQuery v3.5.1.js"></script>-->
    <script src="js/jQuery v3.6.1.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <title>تست اتصال به وب سرویس</title>
</head>
<body>
<div class="container">
    <h2 class="m-4 text-center">تست اتصال به وب سرویس</h2>
    <div class="row">
        <div class="col">
            <div class="card p-3">
                <h5 class="form-control text-center bg-light" id="showToken" style="white-space: pre-wrap; height: auto;"></h5>
                <input class="form-control bg-info" type="button" onclick="getTokenFromServer('PareshTest')" value="دریافت توکن" />
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col">
            <div class="card p-3">
                <input class="form-control text-center h5" id="textInput" type="text" value="morteza" placeholder="متن مورد نظر را درج نمایید" />
                <input class="form-control bg-info" type="button" onclick="checkConnectToApi()" value="اتصال به وب سرویس" />
                <h1 class="text-center mt-2 text-success" id="resValue"></h1>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script>
    function getTokenFromServer(valKay) {
        $.ajax({
            url: "http://localhost:8010/Token",
            type: 'POST',
            data: {'keyValue': valKay },
            error: function (err) {
                alert('در اتصال به وب سرویس خطای زیر رخ داده است \n' + err);
            },
            success: function (data) {
                if (data.result === "OK") {
                    //localStorage.setItem('token', data.result_value.token);
                    $("#showToken").html(data.result_value.token);
                } else {
                    alert(data.text);
                }
            }
        });
        return true;
    }

    function checkConnectToApi() {
        //var token = localStorage.getItem('token');
        var token = $("#showToken").html();
        $.ajax({
            url: "http://localhost:8010/Paresh/HelloApi",
            type: 'POST',
            data: { 'textInput': $("#textInput").val() },
            headers: { "Authorization": 'Bearer ' + token },
            error: function (err) {
                console.log(err);
                alert('در اتصال به وب سرویس خطای زیر رخ داده است \n' + err.statusText);
            },
            success: function (data) {
                console.log(data);
                if (data.result === "OK") {
                    $("#resValue").html(data.result_value.res);
                } else {
                    alert(data.text);
                    $("#resValue").html(data.result_value.res);
                }
            }
        });
        return true;
    }
</script>
