let VERSION_NUMB = 10 ;
const STATIC_CACHE_VERSION = 'static_'+VERSION_NUMB;
const DYNAMIC_CACHE_VERSION = 'dynamic_'+VERSION_NUMB;
const STATIC_ASSETS = [];
const STATIC_ASSETS1 = [
    "http://localhost/sorena_new/assets_app/img/icon/72x72.png",
    "http://localhost/sorena_new/assets_app/img/sample/avatar/avatar.jpg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/bag-add-outline.svg",
    "http://localhost/sorena_new/assets_app/js/base.js?v=2",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/book-outline.svg",
    "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css",
    "http://localhost/sorena_new/assets_app/css/inc/bootstrap/bootstrap.min.css",
    "http://localhost/sorena_new/assets_app/js/lib/bootstrap.min.js",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/business.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/cart.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/close-circle.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/close.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/cube-outline.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/document-text-outline.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/ellipsis-vertical.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/exit-outline.svg",
    "https://sorenamed.com/assets_app/img/fav144.jpg",
    "http://localhost/sorena_new/assets_app/img/fav192.jpg",
    "http://localhost/sorena_new/assets_app/img/fav48.png",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/headset.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/home-outline.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js",
    "http://localhost/sorena_new/assets_app/css/iransans.css",
    "http://localhost/sorena_new/assets_app/css/iransansfa.css",
    "http://localhost/sorena_new/assets_app/fonts/woff/IRANSansXFaNum-Bold.woff",
    "http://localhost/sorena_new/assets_app/fonts/woff/IRANSansXFaNum-Medium.woff",
    "http://localhost/sorena_new/assets_app/fonts/woff/IRANSansXFaNum-Regular.woff",
    "http://localhost/sorena_new/assets_app/css/iranyekan.css",
    "http://localhost/sorena_new/assets_app/css/iranyekanfa.css",
    "http://localhost/sorena_new/assets_app/js/lib/jquery.js",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/layers-outline.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/location.svg",
    "http://localhost/sorena_new/assets_app/img/logo1.png",
    "http://localhost/sorena_new/assets_app/img/logo2.png",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/menu-outline.svg",
    "http://localhost/sorena_new/assets_app/img/mobile-2.jpg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/moon-outline.svg",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/p-200bca6f.system.js",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/p-3460268c.system.entry.js",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/p-8ed28fab.system.js",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/p-af44f89e.system.js",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/person-add-outline.svg",
    "http://localhost/sorena_new/assets_app/js/plugins/progressbar-js/progressbar.min.js",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/share-outline.svg",
    "http://localhost/sorena_new/assets_app/css/inc/splide/splide.min.css",
    "http://localhost/sorena_new/assets_app/js/plugins/splide/splide.min.js",
    "http://localhost/sorena_new/assets_app/css/style.css",
    "http://localhost/sorena_new/assets_app/img/tile5.jpg",
    "http://localhost/sorena_new/assets_app/waiter.gif",
    "https://unpkg.com/ionicons@5.5.2/dist/ionicons/svg/wallet.svg",

];


self.addEventListener('install',function (event){

    event.waitUntil(
        caches.open(STATIC_CACHE_VERSION)
            .then((cache)=>{
                console.log('zx1')
                return cache.addAll(STATIC_ASSETS);
            })
            .then(function (){
                console.log('zx2')
                return self.skipWaiting();
            })
            .catch(e=>console.log(e))
    );
});
self.addEventListener('activate',function (event){
    console.log(caches.keys)
    event.waitUntil(
        caches.keys()
            .then((keys)=>{
                Promise.all(keys.map((key)=>{
                    console.log(key);
                    if(key !== STATIC_CACHE_VERSION && key !== DYNAMIC_CACHE_VERSION){
                        caches.delete(key);
                    }
                }))
            })
            .then(()=> self.clients.claim())
    )
});
self.addEventListener('fetch',function (eventt){
    const request = eventt.request;

    eventt.respondWith(
        caches.match(request)
            .then((response) => {
                return response || fetch_not_found(request)
            })
            .catch((e) => {
                if (request.headers.get('accept').includes('text/html')) {
                    return caches.match('offline.html');
                }
                if (request.url.match(/\.(jpe?g|png|gif|svg)$/)) {
                    return caches.match('images/offline.png');
                }
            })
    )

});

function fetch_not_found2(request){
    return fetch(request).then((res)=> {

        return res;
    })
}
function fetch_not_found(request){
    if (request.headers.get('accept').includes('text/html')) {
        return fetch(request).then((res)=> {

            return res;
        });
    }
    return fetch(request).then((res)=> {
        caches.open(DYNAMIC_CACHE_VERSION)
            .then((cache)=>{
                cache.put(request, res).then(r =>{});
            });
        return res.clone();
    })
}
function fetch_not_found1(request){
    return fetch(request).then((res)=> {
        caches.open(DYNAMIC_CACHE_VERSION)
            .then((cache)=>{
                cache.put(request, res).then(r =>{});
            });
        return res.clone();
    })
}

