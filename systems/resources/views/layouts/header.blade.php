<header class="header position-relative z-9">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-dark navbar-theme-primary fixed-top headroom">
        <div class="container position-relative">
            <a class="navbar-brand mr-lg-3" href="#">
                <img class="navbar-brand-dark" style="background: rgba(255,255,255,0.6);padding: 2px 7px;width: 120px" src="{{url('')}}/template/images/logo2.png" alt="منوی تصویر">
                <img class="navbar-brand-dark" style="background: rgba(255,255,255,0.6);padding: 2px 7px;" src="{{url('')}}/template/images/logo1.png" alt="منوی تصویر">
                <img class="navbar-brand-light" src="{{url('')}}/template/assets/img/logo-color.png" alt="منوی تصویر">
            </a>
            <div class="navbar-collapse collapse" id="navbar-default-primary">
                <div class="navbar-collapse-header">
                    <div class="row">
                        <div class="col-6 collapse-brand">
                            <a href="#">
                                <img src="{{url('')}}/template/assets/img/logo-color.png" alt="منوی تصویر">
                            </a>
                        </div>
                        <div class="col-6 collapse-close">
                            <i class="fas fa-times" data-toggle="collapse" role="button" data-target="#navbar-default-primary" aria-controls="navbar-default-primary" aria-expanded="false" aria-label="تغییر پیمایش"></i>
                        </div>
                    </div>
                </div>
                <ul class="navbar-nav navbar-nav-hover ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="@yield('link_home')#slider_home">صفحه اصلی</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="@yield('link_home')#order_section">ثبت سفارش</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="@yield('link_home')#about_section">درباره ما</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="@yield('link_home')#app_section">دریافت نرم افزار</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">با ما تماس بگیرید</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex align-items-center">
                <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target="#navbar-default-primary" aria-controls="navbar-default-primary" aria-expanded="false" aria-label="تغییر پیمایش">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </nav>
</header>
