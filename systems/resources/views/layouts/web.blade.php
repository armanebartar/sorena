<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="template/assets/img/favicon.png" type="image/png" sizes="16x16">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{url('')}}/template/assets/css/main.css">
    <base href="https://sorenamed.ir/" >
</head>
<body>

<div id="preloader">
    <div class="loader1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>

@include('layouts.header')

<div class="main">
    @yield('main')
</div>
<footer id="footer" class="footer-wrap">
    <div class="footer footer-top section section-md bg-primary text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 mb-4 mb-md-4 mb-lg-0">
                    <div class="rounded-custom text-center shadow-sm" style="background: #fff;box-shadow: 0 0 12px #fff !important;">
                        <div class="card-body py-5">
                            <div class="icon icon-md text-secondary">
                                <i class="ti-location-pin"></i>
                            </div>
                            <div>
                                <h5 style="color: black !important;" class="h6">دفتر مرکزی</h5>
                                <p style="color: black !important;" class="text-muted mb-0">
                                    مشهد، بلوار مدرس، مقابل بنیاد شهید، ساختمان مسکن، طبقه ۴ واحد ۴۴
                                    <br>
                                    کدپستی ۹۱۳۳۹۱۳۱۳۶
                                </p>
                                <table style="color: black">
                                    <tr>
                                        <td style="padding: 10px">
                                            <i class="ti-mobile text-secondary"></i>
                                            تلفن :
                                        </td>
                                        <td style="padding: 35px 34px 5px 5px;text-align: left;">
                                            ۰۵۱۳۲۲۸۰۲۰۴
                                            <br>
                                            ۰۵۱۳۲۲۳۷۰۵۴-۵
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 10px">

                                            فکس :
                                        </td>
                                        <td style="padding: 4px 34px 5px 5px;text-align: left;">
                                            ۰۵۱۳۲۲۸۰۲۰۷
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 mb-4 mb-md-4 mb-lg-0">
                    <div class="rounded-custom text-center shadow-sm" style="background: #fff;box-shadow: 0 0 12px #fff !important;">
                        <div class="card-body py-5">
                            <div class="icon icon-md text-secondary">
                                <i class="ti-location-pin"></i>
                            </div>
                            <div>
                                <h5 style="color: black !important;" class="h6">انبار پخش</h5>
                                <p style="color: black !important;" class="text-muted mb-0">
                                    مشهد، انتهای توس ۷۵، شهید نبی پور ۲۰ (جاده پرکندآباد)، مجتمع انبار های نگین توس، بلوک B پلاک 12
                                    <br>
                                    کد پستی:9197168567
                                </p>
                                <table style="color: black">
                                    <tr>
                                        <td style="padding: 10px">
                                            <i class="ti-mobile text-secondary"></i>
                                            تلفن :
                                        </td>
                                        <td style="padding: 35px 34px 5px 5px;text-align: left;">
                                            ۰۵۱۳۶۱۴۷۲۸۲
                                            <br>
                                            ۰۵۱۳۶۱۴۷۳۴۸-۵۳
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 10px">
                                            فکس :
                                        </td>
                                        <td style="padding: 4px 34px 5px 5px;text-align: left;">
                                            ۰۵۱۳۶۱۴۷۲۸۳

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer py-3 bg-primary text-white border-top border-variant-default">
        <div class="container">
            <div class="row">
                <div class="col p-3">
                    <div class="d-flex text-center justify-content-center align-items-center">
                        <p class="copyright pb-0 mb-0">
                            حق چاپ © 2021. کلیه حقوق محفوظ است.
                            طراحی و توسعه :
                            <a href="https://armanebartar.ir" target="_blank">گروه نرم‌افزاری آرمان برتر</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fas fa-hand-point-up"></span>
</button>

<script src="template/assets/js/vendors/jquery-3.5.1.min.js"></script>
<script src="template/assets/js/vendors/popper.min.js"></script>
<script src="template/assets/js/vendors/bootstrap.min.js"></script>
<script src="template/assets/js/vendors/jquery.magnific-popup.min.js"></script>
<script src="template/assets/js/vendors/jquery.easing.min.js"></script>
<script src="template/assets/js/vendors/mixitup.min.js"></script>
<script src="template/assets/js/vendors/headroom.min.js"></script>
<script src="template/assets/js/vendors/smooth-scroll.min.js"></script>
<script src="template/assets/js/vendors/wow.min.js"></script>
<script src="template/assets/js/vendors/owl.carousel.min.js"></script>
<script src="template/assets/js/vendors/jquery.waypoints.min.js"></script>
<!--<script src="template/assets/js/vendors/countUp.min.js"></script>-->
<script src="template/assets/js/vendors/jquery.countdown.min.js"></script>
<script src="template/assets/js/vendors/validator.min.js"></script>
<script src="template/assets/js/app.js"></script>
@yield('js')
<script>
    var iiii = 0;
    window.setInterval(function (){
        let bb = (iiii % 5) + 1;
        $('#slider_home').removeClass('my_bg1').removeClass('my_bg2').removeClass('my_bg3').removeClass('my_bg4').removeClass('my_bg5').addClass('my_bg'+bb);
        iiii++;
    },10000);
    window.setTimeout(function (){
        $('.my_lazy').each(function (){
            $(this).prop('src',$(this).data('src'));
        });
    },4000);
    $('#btn_check_and_send').on('click',function (e){
        e.preventDefault();
        if($(this).hasClass('act'))
            return;
        let this_btn = $(this);
        let this_btn_text = this_btn.html();
        let this_form = $('#form_check_and_send');
        this_btn.addClass('act').html('در حال پردازش ...');
        let this_data = this_form.serialize();
        let this_link = this_form.prop('action');
        $.ajax({
            url: this_link,
            type: "POST",
            data: this_data,
            dataType: 'json',
            success: function (data) {
                this_btn.removeClass('act').html(this_btn_text);
                alert(data.message);
            }
        });

    });
</script>
</body>
</html>
