<!doctype html>
<html lang="fa-IR" dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>@yield('title')</title>
    <base href="{{url('sorena_new')}}">
    <meta name="description" content="@yield('title')">
    <meta name="keywords" content="bootstrap 5, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets_app/img/fav48.png" sizes="32x32">

    <link rel="stylesheet" href="assets_app/css/style.css">
    <link rel="manifest" href="manifest.json">

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="sorene medical" />
    <link href="assets_app/img/fav192.jpg" rel="apple-touch-startup-image" />
    <link href="assets_app/img/fav.jpg" sizes="48x48" rel="apple-touch-icon" />
    <link href="assets_app/img/fav57.jpg" sizes="57x57" rel="apple-touch-icon" />
    <link href="assets_app/img/fav76.jpg" sizes="76x76" rel="apple-touch-icon" />
    <link href="assets_app/img/fav114.jpg" sizes="114x114" rel="apple-touch-icon" />
    <link href="assets_app/img/fav144.jpg" sizes="144x144" rel="apple-touch-icon" />
    <link href="assets_app/img/fav152.jpg" sizes="152x152" rel="apple-touch-icon" />
    <link href="assets_app/img/fav167.jpg" sizes="167x167" rel="apple-touch-icon" />
    <link href="assets_app/img/fav180.jpg" sizes="180x180" rel="apple-touch-icon" />
    <link href="assets_app/img/fav192.jpg" sizes="192x192" rel="apple-touch-icon" />
    <!--    end safari pwa-->
    <!--    start IE pwa-->
    <meta name="msapplication-TileImage" content="assets_app/img/fav744.png">
    <meta name="msapplication-TileColor" content="#fff">
    <meta name="msapplication-starturl" content="/">
    <!--    end IE pwa-->
    <!--    for browser not supported manifest-->
    <meta name="theme-color" content="#fff">

    <style>
        #appCapsule{
            min-height: 100vh;
            background: linear-gradient(-85deg, rgba(26, 44, 121, 0.6) 30%, rgba(232, 5, 102, 0.7) 100%) !important;
        }
    </style>
</head>

<body style="background-image: url('assets_app/img/tile5.jpg')">

<div id="loader">
    <div class="spinner-border text-primary" role="status"></div>
</div>

@yield('appHeader')

@yield('main')


<div id="notification-danger" class="notification-box tap-to-close">
    <div class="notification-dialog ios-style bg-danger">
        <div class="notification-header">
            <div class="in">
                <strong></strong>
            </div>
            <div class="right">
                <a href="#" class="close-button">
                    <ion-icon name="close-circle"></ion-icon>
                </a>
            </div>
        </div>
        <div class="notification-content">
            <div class="in">
                <div class="text"></div>
            </div>
            {{--                <div class="icon-box text-success">--}}
            {{--                    <ion-icon name="checkmark-circle-outline"></ion-icon>--}}
            {{--                </div>--}}
        </div>
    </div>
</div>
<div id="notification-success" class="notification-box tap-to-close">
    <div class="notification-dialog ios-style bg-success">
        <div class="notification-header">
            <div class="in">
                <strong></strong>
            </div>
            <div class="right">
                <a href="#" class="close-button">
                    <ion-icon name="close-circle"></ion-icon>
                </a>
            </div>
        </div>
        <div class="notification-content">
            <div class="in">
                <div class="text"></div>
            </div>
            {{--                <div class="icon-box text-success">--}}
            {{--                    <ion-icon name="checkmark-circle-outline"></ion-icon>--}}
            {{--                </div>--}}
        </div>
    </div>
</div>
<script src="assets_app/js/lib/bootstrap.min.js"></script>
<script src="assets_app/js/lib/jquery.js"></script>
<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<script src="assets_app/js/plugins/splide/splide.min.js"></script>
<script src="assets_app/js/plugins/progressbar-js/progressbar.min.js"></script>
{{--<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>--}}
{{--<script src="dist/js/jquery-3.6.0.min.js" ></script>--}}
<script src="{{url('')}}/assets_app/js/base.js?v=2"></script>
<script>
    function sep() {
        $(".number").each(function () {
            $(this).text(ToRial($(this).text()));
        })
    }
    function ToRial1(str) {
        str = toEnglishNumber(str);
        str = str.replace(/\,/g, '');
        str = str.replace(/\//g, '');

        var objRegex = new RegExp('(-?[0-9]+)([0-9]{3})');
        while (objRegex.test(str)) {
            str = str.replace(objRegex, '$1,$2');
        }
        // console.log(str);
        return str;
    }
    function fa2la(str){
        str = toEnglishNumber(str);

        str = str.replace(/\,/g, '');
        str = str.replace(/\//g, '');
        return str;
    }
    function toEnglishNumber(strNum) {
        let pn = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
        let en = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

        let cache = strNum;
        for (let i = 0; i < 10; i++) {
            let regex_fa = new RegExp(pn[i], 'g');
            cache = cache.replace(regex_fa, en[i]);
        }
        return cache;
    }
    $(document).ready(function() {
        $("input:text").focus(function() { $(this).select(); } );
        $("input[type=number]").focus(function() { $(this).select(); } );
        var my_body = $('body');
    my_body.on('keyup','.money',function (event) {
        $(this).css({'direction':'ltr','text-align':'left'});
        $(this).val($(this).val().replace(/[^0-9\.۰-۹]/g,''));
        // console.log("which : "+event.which);
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && !((event.which > 47 && event.which < 58) || (event.which > 1775 && event.which < 1786))) {
            event.preventDefault();
        }
        $(this).val(ToRial1($(this).val()));

    });
    });

</script>

@if(Session::has('alert-success'))
    <script>
        $('#notification-success .notification-header .in strong').text('{{Session::has('alert-success-title') ? Session::pull('alert-success-title') : 'تبریک !' }}');
        $('#notification-success .notification-content .in .text').text("{{ Session::pull('alert-success') }}");
        notification('notification-success',3000);
    </script>
@endif
@if(Session::has('alert-danger'))
    <script>
        $('#notification-danger .notification-header .in strong').text('{{Session::has('alert-danger-title') ? Session::pull('alert-danger-title') : 'تبریک !' }}');
        $('#notification-danger .notification-content .in .text').text("{{ Session::pull('alert-danger') }}");
        notification('notification-danger',3000);
    </script>
@endif
@yield('js')


</body>

</html>
