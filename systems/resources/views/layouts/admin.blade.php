<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Favicon-->
    <link rel="icon" href="{{url('assets/images/favicon.ico')}}" type="image/x-icon">
    <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />

    @yield('css')
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />



    <link href="{{url('assets/css/custom.css')}}" rel="stylesheet" />
</head>
<body class="light menu_dark theme-black logo-white submenu-closed rtl" data-notify="{{route('p_getNotify')}}">
<!-- Page Loader -->
<div class="page-loader-wrapper" style="z-index: 0">
    <div class="loader">
        <div class="m-t-30">
            <img class="loading-img-spin" src="{{url(LOGO)}}" width="20" height="20" alt="admin">
        </div>
        <p>{{SITE_NAME}}</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Top Bar -->
@php
    use Illuminate\Support\Facades\Auth;
    $userImage = Auth::guard('admin')->user()->image;
    $imageProfile = is_file(\App\Models\User::imagePath('/').$userImage) ? \App\User::imagePath('/').$userImage : 'assets/images/admin.png';
@endphp
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"
               aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" target="_blank" href="{{route('home')}}" style="padding-left: 5px">
                <img style="height: 40px;position: relative;top: -1px;" src="{{url(LOGO)}}" alt="" />
                <span style="padding-right: 0; font-size:21px " class="logo-name">{{SITE_NAME}}</span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="pull-right">
                <li>
                    <a href="javascript:void(0);" class="sidemenu-collapse">
                        <i class="material-icons">reorder</i>
                    </a>
                </li>
                <li>
                    <a href="" class="btn btn-warning">
                        تخلیه کش
                    </a>
                </li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <!-- Full Screen Button -->
                <li class="fullscreen">
                    <a href="javascript:;" class="fullscreen-btn">
                        <i class="fas fa-expand"></i>
                    </a>
                </li>
                <!-- #END# Full Screen Button -->

                <li class="dropdown user_profile">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <img src="{{ url($imageProfile) }}" width="32" height="32" alt="User">
                    </a>
                    <ul class="dropdown-menu pullDown">
                        <li class="body">
                            <ul class="user_dw_menu">

                                <li>
                                    <a href="{{route('p_profile_edit')}}">
                                        <i class="material-icons">feedback</i>ویرایش اطلاعات
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('p_profile_pass')}}">
                                        <i class="material-icons">key</i>تغییر پسورد
                                    </a>
                                </li>

                                <li>
                                    <a data-toggle="modal" href="#modalLogout" >
                                        <i class="material-icons">power_settings_new</i>خروج
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <!-- #END# Tasks -->
                <li class="pull-right" style="opacity: 0">
                    <a href="javascript:void(0);" class="js-right-sidebar" data-close="true">
                        <i class="fas fa-cog"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<div>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul id="sidebarMenu" class="list">
                <li class="sidebar-user-panel">
                    <div class="user-panel">
                        <div class=" image">

                            <img src="{{ url($imageProfile) }}" class="img-circle user-img-circle" alt="User Image" />
                        </div>
                    </div>
                    <div class="profile-usertitle">
                        <div class="sidebar-userpic-name">{{auth::guard('admin')->user()->name.' '.auth::guard('admin')->user()->family}} </div>
                        <div class="profile-usertitle-job ">مدیر </div>
                    </div>
                </li>

                <li style="display: none">
                    <a id="menuItemHidden" href="{{route('p_dashboard')}}">
                        <i class="fas fa-tachometer-alt"></i>
                        <span>داشبورد</span>
                    </a>
                </li>

                <li>
                    <a href="{{route('p_dashboard')}}">
                        <i class="fas fa-tachometer-alt"></i>
                        <span>داشبورد</span>
                    </a>
                </li>


                @foreach(\App\Models\Post_type::with('specials')->where('id','>',0)->get() as $val)
                    @php(\App\Models\Post_type::checkExistPermission($val))
                    @if(permission($val->type.'sShow',1) || permission('add'.ucfirst($val->type)))
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="far fa-newspaper"></i>
                                <span>{{$val->title}}</span>
                            </a>
                            <ul class="ml-menu">
                                @if(permission('add'.ucfirst($val->type)))
                                    <li>
                                        <a href="{{route('p_posts_add',[$val->type])}}">افزودن {{$val->nameSingle}} جدید</a>
                                    </li>
                                @endif
                                @if(permission($val->type.'sShow'))
                                    <li>
                                        <a id="menuItems{{ucfirst($val->type)}}" href="{{route('p_posts',[$val->type])}}">لیست {{$val->nameTotal}}</a>
                                    </li>
                                @endif
                                @if($val->status_category)
                                    <li>
                                        <a id="menuCat{{ucfirst($val->type)}}" href="{{route('p_posts_groups',[$val->type])}}">دسته ها</a>
                                    </li>
                                @endif
                                @if($val->status_tag)
                                    <li>
                                        <a id="menuTag{{ucfirst($val->type)}}" href="{{route('p_tags')}}">برچسبها</a>
                                    </li>
                                @endif
                                @if($val->status_brand)
                                    <li>
                                        <a id="menuBrand{{ucfirst($val->type)}}" href="{{route('p_posts_brands',[$val->type])}}">برندها</a>
                                    </li>
                                @endif

                            </ul>
                        </li>
                    @endif
                @endforeach
                @if(permission('specialProducts'))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="far fa-newspaper"></i>
                            <span>پستهای خاص</span>
                        </a>
                        <ul class="ml-menu">
                            @foreach(\App\Models\Special::all() as $group)
                                <li>
                                    <a id='addShopCat{{$group->id}}' href="{{route('p_posts_specials',[$group->post_type,$group->id,secure($group->post_type.$group->id)])}}">{{$group->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if(permission('showOptions') || permission('slider') || permission('menu'))
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="far fa-newspaper"></i>
                            <span>تنظیمات سایت</span>
                        </a>
                        <ul class="ml-menu">
                            @if(permission('showOptions'))
                            <li>
                                <a id="menuOptionsCommon" href="{{route('p_options')}}">همگانی</a>
                            </li>
                            @endif
                            @if(permission('slider'))
                            <li>
                                <a id="menuOptionsSliders" href="{{route('p_slider')}}">اسلایدشو</a>
                            </li>
                            @endif
                            @if(permission('m_comments'))
                            <li>
                                <a id="menuOptionsComments" href="{{route('p_m_comments')}}">نظرات مشتریان</a>
                            </li>
                            @endif
                                @if(permission('banner'))
                                    <li>
                                        <a id="menuOptionsBanners" href="{{route('p_banner')}}">بنرها</a>
                                    </li>
                                @endif
                            @if(permission('menu'))

                                    @if(count(MENUS) == 1)
                                        <li>
                                            <a id="menuOptionsMenu{{MENUS[0][1]}}" href="{{route('p_menu',[DEFAULT_LANG[0],MENUS[0][1]])}}">{{MENUS[0][0]}}</a>
                                        </li>
                                    @elseif(count(MENUS) > 1)
                                        <li>
                                            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                                                <span>منوها</span>
                                            </a>
                                            <ul class="ml-menu" style="display: none;">
                                                @foreach(MENUS as $menu)
                                                    <li>
                                                        <a id="menuOptionsMenu{{$menu[1]}}" href="{{route('p_menu',[DEFAULT_LANG[0],$menu[1]])}}">{{$menu[0]}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif

                            @endif
                        </ul>
                    </li>
                @endif

                @if(permission('factorsShow'))
                    <li>
                        <a id="list_orders" href="{{route('p_factors')}}">
                            <i class="fas fa-user"></i>
                            <span>سفارشات دریافتی</span>
                        </a>
                    </li>
                @endif
                @if(permission('userShow'))
                    <li>
                        <a href="{{route('p_users')}}">
                            <i class="fas fa-user"></i>
                            <span>لیست اعضای سایت</span>
                        </a>
                    </li>
                @endif

                @if(permission('staffList'))
                    <li>
                        <a id="menuItemsStaffList" href="{{route('p_staff')}}">
                            <i class="fas fa-user"></i>
                            <span>لیست مدیران سایت</span>
                        </a>
                    </li>
                @endif


{{--                @if(permission('CommentsShow'))--}}

{{--                    <li id="commentLi" class="numb">--}}
{{--                        <a id="commentsMenu" href="{{route('p_comments')}}">--}}
{{--                            <i class="far fa-comments"></i>--}}
{{--                            <span>نظرات دریافتی</span>--}}
{{--                        </a>--}}
{{--                        <span></span>--}}
{{--                    </li>--}}
{{--                @endif--}}

{{--                @if(permission('ContactsShow'))--}}
{{--                    <li id="contactLi" class="numb">--}}
{{--                        <a id="contactsMenu" href="{{route('p_contacts')}}">--}}
{{--                            <i class="far fa-comments"></i>--}}
{{--                            <span>تماس با ما</span>--}}
{{--                        </a>--}}
{{--                        <span></span>--}}
{{--                    </li>--}}
{{--                @endif--}}
            </ul>
        </div>
        <!-- #Menu -->
    </aside>


</div>

@yield('main')

<script src="{{url('assets/js/app.min.js')}}"></script>
<script src="{{url('assets/js/scripts.js')}}"></script>


@yield('js')

@if(Session::has('alert-success') || Session::has('alert-danger'))
    <link href="{{url('assets/js/bundles/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
    <script src="{{url('assets/js/bundles/sweetalert2/dist/sweetalert2.min.js')}}"></script>
    <script>
        @if(Session::has('alert-success'))
swal({
            position: 'top-end',
            type: 'success',
            title: '{{ Session::pull('alert-success') }}',
            showConfirmButton: true,
            timer: 4500,
            animation: true
        });
        @endif

        @if(Session::has('alert-danger'))
swal({
            position: 'top-end',
            type: 'error',
            title: '{!! Session::pull('alert-danger') !!} ',
            showConfirmButton: true,
            timer: 4500,
            animation: true
        });
        @endif
    </script>
@endif

<div class="modal fade" id="modalLogout">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">خروج</h4>
            </div>
            <div class="modal-body">
                آیا مطمئنید که قصد خروج از حساب کاربری خود را دارید؟
            </div>
            <form method="post" action="{{route('logout')}}">
                @csrf
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                    <button type="submit" class="btn btn-primary">بله، خارج میشوم.</button>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
