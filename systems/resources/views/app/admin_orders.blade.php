@extends('layouts.app')
@section('title','سفارشات - تجهیزات پزشکی فردوسی')
@section('js')
    <script>

    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
                <ion-icon name="chevron-back-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">سفارشات</div>
{{--        <div class="right">--}}
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
{{--        </div>--}}
    </div>

@stop

@section('main')
    <style>

    </style>
    <div id="appCapsule" >
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_admin_order',[1])}}" class="btn btn-warning btn-block">در انتظار تایید</a>
                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_admin_order',[3])}}" class="btn btn-primary btn-block">در انتظار تایید مشتری</a>
                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_admin_order',[4])}}" class="btn btn-info btn-block">تایید نهایی شده</a>
                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_admin_order',[-1])}}" class="btn btn-danger btn-block">رد شده</a>
                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_admin_order',[7])}}" class="btn btn-success btn-block">ارسال شده</a>
                </div>
            </div>
        </div>

    </div>



@stop
