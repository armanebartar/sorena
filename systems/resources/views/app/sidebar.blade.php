

<div class="offcanvas offcanvas-start" tabindex="-1" id="sidebarPanel">
    <div class="offcanvas-body">
        <!-- profile box -->
        <div class="profileBox">
            <div class="image-wrapper">
                <img src="{{auth()->check()&&is_file(ROOT.auth()->user()->image) ? ROOT.auth()->user()->image : "assets_app/img/sample/avatar/avatar.jpg"}}" alt="image" class="imaged rounded">
            </div>
            <div class="in">
                <strong>{{auth()->check()?auth()->user()->name.' '.auth()->user()->family : 'ناشناس'}}</strong>
                <div class="text-muted">
{{--                    <ion-icon name="location"></ion-icon>--}}
                    {{auth()->check()?auth()->user()->company : ''}}
                </div>
            </div>
            <a href="#" class="close-sidebar-button" data-bs-dismiss="offcanvas">
                <i class="far fa-times-circle"></i>
{{--                <ion-icon name="close"></ion-icon>--}}
            </a>
        </div>
        <!-- * profile box -->

        <ul class="listview flush transparent no-line image-listview mt-2">
            <li>
                <a href="{{route('app')}}" class="item">
                    <div class="icon-box bg-primary">
                        <i class="fas fa-home"></i>
{{--                        <ion-icon name="home-outline"></ion-icon>--}}
                    </div>
                    <div class="in">
                        صفحه اصلی
                    </div>
                </a>
            </li>
            <li>
                <a href="{{route('app_about')}}" class="item">
                    <div class="icon-box bg-primary">
                        <i class="far fa-building"></i>
{{--                        <ion-icon name="business"></ion-icon>--}}
                    </div>
                    <div class="in">
                        درباره ما
                    </div>
                </a>
            </li>
            <li>
                <a href="{{route('app_add_basket')}}" class="item">
                    <div class="icon-box bg-primary">
                        <i class="far fa-cart-plus"></i>
{{--                        <ion-icon name="cube-outline"></ion-icon>--}}
                    </div>
                    <div class="in">
                        ثبت سفارش
                    </div>
                </a>
            </li>
            @if(auth()->check())
            <li>
                <a href="{{route('app_profile')}}" class="item">
                    <div class="icon-box bg-primary">
                        <i class="fad fa-edit"></i>
{{--                        <ion-icon name="layers-outline"></ion-icon>--}}
                    </div>
                    <div class="in">
                        <div>ویرایش اطلاعات کاربری</div>
                    </div>
                </a>
            </li>
            <li>
                <a href="{{route('app_logout')}}" class="item">
                    <div class="icon-box bg-primary">
                        <i class="fas fa-sign-out"></i>
{{--                        <ion-icon name="exit-outline"></ion-icon>--}}
                    </div>
                    <div class="in">
                        <div>خروج از حساب کاربری</div>
                    </div>
                </a>
            </li>
            @else
                <li>
                    <a href="{{route('app_login')}}" class="item">
                        <div class="icon-box bg-primary">
                            <i class="fas fa-sign-in"></i>
{{--                            <ion-icon name="layers-login"></ion-icon>--}}
                        </div>
                        <div class="in">
                            <div>ورود به حساب کاربری</div>
                        </div>
                    </a>
                </li>
            @endif

            <li>
                <div class="item">
                    <div class="icon-box bg-primary">
                        <i class="fal fa-moon-stars"></i>
{{--                        <ion-icon name="moon-outline"></ion-icon>--}}
                    </div>
                    <div class="in">
                        <div>نسخه تاریک</div>
                        <div class="form-check form-switch">
                            <input class="form-check-input dark-mode-switch" type="checkbox" id="darkmodesidebar">
                            <label class="form-check-label" for="darkmodesidebar"></label>
                        </div>
                    </div>
                </div>
            </li>
        </ul>

{{--        <div class="listview-title mt-2 mb-1">--}}
{{--            <span>ارتباط با ما</span>--}}
{{--        </div>--}}
{{--        <ul class="listview image-listview flush transparent no-line">--}}
{{--            <li>--}}
{{--                <a href="page-chat.html" class="item">--}}
{{--                    <img src="assets_app/img/sample/avatar/avatar7.jpg" alt="image" class="image">--}}
{{--                    <div class="in">--}}
{{--                        <div>سوفی آسولد</div>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="page-chat.html" class="item">--}}
{{--                    <img src="assets_app/img/sample/avatar/avatar3.jpg" alt="image" class="image">--}}
{{--                    <div class="in">--}}
{{--                        <div>سباستین بنت</div>--}}
{{--                        <span class="badge badge-danger">6</span>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="page-chat.html" class="item">--}}
{{--                    <img src="assets_app/img/sample/avatar/avatar10.jpg" alt="image" class="image">--}}
{{--                    <div class="in">--}}
{{--                        <div>بث مورفی</div>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="page-chat.html" class="item">--}}
{{--                    <img src="assets_app/img/sample/avatar/avatar2.jpg" alt="image" class="image">--}}
{{--                    <div class="in">--}}
{{--                        <div>آملیا کابال</div>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="page-chat.html" class="item">--}}
{{--                    <img src="assets_app/img/sample/avatar/avatar5.jpg" alt="image" class="image">--}}
{{--                    <div class="in">--}}
{{--                        <div>هنری دو</div>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        </ul>--}}
    </div>
    <!-- sidebar buttons -->
{{--    <div class="sidebar-buttons">--}}
{{--        <a href="#" class="button">--}}
{{--            <ion-icon name="person-outline"></ion-icon>--}}
{{--        </a>--}}
{{--        <a href="#" class="button">--}}
{{--            <ion-icon name="archive-outline"></ion-icon>--}}
{{--        </a>--}}
{{--        <a href="#" class="button">--}}
{{--            <ion-icon name="settings-outline"></ion-icon>--}}
{{--        </a>--}}
{{--        <a href="#" class="button">--}}
{{--            <ion-icon name="log-out-outline"></ion-icon>--}}
{{--        </a>--}}
{{--    </div>--}}
    <!-- * sidebar buttons -->
</div>
