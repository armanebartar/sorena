@extends('layouts.app')
@section('title','ثبت سفارش - تجهیزات پزشکی فردوسی')
@section('js')
    <script>
        var numb_product = 1000;
        jQuery('.add_product').on('click',add_product);
        @if(auth()->check())
            add_product();
        @else
            add_product(false);
        $('#mobile').focus();
        @endif

        $('#products').on('click','.close_box' ,function (){
            $(this).parents('.card').detach();
        });

        function add_product(focused=true){
            jQuery('#products').append(`<div class="section mt-2">
            <div class="card">
                <div class="card-body">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="form-label" for="name${numb_product}">عنوان محصول</label>
                            <input type="text" name="name[]" class="form-control" id="name${numb_product}" placeholder="عنوان محصول">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
<!--                                <ion-icon name="close-circle"></ion-icon>-->
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed ">
                        <div class="input-wrapper">
                            <label class="form-label" for="numb${numb_product}">تعداد : </label>
                            <input type="text" name="numb[]" class="form-control money" id="numb${numb_product}" placeholder="تعداد" autocomplete="off">
                            <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
<!--                                <ion-icon name="close-circle"></ion-icon>-->
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed ">
                        <div class="input-wrapper">
                            <label class="form-label" for="unit${numb_product}">واحد : </label>
                            <input type="text" name="unit[]" class="form-control " id="unit${numb_product}" placeholder="واحد شمارش" autocomplete="off">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
<!--                                <ion-icon name="close-circle"></ion-icon>-->
                            </i>
                        </div>
                    </div>
                    <i class="close_box far fa-times-circle"></i>
<!--                    <ion-icon class="close_box" name="close-circle"></ion-icon>-->
                </div>
            </div>
        </div>`);

            if(focused) {
                $('#name' + numb_product).focus();
                if(numb_product > 1)
                $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            }
            numb_product++;

        }
        $('#save_factor').on('click',function (e){
            e.preventDefault();
            if($(this).hasClass('act'))
                return;
            let this_btn = $(this);
            let this_btn_text = this_btn.html();
            let this_form = $('#form_save');
            this_btn.addClass('act').html('در حال پردازش ...');
            let this_data = this_form.serialize();
            let this_link = this_form.prop('action');

            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {
                    this_btn.removeClass('act').html(this_btn_text);
                    if(data.res === 1){
                        $('#notification-danger .notification-header .in strong').text('خطای موبایل');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        $('#mobile_wrapper').addClass('alert');
                        window.setTimeout(function (){
                            $('#mobile_wrapper').removeClass('alert');
                        },5000);
                        return;
                    }
                    if(data.res !== 10){
                        $('#notification-danger .notification-header .in strong').text('خطا !');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        return;
                    }
                    location.assign("{{auth()->check() ? route('app') : route('app_confirm_mobile')}}")
                }
            });

        });
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">ثبت سفارش جدید</div>
        <div class="right">
            <i class="add_product fas fa-plus add_product" style="margin: auto"></i>
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
        </div>
    </div>

@stop

@section('main')
    <style>
        #products .card-body {
            padding: 5px 16px;
        }
        #products .card-body .count label {
            display: inline-block;width: 20%;text-align: left
        }
        #products .card-body .count input {
            display: inline-block;width: 75%
        }
        #mobile_wrapper.alert .input-info {
            color: red !important;
        }
        #mobile_wrapper.alert #mobile {
            color: red;
        }
        .close_box {
            color: red;
            font-size: 20px;
            position: absolute;
            top: 5px;
            left: 5px;
            cursor: pointer;
        }
    </style>
    <div id="appCapsule">
        <form id="form_save" method="post" action="{{route('add_basket_save')}}">
            @csrf
            @if(!auth()->check())
            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group basic animated" id="mobile_wrapper">
                            <div class="input-wrapper">
                                <label class="form-label" for="mobile">موبایل :‌</label>
                                <input type="number" name="mobile" class="form-control" id="mobile" placeholder="موبایل">
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
{{--                                    <ion-icon name="close-circle"></ion-icon>--}}
                                </i>
                            </div>
                            <div class="input-info">موبایل خود را مشابه نمونه وارد نمایید : 09123456789</div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div id="products">
                @php($temporary = session('user_add_order_temporary',[]))
                @if($temporary!=[] && isset($temporary['name']))
                    @foreach($temporary['name'] as $kk=>$ooo)
                        <div class="section mt-2">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="name{{$kk}}">عنوان محصول</label>
                                            <input type="text" name="name[]" value="{{$ooo}}" class="form-control" id="name{{$kk}}" placeholder="عنوان محصول">
                                            <i class="clear-input">
                                                <i class="far fa-times-circle"></i>
{{--                                                <ion-icon name="close-circle"></ion-icon>--}}
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form-group boxed ">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="numb{{$kk}}">تعداد : </label>
                                            <input type="text" name="numb[]" value="{{$temporary['numb'][$kk] ?? 1}}" class="form-control money" id="numb{{$kk}}" placeholder="تعداد" autocomplete="off">
                                            <i class="clear-input">
                                                <i class="far fa-times-circle"></i>
{{--                                                <ion-icon name="close-circle"></ion-icon>--}}
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form-group boxed ">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="unit{{$kk}}">واحد : </label>
                                            <input type="text" name="unit[]" value="{{$temporary['unit'][$kk] ?? ''}}" class="form-control" id="unit{{$kk}}" placeholder="واحد شمارش" autocomplete="off">
                                            <i class="clear-input">
                                                <i class="far fa-times-circle"></i>
{{--                                                <ion-icon name="close-circle"></ion-icon>--}}
                                            </i>
                                        </div>
                                    </div>
                                    <i class="close_box far fa-times-circle"></i>
{{--                                    <ion-icon class="close_box" name="close-circle"></ion-icon>--}}
                                </div>
                            </div>
                        </div>

                    @endforeach
                @endif
            </div>
            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="address5">توضیحات</label>
                                <textarea id="address5" name="desc" rows="2" class="form-control"></textarea>
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
{{--                                    <ion-icon name="close-circle"></ion-icon>--}}
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="address6">آدرس</label>
                                <textarea id="address6" name="address" rows="2" class="form-control">{{auth()->check()?auth()->user()->address : ''}}</textarea>
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
{{--                                    <ion-icon name="close-circle"></ion-icon>--}}
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
{{--            <div class="section mt-2">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body" style="padding: 5px 16px">--}}
{{--                        <div class="custom-file-upload" id="fileUpload1" style="height: 85px">--}}
{{--                            <input type="file" id="fileuploadInput" accept=".png, .jpg, .jpeg">--}}
{{--                            <label for="fileuploadInput">--}}
{{--                            <span>--}}
{{--                                <strong>--}}
{{--                                    <ion-icon name="cloud-upload-outline"></ion-icon>--}}
{{--                                    <i>برای آپلود ضربه بزنید</i>--}}
{{--                                </strong>--}}
{{--                            </span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </form>

        <div class="fab-button text bottom-left add_product" style="bottom: 75px;">
            <a href="#" class="fab" style="padding: 7px;height: unset;">
                <i class="fas fa-plus" style="margin: auto"></i>
{{--                <ion-icon name="add-outline" style="margin: auto"></ion-icon>--}}
            </a>
        </div>
    </div>
    <div class="appBottomMenu text-light" style="background: none;border-top: none;">
        <a href="#" id="save_factor" class="btn btn-success btn-block" >
            ذخیره سفارش
        </a>

    </div>


@stop
