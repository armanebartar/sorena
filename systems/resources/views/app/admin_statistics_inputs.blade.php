@extends('layouts.app')
@section('title','ورود از لینک - تجهیزات پزشکی فردوسی')
@section('js')
    <script>
        var page = 0;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        $('#other').on('click',function (){
            page++;
            get_users(page);
        });
        function get_users(page){
            $.ajax({
                url: "{{route('app_statisticsـget_inputs')}}",
                type: "POST",
                dataType: 'json',
                data: 'first={{$first}}&&page='+page,
                success: function (data) {
                    console.log(data)
                    $('#appCapsule #inputs').append(data.devices);
                    if(data.count <1)
                        $('#other').parents('.section').hide();
                }
            });
        }
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app_statistics')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">ورود از لینک
        @if($first)
            (اولین ورود)
            @endif
        </div>
{{--        <div class="right">--}}
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
{{--        </div>--}}
    </div>

@stop

@section('main')
    <style>
        .date{
            display: inline-block;
            direction: ltr;
        }
    </style>
    <div id="appCapsule" >
        <div id="inputs">
            {!! $devices['devices'] !!}
        </div>
        @if($devices['count']>0)
            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <button type="button" class="btn btn-warning btn-block" id="other">موارد بیشتر</button>
                    </div>
                </div>
            </div>
        @endif


    </div>



@stop
