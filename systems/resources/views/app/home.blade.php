@extends('layouts.app')
@section('title','تجهیزات پزشکی فردوسی')
@section('js')
    <script>
        window.setTimeout(function (){
            $('#appCapsule').addClass('my_bg_home');
            $('.load_img').each(function (){
                $(this).prop('src',$(this).data('src'));
            });
        },1000);


        window.setTimeout(function (){
            AddtoHome();
        },3000);

    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary scrolled">
        <div class="left">
            <a href="#" class="headerButton" data-bs-toggle="offcanvas" data-bs-target="#sidebarPanel">
                <i class="fas fa-bars"></i>
{{--                <ion-icon name="menu-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">
            تجهیزات پزشکی فردوسی
        </div>

    </div>
@stop

@section('main')
    <style>
        #appCapsule{
            background-size: cover;
            background-position: top;
        }
        #appCapsule.my_bg_home{
            background-image: url('assets_app/img/mobile-{{rand(1,4)}}.jpg')!important;
            background-size: cover!important;
        }

        #appCapsule .header-large-title{
            background: rgba(255,255,255,0.6);
            padding: 16px;
            width: max-content;
            margin: auto;
            text-align: center;
        }
        #appCapsule .header-large-title{
            background: rgba(255,255,255,0.6);
            padding: 8px 25px;
            width: max-content;
            margin: auto;
            text-align: center;
            color: #000;
            border-radius: 5px 37px;
        }
        #appCapsule .header-large-title h1{
            font-size: 16px;
            color: inherit;
        }
        #home_main .buttons a{
            width: 100%;
            height: 100px;
            opacity: 0.8;
            font-size: 14px;
        }
    </style>
    <div id="appCapsule" class="my_bg3">

        <div class="header-large-title">
            <h1 class="title">تجهیزات پزشکی فردوسی</h1>
            <h1 class="title" >تجهیزگران پویای امید سلامت</h1>
{{--            <h4 class="subtitle">به اپلیکیشن تجهیزات پزشکی فردوسی خوش آمدید</h4>--}}
        </div>



        <div class="section mt-3 mb-3" id="home_main">
            <div class="row buttons">
                <div class="col-6">
                    <a href="{{route('app_user_orders')}}" class="btn btn-primary me-1 mb-1">
                        <i class="fal fa-bags-shopping" style="font-size: 20px;margin-left: 8px;"></i>
{{--                        <ion-icon name="document-text-outline"></ion-icon>--}}
                        سفارشات من
                    </a>
                </div>
                @if(auth()->check())
                    <div class="col-6">
                        <a href="{{route('app_user_order',[3])}}" class="btn btn-info me-1 mb-1">
                            <i class="far fa-cart-arrow-down" style="font-size: 20px;margin-left: 8px;"></i>
{{--                            <ion-icon name="wallet"></ion-icon>--}}
                           پیش فاکتورها
                        </a>
                    </div>
                @else
                    <div class="col-6">
                        <a href="{{route('app_login')}}" class="btn btn-info me-1 mb-1">
                            <i class="fas fa-sign-in" style="font-size: 20px;margin-left: 8px;"></i>
{{--                            <ion-icon name="log-in-outline"></ion-icon>--}}
                            ورود به حساب
                        </a>
                    </div>
                @endif

                <div class="col-12">
                    <a href="{{route('app_add_basket')}}" class="btn btn-success me-1 mb-1">
                        <i class="far fa-cart-plus" style="font-size: 20px;margin-left: 8px;"></i>
{{--                        <ion-icon name="cart"></ion-icon>--}}
                        ثبت سفارش جدید
                    </a>
                </div>
                <div class="col-6">
                    <a href="https://sorenamed.ir/catalog/sorena_sdd.pdf?v={{rand(1,1000)}}" class="btn btn-danger me-1 mb-1">
                        <i class="fas fa-album-collection" style="font-size: 20px;margin-left: 8px;"></i>
{{--                        <ion-icon name="book-outline"></ion-icon>--}}
                        کاتالوگ دیجیتال
                    </a>
                </div>
                <div class="col-6">
                    <a href="{{route('app_contact')}}" class="btn btn-warning me-1 mb-1">
                        <i class="fas fa-headset" style="font-size: 20px;margin-left: 8px;"></i>
{{--                        <ion-icon name="headset"></ion-icon>--}}
                        تماس با ما
                    </a>
                </div>
                @if(auth()->check() && auth()->user()->role == 1)
                    @if(AB_perm('app_admin_orders'))
                        <div class="col-6">
                            <a href="{{route('app_admin_orders')}}" class="btn btn-warning me-1 mb-1">
                                <ion-icon name="document-text-outline"></ion-icon>
                                سفارشات
                            </a>
                        </div>
                    @endif
                    @if(AB_perm('app_register_admin'))
                        <div class="col-6">
                            <a href="{{route('app_register_admin2')}}" class="btn btn-warning me-1 mb-1">
                                <ion-icon name="person-add-outline"></ion-icon>
                                ثبت نام
                            </a>
                        </div>
                    @endif
                    @if(AB_perm('app_admin_add_order'))
                        <div class="col-6">
                            <a href="{{route('app_admin_add_order')}}" class="btn btn-warning me-1 mb-1">
                                <ion-icon name="bag-add-outline"></ion-icon>
                                سفارش برای مشتری
                            </a>
                        </div>
                    @endif
                    @if(AB_perm('app_statistics'))
                        <div class="col-6">
                            <a href="{{route('app_statistics')}}" class="btn btn-warning me-1 mb-1">
                                <ion-icon name="bag-add-outline"></ion-icon>
                                آمار فعالیت
                            </a>
                        </div>
                    @endif
                @endif

            </div>
        </div>







        <!-- app footer -->
        <div class="appFooter" style="background: rgba(255,255,255,0.6)">
            <img data-src="{{url("assets_app/img/logo1.png")}}" src="{{url('assets_app/waiter.gif')}}" style="height: unset;max-width: 30%;" alt="icon" class="footer-logo mb-2 load_img">
            <img data-src="{{url("assets_app/img/logo2.png")}}" src="{{url('assets_app/waiter.gif')}}" style="height: unset;width: 100px;margin-right: 45px;max-width: 30%;" alt="icon" class="footer-logo mb-2 load_img">

{{--            <div class="mt-2">--}}
{{--                <a href="#" class="btn btn-icon btn-sm btn-facebook">--}}
{{--                    <ion-icon name="logo-facebook"></ion-icon>--}}
{{--                </a>--}}
{{--                <a href="#" class="btn btn-icon btn-sm btn-twitter">--}}
{{--                    <ion-icon name="logo-twitter"></ion-icon>--}}
{{--                </a>--}}
{{--                <a href="#" class="btn btn-icon btn-sm btn-linkedin">--}}
{{--                    <ion-icon name="logo-linkedin"></ion-icon>--}}
{{--                </a>--}}
{{--                <a href="#" class="btn btn-icon btn-sm btn-instagram">--}}
{{--                    <ion-icon name="logo-instagram"></ion-icon>--}}
{{--                </a>--}}
{{--                <a href="#" class="btn btn-icon btn-sm btn-whatsapp">--}}
{{--                    <ion-icon name="logo-whatsapp"></ion-icon>--}}
{{--                </a>--}}
{{--                <a href="#" class="btn btn-icon btn-sm btn-secondary goTop">--}}
{{--                    <ion-icon name="arrow-up-outline"></ion-icon>--}}
{{--                </a>--}}
{{--            </div>--}}

        </div>
    </div>
{{--    <div class="appBottomMenu" style="min-height: 40px">--}}
{{--        <div class="footer-title" style="font-size: 12px">--}}
{{--            ©         تمامی حقوق برای تجهیزات پزشکی فردوسی محفوظ است.--}}
{{--        </div>--}}
{{--    </div>--}}
    @include('app.sidebar')
    <div id="notification-welcome" class="notification-box">
        <div class="notification-dialog android-style">
            <div class="notification-header">
                <div class="in">
                    <img data-src="{{url('assets_app/img/icon/72x72.png')}}" src="{{url('assets_app/waiter.gif')}}" alt="image" class="imaged w24 load_img">
                    <strong>موبایل کیت</strong>
                    <span>همین الان</span>
                </div>
                <a href="#" class="close-button">
                    <i class="far fa-times-circle"></i>
{{--                    <ion-icon name="close"></ion-icon>--}}
                </a>
            </div>
            <div class="notification-content">
                <div class="in">
                    <h3 class="subtitle">به موبایل کیت خوش آمدید</h3>
                    <div class="text">
                        Mobilekit قالب کیت رابط کاربری موبایل آماده PWA است.
                        راه عالی برای راه اندازی وب سایت های تلفن همراه و پروژه های pwa.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- iOS Add to Home Action Sheet -->
    <div class="offcanvas offcanvas-bottom action-sheet inset ios-add-to-home" tabindex="-1"
         id="ios-add-to-home-screen">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title">افزودن به صفحه اصلی</h5>
            <a href="#" class="close-button" data-bs-dismiss="offcanvas">
                <i class="far fa-times-circle"></i>
{{--                <ion-icon name="close"></ion-icon>--}}
            </a>
        </div>
        <div class="offcanvas-body">
            <div class="action-sheet-content text-center">
                <div class="mb-1"><img data-src="{{url('assets_app/img/fav48.jpg')}}" src="{{url('assets_app/waiter.gif')}}" alt="image" class="imaged w48 load_img">
                </div>
                <h4>تجهیزات پزشکی فردوسی</h4>
                <div>
                    نصب در صفحه اصلی گوشی  شما..
                </div>
                <div>
                    روی

                    <ion-icon name="share-outline"></ion-icon>
                    ضربه بزنید و به صفحه اصلی اضافه کنید.
                    (add to home screen)
                </div>
            </div>
        </div>
    </div>
    <!-- * iOS Add to Home Action Sheet -->


    <!-- Android Add to Home Action Sheet -->
    <div class="offcanvas offcanvas-top action-sheet inset android-add-to-home" tabindex="-1"
         id="android-add-to-home-screen">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title">افزودن به صفحه اصلی</h5>
            <a href="#" class="close-button" data-bs-dismiss="offcanvas">
                <i class="far fa-times-circle"></i>
{{--                <ion-icon name="close"></ion-icon>--}}
            </a>
        </div>
        <div class="offcanvas-body">
            <div class="action-sheet-content text-center">
                <div class="mb-1"><img data-src="{{url('assets_app/img/fav192.jpg')}}" src="{{url('assets_app/waiter.gif')}}" alt="image" class="imaged w48 load_img">
                </div>
                <h4>تجهیزات پزشکی فردوسی</h4>
                <div>
                    نصب در صفحه اصلی گوشی  شما..
                </div>
                <div>
                    روی
                    <i class="far fa-ellipsis-v"></i>
{{--                    <ion-icon name="ellipsis-vertical"></ion-icon>--}}
                    ضربه بزنید و به صفحه اصلی اضافه کنید.
                    (add to home screen)

                </div>
            </div>
        </div>
    </div>
    <!-- * Android Add to Home Action Sheet -->
@stop
