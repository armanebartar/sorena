@extends('layouts.app')
@section('title','ثبت نام از لینک - تجهیزات پزشکی فردوسی')
@section('js')
    <script>
        var page = 0;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        $('#other').on('click',function (){
            page++;
            $('#other').text('در حال پردازش ...');
            get_users(page);

        });
        var self1 = "{{$self}}";
        function get_users(page,is_search=false){
            let search = $('#search_field').val();
            $.ajax({
                url: "{{route('app_statisticsـget_registers')}}",
                type: "POST",
                dataType: 'json',
                data: {self1,page,search},
                {{--data: 'self={{$self}}&page='+page+'&serach='+serach,--}}
                success: function (data) {
                    if(is_search)
                        $('#appCapsule #inputs').html(data.users);
                    else
                        $('#appCapsule #inputs').append(data.users);
                    $('#other').text('موارد بیشتر');
                    if(data.count <1)
                        $('#other').parents('.section').hide();
                    else
                        $('#other').parents('.section').show();
                }
            });
        }
        $('#search_icon').on('click',function (){
            page = 0;
            get_users(page,true);
        });
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app_statistics')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">
        @if($self)
            ثبت نامهای مستقیم
            @else
                ثبت نام از لینک
            @endif
        </div>
        <div class="right">
            <a href="#" class="headerButton toggle-searchbox">
                <i class="fas fa-search"></i>
{{--                <ion-icon name="search-outline"></ion-icon>--}}
            </a>
        </div>
    </div>

    <div id="search" class="appHeader">
        <div class="form-group searchbox">
            <input id="search_field" type="text" class="form-control" value="" placeholder="جستجو در نام، نشان، موبایل و سازمان">
            <i class="input-icon" id="search_icon">
                <i class="fas fa-search"></i>
{{--                <ion-icon name="search-outline"></ion-icon>--}}
            </i>
            <a href="#" class="ms-1 close toggle-searchbox">
                <i class="far fa-times-circle"></i>
{{--                <ion-icon name="close-circle"></ion-icon>--}}
            </a>
        </div>
    </div>

@stop

@section('main')
    <style>
        .date{
            display: inline-block;
            direction: ltr;
        }
    </style>
    <div id="appCapsule">

        <div id="inputs">
            {!! $users['users'] !!}
        </div>
        @if($users['count']>0)
            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <button type="button" class="btn btn-warning btn-block" id="other">موارد بیشتر</button>
                    </div>
                </div>
            </div>
        @endif


    </div>



@stop
