@extends('layouts.app')
@section('title','آمار فعالیت - تجهیزات پزشکی فردوسی')
@section('js')
    <script>

    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">آمار فعالیت</div>
{{--        <div class="right">--}}
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
{{--        </div>--}}
    </div>

@stop

@section('main')
    <style>

    </style>
    <div id="appCapsule" >
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_statisticsـregisters',[0])}}" class="btn btn-info btn-block">
                        عضو از طریق لینک
                        ({{\App\Models\User::where([['importer_aff',auth()->user()->id]])->count()}})
                    </a>
                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_statisticsـregisters',[1])}}" class="btn btn-danger btn-block">
                        عضو مستقیم
                        ({{\App\Models\User::where([['importer',auth()->user()->id]])->count()}})
                    </a>
                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_statisticsـinputs',['first'=>1])}}" class="btn btn-warning btn-block">
                        اولین ورود از لینک
                        ({{\App\Models\Log_aff::where([['aff_id',auth()->user()->id],['first_aff',1]])->count()}})
                    </a>
                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <a href="{{route('app_statisticsـinputs',['first'=>0])}}" class="btn btn-primary btn-block">
                        ورود از طریق لینک
                        ({{\App\Models\Log_aff::where([['aff_id',auth()->user()->id]])->count()}})
                    </a>
                </div>
            </div>
        </div>



    </div>



@stop
