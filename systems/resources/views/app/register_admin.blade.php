@extends('layouts.app')
@section('title','ثبت نام - تجهیزات پزشکی فردوسی')
@section('js')
    <script>
        $('#btn_login').on('click',function (e){
            e.preventDefault();

            if($(this).hasClass('act'))
                return;
            let this_btn = $(this);
            let this_btn_text = this_btn.html();
            let this_form = $('#form_save');
            this_btn.addClass('act').html('در حال پردازش ...');
            let this_data = this_form.serialize();
            let this_link = this_form.prop('action');

            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {
                    this_btn.removeClass('act').html(this_btn_text);
                    if(data.res === 9){
                        location.assign("{{route('app_login')}}");
                        return;
                    }
                    if(data.res !== 10){
                        $('#notification-danger .notification-header .in strong').text('خطا !');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        return;
                    }
                    $('#notification-success .notification-header .in strong').text('تبریک !');
                    $('#notification-success .notification-content .in .text').text(data.message);
                    notification('notification-success',3000);
                    $('[name=family]').val('');
                    $('[name=mobile]').val('');
                    $('[name=company]').val('');
                    $('[name=company_post]').val('');
                    $('[name=pass]').val('');
                    $('[name=desc]').val('');
                    $('[name=name]').val('').focus();
                }
            });

        });
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">ثبت نام کاربر جدید</div>
{{--        <div class="right">--}}
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
{{--        </div>--}}
    </div>

@stop

@section('main')
    <style>

    </style>
    <form id="form_save" method="post" action="{{route('app_register_admin_save')}}">
        @csrf

    <div id="appCapsule" >
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" name="name" class="form-control" id="name" placeholder="نام">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" name="family" class="form-control" id="family" placeholder="نام خانوادگی">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="number" name="mobile" class="form-control" id="mobile" placeholder="موبایل">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" name="company" class="form-control" id="company" placeholder="شرکت / سازمان">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" name="company_post" class="form-control" id="company_post" placeholder="سمت در سازمان">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" name="pass" class="form-control" id="pass" placeholder="کلمه عبور">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" name="desc" class="form-control" id="desc" placeholder="توضیحات">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </form>
    <div class="form-button-group">
        <button type="submit" id="btn_login" class="btn btn-primary btn-block btn-lg">ثبت نام</button>
    </div>


@stop
