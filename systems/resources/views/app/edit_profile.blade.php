@extends('layouts.app')
@section('title','ویرایش پروفایل - تجهیزات پزشکی فردوسی')
@section('js')
    <script>

        $('#confirm_mobile').on('click',function (e){
            e.preventDefault();
            if($(this).hasClass('act'))
                return;
            let this_btn = $(this);
            let this_btn_text = this_btn.html();
            let this_form = $('#form_save');
            this_btn.addClass('act').html('در حال پردازش ...');
            let this_data = this_form.serialize();
            let this_link = this_form.prop('action');

            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {
                    this_btn.removeClass('act').html(this_btn_text);
                    if(data.res === 9){
                        location.assign("{{route('app_login')}}");
                        return;
                    }
                    if(data.res !== 10){
                        $('#notification-danger .notification-header .in strong').text('خطا !');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        return;
                    }
                    $('#notification-success .notification-header .in strong').text('تبریک !');
                    $('#notification-success .notification-content .in .text').text(data.message);
                    notification('notification-success',3000);
                    window.setTimeout(function (){
                        location.assign("{{route('app')}}");
                    },2500);
                }
            });

        });
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton goBack">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">ویرایش اطلاعات</div>
        <div class="right">
        </div>
    </div>
@stop

@section('main')
    <div id="appCapsule">

        <div class="login-form">

            <div class="section mt-2 mb-5">

                <form id="form_save" action="{{route('app_profile_save')}}">
                    @csrf

                    <div class="card">
                        <div class="card-body">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="form-label" for="fname">نام و نام خانوادگی : </label>
                            <input type="text" class="form-control" value="{{auth()->user()->name}}" name="name" id="fname" placeholder="نام و نام خانوادگی  ">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>


                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="form-label" for="company">نام سازمان  : </label>
                            <input type="text" name="company" value="{{auth()->user()->company}}" class="form-control" id="company" placeholder="سازمان  ">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="form-label" for="company_post">سمت در سازمان : </label>
                            <input type="text" name="company_post" value="{{auth()->user()->company_post}}" class="form-control" id="company_post" placeholder="سمت در سازمان  ">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="form-label" for="tel">تلفن : </label>
                            <input type="text" name="tel" value="{{auth()->user()->tel}}" class="form-control" id="tel" placeholder="تلفن  ">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="form-label" for="address">آدرس : </label>
                            <input type="text" name="address" value="{{auth()->user()->address}}" class="form-control" id="address" placeholder="آدرس  ">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>
                        </div>
                    </div>
                    <div class="appBottomMenu" style="background: none;border-top: none;">
                        <button type="submit" id="confirm_mobile" class="btn btn-success btn-block btn-lg">ذخیره تغییرات</button>
                    </div>

                </form>
            </div>
        </div>



    </div>
@stop
