@extends('layouts.app')
@section('title','ثبت سفارش - تجهیزات پزشکی فردوسی')
@section('js')
    <link href="dist/js/select2-develop/dist/css/select2.min.css" rel="stylesheet" />
    <script src="dist/js/select2-develop/dist/js/select2.full.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.my_select2').select2();
        });
        var numb_product = 1000;
        jQuery('.add_product').on('click',add_product);
        @if(auth()->check())
            add_product();
        @else
            add_product(false);
        $('#mobile').focus();

        @endif

        function add_product(focused=true){
            jQuery('#products').append(`<div class="section mt-2">
            <div class="card">
                <div class="card-body">
                <i class="far fa-times-circle close_box"></i>
                <ion-icon class="close_box" name="close-circle"></ion-icon>
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <label class="form-label" for="name${numb_product}">عنوان محصول</label>
                            <input type="text" name="name[]" class="form-control" id="name${numb_product}" placeholder="عنوان محصول">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
                    <div class="form-group boxed ">
                        <div class="input-wrapper">
                            <label class="form-label" for="numb${numb_product}">تعداد : </label>
                            <input type="text" name="numb[]" class="form-control money" id="numb${numb_product}" placeholder="تعداد" autocomplete="off">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed ">
                        <div class="input-wrapper">
                            <label class="form-label" for="unit${numb_product}">واحد شمارش</label>
                            <input type="text" name="unit[]" class="form-control" id="unit${numb_product}" placeholder="واحد شمارش">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
@if(AB_perm('app_admin_add_order_by_price'))
                    <div class="form-group boxed ">
                        <div class="input-wrapper">
                            <label class="form-label" for="price${numb_product}">
قیمت
<small>(ریال)</small>
 </label>
                            <input type="text" name="price[]" class="form-control money" id="price${numb_product}" placeholder="قیمت" >
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>
@endif
                </div>
            </div>
        </div>`);

            if(focused) {
                $('#name' + numb_product).focus();
                if(numb_product > 1)
                $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            }
            numb_product++;
        }

        window.setInterval(function (){
            let this_data = $('#form_save').serialize();
            let this_link = "{{route('app_admin_add_order_temporary')}}";
            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {

                }
            });
        },10000);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });
        $('#save_factor').on('click',function (e){
            e.preventDefault();
            if($(this).hasClass('act'))
                return;
            let this_btn = $(this);
            let this_btn_text = this_btn.html();
            let this_form = $('#form_save');
            this_btn.addClass('act').html('در حال پردازش ...');
            let this_data = this_form.serialize();
            let this_link = this_form.prop('action');

            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {
                    this_btn.removeClass('act').html(this_btn_text);
                    if(data.res === 1){
                        $('#notification-danger .notification-header .in strong').text('خطای موبایل');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        $('#mobile_wrapper').addClass('alert');
                        window.setTimeout(function (){
                            $('#mobile_wrapper').removeClass('alert');
                        },5000);
                        return;
                    }
                    if(data.res !== 10){
                        $('#notification-danger .notification-header .in strong').text('خطا !');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        return;
                    }
                    location.assign("{{auth()->check() ? route('app') : route('app_confirm_mobile')}}")
                }
            });

        });

        $('#user_target').on('change',function (){
            let user_id = $(this).val();
            $.ajax({
                url: "{{route('app_get_user_address')}}",
                type: "POST",
                data: {user_id},
                dataType: 'json',
                success: function (data) {
                    $('[name=address]').val(data.address);
                }
            });
        });
        $('#appCapsule').on('click','.close_box' ,function (){
            $(this).parents('.section').detach();
        });
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">ثبت سفارش جدید</div>
        <div class="right">
            <i class="add_product fas fa-plus add_product" style="margin: auto"></i>
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
        </div>
    </div>

@stop

@section('main')
    <style>
        #products .card-body{
            padding: 5px 16px;
        }
        #products .card-body .count label{
            display: inline-block;width: 20%;text-align: left
        }
        #products .card-body .count input{
            display: inline-block;width: 75%
        }
        #mobile_wrapper.alert .input-info{
            color: red !important;
        }
        #mobile_wrapper.alert #mobile{
            color: red;
        }
        .close_box {
            color: red;
            font-size: 25px;
            position: absolute;
            top: 1px;
            left: 1px;
            cursor: pointer;
            /*z-index: 1000;*/
        }
    </style>
    <div id="appCapsule" >
        <form id="form_save" method="post" action="{{route('app_admin_add_order_save')}}">
            @csrf

            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="user_target">کاربر مورد نظر</label>
                                <select class="form-control my_select2 form-select" id="user_target" name="user_id">
                                    <option value="0">انتخاب کاربر</option>
                                    @foreach(\App\Models\User::where('role',2)->get() as $user)
                                        <option value="{{$user->id}}">{{$user->name}} {{$user->family}} {{$user->company}} {{$user->mobile}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="input-info">کاربری که قصد ثبت سفارش برای وی را دارید با تایپ در باکس بالا انتخاب نمایید </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed" style="padding: 0 0 5px 0;">
                            <div class="input-wrapper">
                                <label class="form-label" for="name_company">سربرگ پیش فاکتور :‌</label>
                                <p>
                                    <label>
                                        <input checked="checked" style="position: relative;top: 5px" type="radio" name="name_company" value="1">
                                        تجهیزات پزشکی فردوسی
                                    </label>
                                </p>
                                <p>
                                    <label>
                                        <input style="position: relative;top: 5px" type="radio" name="name_company" value="2">
                                        تجهیزگران پویای امید سلامت
                                    </label>
                                </p>
                                <select style="display: none" name="name_company1" class="form-control" id="name_company">
                                    <option value="1">تجهیزات پزشکی فردوسی</option>
                                    <option value="2">تجهیزگران پویای امید سلامت</option>
                                </select>
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
{{--                                    <ion-icon name="close-circle"></ion-icon>--}}
                                </i>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div id="products">
                @php($temporary = session('admin_add_order_temporary',[]))
                @if($temporary!=[] && isset($temporary['name']))
                    @foreach($temporary['name'] as $kk=>$ooo)
                        <div class="section mt-2">
                            <div class="card">
                                <div class="card-body">
                                    <i class="far fa-times-circle close_box"></i>
{{--                                    <ion-icon class="close_box" name="close-circle"></ion-icon>--}}
                                    <div class="form-group boxed">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="name{{$kk}}">عنوان محصول</label>
                                            <input type="text" name="name[]" value="{{$ooo}}" class="form-control" id="name{{$kk}}" placeholder="عنوان محصول">
                                            <i class="clear-input">
                                                <i class="far fa-times-circle"></i>
{{--                                                <ion-icon name="close-circle"></ion-icon>--}}
                                            </i>
                                        </div>
                                    </div>
                                    <div class="form-group boxed ">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="numb{{$kk}}">تعداد : </label>
                                            <input type="text" name="numb[]" value="{{$temporary['numb'][$kk] ?? 1}}" class="form-control money" id="numb{{$kk}}" placeholder="تعداد" autocomplete="off">
                                            <i class="clear-input">
                                                <i class="far fa-times-circle"></i>
{{--                                                <ion-icon name="close-circle"></ion-icon>--}}
                                            </i>
                                        </div>
                                    </div>

                                    <div class="form-group boxed ">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="unit{{$kk}}">واحد شمارش</label>
                                            <input type="text" name="unit[]" value="{{$temporary['unit'][$kk] ?? ''}}" class="form-control" id="unit{{$kk}}" placeholder="واحد شمارش">
                                            <i class="clear-input">
                                                <i class="far fa-times-circle"></i>
{{--                                                <ion-icon name="close-circle"></ion-icon>--}}
                                            </i>
                                        </div>
                                    </div>
                                    @if(AB_perm('app_admin_add_order_by_price'))
                                    <div class="form-group boxed ">
                                        <div class="input-wrapper">
                                            <label class="form-label" for="price{{$kk}}">
                                                قیمت
                                                <small>(ریال)</small>
                                            </label>
                                            <input type="text" name="price[]" value="{{$temporary['price'][$kk] ?? ''}}" class="form-control money" id="price{{$kk}}" placeholder="قیمت" >
                                            <i class="clear-input">
                                                <i class="far fa-times-circle"></i>
{{--                                                <ion-icon name="close-circle"></ion-icon>--}}
                                            </i>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="address5">توضیحات</label>
                                <textarea name="desc" id="address5" rows="2" class="form-control"></textarea>
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
{{--                                    <ion-icon name="close-circle"></ion-icon>--}}
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section mt-2" style="padding-bottom: 35px">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="address6">آدرس</label>

                                <textarea id="address6" name="address" rows="2" class="form-control"></textarea>
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
{{--                                    <ion-icon name="close-circle"></ion-icon>--}}
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
{{--            <div class="section mt-2">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body" style="padding: 5px 16px">--}}
{{--                        <div class="custom-file-upload" id="fileUpload1" style="height: 85px">--}}
{{--                            <input type="file" id="fileuploadInput" accept=".png, .jpg, .jpeg">--}}
{{--                            <label for="fileuploadInput">--}}
{{--                            <span>--}}
{{--                                <strong>--}}
{{--                                    <ion-icon name="cloud-upload-outline"></ion-icon>--}}
{{--                                    <i>برای آپلود ضربه بزنید</i>--}}
{{--                                </strong>--}}
{{--                            </span>--}}
{{--                            </label>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </form>

        <div class="fab-button text bottom-left add_product" style="bottom: 75px;">
            <a href="#" class="fab" style="padding: 7px;height: unset;">
                <i class=" fas fa-plus " style="margin: auto"></i>
{{--                <ion-icon name="add-outline" style="margin: auto"></ion-icon>--}}
            </a>
        </div>
    </div>
    <div class="appBottomMenu text-light" style="background: none;border-top: none;">
        <a href="#" id="save_factor" class="btn btn-success btn-block" >
            ذخیره سفارش
        </a>

    </div>


@stop
