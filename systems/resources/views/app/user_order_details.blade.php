@extends('layouts.app')
@section('title','جزئیات سفارش شماره '.$order->id)
@section('js')
{{--    <script src="{{url('')}}/assets_app/js/plugins/html2pdf-master/lib/html2pdf.min.js"></script>--}}
    <script src="{{url('')}}/assets_app/html2pdf.js"></script>
    <script>
        $('.name_info').on('click',function (){
            $(this).parents('.form-group').find('input').val($(this).text());
        });
        $('#btn_save').on('click',function (e){
            e.preventDefault();
            if($(this).hasClass('act'))
                return;
            let this_btn = $(this);
            let this_btn_text = this_btn.html();
            let this_form = $('#form_save');
            this_btn.addClass('act').html('در حال پردازش ...');
            let this_data = this_form.serialize();
            let this_link = this_form.prop('action');

            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {
                    this_btn.removeClass('act').html(this_btn_text);

                    if(data.res !== 10){
                        $('#notification-danger .notification-header .in strong').text('خطا !');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        return;
                    }
                    $('#notification-success .notification-header .in strong').text('تبریک !');
                    $('#notification-success .notification-content .in .text').text(data.message);
                    notification('notification-success',3000);
                    window.setTimeout(function (){
                        location.assign("{{route('app_user_order',[$order->status])}}");
                    },2500);

                }
            });

        });

        $('#printed').on('click',function (){
            printDiv();
        });
        function printDiv() {
            let divContents = document.getElementById("GFG").innerHTML;
            let a = window.open('', '', 'height=500, width=500');
            a.document.write('<html><head><title>order_{{$order->id}}</title></head>');
            a.document.write('<body > <small>سفارش شماره {{$order->id}} <small><br>');
            a.document.write(divContents);
            a.document.write('</body></html>');
            a.document.close();
            a.print();
            // a.close();
        }
        $('.numb_prod').on('keyup',function (){
            calc_total();
        });

        function calc_total(){
            let total = 0;
            $('.numb_prod').each(function (){
                let new_numb = fa2la($(this).val());
                let tot = new_numb*$(this).data('price');
                $('#row_'+$(this).data('id')+' .numb').data('vall',new_numb).text(ToRial1(new_numb.toString()));
                $(this).parents('.card-body').find('.total').text(ToRial1(tot.toString()))
                total += tot;
            });
            $('.all_total2').text(ToRial1(total.toString()));

            let total_factor = 0;
            $('.rrr').each(function (){
                let tot2 = $(this).find('.numb').data('vall') * $(this).find('.price').data('vall');

                total_factor += tot2;
                $(this).find('.total').text(ToRial1(tot2.toString()));
            });
            $('#factor_total').text(ToRial1(total_factor.toString()));
            $('#factor_final').text(ToRial1(total_factor.toString()));
        }

        $('#appCapsule').on('click', '.close_box' ,function (){
            $(this).parents('.card').detach();
            calc_total();
        });

        function generatePDF() {
            let divContents = document.getElementById("GFG").innerHTML;
            let a = window.open('', '', 'height=500, width=3000');
            a.document.write('<html><head><meta charset="utf-8" /><title>order_{{$order->id}}</title></head>');
            a.document.write('<body > <small>سفارش شماره {{$order->id}} <small><br>');
            a.document.write(divContents);
            let linkee = "{{url('assets_app/html2pdf.js')}}";
            a.document.write('</body>');
            a.document.write(`<script src="${linkee}"><\/script>`);
            a.document.write('<script>html2pdf(document.body);<\/script>');
            a.document.write('</html>');
            // a.document.close();
            // a.print();
            // a.close();
        }


        function generatePDF1() {
            // Choose the element that our invoice is rendered in.
            const element = document.getElementById("GFG");

            html2pdf(element);
            return;
            // Choose the element and save the PDF for our user.
            let options = {
                filename: 'my-file.pdf'
            };

// Create instance of html2pdf class
            let exporter = new html2pdf(element, options);

// Download the PDF or...
            exporter.getPdf(true).then((pdf) => {
                console.log('pdf file downloaded');
            });

// Get the jsPDF object to work with it
            exporter.getPdf(false).then((pdf) => {
                console.log('doing something before downloading pdf file');
                pdf.save();
            });
        }
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app_user_order',[$status])}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">جزئیات سفارش شماره {{$order->id}}</div>
{{--        <div class="right">--}}
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
{{--        </div>--}}
    </div>

@stop

@section('main')
    <style>

        .orders p{
            margin-bottom: 5px;
        }
        .orders p span.time{
            direction: ltr;
            display: inline-block;
        }
        .close_box {
            color: red;
            font-size: 20px;
            position: absolute;
            top: 5px;
            left: 5px;
            cursor: pointer;
        }

    </style>
    <div id="appCapsule">
        <form id="form_save" method="post" action="{{route('app_user_order_confirm',[$order->id])}}">
            @csrf
            <input type="hidden" name="id" value="{{$order->id}}">
            <input type="hidden" name="secure" value="{{secure($order->id)}}">
            <input type="hidden" name="status" value="{{$order->status}}">

            <div  class="section mt-2 orders">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px;background: lightskyblue;">
                        <h3>سفارش شماره : {{$order->id}}</h3>
                        <p>تاریخ ثبت : <span class="time">{{jdate('Y-m-d H:i',$order->time)}}</span></p>
                      @if($status == 3)
                            <p>تاریخ انقضا : <span class="time">{{jdate('Y-m-d H:i',$order->expire)}}</span></p>
                        @endif
                    </div>
                </div>
            </div>
            @if($order->desc)
            <div  class="section mt-2 orders">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px;background: lightskyblue;">
                        <h4>توضیحات  :</h4>
                        <p>{{$order->desc}}</p>
                    </div>
                </div>
            </div>
            @endif

            @php($all_total = 0)
            @if($order->details && count($order->details))
                @foreach($order->details as $product)
                <div class="section mt-2">
                    <div class="card">
                        <div class="card-body" style="padding: 5px 16px">

                                <h4>{{$product->product_name}}</h4>
                            @if($status > 2)
                                @php($all_total += ($product->numb*$product->price))
                                <i class="far fa-times-circle close_box"></i>
{{--                                <ion-icon class="close_box" name="close-circle"></ion-icon>--}}
                                <div class="form-group boxed" style="padding: 0;">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="numb_{{$product->id}}">تعداد :‌</label>
                                        <input data-price="{{$product->price}}" data-id="{{$product->id}}" type="text" name="numb_{{$product->id}}" value="{{$product->numb}}" class="form-control numb_prod money" id="numb_{{$product->id}}" placeholder="تعداد">
                                        <i class="clear-input">
                                            <i class="far fa-times-circle"></i>
{{--                                            <ion-icon name="close-circle"></ion-icon>--}}
                                        </i>
                                    </div>
                                </div>
                                <h4>
                                    واحد شمارش :
                                    {{$product->unit}}
                                </h4>
                                <h4>قیمت واحد :
                                    {{sep($product->price)}}
                                    ریال
                                </h4>
                                <h4>قیمت کل :
                                    <span class="total">{{sep($product->price*$product->numb)}}</span>
                                    ریال
                                </h4>

                            @else
                                <h4>تعداد :‌
                                    {{$product->numb}}
                                    {{$product->unit}}
                                </h4>
                            @endif

                        </div>
                    </div>
                </div>
                @endforeach

                 @if($status>2)

                    <div class="section mt-2" >
                        <div class="card">
                            <div class="card-body" style="padding: 5px 16px">
                                <h4>
                                    جمع کل :
                                    <span class="all_total2">{{sep($all_total)}}</span>
                                    ریال
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="section mt-2" style="margin-bottom: 50px">
                        <div class="card">
                            <div class="card-body" style="padding: 5px 16px">
                                <button type="button" class="btn btn-primary" id="printed">پرینت پیش فاکتور</button>
                                <button type="button" onclick="generatePDF()">دانلود pdf</button>
                            </div>
                        </div>
                    </div>
                @endif
            @else
                <div class="section mt-2">
                    <div class="card">
                        <div class="card-body" style="padding: 5px 16px">
                            این سفارش فاقد محصول است !
                        </div>
                    </div>
                </div>
            @endif
            @if($status == 3)
                <div class="form-button-group">
                    <a id="btn_cancel" href="#"  class="btn btn-warning btn-block btn-lg" data-bs-toggle="modal" data-bs-target="#DialogBasic">لغو سفارش</a>
                    <a id="btn_save" href="#"  class="btn btn-success btn-block btn-lg">تایید سفارش</a>
                </div>
            @endif
        </form>
    </div>

    <div class="modal fade dialogbox" id="DialogBasic" data-bs-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">لغو سفارش</h5>
                </div>
                <div class="modal-body">
                    مطمئنید که قصد لغو این سفارش را دارید؟
                </div>
                <div class="modal-footer">
                    <div class="btn-inline">
                        <a href="#" class="btn btn-text-secondary" data-bs-dismiss="modal">خیر</a>
                        <a href="{{route('app_user_user_cancel_order',[$order->id,secure($order->id.'5')])}}" class="btn btn-text-primary">بله</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@if($status>2)
    <div id="GFG"
         style="display: none"
    ><meta charset="utf-8" />
        <style>
            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: 100;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Thin.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Thin.woff2') format('woff2');
            }

            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: 200;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-UltraLight.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-UltraLight.woff2') format('woff2');
            }

            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: 300;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Light.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Light.woff2') format('woff2');
            }

            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: 500;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Medium.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Medium.woff2') format('woff2');
            }

            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: 600;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-DemiBold.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-DemiBold.woff2') format('woff2');
            }

            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: 800;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-ExtraBold.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-ExtraBold.woff2') format('woff2');
            }

            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: 900;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Black.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Black.woff2') format('woff2');
            }

            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: bold;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Bold.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Bold.woff2') format('woff2');
            }

            @font-face {
                font-family: iransans;
                font-style: normal;
                font-weight: normal;
                src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Regular.woff') format('woff'),
                url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Regular.woff2') format('woff2');
            }
            #wrapper_print *{
                direction: rtl;
                border: none;
                padding: 0;
                margin: 0;
                font-family: iransans;
                font-size: 13px;
                box-sizing: border-box;
            }
            #wrapper_print{
                width: 210mm;
                max-width: 95% !important;
                border: 1px solid #000;
                min-height: 200px;
            }
            #wrapper_print #top{
                padding: 5px;
                border-bottom: 2px solid #777
            }
            #wrapper_print #top>div{
                display: inline-block;
                width: 32%;
            }
            #wrapper_print #top .logo img:first-child{
                width: 25mm;position: relative;top: -9px;
            }
            #wrapper_print #top .logo img:last-child{
                width: 15mm;
            }
            #wrapper_print #top h3{
                text-align: center;margin-bottom: 5px;
            }
            #wrapper_print #top h2{
                font-size: 16px;text-align: center;font-weight: bold;
            }
            #wrapper_print #top .time{
                font-size: 13px;text-align: left;
            }
            #wrapper_print #customer {
                padding: 5px;
                border-bottom: 2px solid #777;
            }
            #wrapper_print #customer .name{
                display: inline-block;
                width: 30%;
            }
            #wrapper_print #customer .company{
                display: inline-block;
                width: 30%;
            }
            #wrapper_print #customer .mobile{
                display: inline-block;
                width: 30%;
            }
            #wrapper_print #customer .address{
                padding: 15px 0;
            }
            #wrapper_print #details>div{
                border: 1px solid #999;
            }
            #wrapper_print #details>div>div{
                display: inline-block;
                border-left: 1px solid #999;
                padding: 5px;
            }
            #wrapper_print #details .radif{
                width: 5%;
            }
            #wrapper_print #details .product{
                width: 45%;
            }
            #wrapper_print #details .numb{
                width: 7%;
            }
            #wrapper_print #details .unit{
                width: 8%;
            }
            #wrapper_print #details .price{
                width: 15%;
            }
            #wrapper_print #details .total{
                width: 15%;
                border-left: none;
            }
            #wrapper_print #details .desc{
                width: 66%;
                border-left: none;

            }
            #wrapper_print #details .all_total{
                width: 33%;
                border-left: none;
                padding: 0;
            }
            #wrapper_print #details .all_total>div{
                border-bottom: 1px dashed #bbb;
                padding: 5px;
            }
            #wrapper_print #details .all_total>div:last-child{
                border-bottom: none;

            }
            #wrapper_print  #details .all_total>div b{
                display: inline-block;
                width: 40%;
            }
            #wrapper_print #details .all_total>div span{
                display: inline-block;
                width: 50%;
                text-align: left;
            }
            #wrapper_print #sighn>div{
                display: inline-block;
                width: 45%;
                padding-top: 95px;
                text-align: center;
                padding-bottom: 40px;
            }
            #wrapper_print #address{
                margin-top: 25px;
            }
            #wrapper_print #address>div{
                display: inline-block;
                width: 30%;
                position: relative;
            }
            #wrapper_print #address>div img{
                width: 25px;
                position: absolute;
                top: 0;
                right: 4px;
            }
            #wrapper_print #address>div p{
                text-align: justify;
                margin-right: 34px;
            }
        </style>
    <div id="wrapper_print">
        <div id="top">
            <div class="logo">
                <img src="{{url('')}}/assets_app/logo1.png">
                <img src="{{url('')}}/assets_app/logo_fer.png">
            </div>
            <div >
                <h3>«پیش فاکتور»</h3>
                <h2>
                    {{$order->company==2 ? 'تجهیزگران پویای امید سلامت' : 'تجهیزات پزشکی فردوسی'}}

                </h2>
            </div>
            <div class="time">
                <p>تاریخ :
                    <span style="display: inline-block">{{jdate('Y/m/d',$order->time)}}</span>
                </p>
                <p>شماره :
                    <span style="display: inline-block">{{jdate('Y',$order->time)}} - 000{{$order->id}}</span>

                </p>
            </div>
        </div>
        <div id="customer">
            <div class="name">
                <b>خریدار <small>(شرکت/آقا/خانم)</small> : </b>
                <span>{{$order->user ? $order->user->name.' '.$order->user->family : '--'}}</span>
            </div>
            <div class="company">
                <b>شرکت / سازمان : </b>
                <span>{{$order->user ? $order->user->company : '--'}}</span>
            </div>
            <div class="mobile">
                <b>تلفن : </b>
                <span>{{$order->user ? $order->user->mobile : $order->user_mobile}}</span>
            </div>
            <div class="address">
                <b>آدرس : </b>
                <span>
                    @if($order->address_static)
                        {{$order->address_static}}
                    @else
                    {{$order->user ? $order->user->address.' - '.$order->user->company : ' -- '}}
                    @endif
                </span>
            </div>
        </div>
        <div id="factor" style="padding: 5px">
            <div id="details">
                <div class="rr">
                    <div class="radif"><b>ردیف</b></div>
                    <div class="product"><b>عنوان محصول</b></div>
                    <div class="numb"><b>تعداد</b></div>
                    <div class="unit"><b>واحد</b></div>
                    <div class="price"><b>قیمت واحد</b><small>(ریال)</small></div>
                    <div class="total"><b>مبلغ کل</b><small>(ریال)</small></div>
                </div>
                @php($ij = 1)
                @php($tott = 0)
                @foreach($order->details as $product)
                <div id="row_{{$product->id}}" class="rr rrr">
                    <div class="radif">{{$ij++}}</div>
                    <div class="product">{{$product->product_name}}</div>
                    <div data-vall="{{$product->numb}}" class="numb">{{$product->numb}}</div>
                    <div class="unit">{{$product->unit}}</div>
                    <div data-vall="{{$product->price}}" class="price">{{sep($product->price)}}</div>
                    <div class="total">{{sep($product->price*$product->numb)}}</div>
                    @php($tott += $product->price*$product->numb)
                </div>
                @endforeach
                <div>
                    <div class="desc">
                        <p>توضیحات :
                            {{$order->desc}}
                        </p>
                    </div>
                    <div class="all_total">
                        <div>
                            <b>مبلغ کل <small>(ریال)</small> </b>
                            <span id="factor_total">{{sep($tott)}}</span>
                        </div>
                        <div><b>تخفیف : </b><span>0</span></div>
                        <div><b>عوارض : </b><span>0</span></div>
                        <div>
                            <b>قابل پرداخت <small>(ریال)</small> </b>
                            <span id="factor_final">{{sep($tott)}}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div id="sighn">
                <div style="display: inline-block;width: 45%;position: relative">
                    @php($company_id = $order->company==2 ? 2 : 1)
                    <img style="width: 54%;position: absolute;top: 0;left: 19%;" src="{{url('assets_app/img/s_'.$company_id.'.png')}}">
                    <b>امضا فروشنده </b>
                </div>
                <div style="display: inline-block;width: 45%">
                    <b>امضا خریدار</b>
                </div>
            </div>

            <div id="address">
                <div class="address">
                    <img src="{{url('')}}/assets_app/address.png">
                    <p>مشهد، بلوار مدرس، مقابل بنیاد شهید، ساختمان مسکن، طبقه ۴ واحد ۴۴
                        کدپستی ۹۱۳۳۹۱۳۱۳۶
                    </p>
                </div>
                <div class="tel" style="margin-right: 25px;width: 25%;">
                    <img src="{{url('')}}/assets_app/tel.png">
                    <p>
                        ۰۵۱۳۲۲۸۰۲۰۴
                        <br>
                        ۰۵۱۳۲۲۳۷۰۵۴-۵
                        <br>
                        <span style="opacity: 0;">sd</span>
                    </p>
                </div>
                <div class="fax">
                    <img src="{{url('')}}/assets_app/fax.png">
                    <p>
                        ۰۵۱۳۲۲۸۰۲۰۷
                        <br>
                        <span style="opacity: 0;">sd</span>
                        <br>
                        <span style="opacity: 0;">sd</span>
                    </p>
                </div>

            </div>

        </div>

    </div>
    </div>
@endif
@stop
