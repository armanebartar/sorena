@extends('layouts.app')
@section('title',$titre.' - تجهیزات پزشکی فردوسی')
@section('js')
    <script>

    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app_user_orders')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">سفارشات  {{$titre}}</div>
{{--        <div class="right">--}}
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
{{--        </div>--}}
    </div>

@stop

@section('main')
    <style>
        .orders>a{
            color: #888;
        }
        .orders>a p{
            margin-bottom: 5px;
        }
        .orders>a p span.time{
            direction: ltr;
            display: inline-block;
        }
    </style>
    <div id="appCapsule">
        @if($orders && count($orders))
            @foreach($orders as $order)
                <div  class="section mt-2 orders">
                    <a style="" href="{{route('app_user_order_details',[$order->id,$status])}}">
                        <div class="card">
                            <div class="card-body" style="padding: 5px 16px">
                                <h3>سفارش شماره : {{$order->id}}</h3>
                                <p>تاریخ ثبت : <span class="time">{{jdate('Y-m-d H:i',$order->time)}}</span></p>
                                <p>سازمان : {{$order->user ? $order->user->company.' - '.$order->user->company_post : ''}}</p>
                                <p>موبایل : {{$order->user ? $order->user->mobile : $order->user_mobile}}</p>

                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        @else
            <div class="section mt-2">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
در حال حاضر هیچ سفارش
                        {{$titre}}
           وجود ندارد
                    </div>
                </div>
            </div>
        @endif



    </div>



@stop
