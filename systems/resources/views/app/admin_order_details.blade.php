@extends('layouts.app')
@section('title','جزئیات سفارش شماره '.$order->id)
@section('js')
    <script>
        $('.name_info').on('click',function (){
            $(this).parents('.form-group').find('input').val($(this).text());
        });
        $('#btn_save').on('click',function (e){
            e.preventDefault();
            if($(this).hasClass('act'))
                return;
            let this_btn = $(this);
            let this_btn_text = this_btn.html();
            let this_form = $('#form_save');
            this_btn.addClass('act').html('در حال پردازش ...');
            let this_data = this_form.serialize();
            let this_link = this_form.prop('action');

            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {
                    this_btn.removeClass('act').html(this_btn_text);

                    if(data.res !== 10){
                        $('#notification-danger .notification-header .in strong').text('خطا !');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        $("html, body").animate({ scrollTop: 0 }, 500);
                        return;
                    }
                    $('#notification-success .notification-header .in strong').text('تبریک !');
                    $('#notification-success .notification-content .in .text').text(data.message);
                    notification('notification-success',3000);
                    window.setTimeout(function (){
                        location.assign("{{route('app_admin_order',[$status])}}");
                    },2500);

                }
            });

        });

        $('#appCapsule').on('click','.close_box' ,function (){
            $(this).parents('.section').detach();
        });
        let prod_row = 10000;
        $('.add_product').on('click',function (){
            add_product();
        });
        function add_product(focused=true){
            prod_row++;
            jQuery('#products').append(`
            <div class="section mt-2">
                    <div class="card">
                        <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed" style="padding: 0 0 5px 0;">
                            <div class="input-wrapper">
                                <label class="form-label" for="name_${prod_row}">عنوان محصول :‌</label>
                                        <input type="text" name="namee[]" value="" class="form-control" id="name_${prod_row}" placeholder="عنوان محصول">
                                        <i class="clear-input">
                                            <i class="far fa-times-circle"></i>
                                        </i>
                            </div>
                        </div>
                        <div class="form-group boxed" style=" padding: 0 0 5px 0;">
                            <div class="input-wrapper">
                                <label class="form-label" for="numb_${prod_row}">تعداد :‌</label>
                                    <input type="text" name="numbb[]" value="" class="form-control money" id="numb_${prod_row}" placeholder="تعداد">
                                    <i class="clear-input">
                                        <i class="far fa-times-circle"></i>
                                    </i>
                            </div>
                        </div>
                        <div class="form-group boxed" style="padding: 0 0 5px 0;">
                            <div class="input-wrapper">
                                <label class="form-label" for="unit_${prod_row}">واحد شمارش :‌</label>
                                    <input type="text" name="unitt[]" value="" class="form-control" id="unit_${prod_row}" placeholder="واحد شمارش">
                                    <i class="clear-input">
                                        <i class="far fa-times-circle"></i>
                                     </i>
                            </div>
                        </div>
                        <div class="form-group boxed" style="padding: 0 0 5px 0;">
                            <div class="input-wrapper">
                                <label class="form-label" for="price_${prod_row}">
                                            قیمت واحد
                                    <small>(ریال)</small>
                                </label>
                                <input type="text" name="pricee[]" value="" class="form-control money" id="price_${prod_row}" placeholder="قیمت واحد">
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
                                </i>
                            </div>
                        </div>
                 </div>
        </div>
    </div>`);

        }
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app_admin_order',[$status])}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">جزئیات سفارش شماره {{$order->id}}</div>
        <div class="right">
            <i class="fas fa-plus add_product" style="margin: auto"></i>
        </div>
    </div>

@stop

@section('main')
    <style>

        .orders p{
            margin-bottom: 5px;
        }
        .orders p span.time{
            direction: ltr;
            display: inline-block;
        }
        .close_box {
            color: red;
            font-size: 20px;
            position: absolute;
            top: 5px;
            left: 5px;
            cursor: pointer;
        }
    </style>
    <div id="appCapsule" >
        <form id="form_save" method="post" action="{{route('app_admin_save_change_order')}}">
            @csrf
            <input type="hidden" name="id" value="{{$order->id}}">
            <input type="hidden" name="secure" value="{{secure($order->id)}}">

            <div  class="section mt-2 orders">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px;background: lightskyblue;">
                        <h3>سفارش شماره : {{$order->id}}</h3>
                        <p>تاریخ ثبت : <span class="time">{{jdate('Y-m-d H:i',$order->time)}}</span></p>
                        <p>مشتری : {{$order->user ? $order->user->name.' '.$order->user->family : 'ناشناس'}}</p>
                        <p>سازمان : {{$order->user ? $order->user->company.' - '.$order->user->company_post : ''}}</p>
                        <p>موبایل : {{$order->user ? $order->user->mobile : $order->user_mobile}}</p>
                    </div>
                </div>
            </div>
            @if($order->desc)
            <div  class="section mt-2 orders">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px;background: lightskyblue;">
                        <h4>توضیحات مشتری :</h4>
                        <p>{{$order->desc}}</p>
                    </div>
                </div>
            </div>
            @endif
            @if($order->details && count($order->details))
                @if($order->status < 3)
                <div class="section mt-2">
                    <div class="card">
                        <div class="card-body" style="padding: 5px 16px">
                            <div class="form-group boxed" style="padding: 0 0 5px 0;">
                                <div class="input-wrapper">
                                    <label class="form-label" for="name_company">سربرگ پیش فاکتور :‌</label>
                                    <p>
                                        <label>
                                            <input checked="checked" style="position: relative;top: 5px" type="radio" name="name_company" value="1">
                                            تجهیزات پزشکی فردوسی
                                        </label>
                                    </p>
                                    <p>
                                        <label>
                                            <input style="position: relative;top: 5px" type="radio" name="name_company" value="2">
                                            تجهیزگران پویای امید سلامت
                                        </label>
                                    </p>
                                    <i class="clear-input">
                                        <i class="far fa-times-circle"></i>
{{--                                        <ion-icon name="close-circle"></ion-icon>--}}
                                    </i>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                @endif
                @foreach($order->details as $product)
                <div class="section mt-2">
                    <div class="card">
                        <div class="card-body" style="padding: 5px 16px">
                            @if($status < 5)

                                <div class="form-group boxed" style="padding: 0 0 5px 0;">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="name_{{$product->id}}">عنوان محصول :‌</label>
                                        <input type="text" name="name_{{$product->id}}" value="{{$product->product_name}}" class="form-control" id="name_{{$product->id}}" placeholder="عنوان محصول">
                                        <i class="clear-input">
                                            <i class="far fa-times-circle"></i>
{{--                                            <ion-icon name="close-circle"></ion-icon>--}}
                                        </i>
                                    </div>
                                    <div class="input-info name_info">{{$product->product_name}}</div>
                                </div>
                                <div class="form-group boxed" style=" padding: 0 0 5px 0;">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="numb_{{$product->id}}">تعداد :‌</label>
                                        <input type="text" name="numb_{{$product->id}}" value="{{$product->numb}}" class="form-control money" id="numb_{{$product->id}}" placeholder="تعداد">
                                        <i class="clear-input">
                                            <i class="far fa-times-circle"></i>
{{--                                            <ion-icon name="close-circle"></ion-icon>--}}
                                        </i>
                                    </div>
                                    <div class="input-info">{{$product->numb}}</div>
                                </div>
                                <div class="form-group boxed" style="padding: 0 0 5px 0;">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="unit_{{$product->id}}">واحد شمارش :‌</label>
                                        <input type="text" name="unit_{{$product->id}}" value="{{$product->unit}}" class="form-control" id="unit_{{$product->id}}" placeholder="واحد شمارش">
                                        <i class="clear-input">
                                            <i class="far fa-times-circle"></i>
{{--                                            <ion-icon name="close-circle"></ion-icon>--}}
                                        </i>
                                    </div>
                                    <div class="input-info">{{$product->unit}}</div>
                                </div>
                                @if(AB_perm('app_admin_add_order_by_price'))
                                <div class="form-group boxed" style="padding: 0 0 5px 0;">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="price_{{$product->id}}">
                                            قیمت واحد
                                            <small>(ریال)</small>
                                        </label>
                                        <input type="text" name="price_{{$product->id}}" value="{{$product->price}}" class="form-control money" id="price_{{$product->id}}" placeholder="قیمت واحد">
                                        <i class="clear-input">
                                            <i class="far fa-times-circle"></i>
{{--                                            <ion-icon name="close-circle"></ion-icon>--}}
                                        </i>
                                    </div>
                                    <div class="input-info">{{sep($product->price)}}</div>
                                </div>
                                @endif
                                <i class="far fa-times-circle close_box"></i>
{{--                                <ion-icon class="close_box" name="close-circle"></ion-icon>--}}
                            @else
                                <h4>{{$product->product_name}}</h4>
                                <h4>تعداد :‌
                                    {{$product->numb}}
                                    {{$product->unit}}
                                </h4>
                                <h4>قیمت واحد
                                    <small>(ریال)</small>
                                    :
                                {{sep($product->price)}}
                                </h4>
                                <h4>قیمت کل
                                    <small>(ریال)</small>
                                    :
                                {{sep($product->price*$product->numb)}}
                                </h4>
                            @endif

                        </div>
                    </div>
                </div>
                @endforeach
                <div id="products"></div>
            @else
                <div class="section mt-2">
                    <div class="card">
                        <div class="card-body" style="padding: 5px 16px">
                            این سفارش فاقد محصول است !
                        </div>
                    </div>
                </div>
            @endif
            <div class="section mt-2" style="padding-bottom: 35px">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="address6">آدرس</label>
                                @if($order->address_static)
                                    @php($address = $order->address_static)
                                @else
                                    @php($address = $order->user ? $order->user->address.' - '.$order->user->company : ' -- ')
                                @endif
                                <textarea id="address6" name="address" rows="2" class="form-control">{{$address}}</textarea>
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
{{--                                    <ion-icon name="close-circle"></ion-icon>--}}
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section mt-2" style="padding-bottom: 35px">
                <div class="card">
                    <div class="card-body" style="padding: 5px 16px">
                        <div class="form-group boxed">
                            <div class="input-wrapper">
                                <label class="form-label" for="desc6">توضیحات</label>

                                <textarea id="desc6" name="desc" rows="2" class="form-control"></textarea>
                                <i class="clear-input">
                                    <i class="far fa-times-circle"></i>
                                    {{--                                    <ion-icon name="close-circle"></ion-icon>--}}
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($status < 5)
                <div class="form-button-group">
                    <button type="submit" id="btn_save" class="btn btn-primary btn-block btn-lg">ذخیره تغییرات</button>
                </div>
            @endif
        </form>
    </div>



@stop
