@extends('layouts.app')
@section('title','تماس با ما - تجهیزات پزشکی فردوسی')
@section('js')
    <script>

    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">تماس با ما</div>
{{--        <div class="right">--}}
{{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
{{--        </div>--}}
    </div>

@stop

@section('main')
    <style>

    </style>
    <div id="appCapsule">
        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <h5  class="h6">دفتر مرکزی</h5>
                    <p  class="text-muted mb-0">
                        مشهد، بلوار مدرس، مقابل بنیاد شهید، ساختمان مسکن، طبقه ۴ واحد ۴۴
                        <br>
                        کدپستی ۹۱۳۳۹۱۳۱۳۶
                    </p>
                    <table style="color: black">
                        <tr>
                            <td style="padding: 10px">
                                <i class="ti-mobile text-secondary"></i>
                                تلفن :
                            </td>
                            <td style="padding: 35px 34px 5px 5px;text-align: left;">
                                <a href="tel:05132280201">۰۵۱۳۲۲۸۰۲۰۴</a><br>
                                <a href="tel:05132237054">۰۵۱۳۲۲۳۷۰۵۴</a><br>
                                <a href="tel:05132237055">۰۵۱۳۲۲۳۷۰۵۵</a>

                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">

                                فکس :
                            </td>
                            <td style="padding: 4px 34px 5px 5px;text-align: left;">
                                ۰۵۱۳۲۲۸۰۲۰۷

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="section mt-2">
            <div class="card">
                <div class="card-body" style="padding: 5px 16px">
                    <h5 class="h6">انبار پخش</h5>
                    <p class="text-muted mb-0">
                        مشهد، انتهای توس ۷۵، شهید نبی پور ۲۰ (جاده پرکندآباد)، مجتمع انبار های نگین توس، بلوک B پلاک 12
                        <br>
                        کد پستی:9197168567
                    </p>
                    <table style="color: black">
                        <tr>
                            <td style="padding: 10px">
                                <i class="ti-mobile text-secondary"></i>
                                تلفن :
                            </td>
                            <td style="padding: 35px 34px 5px 5px;text-align: left;">
                                <a href="tel:05136147282">۰۵۱۳۶۱۴۷۲۸۲</a><br>
                                <a href="tel:05136147348">۰۵۱۳۶۱۴۷۳۴۸</a><br>
                                <a href="tel:05136147349">۰۵۱۳۶۱۴۷۳۴۹</a><br>
                                <a href="tel:05136147350">۰۵۱۳۶۱۴۷۳۵۰</a><br>
                                <a href="tel:05136147351">۰۵۱۳۶۱۴۷۳۵۱</a><br>
                                <a href="tel:05136147352">۰۵۱۳۶۱۴۷۳۵۲</a><br>
                                <a href="tel:05136147353">۰۵۱۳۶۱۴۷۳۵۳</a>

                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px">

                                فکس :
                            </td>
                            <td style="padding: 4px 34px 5px 5px;text-align: left;">
                                ۰۵۱۳۶۱۴۷۲۸۳

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>



@stop
