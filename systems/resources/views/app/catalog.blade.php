@extends('layouts.app')
@section('title','کاتالوگ دیجیتال - تجهیزات پزشکی فردوسی')
@section('js')
    <script>

    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">{{$name}}</div>
        {{--        <div class="right">--}}
        {{--            <ion-icon class="add_product" name="add-outline" style="margin: auto"></ion-icon>--}}
        {{--        </div>--}}
    </div>

@stop

@section('main')
    <div id="appCapsule">
        <iframe style="width: 100%;height: 100vh" src="{{$link}}"></iframe>
    </div>
@stop
