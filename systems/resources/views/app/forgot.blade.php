@extends('layouts.app')
@section('title',$titre.' - تجهیزات پزشکی فردوسی')
@section('js')
    <script>
        $('#btn_login').on('click',function (e){
            e.preventDefault();
            if($(this).hasClass('act'))
                return;
            let this_btn = $(this);
            let this_btn_text = this_btn.html();
            let this_form = $('#form_login');
            this_btn.addClass('act').html('در حال پردازش ...');
            let this_data = this_form.serialize();
            let this_link = this_form.prop('action');

            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {
                    this_btn.removeClass('act').html(this_btn_text);
                    if(data.res === 9){
                        location.assign("{{route('app_login')}}");
                        return;
                    }
                    if(data.res !== 10){
                        $('#notification-danger .notification-header .in strong').text('خطا !');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        return;
                    }
                    $('#notification-success .notification-header .in strong').text('تبریک !');
                    $('#notification-success .notification-content .in .text').text(data.message);
                    notification('notification-success',3000);
                    window.setTimeout(function (){
                        location.assign("{{route('app_confirm_mobile')}}");
                    },2000);

                }
            });

        });
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app_login')}}" class="headerButton">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">{{$titre}}</div>
        <div class="right">
            <a class="text-primary" href="{{route('app_login')}}">ورود</a>
        </div>
    </div>

@stop

@section('main')
    <style>

    </style>
    <div id="appCapsule">
        <div class="login-form mt-1">

            <div class="section mt-1">
                <h1>{{$titre}}</h1>
                <h4>موبایل خود را در کادر زیر وارد نمایید</h4>
            </div>
            <div class="section mt-1 mb-5">
                <form action="{{route('app_forgoted')}}" method="post" id="form_login">
                    @csrf
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="number" name="mobile" class="form-control" id="email1" placeholder="موبایل">
                            <i class="clear-input">
                                <i class="far fa-times-circle"></i>
{{--                                <ion-icon name="close-circle"></ion-icon>--}}
                            </i>
                        </div>
                    </div>


                    <div class="form-button-group">
                        <button type="submit" id="btn_login" class="btn btn-primary btn-block btn-lg">بررسی</button>
                    </div>

                </form>
            </div>
        </div>
    </div>



@stop
