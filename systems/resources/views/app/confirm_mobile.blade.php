@extends('layouts.app')
@section('title','تایید موبایل - تجهیزات پزشکی فردوسی')
@section('js')
    <script>

        $('#confirm_mobile').on('click',function (e){
            e.preventDefault();
            if($(this).hasClass('act'))
                return;
            let this_btn = $(this);
            let this_btn_text = this_btn.html();
            let this_form = $('#form_save');
            this_btn.addClass('act').html('در حال پردازش ...');
            let this_data = this_form.serialize();
            let this_link = this_form.prop('action');

            $.ajax({
                url: this_link,
                type: "POST",
                data: this_data,
                dataType: 'json',
                success: function (data) {
                    this_btn.removeClass('act').html(this_btn_text);
                    if(data.res === 9){
                        location.assign("{{route('app')}}");
                        return;
                    }
                    if(data.res !== 10){
                        $('#notification-danger .notification-header .in strong').text('خطا !');
                        $('#notification-danger .notification-content .in .text').text(data.message);
                        notification('notification-danger',3000);
                        return;
                    }
                    location.assign("{{route('app_profile')}}")
                }
            });

        });
        window.setTimeout(function (){
            $('#smscode').focus();
        },500);
    </script>
@stop

@section('appHeader')
    <div class="appHeader bg-primary">
        <div class="left">
            <a href="{{route('app')}}" class="headerButton goBack">
                <i class="fas fa-chevron-right"></i>
{{--                <ion-icon name="chevron-back-outline"></ion-icon>--}}
            </a>
        </div>
        <div class="pageTitle">تایید موبایل</div>
        <div class="right">
        </div>
    </div>
@stop

@section('main')
    <div id="appCapsule" >

        <div class="login-form">
            <div class="section">
                <h1 style="opacity: 0;">تایید موبایل</h1>
                <h4>کد دریافتی را در کادر زیر وارد نمایید</h4>
            </div>
            <div class="section mt-2 mb-5">
                <form id="form_save" action="{{route('app_confirm_mobile_save')}}">
                    @csrf
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="number" name="code" class="form-control verify-input" id="smscode" placeholder="••••••" maxlength="5">
                        </div>
                    </div>

                    <div class="appBottomMenu" style="background: none;border-top: none;">
                        <button type="submit" id="confirm_mobile" class="btn btn-success btn-block btn-lg">تایید</button>
                    </div>

                </form>
            </div>
        </div>



    </div>
@stop
