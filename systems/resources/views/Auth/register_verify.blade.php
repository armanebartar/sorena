@extends('layouts.web')

@section('title') {{$opt['register2_title']}} @stop
@section('seo_title') {{$opt['register2_title']}} @stop
@section('seo_desc') {{$opt['register2_title']}} @stop


@section('main')

    <section class="breadscrumb-section pt-0">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-12">
                    <div class="breadscrumb-contain">
                        <h2 class="mb-2">تایید عضویت</h2>
                        <nav>
                            <ol class="breadcrumb mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{route('home')}}">
                                        <i class="fa-solid fa-house"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item active">تایید عضویت</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="log-in-section section-b-space">
        <div class="container-fluid-lg w-100">
            <div class="row">
                <div class="col-xxl-6 col-xl-5 col-lg-6 d-lg-block d-none ms-auto">
                    <div class="image-contain">
                        <img src="{{url('dist/images/inner-page/sign-up.png')}}" class="img-fluid" alt="ثبت نام {{$opt['site_name']}}">
                    </div>
                </div>

                <div class="col-xxl-4 col-xl-5 col-lg-6 me-auto">
                    <div class="log-in-box">
                        <div class="log-in-title">
                            <h3>{{$opt['register2_title']}}</h3>
                            <h4> {{$opt['register2_title2']}}</h4>
                            <p style="color: red;font-size: 0.8rem">
                                {{session('alert-danger-register2')}}
                                @php(session()->forget('alert-danger-register2'))
                            </p>
                           2
                        </div>

                        <div class="input-box">
                            <form class="row g-4" action="{{route('register_verify')}}" method="post">
                                @csrf
                                <div class="col-12">
                                    <div class="form-floating theme-form-floating">
                                        <input type="text" class="form-control" name="code" value="{{old('code')}}" id="fullname" placeholder="نام و نام خانوادگی ">
                                        <label for="fullname">{{$opt['register2_code']}} :</label>
                                    </div>
                                </div>


                                @if(isset(CAPTCHA['register_verify']) && CAPTCHA['register_verify'][0])

                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-floating theme-form-floating">
                                                    <input type="text" name="captcha" value="" class="form-control" id="captcha" placeholder="کد امنیتی">
                                                    <label for="captcha">کد امنیتی</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="captchaRefresh" style="cursor: pointer">
                                                <img src="{{url('assets/images/refresh-icon.png')}}">
                                                <img id="captcha" class="cap col-xs-5" src="{{route('captcha',['capRegister_verify',CAPTCHA['register_verify'][1],CAPTCHA['register_verify'][2]])}}">

                                            </div>
                                        </div>

                                    </div>

                                @endif



                                <div class="col-12">
                                    <button class="btn btn-animation w-100" type="submit">
                                        {{$opt['register2_btn']}}
                                    </button>
                                </div>
                            </form>
                        </div>


                        <div class="other-log-in">
                            <h6></h6>
                        </div>

                        <div class="sign-up-box">
                            <h4>{{$opt['register_login']}}</h4>
                            <a href="{{route('login')}}">
                                {{$opt['register_btnLogin']}}
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xxl-7 col-xl-6 col-lg-6"></div>
            </div>
        </div>
    </section>



@endsection

