@extends('layouts.auth')

@section('title') ورود به حساب کاربری - تجهیزات پزشکی فردوسی @stop
@section('seo_title')  ورود به حساب کاربری - تجهیزات پزشکی فردوسی  @stop
@section('seo_desc')  ورود به حساب کاربری - تجهیزات پزشکی فردوسی  @stop
@section('link_home',url(''))
@section('js')
    <script>
        $(function () {
            $('#captchaRefresh').on('click',function () {
                $('#captcha').prop('src',$('#captcha').prop('src')+'1');
            })
        })
    </script>
@stop


@section('main')

    <section class="section section-lg section-header position-relative min-vh-100 flex-column d-flex justify-content-center" style="background: url('template/assets/img/slider-bg-1.svg')no-repeat center bottom / cover">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-7 col-lg-6">
                    <div class="hero-content-left text-white">
                        <h1 class="display-2">خوش آمدید !</h1>
                        <p class="lead">
                            جهت استفاده کامل از امکانات سامانه وارد حساب کاربری خود شوید.
                        </p>
                    </div>
                </div>
                <div class="col-md-5 col-lg-5">
                    <div class="card login-signup-card shadow-lg mb-0">
                        <div class="card-body px-md-5 py-5">
                            <div class="mb-5">
                                <h3>وارد شدن</h3>
                                <p class="text-muted">برای ادامه وارد حساب خود شوید.</p>
                                <p class="text-danger">{{session('alert-danger-login','')}}</p>
                                @php(session(['alert-danger-login'=>'']))
                            </div>

                            <!--login form-->
                            <form class="login-signup-form" method="post" action="{{route('login')}}">
                                @csrf
                                <div class="form-group">
                                    <label class="font-weight-bold">موبایل</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-icon">
                                            <i class="ti-mobile"></i>
                                        </div>
                                        <input type="text" class="form-control" name="username" value="{{old('username')}}" placeholder="" required="">
                                    </div>
                                </div>
                                <!-- Password -->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label class="font-weight-bold">کلمه عبور</label>
                                        </div>
                                        <div class="col-auto">
                                            <a href="{{route('forgot')}}" class="form-text small text-muted">
                                                رمز عبور خود را فراموش کرده اید؟
                                            </a>
                                        </div>
                                    </div>
                                    <div class="input-group input-group-merge">
                                        <div class="input-icon">
                                            <i class="ti-lock"></i>
                                        </div>
                                        <input type="password" name="password" value="{{old('password')}}" class="form-control" placeholder="رمز ورود خود را وارد کنید">
                                    </div>
                                </div>

                                <!-- Submit -->
                                <button type="submit" class="btn btn-block btn-secondary mt-4 mb-3">ورود</button>

                            </form>
                        </div>
{{--                        <div class="card-footer bg-soft text-center border-top px-md-5"><small>ثبت نشده؟ </small>--}}
{{--                            <a href="sign-up.html" class="small">ایجاد حساب کاربری</a>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>



@stop
