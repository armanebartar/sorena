@extends('layouts.auth')

@section('title') تایید موبایل - تجهیزات پزشکی فردوسی @stop
@section('seo_title')  تایید موبایل - تجهیزات پزشکی فردوسی  @stop
@section('seo_desc')  تایید موبایل - تجهیزات پزشکی فردوسی  @stop
@section('link_home',url(''))
@section('js')
    <script>
        $(function () {
            $('#captchaRefresh').on('click',function () {
                $('#captcha').prop('src',$('#captcha').prop('src')+'1');
            })
        })
    </script>
@stop


@section('main')

    <section class="section section-lg section-header position-relative min-vh-100 flex-column d-flex justify-content-center" style="background: url('{{url("template/assets/img/slider-bg-1.svg")}}')no-repeat center bottom / cover">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-7 col-lg-6">
                    <div class="hero-content-left text-white">
                        <h1 class="display-2">تایید موبایل !</h1>
                        <p class="lead">
                            جهت بازیابی کلمه عبور خود، لطفا کد دریافتی از طریق پیامک را در کادر مربوطه وارد نمایید.
                        </p>
                    </div>
                </div>
                <div class="col-md-5 col-lg-5">
                    <div class="card login-signup-card shadow-lg mb-0">
                        <div class="card-body px-md-5 py-5">
                            <div class="mb-5">
                                <h3>تایید موبایل</h3>
                                <p class="text-muted">لطفا کد دریافتی را در کادر زیر وارد نمایید.</p>
                                <p class="text-danger">{{session('alert-danger-forgot2','')}}</p>
                                @php(session(['alert-danger-forgot2'=>'']))
                            </div>

                            <!--login form-->
                            <form class="login-signup-form" method="post" action="{{route('forgot_verify')}}">
                                @csrf
                                <div class="form-group">
                                    <label class="font-weight-bold">کد تایید :</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-icon">
                                            <i class="ti-mobile"></i>
                                        </div>
                                        <input type="text" class="form-control" name="code" value="{{old('code')}}" placeholder="" required="">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-block btn-secondary mt-4 mb-3">بررسی کد تایید</button>

                            </form>
                        </div>
                        <div class="card-footer bg-soft text-center border-top px-md-5"><small>ثبت نشده؟ </small>
                            <a href="sign-up.html" class="small">ایجاد حساب کاربری</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@stop
