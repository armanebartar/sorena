@extends('layouts.web')

@section('title') {{$opt['register_title']}} @stop
@section('seo_title') {{$opt['register_title']}} @stop
@section('seo_desc') {{$opt['register_desc']}} @stop
@section('js')
    <script>
        window.addEventListener('load',function (){
            $('#captchaRefresh').on('click',function () {
                $('#captcha').prop('src',$('#captcha').prop('src')+'1');
            })
        });

    </script>
@stop

@section('main')

    <section class="breadscrumb-section pt-0">
        <div class="container-fluid-lg">
            <div class="row">
                <div class="col-12">
                    <div class="breadscrumb-contain">
                        <h2 class="mb-2">عضویت</h2>
                        <nav>
                            <ol class="breadcrumb mb-0">
                                <li class="breadcrumb-item">
                                    <a href="{{route('home')}}">
                                        <i class="fa-solid fa-house"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item active">عضویت</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="log-in-section section-b-space">
        <div class="container-fluid-lg w-100">
            <div class="row">
                <div class="col-xxl-6 col-xl-5 col-lg-6 d-lg-block d-none ms-auto">
                    <div class="image-contain">
                        <img src="{{url('dist/images/inner-page/sign-up.png')}}" class="img-fluid" alt="ثبت نام {{$opt['site_name']}}">
                    </div>
                </div>

                <div class="col-xxl-4 col-xl-5 col-lg-6 me-auto">
                    <div class="log-in-box">
                        <div class="log-in-title">
                            <h3>به {{$opt['site_name']}} خوش آمدید</h3>
                            <h4> {{$opt['register_title']}}</h4>

                            <p style="color: red">
                                @foreach(session('errorsRegister',[]) as $err)
                                    {{$err}}
                                    <br>
                                @endforeach
                                @php(session()->forget('errorsRegister'))
                            </p>
                        </div>

                        <div class="input-box">
                            <form class="row g-4" method="post" action="{{route('register')}}">
                                @csrf
                                <div class="col-12">
                                    <div class="form-floating theme-form-floating">
                                        <input type="text" class="form-control" name="name" value="{{old('name')}}" id="fullname" placeholder="نام و نام خانوادگی ">
                                        <label for="fullname">نام و نام خانوادگی : </label>
                                    </div>
                                </div>
{{--                                <div class="col-12">--}}
{{--                                    <div class="form-floating theme-form-floating">--}}
{{--                                        <input type="text" class="form-control" name="family" value="{{old('family')}}" id="fullname2" placeholder="نام خانوادگی ">--}}
{{--                                        <label for="fullname2">{{$opt['family']}}</label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-12">--}}
{{--                                    <div class="form-floating theme-form-floating">--}}
{{--                                        <input type="text" class="form-control" name="username" value="{{old('username')}}" id="email" placeholder="نام کاربری">--}}
{{--                                        <label for="email">{{$opt['username']}}</label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-12">
                                    <div class="form-floating theme-form-floating">
                                        <input type="text" class="form-control" name="mobile" value="{{old('mobile')}}" id="mobile" placeholder="موبایل">
                                        <label for="mobile">{{$opt['mobile']}}</label>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-floating theme-form-floating">
                                        <input type="password" name="pass" value="{{old('pass')}}" class="form-control" id="password" placeholder="رمز عبور">
                                        <label for="password">رمز عبور</label>
                                    </div>
                                </div>
{{--                                <div class="col-12">--}}
{{--                                    <div class="form-floating theme-form-floating">--}}
{{--                                        <input type="password" name="pass2" value="" class="form-control" id="password2" placeholder="تکرار رمز عبور">--}}
{{--                                        <label for="password2">تکرار رمز عبور</label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                @if(isset(CAPTCHA['register']) && CAPTCHA['register'][0])

                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-floating theme-form-floating">
                                                    <input type="text" name="captcha" value="" class="form-control" id="captcha" placeholder="کد امنیتی">
                                                    <label for="captcha">کد امنیتی</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="captchaRefresh" style="cursor: pointer">
                                                <img src="{{url('assets/images/refresh-icon.png')}}">
                                                <img id="captcha" class="cap col-xs-5" src="{{route('captcha',['capRegister',CAPTCHA['register'][1],CAPTCHA['register'][2]]).'?i='.rand(1,1000)}}">

                                            </div>
                                        </div>

                                    </div>

                                @endif

                                <div class="col-12">
                                    <div class="forgot-box">
                                        <div class="form-check ps-0 m-0 remember-box">
                                            <input class="checkbox_animated check-box" checked="checked" name="roles" type="checkbox" id="customCheck3">
                                            <label class="form-check-label" for="customCheck3">
                                                {!! $opt['login_rules_alarm'] !!}
                                            </label>


                                        </div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button class="btn btn-animation w-100" type="submit">
                                        {{$opt['register_btn']}}
                                    </button>
                                </div>
                            </form>
                        </div>

{{--                        <div class="other-log-in">--}}
{{--                            <h6>یا</h6>--}}
{{--                        </div>--}}

{{--                        <div class="log-in-button">--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin"--}}
{{--                                       class="btn google-button w-100">--}}
{{--                                        <img src="assets/images/inner-page/google.png" class="blur-up lazyload"--}}
{{--                                             alt="">--}}
{{--                                        ثبت نام با گوگل--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="https://www.facebook.com/" class="btn google-button w-100">--}}
{{--                                        <img src="assets/images/inner-page/facebook.png" class="blur-up lazyload"--}}
{{--                                             alt=""> ثبت نام با فیسبوک--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}

                        <div class="other-log-in">
                            <h6></h6>
                        </div>

                        <div class="sign-up-box">
                            <h4>{{$opt['register_login']}}</h4>
                            <a href="{{route('login')}}">
                                {{$opt['register_btnLogin']}}
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xxl-7 col-xl-6 col-lg-6"></div>
            </div>
        </div>
    </section>

@endsection

