@extends('layouts.web')
@section('link_home','')
@section('title','جزئیات سفارش - تجهیزات پزشکی فردوسی')

@section('js')
    <script>
        $('#printed').on('click',function (){
            printDiv();
        });
        $('#generatePDF').on('click',function (){
            generatePDF();
        });
        function printDiv() {
            let divContents = document.getElementById("GFG").innerHTML;
            let a = window.open('', '', 'height=500, width=500');
            a.document.write('<html><head><title>order_{{$order->id}}</title></head>');
            a.document.write('<body > <small>سفارش شماره {{$order->id}} <small><br>');
            a.document.write(divContents);
            a.document.write('</body></html>');
            a.document.close();
            a.print();
            // a.close();
        }
        function generatePDF() {
            let divContents = document.getElementById("GFG").innerHTML;
            let a = window.open('', '', 'height=500, width=3000');
            a.document.write('<html><head><meta charset="utf-8" /><title>order_{{$order->id}}</title></head>');
            a.document.write('<body > <small>سفارش شماره {{$order->id}} <small><br>');
            a.document.write(divContents);
            let linkee = "{{url('assets_app/html2pdf.js')}}";
            a.document.write('</body>');
            a.document.write(`<script src="${linkee}"><\/script>`);
            a.document.write('<script>html2pdf(document.body);<\/script>');
            a.document.write('</html>');
            // a.document.close();
            // a.print();
            // a.close();
        }
    </script>
@stop

@section('main')
    <section class="" style="background: url('{{url("template/assets/img/header-bg-5.jpg")}}')no-repeat center center / cover">
        <div class="section-lg bg-gradient-primary text-white section-header">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-7">
                        <div class="page-header-content text-center">
                            <h1>سفارش شماره {{$order->id}}</h1>
                            <nav aria-label="جزئیات سفارش" class="d-flex justify-content-center">
                                <ol class="breadcrumb breadcrumb-transparent breadcrumb-text-light">
                                    <li class="breadcrumb-item"><a href="{{url('')}}">خانه</a></li>
                                    <li class="breadcrumb-item"><a href="{{route('profile')}}">پروفایل</a></li>

                                    <li class="breadcrumb-item active" aria-current="page">سفارش شماره {{$order->id}}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        #profile #sidebar ul{
            padding-right: 0;
            border: 1px solid #aaa;
        }
        #profile #sidebar li{
            list-style: none;
            text-align: right;
            padding: 0;
            border-bottom: 1px solid #aaa;
        }
        #profile #sidebar li a{
            display: block;
            width: 100%;
            height: 100%;
            padding: 10px 5px;
            transition: 0.4s;
        }
        #profile #sidebar li a:hover{
            background: #ccf;
        }
        #profile #sidebar li.active a{
            background: #ccf;
        }
        .box-order{
            box-shadow: 1px 1px 8px #aaa;
            border-radius: 5px;
            padding: 30px 15px;
            font-size: 15px;
            text-align: right;
            line-height: 40px;
            margin-bottom: 15px;
        }
    </style>
    <section id="profile" class="section section-lg  " style="padding-top: 2rem">
        <div class="container">

            <div class="row align-items-center justify-content-center">
                <div id="sidebar" class="col-md-3 mb-4 mb-md-4 mb-lg-0">
                    <div class="feature-widget text-center ">

                        <ul>

                            <li class="{{$status==3 ? 'active' : ''}}"><a href="{{route('profile',[3])}}">سفارشات در انتظار تایید من</a> </li>
                            <li class="{{$status==2 ? 'active' : ''}}"><a href="{{route('profile',[2])}}">سفارشات در انتظار تایید مدیر</a> </li>
                            <li class="{{$status==4 ? 'active' : ''}}"><a href="{{route('profile',[4])}}">سفارشات تایید شده</a> </li>
                            <li class="{{$status==0 ? 'active' : ''}}"><a href="{{route('profile',[0])}}">سفارشات رد شده</a> </li>
{{--                            <li class="{{$status==5 ? 'active' : ''}}"><a href="{{route('profile',[5])}}">ویرایش اطلاعات کاربری</a> </li>--}}
                        </ul>
                    </div>
                </div>

                <div class="col-md-9 mb-4 mb-md-4 mb-lg-0">
                    <div class="feature-widget text-center ">
                        <div id="wrapper_print">
                            <div id="top">
                                <div class="logo">
                                    <img src="{{url('')}}/assets_app/logo1.png">
                                    <img src="{{url('')}}/assets_app/logo_fer.png">
                                </div>
                                <div >
                                    <h3>«پیش فاکتور»</h3>
                                    <h2>
                                        {{$order->company==2 ? 'تجهیزگران پویای امید سلامت' : 'تجهیزات پزشکی فردوسی'}}

                                    </h2>
                                </div>
                                <div class="time">
                                    <p>تاریخ :
                                        <span style="display: inline-block">{{jdate('Y/m/d',$order->time)}}</span>
                                    </p>
                                    <p>شماره :
                                        <span style="display: inline-block">{{jdate('Y',$order->time)}} - 000{{$order->id}}</span>

                                    </p>
                                </div>
                            </div>
                            <div id="customer">
                                <div class="name">
                                    <b>خریدار <small>(شرکت/آقا/خانم)</small> : </b>
                                    <span>{{$order->user ? $order->user->name.' '.$order->user->family : '--'}}</span>
                                </div>
                                <div class="company">
                                    <b>شرکت / سازمان : </b>
                                    <span>{{$order->user ? $order->user->company : '--'}}</span>
                                </div>
                                <div class="mobile">
                                    <b>تلفن : </b>
                                    <span>{{$order->user ? $order->user->mobile : $order->user_mobile}}</span>
                                </div>
                                <div class="address">
                                    <b>آدرس : </b>
                                    <span>
                    @if($order->address_static)
                                            {{$order->address_static}}
                                        @else
                                            {{$order->user ? $order->user->address.' - '.$order->user->company : ' -- '}}
                                        @endif
                </span>
                                </div>
                            </div>
                            <div id="factor" style="padding: 5px">
                                <div id="details">
                                    <div class="rr">
                                        <div class="radif"><b>ردیف</b></div>
                                        <div class="product"><b>عنوان محصول</b></div>
                                        <div class="numb"><b>تعداد</b></div>
                                        <div class="unit"><b>واحد</b></div>
                                        <div class="price"><b>قیمت واحد</b><small>(ریال)</small></div>
                                        <div class="total"><b>مبلغ کل</b><small>(ریال)</small></div>
                                    </div>
                                    @php($ij = 1)
                                    @php($tott = 0)
                                    @foreach($order->details as $product)
                                        <div id="row_{{$product->id}}" class="rr rrr">
                                            <div class="radif">{{$ij++}}</div>
                                            <div class="product">{{$product->product_name}}</div>
                                            <div data-vall="{{$product->numb}}" class="numb">{{$product->numb}}</div>
                                            <div class="unit">{{$product->unit}}</div>
                                            <div data-vall="{{$product->price}}" class="price">{{sep($product->price)}}</div>
                                            <div class="total">{{sep($product->price*$product->numb)}}</div>
                                            @php($tott += $product->price*$product->numb)
                                        </div>
                                    @endforeach
                                    <div>
                                        <div class="desc">
                                            <p>توضیحات :
                                                {{$order->desc}}
                                            </p>
                                        </div>
                                        <div class="all_total">
                                            <div>
                                                <b>مبلغ کل <small>(ریال)</small> </b>
                                                <span id="factor_total">{{sep($tott)}}</span>
                                            </div>
                                            <div><b>تخفیف : </b><span>0</span></div>
                                            <div><b>عوارض : </b><span>0</span></div>
                                            <div>
                                                <b>قابل پرداخت <small>(ریال)</small> </b>
                                                <span id="factor_final">{{sep($tott)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="sighn">
                                    <div style="display: inline-block;width: 45%;position: relative">
                                        @php($company_id = $order->company==2 ? 2 : 1)
                                        <img style="width: 54%;position: absolute;top: 0;left: 19%;" src="{{url('assets_app/img/s_'.$company_id.'.png')}}">
                                        <b>امضا فروشنده </b>
                                    </div>
                                    <div style="display: inline-block;width: 45%">
                                        <b>امضا خریدار</b>
                                    </div>
                                </div>

                                <div id="address">
                                    <div class="address">
                                        <img src="{{url('')}}/assets_app/address.png">
                                        <p>مشهد، بلوار مدرس، مقابل بنیاد شهید، ساختمان مسکن، طبقه ۴ واحد ۴۴
                                            کدپستی ۹۱۳۳۹۱۳۱۳۶
                                        </p>
                                    </div>
                                    <div class="tel" style="margin-right: 25px;width: 25%;">
                                        <img src="{{url('')}}/assets_app/tel.png">
                                        <p>
                                            ۰۵۱۳۲۲۸۰۲۰۴
                                            <br>
                                            ۰۵۱۳۲۲۳۷۰۵۴-۵
                                            <br>
                                            <span style="opacity: 0;">sd</span>
                                        </p>
                                    </div>
                                    <div class="fax">
                                        <img src="{{url('')}}/assets_app/fax.png">
                                        <p>
                                            ۰۵۱۳۲۲۸۰۲۰۷
                                            <br>
                                            <span style="opacity: 0;">sd</span>
                                            <br>
                                            <span style="opacity: 0;">sd</span>
                                        </p>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div style="padding-top: 20px;">
                            <button type="button" id="printed" class="btn btn-info">پرینت پیش فاکتور</button>
                            <button type="button" id="generatePDF" class="btn btn-primary">دانلود pdf</button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @if($status == 3)
                                <button type="button" id="cancel_order" class="btn btn-warning">لغو سفارش</button>
                                <button type="button" id="confirm_order" class="btn btn-success">تایید سفارش</button>
                            @endif
                        </div>
                        <div id="GFG"
                             style="display: none"
                        ><meta charset="utf-8" />
                            <style>
                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: 100;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Thin.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Thin.woff2') format('woff2');
                                }

                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: 200;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-UltraLight.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-UltraLight.woff2') format('woff2');
                                }

                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: 300;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Light.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Light.woff2') format('woff2');
                                }

                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: 500;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Medium.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Medium.woff2') format('woff2');
                                }

                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: 600;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-DemiBold.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-DemiBold.woff2') format('woff2');
                                }

                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: 800;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-ExtraBold.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-ExtraBold.woff2') format('woff2');
                                }

                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: 900;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Black.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Black.woff2') format('woff2');
                                }

                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: bold;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Bold.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Bold.woff2') format('woff2');
                                }

                                @font-face {
                                    font-family: iransans;
                                    font-style: normal;
                                    font-weight: normal;
                                    src: url('{{url('')}}/assets_app/fonts/woff/IRANSansX-Regular.woff') format('woff'),
                                    url('{{url('')}}/assets_app/fonts/woff2/IRANSansX-Regular.woff2') format('woff2');
                                }
                                #wrapper_print *{
                                    direction: rtl;
                                    border: none;
                                    padding: 0;
                                    margin: 0;
                                    font-family: iransans;
                                    font-size: 13px;
                                    box-sizing: border-box;
                                }
                                #wrapper_print{
                                    width: 210mm;
                                    max-width: 95% !important;
                                    border: 1px solid #000;
                                    min-height: 200px;
                                }
                                #wrapper_print #top{
                                    padding: 5px;
                                    border-bottom: 2px solid #777
                                }
                                #wrapper_print #top>div{
                                    display: inline-block;
                                    width: 32%;
                                }
                                #wrapper_print #top .logo img:first-child{
                                    width: 25mm;position: relative;top: -9px;
                                }
                                #wrapper_print #top .logo img:last-child{
                                    width: 15mm;
                                }
                                #wrapper_print #top h3{
                                    text-align: center;margin-bottom: 5px;
                                }
                                #wrapper_print #top h2{
                                    font-size: 16px;text-align: center;font-weight: bold;
                                }
                                #wrapper_print #top .time{
                                    font-size: 13px;text-align: left;
                                }
                                #wrapper_print #customer {
                                    padding: 5px;
                                    border-bottom: 2px solid #777;
                                }
                                #wrapper_print #customer .name{
                                    display: inline-block;
                                    width: 30%;
                                }
                                #wrapper_print #customer .company{
                                    display: inline-block;
                                    width: 30%;
                                }
                                #wrapper_print #customer .mobile{
                                    display: inline-block;
                                    width: 30%;
                                }
                                #wrapper_print #customer .address{
                                    padding: 15px 0;
                                }
                                #wrapper_print #details>div{
                                    border: 1px solid #999;
                                }
                                #wrapper_print #details>div>div{
                                    display: inline-block;
                                    border-left: 1px solid #999;
                                    padding: 5px;
                                }
                                #wrapper_print #details .radif{
                                    width: 5%;
                                }
                                #wrapper_print #details .product{
                                    width: 45%;
                                }
                                #wrapper_print #details .numb{
                                    width: 7%;
                                }
                                #wrapper_print #details .unit{
                                    width: 8%;
                                }
                                #wrapper_print #details .price{
                                    width: 15%;
                                }
                                #wrapper_print #details .total{
                                    width: 15%;
                                    border-left: none;
                                }
                                #wrapper_print #details .desc{
                                    width: 66%;
                                    border-left: none;

                                }
                                #wrapper_print #details .all_total{
                                    width: 33%;
                                    border-left: none;
                                    padding: 0;
                                }
                                #wrapper_print #details .all_total>div{
                                    border-bottom: 1px dashed #bbb;
                                    padding: 5px;
                                }
                                #wrapper_print #details .all_total>div:last-child{
                                    border-bottom: none;

                                }
                                #wrapper_print  #details .all_total>div b{
                                    display: inline-block;
                                    width: 40%;
                                }
                                #wrapper_print #details .all_total>div span{
                                    display: inline-block;
                                    width: 50%;
                                    text-align: left;
                                }
                                #wrapper_print #sighn>div{
                                    display: inline-block;
                                    width: 45%;
                                    padding-top: 95px;
                                    text-align: center;
                                    padding-bottom: 40px;
                                }
                                #wrapper_print #address{
                                    margin-top: 25px;
                                }
                                #wrapper_print #address>div{
                                    display: inline-block;
                                    width: 30%;
                                    position: relative;
                                }
                                #wrapper_print #address>div img{
                                    width: 25px;
                                    position: absolute;
                                    top: 0;
                                    right: 4px;
                                }
                                #wrapper_print #address>div p{
                                    text-align: justify;
                                    margin-right: 34px;
                                }
                            </style>
                            <div id="wrapper_print">
                                <div id="top">
                                    <div class="logo">
                                        <img src="{{url('')}}/assets_app/logo1.png">
                                        <img src="{{url('')}}/assets_app/logo_fer.png">
                                    </div>
                                    <div >
                                        <h3>«پیش فاکتور»</h3>
                                        <h2>
                                            {{$order->company==2 ? 'تجهیزگران پویای امید سلامت' : 'تجهیزات پزشکی فردوسی'}}

                                        </h2>
                                    </div>
                                    <div class="time">
                                        <p>تاریخ :
                                            <span style="display: inline-block">{{jdate('Y/m/d',$order->time)}}</span>
                                        </p>
                                        <p>شماره :
                                            <span style="display: inline-block">{{jdate('Y',$order->time)}} - 000{{$order->id}}</span>

                                        </p>
                                    </div>
                                </div>
                                <div id="customer">
                                    <div class="name">
                                        <b>خریدار <small>(شرکت/آقا/خانم)</small> : </b>
                                        <span>{{$order->user ? $order->user->name.' '.$order->user->family : '--'}}</span>
                                    </div>
                                    <div class="company">
                                        <b>شرکت / سازمان : </b>
                                        <span>{{$order->user ? $order->user->company : '--'}}</span>
                                    </div>
                                    <div class="mobile">
                                        <b>تلفن : </b>
                                        <span>{{$order->user ? $order->user->mobile : $order->user_mobile}}</span>
                                    </div>
                                    <div class="address">
                                        <b>آدرس : </b>
                                        <span>
                                            @if($order->address_static)
                                                {{$order->address_static}}
                                            @else
                                                {{$order->user ? $order->user->address.' - '.$order->user->company : ' -- '}}
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div id="factor" style="padding: 5px">
                                    <div id="details">
                                        <div class="rr">
                                            <div class="radif"><b>ردیف</b></div>
                                            <div class="product"><b>عنوان محصول</b></div>
                                            <div class="numb"><b>تعداد</b></div>
                                            <div class="unit"><b>واحد</b></div>
                                            <div class="price"><b>قیمت واحد</b><small>(ریال)</small></div>
                                            <div class="total"><b>مبلغ کل</b><small>(ریال)</small></div>
                                        </div>
                                        @php($ij = 1)
                                        @php($tott = 0)
                                        @foreach($order->details as $product)
                                            <div id="row_{{$product->id}}" class="rr rrr">
                                                <div class="radif">{{$ij++}}</div>
                                                <div class="product">{{$product->product_name}}</div>
                                                <div data-vall="{{$product->numb}}" class="numb">{{$product->numb}}</div>
                                                <div class="unit">{{$product->unit}}</div>
                                                <div data-vall="{{$product->price}}" class="price">{{sep($product->price)}}</div>
                                                <div class="total">{{sep($product->price*$product->numb)}}</div>
                                                @php($tott += $product->price*$product->numb)
                                            </div>
                                        @endforeach
                                        <div>
                                            <div class="desc">
                                                <p>توضیحات :
                                                    {{$order->desc}}
                                                </p>
                                            </div>
                                            <div class="all_total">
                                                <div>
                                                    <b>مبلغ کل <small>(ریال)</small> </b>
                                                    <span id="factor_total">{{sep($tott)}}</span>
                                                </div>
                                                <div><b>تخفیف : </b><span>0</span></div>
                                                <div><b>عوارض : </b><span>0</span></div>
                                                <div>
                                                    <b>قابل پرداخت <small>(ریال)</small> </b>
                                                    <span id="factor_final">{{sep($tott)}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="sighn">
                                        <div style="display: inline-block;width: 45%;position: relative">
                                            @php($company_id = $order->company==2 ? 2 : 1)
                                            <img style="width: 54%;position: absolute;top: 0;left: 19%;" src="{{url('assets_app/img/s_'.$company_id.'.png')}}">
                                            <b>امضا فروشنده </b>
                                        </div>
                                        <div style="display: inline-block;width: 45%">
                                            <b>امضا خریدار</b>
                                        </div>
                                    </div>

                                    <div id="address">
                                        <div class="address">
                                            <img src="{{url('')}}/assets_app/address.png">
                                            <p>مشهد، بلوار مدرس، مقابل بنیاد شهید، ساختمان مسکن، طبقه ۴ واحد ۴۴
                                                کدپستی ۹۱۳۳۹۱۳۱۳۶
                                            </p>
                                        </div>
                                        <div class="tel" style="margin-right: 25px;width: 25%;">
                                            <img src="{{url('')}}/assets_app/tel.png">
                                            <p>
                                                ۰۵۱۳۲۲۸۰۲۰۴
                                                <br>
                                                ۰۵۱۳۲۲۳۷۰۵۴-۵
                                                <br>
                                                <span style="opacity: 0;">sd</span>
                                            </p>
                                        </div>
                                        <div class="fax">
                                            <img src="{{url('')}}/assets_app/fax.png">
                                            <p>
                                                ۰۵۱۳۲۲۸۰۲۰۷
                                                <br>
                                                <span style="opacity: 0;">sd</span>
                                                <br>
                                                <span style="opacity: 0;">sd</span>
                                            </p>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>

@stop
