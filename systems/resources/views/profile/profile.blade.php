@extends('layouts.web')
@section('link_home','')
@section('title','پروفایل - تجهیزات پزشکی فردوسی')

@section('main')
    <section class="" style="background: url('{{url("template/assets/img/header-bg-5.jpg")}}')no-repeat center center / cover">
        <div class="section-lg bg-gradient-primary text-white section-header">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-7">
                        <div class="page-header-content text-center">
                            <h1>{{$titre}}</h1>
                            <nav aria-label="{{$titre}}" class="d-flex justify-content-center">
                                <ol class="breadcrumb breadcrumb-transparent breadcrumb-text-light">
                                    <li class="breadcrumb-item"><a href="{{url('')}}">خانه</a></li>
                                    <li class="breadcrumb-item"><a href="{{route('profile')}}">پروفایل</a></li>

                                    <li class="breadcrumb-item active" aria-current="page">{{$titre}}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        #profile #sidebar ul{
            padding-right: 0;
            border: 1px solid #aaa;
        }
        #profile #sidebar li{
            list-style: none;
            text-align: right;
            padding: 0;
            border-bottom: 1px solid #aaa;
        }
        #profile #sidebar li a{
            display: block;
            width: 100%;
            height: 100%;
            padding: 10px 5px;
            transition: 0.4s;
        }
        #profile #sidebar li a:hover{
            background: #ccf;
        }
        #profile #sidebar li.active a{
            background: #ccf;
        }
        .box-order{
            box-shadow: 1px 1px 8px #aaa;
            border-radius: 5px;
            padding: 30px 15px;
            font-size: 15px;
            text-align: right;
            line-height: 40px;
            margin-bottom: 15px;
        }
    </style>
    <section id="profile" class="section section-lg  " style="padding-top: 2rem">
        <div class="container">

            <div class="row align-items-center justify-content-center">
                <div id="sidebar" class="col-md-3 mb-4 mb-md-4 mb-lg-0">
                    <div class="feature-widget text-center ">

                        <ul>

                            <li class="{{$status==3 ? 'active' : ''}}"><a href="{{route('profile',[3])}}">سفارشات در انتظار تایید من</a> </li>
                            <li class="{{$status==2 ? 'active' : ''}}"><a href="{{route('profile',[2])}}">سفارشات در انتظار تایید مدیر</a> </li>
                            <li class="{{$status==4 ? 'active' : ''}}"><a href="{{route('profile',[4])}}">سفارشات تایید شده</a> </li>
                            <li class="{{$status==0 ? 'active' : ''}}"><a href="{{route('profile',[0])}}">سفارشات رد شده</a> </li>
{{--                            <li class="{{$status==5 ? 'active' : ''}}"><a href="{{route('profile',[5])}}">ویرایش اطلاعات کاربری</a> </li>--}}
                        </ul>
                    </div>
                </div>

                <div class="col-md-9 mb-4 mb-md-4 mb-lg-0">
                    <div class="feature-widget text-center ">
                        @if(count($orders)>0)
                            @foreach($orders as $order)
                                <div class="box-order">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <b>سفارش شماره : {{$order->id}}</b>
                                        </div>
                                        <div class="col-md-3">
                                            تاریخ ثبت :
                                            {{jdate('Y-m-d H:i',strtotime($order->created_at))}}
                                        </div>
                                        <div class="col-md-3">
                                            گیرنده :
                                            {{$order->user ? $order->user->name.' '.$order->user->family : '--' }}
                                        </div>
                                        <div class="col-md-3">
                                            موبایل :
                                            {{$order->user_mobile}}
                                        </div>
                                        <div class="col-md-10">
                                            آدرس :
                                            {{$order->address_static}}
                                        </div>
                                        <div class="col-md-2">
                                            <a style="padding: 11px 4px;" href="{{route('profile_order_detail',['id'=>$order->id,secure($order->id,'mobile')])}}" class="btn btn-warning">جزئیات سفارش</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="box-order">
                                <div class="row">
                                    <div class="col-md-12">
                                    در حال حاضر سفارشی در این بخش ندارید.
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>

            </div>
        </div>
    </section>

@stop
