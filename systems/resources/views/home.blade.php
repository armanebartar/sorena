@extends('layouts.web')
@section('link_home','')
@section('title','تجهیزات پزشکی فردوسی')

@section('main')

    <style>
        #slider_home.my_bg1 {
            background-image: url('template/images/bg1.jpg')
        }
        #slider_home.my_bg2 {
            background-image: url('template/images/bg5.jpg')
        }
        #slider_home.my_bg3 {
            background-image: url('template/images/bg3.jpg')
        }
        #slider_home.my_bg4 {
            background-image: url('template/images/bg2.jpg')
        }
        #slider_home.my_bg5 {
            background-image: url('template/images/bg4.jpg')
        }
    </style>
    <section id="slider_home" class="section section-xl pt-9 pb-9 my_bg1 section-header text-white gradient-overly-right-color" style="transition: 0.7s">
        <div class="container">
            <div class="row">
                <div class="owl-carousel owl-theme hero-content-slider custom-dot custom-dot-2">
                    <div class="item">
                        <div class="col-md-12 col-lg-12 col-12">
                            <div class="hero-content-wrap">
                                <h1 class="display-2">تجهیزات پزشکی فردوسی</h1>
                                <p class="lead">توزیع کننده تجهیزات و ملزومات پزشکی در سراسر کشور</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-12 col-lg-12 col-12">
                            <div class="hero-content-wrap position-relative z-index">
                                <h1 class="display-2">تجهیزگران پویای امید سلامت</h1>
                                <p class="lead">تولید کننده، وارد کننده و توزیع کننده تجهیزات و ملزومات پزشکی در سراسر کشور </p>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-md-12 col-lg-12 col-12">
                            <div class="hero-content-wrap text-white">
                                <h1 class="display-2"> سورنا</h1>
                                <p class="lead" style="direction: rtl">سورنا در راه است ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-sm pb-0 mt-n8 z-5 position-relative">
        <div class="container">
            <div class="row">
                <style>
                    .promo-hover-bg-1::before {
                        background-image: url("./images/7.jpg");
                    }
                    .promo-hover-bg-2::before {
                        background-image: url("./images/6.jpg");
                    }
                    .promo-hover-bg-3::before {
                        background-image: url("./images/12.jpg");
                    }
                </style>
                <div class="col-md-6 col-lg-4 mb-md-4 mb-4 mb-lg-0">
                    <div class="single-promo-block promo-hover-bg-1 hover-image shadow p-5 rounded-custom bg-white">
                        <div class="icon icon-lg text-primary">

                            <i class="fas fa-clinic-medical"></i>
                        </div>
                        <div class="promo-block-content">
                            <h5>تجهیزات پزشکی فردوسی</h5>
                            <p class="mb-0">توزیع کننده تجهیزات و ملزومات پزشکی در سراسر کشور </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-md-4 mb-4 mb-lg-0">
                    <div class="single-promo-block promo-hover-bg-2 hover-image shadow p-5 rounded-custom bg-white">
                        <div class="icon icon-lg text-primary">
                            <i class="fa fa-clock"></i>
                        </div>
                        <div class="promo-block-content">
                            <h5>تجهیزگران پویای امید سلامت</h5>
                            <p class="mb-0">تولید کننده، وارد کننده و توزیع کننده تجهیزات و ملزومات پزشکی در سراسر کشور</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-md-4 mb-4 mb-lg-0">
                    <div class="single-promo-block promo-hover-bg-3 hover-image shadow p-5 rounded-custom bg-white">
                        <div class="icon icon-lg text-primary"><i class="fas fa-briefcase-medical"></i></div>
                        <div class="promo-block-content">
                            <h5> سورنا </h5>
                            <p class="mb-0">سورنا در راه است ...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="order_section" class="py-0 text-white" style="background: url('template/assets/img/slider-img-5.jpg')no-repeat center center fixed">
        <div class="section section-sm bg-gradient-primary">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="section-heading text-center mb-5">
                            <h2>ثبت سفارش با چند فرآیند ساده</h2>
                        </div>
                    </div>
                </div>
                <div class="row" style="background: rgba(255,255,255,0.5);padding: 50px 15px 15px 15px;color: #333">
                    <div class="col-lg-3 col-md-6 col-12 mb-4 mb-md-4 mb-lg-0">
                        <div class="step-dots right-radial-top text-center">
                            <div style="background-color: #fff" class="icon icon-shape icon-lg bg-default-alt text-default rounded-circle">
                                <i class="fas fa-shopping-basket"></i>
                            </div>
                            <h5 style="color: #000" class="mt-4 mb-3">ثبت اولیه سفارش</h5>
                            <p>به ساده ترین شکل ممکن سفارش خود را ثبت نمایید، ربات هوشمند ما، منظور شما را متوجه می‌شود</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 mb-4 mb-md-4 mb-lg-0">
                        <div class="step-dots right-radial-bottom text-center">
                            <div style="background-color: #fff" class="icon icon-shape icon-lg bg-secondary-alt text-secondary rounded-circle">
                                <i class="fa fa-user-clock"></i>
                            </div>
                            <h5 style="color: #000" class="mt-4 mb-3">پردازش سفارش</h5>
                            <p>با الگوریتمهای هوش مصنوعی سفارش شما در حداقل زمان پردازش می‌شود و به تایید کارشناس فروش می‌رساند</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 mb-4 mb-md-4 mb-lg-0">
                        <div class="step-dots right-radial-top text-center">
                            <div style="background-color: #fff" class="icon icon-shape icon-lg bg-warning-alt text-warning rounded-circle">
                                <i class="fas fa-truck-moving"></i>
                            </div>
                            <h5 style="color: #000" class="mt-4 mb-3">تایید نهایی</h5>
                            <p>در کارتابل خود به سهولت میتوانید با کارشناسان در مورد سفارش خود گفتگو کنید و در نهایت آنرا تایید نمایید </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12 mb-4 mb-md-4 mb-lg-0">
                        <div class="step-dots text-center">
                            <div style="background-color: #fff" class="icon icon-shape icon-lg bg-success-alt text-success rounded-circle">
                                <i class="fas fa-shield-alt"></i>
                            </div>
                            <h5 style="color: #000" class="mt-4 mb-3">دریافت سفارش </h5>
                            <p>در کمترین زمان ممکن سفارش شما ارسال میگرددو میتوانید آنرا در محل خود تحویل بگیرید</p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row justify-content-center">
                            <div class="col-12 col-lg-7 text-center">
                                <div class="subscribe-content">
                                    <h2 class="h4 modal-title my-2">هم اکنون به جمع مشتریان ما بپیوندید</h2>
                                </div>
                                <form id="form_check_and_send" method="post" action="{{route('send_verify_app')}}">
                                    @csrf

                                    <div class="form-group">
                                        <div class="d-sm-flex flex-column flex-sm-row mb-3 justify-content-center">
                                            <input name="mobile" type="text" id="inputYourMail" placeholder="شماره تلفن خود را اینجا وارد کنید" class="mr-sm-1 mb-2 mb-sm-0 form-control form-control-lg">
                                            <button id="btn_check_and_send" type="submit" class="ml-sm-1 btn btn-secondary">ثبت نام</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about_section" class="section section-lg  bg-primary text-white">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-md-12 col-lg-6 mb-4 mb-md-4 mb-lg-0">
                    <div class="card bg-primary position-relative  bg-secondary  shadow-lg fancy-radius p-3">
                        <div class="dot-shape-top position-absolute">
                            <img src="template/assets/img/color-shape.svg" alt="نقطه" class="img-fluid">
                        </div>
                        <img class="fancy-radius img-fluid" src="template/assets/img/about-us.jpg" alt="میز مدرن">
                        <div class="dot-shape position-absolute bottom-0">
                            <img src="template/assets/img/dot-shape.png" alt="نقطه">
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="about-content-right">
                        <h5>بنام هستی بخش</h5>
                        <h3>سلام و درود و تحیت</h3>
                        <p style="margin-bottom: 0;text-align: justify;">به آنهائیکه از لطافت روح و جسم و نور دیدگانشان به سبب سلامت همنوعان خود می گذرند.</p>
                        <p style="margin-bottom: 0;text-align: justify;">به رؤسا، مدیران، پزشکان، پیراپزشکان، مسئولین تدارکات، انبارداران و کادر درمانی پر افتخار و سربلند دانشگاهها و مراکز درمانی که غایت لذتشان تسکین آلام مردم شریف و غیور کشورم ست.</p>
                        <p style="text-align: justify;">آنانیکه با تمام قوا با برنامه ریزی های مدبرانه خویش برای سعادت و سلامت ایران و ایرانی می کوشند.</p>

                        <p style="text-align: justify;">
                            سپاس و ستایش پروردگار یکتا را که رخصت و استعانت فرمود تا در معیت این طیف خدوم، در جهت کشوری عاری از هرگونه رنج و بیماری گام نهیم.
                            <br>
                            در راستای حرکت به سمت چنین آرمانی، دست شما را با صداقت و صمیمیت می فشاریم و وعده می دهیم که هیچگاه از اصول شناخته شده اخلاق تجارت و فراتر از آن، اخلاق انسانی در امر تجارت عدول ننمائیم.
                        </p>


                        <p style="text-align: justify;">
                            مجموعه های تجهیزات پزشکی فردوسی و شرکت تجهیزگران پویای امید سلامت با سابقه دو دهه تجربه در حوزه تأمین تجهیزات و ملزومات پزشکی، اولین واحد ریجستر شده در وزارت بهداشت، درمان و آموزش پزشکی کشور ( سامانه Imed ) و دارای قریب به یکصد نمایندگی در حوزه تجهیزات و ملزومات پزشکی تولیدات داخلی و وارداتی معتبر می باشد.
                            <br>
                            حسن نیت و حضور مؤثر مجموعه های یاد شده در امر تامین تجهیزات و ملزومات پزشکی بویژه در بحران پاندومی ویروس کرونا در جهت رفاه و خدمت رسانی به جامعه بهداشت و درمان ‌و پزشکی کشور تاثیر بسزایی داشته و امیدست در جهت این مهم قرین توفیقات روزافزون باشد.
                        </p>
                        <div class="feature-tabs-wrap" style="display: none">
                            <ul class="nav nav-tabs mb-3 border-bottom-0 feature-tabs" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="py-2 d-flex align-items-center" href="#feature-tab-1" data-toggle="tab">
                                        <h6 class="mb-0">داستان ما</h6>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="py-2 d-flex align-items-center" href="#feature-tab-2" data-toggle="tab">
                                        <h6 class="mb-0">ماموریت ما</h6>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="py-2 d-flex align-items-center active" href="#feature-tab-3" data-toggle="tab">
                                        <h6 class="mb-0">چشم‌انداز ما</h6>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content feature-tab-content">
                                <div class="tab-pane" id="feature-tab-1">
                                    <p>از روشهای تست مکیدن منابع به طور عینی کسب درآمد در برابر استانداردهای ROI سازگار. قبل از ترازبندی های بی سیم ، به طور فعال منابع مجازی را متفرق کنید.</p>

                                    <ul class="list-unstyled tech-feature-list">
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>طراحی وب سایت های <strong>خلاق</strong></li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>کتاب راهنمای رویه های <strong>حسابداری</strong></li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>مبانی حسابداری <strong>هزینه</strong></li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong>شرکت</strong> مدیریت وجه نقد</li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>خدمات بهینه سازی <strong>SEO</strong></li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>راه حل های مارک <strong>شرکت</strong></li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>ضمانت بازگشت <strong>45 روزه</strong> پول</li>
                                    </ul>
                                </div>
                                <div class="tab-pane" id="feature-tab-2">
                                    <p>به طرز فعالانه جایهای متعامد را از فرصتهای قابل همکاری باز کنید. پس از خدمات وب پردرآمد ، مواد 24/365 را به صورت پویا درگیر کنید. با اقتدار ظرفیت های متنوع را در برابر تو رفتگی در دیوار عالی پرورش می دهد.</p>
                                    <p>الگوی های یکپارچه را از طریق کانال های تیم سازی ، یکنواخت افزایش می دهید. فسفلورسنت دارای خدمات قوی با محتوای عمودی است. شبکه جهانی نوآوری ویروسی بدون منابع احتمالی. به سرعت.</p>
                                    <p>پس از روشهای فعالانه توانمندسازی ، کانالهای کلید در دست را به طور شایسته ای نوآوری کنید. تجربه های متعامد را به طور مداوم از طریق آجر و کلیک افزایش دهید. بدون نیاز به سازگاری با سرمایه ، سرمایه انسانی بنگاه اقتصادی را به طور انرژی از بین ببرید.</p>
                                </div>
                                <div class="tab-pane active" id="feature-tab-3">
                                    <p>الگوی های یکپارچه را از طریق کانال های تیم سازی ، یکنواخت افزایش می دهید. فسفلورسنت دارای خدمات قوی با محتوای عمودی است. شبکه جهانی نوآوری ویروسی بدون منابع احتمالی. به سرعت.</p>
                                    <ul class="list-unstyled tech-feature-list mb-4">
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>مبانی حسابداری <strong>هزینه</strong></li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong>شرکت</strong> مدیریت وجه نقد</li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>خدمات بهینه سازی <strong>SEO</strong></li>
                                        <li class="py-1"><span class="ti-control-backward mr-2 text-secondary"></span><strong></strong>راه حل های مارک <strong>شرکت</strong></li>
                                    </ul>
                                    <p>پس از روشهای فعالانه توانمندسازی ، کانالهای کلید در دست را به طور شایسته ای نوآوری کنید. به طور مداوم تجربیات متعامد را ارتقا دهید. بدون نیاز به سازگاری با سرمایه ، سرمایه انسانی بنگاه اقتصادی را به طور انرژی از بین ببرید.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="app_section" class="section py-0" style="background: url('template/assets/img/hero-bg11.jpg')no-repeat center fixed">
        <div class="section-lg section bg-gradient-primary text-white  ">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-9 col-md-10 col-lg-9">
                        <div class="section-title text-center mb-5">
                            <h2>جهت سهولت در ثبت سفارش اپلیکیشنهای ما را نصب نمایید</h2>
                            <p class="lead">از طریق اپلیکیشن سورنا میتوانید به راحتی سفارش خود را ثبت و پیگیری نمایید و به راحتی با ما در ارتباط باشید</p>
                        </div>
                        <div class="download-btn-wrap text-center">
                            <a class="btn btn-pill border border-variant-light  text-white  shadow-hover mr-md-3 mb-4 mb-md-3 mb-lg-0" href="{{route('app')}}">
                                <div class="d-flex align-items-center">
                                    <span class="icon icon-md mr-3 h-auto"><i class="fab fa-apple"></i></span>
                                    <div class="d-block text-left">
                                        <small class="font-small ">بارگیری در</small>
                                        <div class="h6 mb-0">اپل و آیفون</div>
                                    </div>
                                </div>
                            </a>
                            <a class="btn btn-pill border border-variant-light  text-white  shadow-hover mr-md-3 mb-4 mb-md-3 mb-lg-0" href="{{route('app')}}">
                                <div class="d-flex align-items-center">
                                    <span class="icon icon-md mr-3 h-auto"><i class="fab fa-google-play"></i></span>
                                    <div class="d-block text-left">
                                        <small class="font-small ">بارگیری در</small>
                                        <div class="h6 mb-0">اندروید</div>
                                    </div>
                                </div>
                            </a>
                            {{--                            <a class="btn btn-pill border border-variant-light  text-white  shadow-hover" href="#">--}}
                            {{--                                <div class="d-flex align-items-center">--}}
                            {{--                                    <span class="icon icon-md mr-3 h-auto"><i class="fab fa-windows"></i></span>--}}
                            {{--                                    <div class="d-block text-left">--}}
                            {{--                                        <small class="font-small ">مشاهده نسخه</small>--}}
                            {{--                                        <div class="h6 mb-0">تحت وب</div>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(0)
        <section  class="section section-lg text-white bg-gradient-primary">
            <video class="fit-cover w-100 h-100 position-absolute z--1" src="corporate.mp4" autoplay="true" type="video/mp4" muted="" loop="true"></video>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-9 col-md-10 col-lg-9">
                        <div class="section-title text-center mb-5">
                            <h2>جهت سهولت در ثبت سفارش اپلیکیشنهای ما را نصب نمایید</h2>
                            <p class="lead">از طریق اپلیکیشن سورنا میتوانید به راحتی سفارش خود را ثبت و پیگیری نمایید و به راحتی با ما در ارتباط باشید</p>
                        </div>
                        <div class="download-btn-wrap text-center">
                            <a class="btn btn-pill border border-variant-light text-white shadow-hover mr-md-3 mb-4 mb-md-3 mb-lg-0" href="{{route('app')}}">
                                <div class="d-flex align-items-center">
                                    <span class="icon icon-md mr-3 h-auto"><i class="fab fa-apple"></i></span>
                                    <div class="d-block text-left">
                                        <small class="font-small">بارگیری در</small>
                                        <div class="h6 mb-0">اپل و آیفون</div>
                                    </div>
                                </div>
                            </a>
                            <a class="btn btn-pill border border-variant-light text-white shadow-hover mr-md-3 mb-4 mb-md-3 mb-lg-0" href="{{route('app')}}">
                                <div class="d-flex align-items-center">
                                    <span class="icon icon-md mr-3 h-auto"><i class="fab fa-google-play"></i></span>
                                    <div class="d-block text-left">
                                        <small class="font-small">بارگیری در</small>
                                        <div class="h6 mb-0">سیستم عامل اندروید</div>
                                    </div>
                                </div>
                            </a>
                            {{--                        <a class="btn btn-pill border border-variant-light text-white shadow-hover" href="#">--}}
                            {{--                            <div class="d-flex align-items-center">--}}
                            {{--                                <span class="icon icon-md mr-3 h-auto"><i class="fab fa-windows"></i></span>--}}
                            {{--                                <div class="d-block text-left">--}}
                            {{--                                    <small class="font-small">مشاهده</small>--}}
                            {{--                                    <div class="h6 mb-0">نسخه تحت وب</div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                            {{--                        </a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--testimonial section start-->

        <section class="section section-lg  bg-primary">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-8">
                        <div class="section-heading mb-5 text-center text-white">
                            <h2>آنچه مشتریان درباره ما می گویند</h2>
                            <p class="lead">
                                منابع شفاف داخلی یا "ارگانیک" را به سرعت تغییر شکل می دهیم در حالی که کسب و کار الکترونیکی مکنده منابع نوآورانه جذاب داخلی است.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-12">
                        <div class="owl-carousel owl-theme client-testimonial custom-dot">
                            <div class="item">
                                <div class="testimonial-single shadow-sm bg-white rounded-custom p-5">
                                    <div class="quotation mb-4">
                                        <span class="icon icon-md icon-lg  icon-light "><i class="fas fa-quote-left"></i></span>
                                    </div>
                                    <blockquote class="blockquote">
                                        روابط توزیع شده را قاطعانه به تأخیر بیندازید در حالی که سهام عدالت سرمایه های فکری را سرمایه گذاری می کرد و همه چیز را از نظر انرژی تحت عمل پیش می برد.
                                    </blockquote>
                                    <div class="d-flex justify-content-md-between justify-content-lg-between align-items-center pt-3 rtl">
                                        <div class="media align-items-center">
                                            <img src="template/assets/img/team/team-4.jpg" alt="تیم" class="avatar avatar-sm mr-3">
                                            <div class="media-body">
                                                <h6 class="mb-0"> کیان</h6>
                                                <small>مدیر عامل ، تم تگ</small>
                                            </div>
                                        </div>
                                        <div class="client-ratting d-none d-md-block d-lg-block">
                                            <ul class="list-inline mb-0">
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                            </ul>
                                            <span class="font-weight-bold small">5.0 <span class="font-weight-lighter">از 5</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-single shadow-sm bg-white rounded-custom p-5">
                                    <div class="quotation mb-4">
                                        <span class="icon icon-md icon-lg  icon-light "><i class="fas fa-quote-left"></i></span>
                                    </div>
                                    <blockquote class="blockquote">
                                        ضروریات ذاتی و بدون خدمات نسل بعدی را ذاتاً تسهیل می کنید. با تحریک انقلابی ، بهترین شیوه های سازمانی کاربران جهانی را متحول کرد.
                                    </blockquote>
                                    <div class="d-flex justify-content-md-between justify-content-lg-between align-items-center pt-3 rtl">
                                        <div class="media align-items-center">
                                            <img src="template/assets/img/team/team-1.jpg" alt="تیم" class="avatar avatar-sm mr-3">
                                            <div class="media-body">
                                                <h6 class="mb-0">پرتول کارول</h6>
                                                <small>رهبر تیم ، برچسب های تم</small>
                                            </div>
                                        </div>
                                        <div class="client-ratting d-none d-md-block d-lg-block">
                                            <ul class="list-inline mb-0">
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                            </ul>
                                            <span class="font-weight-bold small">5.0 <span class="font-weight-lighter">از 5</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-single shadow-sm bg-white rounded-custom p-5">
                                    <div class="quotation mb-4">
                                        <span class="icon icon-md icon-lg  icon-light "><i class="fas fa-quote-left"></i></span>
                                    </div>
                                    <blockquote class="blockquote">
                                        روابط توزیع شده را قاطعانه به تأخیر بیندازید در حالی که سهام عدالت سرمایه های فکری را سرمایه گذاری می کرد و همه چیز را از نظر انرژی تحت عمل پیش می برد.
                                    </blockquote>
                                    <div class="d-flex justify-content-md-between justify-content-lg-between align-items-center pt-3 rtl">
                                        <div class="media align-items-center">
                                            <img src="template/assets/img/team/team-2.jpg" alt="تیم" class="avatar avatar-sm mr-3">
                                            <div class="media-body">
                                                <h6 class="mb-0"> کیان</h6>
                                                <small>مدیر عامل ، تم تگ</small>
                                            </div>
                                        </div>
                                        <div class="client-ratting d-none d-md-block d-lg-block">
                                            <ul class="list-inline mb-0">
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                            </ul>
                                            <span class="font-weight-bold small">5.0 <span class="font-weight-lighter">از 5</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-single shadow-sm bg-white rounded-custom p-5">
                                    <div class="quotation mb-4">
                                        <span class="icon icon-md icon-lg  icon-light "><i class="fas fa-quote-left"></i></span>
                                    </div>
                                    <blockquote class="blockquote">
                                        ضروریات ذاتی و بدون خدمات نسل بعدی را ذاتاً تسهیل می کنید. با تحریک انقلابی ، بهترین شیوه های سازمانی کاربران جهانی را متحول کرد.
                                    </blockquote>
                                    <div class="d-flex justify-content-md-between justify-content-lg-between align-items-center pt-3 rtl">
                                        <div class="media align-items-center">
                                            <img src="template/assets/img/team/team-6.jpg" alt="تیم" class="avatar avatar-sm mr-3">
                                            <div class="media-body">
                                                <h6 class="mb-0">پرتول کارول</h6>
                                                <small>رهبر تیم ، برچسب های تم</small>
                                            </div>
                                        </div>
                                        <div class="client-ratting d-none d-md-block d-lg-block">
                                            <ul class="list-inline mb-0">
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                                <li class="list-inline-item mr-0"><span class="icon icon-xs font-small text-warning"><i class="fas fa-star ratting-color"></i></span></li>
                                            </ul>
                                            <span class="font-weight-bold small">5.0 <span class="font-weight-lighter">از 5</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--testimonial section end-->
    @endif

    <div class="section section-sm ">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <style>
                        #owl_logos .owl-item{
                            /*width: unset !important;*/
                        }
                        #owl_logos .owl-item .item {
                            width: 80% !important;
                        }
                        #owl_logos .img-fluid {
                            position: relative;
                        }
                        #owl_logos .arm_1 {
                            height: 42px !important;
                        }
                        #owl_logos .arm_2 {
                            top: 5px;
                        }
                        #owl_logos .arm_6 {
                            top: -13px;
                        }
                        #owl_logos .arm_8 {
                            top: 4px;
                        }
                        #owl_logos .arm_9 {
                            top: -5px;
                        }
                        #owl_logos .arm_10 {
                            top: -5px;
                        }
                        #owl_logos .arm_12 {
                            top: 15px;
                        }
                        #owl_logos .arm_13 {
                            top: 15px;
                        }
                        #owl_logos .arm_15 {
                            height: 50px;
                        }
                        #owl_logos .arm_17 {
                            top: 2px;
                            height: 54px;
                        }
                        #owl_logos .arm_18 {
                            top: 5px;
                            height: 48px;
                        }
                        #owl_logos .arm_19{
                            height: 48px;
                            top: 4px;
                        }
                        #owl_logos .arm_22{
                            top: 6px;
                        }
                        #owl_logos .arm_23{
                            top: 1px;
                        }
                        #owl_logos .arm_24{
                            top: 16px;
                        }
                        @php($ccc = [0,30,42,37,38,36,18,29,39,25,25,44,44,43,30,27,16=>31,27,18=>30,
19=>30,
21=>15,
22=>27,
23=>24,
24=>34,
25=>9,
26=>17,
27=>8,
28=>-8,
29=>-12

])
                        @foreach($ccc as $kk=>$cc)
                        #owl_logos .arm_{{$kk}}{
                            top: {{$cc}}px;
                        }
                        @endforeach
                        @media (min-width: 992px) {
                            .section-sm {
                                padding-top: 1rem;
                                padding-bottom: 0rem;
                            }
                            #owl_logos>div{
                                position: relative;
                                top: 7px;
                            }
                        }
                    </style>
                    <div id="owl_logos" class="owl-carousel owl-theme clients-carousel">
                        @for($i=1;$i<=29;$i++)
                            @continue(in_array($i,[11,16,20]))
                            <div class="item single-client">
                                <img data-src="files/logos/{{$i}}.png" style="{{$i==1?"height:60px":''}}" alt="آرم مشتری" class="my_lazy img-fluid arm_{{$i}}">
                            </div>
                        @endfor

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
