 @extends('layouts.admin')

 @section('title','تنظیمات سایت')

 @section('css')
     <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
    <style>
        .errorsAction li {
            margin-right: 35px;
            list-style-type: square !important;
            color: red;
        }
    </style>
 @endsection
 @section('js')

     <script src="{{url('assets/js/admin.js')}}"></script>
     <script src="{{url('assets/js/form.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
    <script>
        $('select').formSelect();
        $('.addRow').on('click',function () {
            var newOption = "<div class='row'>";
            newOption += "<div class='col-4'><input type='text' name='title[]' value='' class='form-control'></div>";
            newOption += "<div class='col-4'><input type='text' name='key[]' value='' class='form-control'></div>";
            newOption += "<div class='col-4 sel'><select name='type[]'><option value='text'>متن کوتاه</option><option value='texterea'>متن چندخطی</option><option value='image'>تصویر</option></select></div></div>";
            $(this).parent().find('.options').append(newOption);
            $('select').formSelect();
        })
    </script>
 @endsection

 @section('main')



<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">

                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{route('p_articles')}}">
                                <i class="fas fa-home"></i> داشبورد</a>
                        </li>
                        <li class="breadcrumb-item bcrumb-2">
                            <a href="javascript:void(0);">توسعه دهنده</a>
                        </li>
                        <li class="breadcrumb-item active">تنظیمات سایت</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <form method="post" action="{{route('p_dev_groupOptionSave')}}">
                            {{csrf_field()}}
                        <div class="row">

                                <div class="col-2">گروه جدید</div>
                                <div class="col-5"><input type="text" placeholder="نام گروه" name="groupName"></div>
                                <div class="col-2"><input type="submit" class="btn btn-success" value="افزودن گروه جدید"></div>

                        </div>
                        </form>
                    </div>

                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($groups as $k=>$group)
                            <li role="presentation">
                                <a href="#tab{{$k}}" data-toggle="tab" class="{{$k<1?'active show':''}}">
                                    {{$group->name}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <style>
                                .select-wrapper{
                                    padding-top: 15px !important;
                                }
                            </style>

                            @foreach($groups as $k=>$group)
                                <div role="tabpanel" class="tab-pane fade {{$k<1?'active show':''}}" id="tab{{$k}}">
                                    <form method="post" action="{{route('p_dev_optionsSave')}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="groupe" value="{{$group->id}}">

                                    <div class="row" style="padding-bottom: 15px;color: #000;font-weight: 800">
                                        <div class="col-4">عنوان : </div>
                                        <div class="col-4">کلید :</div>
                                        <div class="col-4">نوع : </div>
                                    </div>

                                    <div class="options">
                                    @foreach($options[$group->id] as $option)
                                        <div class="row">
                                            <div class="col-4"><input type="text" name="title[]" value="{{$option->title}}" class="form-control"></div>
                                            <div class="col-4"><input type="text" name="key[]" value="{{$option->key}}" class="form-control"></div>
                                            <div class="col-4 sel">
                                                <select name="type[]">
                                                    <option {{$option->type=='text'?"selected=''":''}} value="text">متن کوتاه</option>
                                                    <option {{$option->type=='texterea'?"selected=''":''}} value="texterea">متن چندخطی</option>
                                                    <option {{$option->type=='image'?"selected=''":''}} value="image">تصویر</option>
                                                </select>
                                            </div>
                                        </div>

                                    @endforeach

                                    </div>
                                        <button type="button" class="btn btn-warning addRow">افزودن آپشن جدید</button>
                                        <button type="submit" class="btn btn-success ">ذخیره آپشنها</button>
                                    </form>
                                </div>
                            @endforeach



                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


    @endsection