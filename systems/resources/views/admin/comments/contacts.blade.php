@extends('layouts.admin')


    @section('title')
        {{$title}}
    @endsection

    @section('css')
        <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
    @endsection
    @section('js')
        <script>
         $('#contactsMenu').addClass('cvcd').parent().addClass('active').parent().parent().addClass('active');
        </script>
        <script src="{{url('assets/js/table.min.js')}}"></script>
        <script src="{{url('assets/js/admin.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/dataTables.buttons.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.flash.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/jszip.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/pdfmake.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/vfs_fonts.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.html5.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.print.min.js')}}"></script>


        {{--        <script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>--}}
        <script>

            $('#tableExport').DataTable({
                dom: 'Bfrtip',
                "displayLength": "{{$maxRow}}",

                paginate:true,
                lengthChange:true,
                info:false,
                filter:true,

                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            var UrlSeen = "{{route('p_contacts_seen')}}";
            var csrfFF = "{{csrf_token()}}";


            $('#tbody1').on('click','.showComment',function (e) {
                e.preventDefault();
                var parentTr=$(this).parent().parent();
                $('#btnModalShow').trigger('click');
                $('#modalShow .coment').text(parentTr.data('content'));
                $('#modalShow .sender').html(parentTr.data('sender'));
                $('#modalShow .email').text(parentTr.data('email'));
                $('#modalShow .title').html(parentTr.data('title'));
                $('#modalShow .type').text(parentTr.data('type'));
                $('#boxActions').data('id',parentTr.data('id')).data('coment',parentTr.data('content'));
                if(parentTr.hasClass('notVisited')){
                    parentTr.removeClass('notVisited');
                }

                    var commentId = parentTr.data('id');

                    $.ajax({
                        url: UrlSeen,
                        type: "POST",
                        data: '_token='+csrfFF+'&id='+commentId,
                        success: function (data) {

                    }
                    });

            });

            $('#tbody1').on('click','.deActivated',function () {
                var parentTr=$(this).parent().parent();
                $('#btnModalDeActive').trigger('click');
                $('#modalDeActiveNews .coment').text(parentTr.data('content'));
                $('#modalDeActiveNews .sender').html(parentTr.data('sender'));
                $('#modalDeActiveNews .title').html(parentTr.data('title'));
                $('#modalDeActiveNews .type').text(parentTr.data('type'));
                $("#modalDeActiveNews .id").val($(this).parent().parent().data('id'));
            });

            $('#tbody1').on('click','.activated',function () {
                var parentTr=$(this).parent().parent();
                $('#btnModalActive').trigger('click');
                $('#modalActiveNews .coment').text(parentTr.data('content'));
                $('#modalActiveNews .sender').html(parentTr.data('sender'));
                $('#modalActiveNews .title').html(parentTr.data('title'));
                $('#modalActiveNews .type').text(parentTr.data('type'));
                $("#modalActiveNews .id").val($(this).parent().parent().data('id'));
            });



            $('#tbody1').on('click','.btn-info',function () {
                var parentTr=$(this).parent().parent();
                $('#btnEdit').trigger('click');
                $('#modalEdit .comment').val(parentTr.data('content'));
                $('#modalEdit .id').val(parentTr.data('id'));

                if(parentTr.hasClass('notVisited')){
                    parentTr.removeClass('notVisited');
                }

                var commentId = parentTr.data('id');

                $.ajax({
                    url: UrlSeen,
                    type: "POST",
                    data: '_token='+csrfFF+'&id='+commentId,
                    success: function (data) {

                    }
                });
            });


            $('#tbody1').on('click','.actions .btn-danger',function () {
                $('#btnModalRemove').trigger('click');
                var parentTr=$(this).parent().parent();
                $('#modalRemoveNews .coment').text(parentTr.data('content'));
                $('#modalRemoveNews .sender').html(parentTr.data('sender'));
                $('#modalRemoveNews .title').html(parentTr.data('title'));
                $('#modalRemoveNews .type').text(parentTr.data('type'));
                $("#modalRemoveNews .id").val($(this).parent().parent().data('id'));

                if(parentTr.hasClass('notVisited')){
                    parentTr.removeClass('notVisited');
                }

                var commentId = parentTr.data('id');

                $.ajax({
                    url: UrlSeen,
                    type: "POST",
                    data: '_token='+csrfFF+'&id='+commentId,
                    success: function (data) {

                    }
                });
            });


        </script>
    @endsection

    @section('main')
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>
                                <li class="breadcrumb-item bcrumb-2">
                                    <a href="javascript:void(0);">کامنتها</a>
                                </li>
                                <li class="breadcrumb-item active">{{$title}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Exportable Table -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>لیست</strong> {{$title}}</h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">


                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                    <style>
                                        #tbody1 td{
                                            font-size: 13px;
                                        }
                                        #tbody1 img{
                                            max-width: 100%;
                                            cursor: pointer;
                                        }
                                        #tbody1 .actions a{
                                            width: 18px !important;
                                            height: 18px !important;
                                            font-size: 10px;
                                        }
                                        .actShow span{
                                            font-size: 11px;
                                            padding: 0 14px;
                                        }
                                        .modalShowImage img{
                                            max-width: 100%;
                                        }
                                        .modal-body b{
                                            font-size: 14px;
                                            font-weight: 800;
                                            color: #333;
                                        }
                                        #tbody1 .notVisited{
                                            color: #000;
                                            font-weight: 800;
                                        }
                                    </style>
                                    <table style="min-width: 800px" id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                        <thead>
                                        <tr>
                                            <th class="w5">ردیف</th>

                                            <th class="w15">فرستنده</th>
                                            <th class="w15">موضوع</th>
                                            <th class="w30">کامنت</th>
                                            <th class="w10">وضعیت</th>
                                            <th class="w10">تاریخ ثبت</th>
                                            <th class="w15">عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody1">
                                            {!! $tbl !!}
                                        </tbody>

                                    </table>
                                </div>
                                <div class="row" >
                                    <div class="col-md-4">
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Exportable Table -->
            </div>

            <a id="btnModalShow" data-toggle="modal" href="#modalShow"></a>
            <div class="modal fade" id="modalShow">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">جزئیات کامنت</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-3"><b>فرستنده : </b> </div>
                                <div class="col-9 sender"></div>
                            </div>
                            <div class="row">
                                <div class="col-3"><b>شماره تماس : </b></div>
                                <div class="col-9 email"></div>
                            </div>
                            <div class="row">
                                <div class="col-3"><b>موضوع : </b></div>
                                <div class="col-9 title"></div>
                            </div>

                            <div class="row">
                                <div class="col-3"><b>کامنت : </b></div>
                                <div class="col-12 coment"></div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>



            <a id="btnModalActive" data-toggle="modal" href="#modalActiveNews"></a>
            <div class="modal fade" id="modalActiveNews">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">فعال کردن کامنت</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h5>آیا مطمئنید که قصد فعال کردن این کامنت را دارید؟</h5><br>
                            <div class="row">
                                <div class="col-3"><b>فرستنده : </b> </div>
                                <div class="col-9 sender"></div>
                            </div>

                            <div class="row">
                                <div class="col-3"><b>موضوع : </b></div>
                                <div class="col-9 title"></div>
                            </div>

                            <div class="row">
                                <div class="col-3"><b>کامنت : </b></div>
                                <div class="col-12 coment"></div>
                            </div>
                        </div>
                        <form method="post" action="{{route('p_contacts_activated')}}">
                            {{csrf_field()}}
                            <input class="id" type="hidden" name="id">
                            <input  type="hidden" name="status" value="publish">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                                <button type="submit" class="btn btn-info waves-effect">بله، فعال شود.</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <a id="btnModalDeActive" data-toggle="modal" href="#modalDeActiveNews"></a>
            <div class="modal fade" id="modalDeActiveNews">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">غیرفعال کردن کامنت</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h5>آیا مطمئنید که قصد غیرفعال کردن این کامنت را دارید؟</h5><br>
                            <div class="row">
                                <div class="col-3"><b>فرستنده : </b> </div>
                                <div class="col-9 sender"></div>
                            </div>

                            <div class="row">
                                <div class="col-3"><b>موضوع : </b></div>
                                <div class="col-9 title"></div>
                            </div>

                            <div class="row">
                                <div class="col-3"><b>کامنت : </b></div>
                                <div class="col-12 coment"></div>
                            </div>
                        </div>
                        <form method="post" action="{{route('p_contacts_activated')}}">
                            {{csrf_field()}}
                            <input class="id" type="hidden" name="id">
                            <input  type="hidden" name="status" value="unpublish">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                            <button type="submit" class="btn btn-info waves-effect">بله، غیرفعال شود.</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>


            <a id="btnModalRemove" data-toggle="modal" href="#modalRemoveNews"></a>
            <div class="modal fade" id="modalRemoveNews">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">حذف کامنت</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h5>آیا مطمئنید که قصد حذف کردن این کامنت</h5><br>
                            <div class="row">
                                <div class="col-3"><b>فرستنده : </b> </div>
                                <div class="col-9 sender"></div>
                            </div>

                            <div class="row">
                                <div class="col-3"><b>موضوع : </b></div>
                                <div class="col-9 title"></div>
                            </div>

                            <div class="row">
                                <div class="col-3"><b>کامنت : </b></div>
                                <div class="col-12 coment"></div>
                            </div>
                        </div>
                        <form method="post" action="{{route('p_contacts_remove')}}">
                            {{csrf_field()}}
                            <input class="id" type="hidden" name="id">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                            <button type="submit" class="btn btn-info waves-effect">بله، حذف شود.</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>


        </section>

    @endsection
