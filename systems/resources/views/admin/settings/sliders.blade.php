 @extends('layouts.admin')

 @section('title','اسلایدشوها')

 @section('css')
     <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
    <style>
        .errorsAction li {
            margin-right: 35px;
            list-style-type: square !important;
            color: red;
        }
        #langs .active{
            background-color: blue !important;
        }
        #tbody1 .actions a.btn{
            width: 25px !important;
            height: 25px !important;
        }
        .modal-body label{
            font-size: 13px;
            margin-bottom: 0px;
            padding-right: 10px !important;
        }
        .modal-body input{
            border: 1px solid darkgray !important;
            border-radius: 5px !important;
            height: 27px !important;
            padding-right: 10px !important;
            padding-left: 10px !important;
            box-sizing: border-box !important;
        }
        .modal-body textarea{
            border: 1px solid darkgray !important;
            border-radius: 5px !important;
            padding-right: 5px !important;
            padding-left: 5px !important;
            box-sizing: border-box !important;
        }
        .modal-body .form-group{
            margin-bottom: 10px;
        }
    </style>
 @endsection
 @section('js')
     <script>

         $('#menuOptionsSliders').addClass('cvcd').parent().addClass('active').parent().parent().addClass('active');


     </script>
     <script src="{{url('assets/js/admin.js')}}"></script>
     <script src="{{url('assets/js/form.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
    <script>
        var thisBtn;
        $('.addSlide').on('click',function (e) {
            e.preventDefault();
            thisBtn=$(this);
            $('#modalAdd .titre').text($(this).data('titre'));
            $('#modalAdd [name=lang]').val($(this).data('lang'));
            $('#modalAdd [name=group]').val($(this).data('group'));
            $('#modalAdd [name=secure]').val($(this).data('secure'));
            $('#modalAdd [name=title]').val('');
            $('#modalAdd [name=link]').val('');
            $('#modalAdd [name=desc]').val('');
            $('#modalAdd [name=image]').val('');
            $('#modalAdd [name=sort]').val('1');
            $('#modalAdd .best_size').text($(this).parents('.tab-pane').data('img'));
            $('#modalAdd').modal('show');
        });

        function afterAdd(data,thisForm){
            if(data.res === 10){
                thisBtn.parents('.tab-pane').find('tbody').html(data.tbl);
                window.setTimeout(function () {
                    $('#modalAdd').modal('hide');
                    $('#modalRemove').modal('hide');
                },3000);
            }
        }


        $('#tbody1').on('click','.edited',function (e) {
            e.preventDefault();
            thisBtn=$(this);
            $('#modalEdit [name=id]').val($(this).parent().data('id'));
            $('#modalEdit [name=secure]').val($(this).parent().data('secure'));
            $('#modalEdit [name=sort]').val($(this).parent().data('sort'));
            $('#modalEdit [name=title]').val($(this).parents('tr').find('.title').text());
            $('#modalEdit [name=link]').val($(this).parent().data('link'));
            $('#modalEdit [name=desc]').val($(this).parents('tr').find('.desc').text());
            $('#modalEdit [name=image]').val('');
            $('#modalEdit .best_size').text($(this).parents('.tab-pane').data('img'));
            $('#modalEdit').modal('show');
        });

        $('#tbody1').on('click','.removed',function (e) {
            e.preventDefault();
            thisBtn=$(this);
            $('#modalRemove [name=id]').val($(this).parent().data('id'));
            $('#modalRemove [name=secure]').val($(this).parent().data('secure'));
            $('#modalRemove .title').text($(this).parents('tr').find('.title').text());
            $('#modalRemove img').prop('src',$(this).parents('tr').find('img').prop('src'));
            $('#modalRemove').modal('show');
        });

    </script>
 @endsection

 @section('main')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{route('p_dashboard')}}">
                                <i class="fas fa-home"></i>
                                داشبورد
                            </a>
                        </li>
                        <li class="breadcrumb-item bcrumb-2">
                            <a href="javascript:void(0);">
                                تنظیمات سایت
                            </a>
                        </li>
                        <li class="breadcrumb-item active">
                            اسلایدشوها
                        {{$title}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div id="langs">
                            <a href="{{route('p_slider')}}" class="btn btn-info {{DEFAULT_LANG[0]==$lang ? 'active' : ''}}">{{DEFAULT_LANG[1]}}</a>
                            @foreach(LANGUAGES as $lll)
                                <a href="{{route('p_slider',[$lll[0]])}}" class="btn btn-info {{$lll[0] == $lang ? 'active' : ''}}">{{$lll[1]}}</a>
                            @endforeach
                        </div>

                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($groups as $k=>$group)
                                @if(permission('editSlide'.$group->id))
                                    <li role="presentation">
                                        <a href="#tab{{$group->id}}" data-toggle="tab" class="{{$group->id==$default?'active show':''}}">
                                            {{$group->name}}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>

                        <div class="tab-content" id="tbody1">
                            <?php $iii=0; ?>
                            @foreach($groups as $k=>$group)
                                @if(permission('editSlide'.$group->id))
                                    @php
                                        $image_sizes = [];
                                        foreach (explode('**',$group->image_sizes) as $val)
                                            $image_sizes[]=explode('*',$val);
                                    @endphp
                                    <div role="tabpanel" data-img="مناسبترین سایز :
{{$image_sizes[count($image_sizes)-1][0].'*'.$image_sizes[count($image_sizes)-1][1]}}
                                        پیکسل" class="tab-pane fade {{$group->id==$default?'active show':''}}" id="tab{{$group->id}}">
                                        <button data-titre="افزودن  {{$group->name}}" data-id="0" data-lang="{{$lang}}" data-group="{{$group->id}}" data-secure="{{\App\Models\Post::secure([$lang,$group->id])}}" class="btn btn-success addSlide btn-sm">افزودن اسلاید جدید</button>
                                       <table class="table">
                                           <thead>
                                           <tr>
                                               <th class="w5">ردیف</th>
                                               <th class="w30">عنوان</th>
                                               <th class="w40">توضیحات</th>
                                               <th class="w10">تصویر</th>
                                               <th class="w15">عملیات</th>
                                           </tr>
                                           </thead>
                                        @php($kk = 1)
                                           <tbody>
                                            {!! \App\Http\Controllers\admin\settings\SliderController::tbl($slides[$group->id]) !!}
                                           </tbody>
                                       </table>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="modal fade" id="modalAdd">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">  <span class="titre"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form class="send_ajax" data-after="afterAdd" method="post" id="formAdd" class="send_ajax" action="{{route('p_slider_add')}}">
                    @csrf
                    <input type="hidden" name="lang" value="{{$lang}}">
                    <input type="hidden" name="group" >
                    <input type="hidden" name="secure" >
                    <div class="form-group">
                        <label>عنوان : </label>
                        <input type="text" name="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>لینک : </label>
                        <input type="text" name="link" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>توضیحات : </label>
                        <textarea name="desc" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>تصویر : <sup>*</sup></label>
                        <input type="file" name="image" class="form-control">
                        <p class="best_size"></p>
                    </div>
                    <div class="form-group">
                        <label>اولویت : </label>
                        <input type="number" name="sort" class="form-control">
                        <small>هر چه عدد بزرگتر باشد به ابتدای لیست نزدیکتر میشود</small>
                    </div>
                    <p class="mySuccess"></p>
                    <p class="myAlert"></p>
                    <div class="form-group">
                        <button id="btnAdd" type="submit" class="btn btn-success">ذخیره</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modalEdit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ویرایش</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form class="send_ajax" data-after="afterAdd" method="post" id="formEdit" action="{{route('p_slider_edit')}}">
                    @csrf
                    <input type="hidden" name="lang" value="{{$lang}}">
                    <input type="hidden" name="id" >
                    <input type="hidden" name="secure" >
                    <div class="form-group">
                        <label>عنوان : </label>
                        <input type="text" name="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>لینک : </label>
                        <input type="text" name="link" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>توضیحات : </label>
                        <textarea name="desc" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>تصویر : </label>
                        <input type="file" name="image" class="form-control">
                        <p style="margin-bottom: 0">اگر تصویر معتبری انتخاب نشود، تصویر قبلی حفظ می‌گردد</p>
                        <p class="best_size"></p>
                    </div>
                    <div class="form-group">
                        <label>اولویت : </label>
                        <input type="number" name="sort" class="form-control">
                        <small>هر چه عدد بزرگتر باشد به ابتدای لیست نزدیکتر میشود</small>
                    </div>
                    <p class="mySuccess"></p>
                    <p class="myAlert"></p>
                    <div class="form-group">
                        <button id="btnEdit" type="submit" class="btn btn-success">ذخیره</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modalRemove">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">حذف</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <h6>آیا مطمئنید که قصد حذف اسلاید زیر را دارید؟</h6>
                <p class="title"></p>
                <img >
                <form class="send_ajax" data-after="afterAdd" method="post" id="formRemove" action="{{route('p_slider_remove')}}">
                    @csrf
                    <input type="hidden" name="id" >
                    <input type="hidden" name="secure" >

                    <p class="mySuccess"></p>
                    <p class="myAlert"></p>
                    <div class="form-group">
                        <button id="btnRemove" type="submit" class="btn btn-success">بله، حذف شود.</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>



 @endsection
