 @extends('layouts.admin')

 @section('title','تنظیمات سایت')

 @section('css')

     <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    <style>
        .errorsAction li {
            margin-right: 35px;
            list-style-type: square !important;
            color: red;
        }
        #langs .active{
            background-color: blue !important;
        }
    </style>
 @endsection
 @section('js')
     <script>

         $('#menuOptionsCommon').addClass('cvcd').parent().addClass('active').parent().parent().addClass('active');


     </script>
     <script src="{{url('assets/js/admin.js')}}"></script>
     <script src="{{url('assets/js/form.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
     <script src="//cdn.ckeditor.com/4.16.0/basic/ckeditor.js"></script>

    <script>
        $(function() {
            var config = {};
            config.language = 'fa';
            //        config.uiColor = '#2f6dbb';
            config.font_names =
                "Tahoma;" +
                "Nazanin/Nazanin, B Nazanin, BNazanin;" +
                "Yekan/Yekan, BYekan, B Yekan, Web Yekan;" +
                "IranSans/IranSans, IranSansWeb;" +
                "Parastoo/Parastoo;" +
                "Arial/Arial, Helvetica, sans-serif;" +
                "Times New Roman/Times New Roman, Times, serif;";

            $('.ckeditor_1',function (){
                CKEDITOR.replace($(this).prop('id'),config);
            });
{{--            @foreach($groups as $k=>$group)--}}
{{--                @if(permission('editOptions'.$group->id))--}}
{{--                    @foreach($options[$group->id] as $option)--}}
{{--                        @if($option->type=='ckeditor')--}}
{{--                            CKEDITOR.replace('ckeditor{{$option->id}}',config);--}}
{{--                        @endif--}}
{{--                    @endforeach--}}
{{--                @endif--}}
{{--            @endforeach--}}
        });
        let thisBtnClick;
        $('.btn_image').on('click',function (){
            $('#file_image').trigger('click');
            thisBtnClick = $(this).attr('id');
        });

        let file_image = document.getElementById('file_image');

        file_image.addEventListener('change',function (e){
            let this_btn = thisBtnClick;
            if(!file_image.files[0].type.toLowerCase().includes('image')){
                $('#'+this_btn).parent().parent().parent().find('.myAlertImage').show().text("فایل انتخابی یک تصویر نیست.");
                window.setTimeout(function (){
                    $('#'+this_btn).parent().parent().parent().find('.myAlertImage').hide();
                },3000);
                return;
            }
            if(file_image.files[0].size > (2 * 1024 * 1024)){
                $('#'+this_btn).parent().parent().parent().find('.myAlertImage').show().text("حجم فایل انتخابی زیاد است.");
                window.setTimeout(function (){
                    $('#'+this_btn).parent().parent().parent().find('.myAlertImage').hide();
                },3000);
                return;
            }






            let thisProgress = $('#'+this_btn).parent().parent().parent().find('.progress-bar');
            setProgress(thisProgress,'0');

            $('#'+this_btn).parent().parent().parent().find('.myAlertImage').hide();

            const xhr = new XMLHttpRequest();
            xhr.open('POST',"{{route('p_options_save_image')}}");
            let formData = new FormData(document.getElementById('form_image'));
            xhr.addEventListener('load',function (event){
                let res = JSON.parse(xhr.responseText);

                thisProgress.parent().hide();
                if(res.res === 10){
                    $('#'+this_btn).parent().parent().parent().find('img').prop('src',res.thumb).parent().prop('href',res.url);;
                    $('#'+this_btn).parent().parent().parent().find('.img_url').val(res.path+res.fileName);
                }else {
                    $('#'+this_btn).parent().parent().parent().find('.myAlertImage').show().text(res.myAlert);
                    window.setTimeout(function (){
                        $('#'+this_btn).parent().parent().parent().find('.myAlertImage').hide();
                    },3000);
                }
            });
            xhr.upload.addEventListener('progress',function (event){
                let percent = parseInt((event.loaded / event.total) * 100);
                setProgress(thisProgress,percent);
            });
            xhr.send(formData);
        });
        function setProgress(thisProgress,percent){
            thisProgress.attr('class',`progress-bar width-per-${percent}`);
            thisProgress.attr('aria-valuenow',percent);
            thisProgress.text(percent+" %");
            thisProgress.parent().show();
        }
        function beforeSave(){

            $('.ckeditor').each(function (){
                $('#'+$(this).attr('id')).val(CKEDITOR.instances[$(this).attr('id')].getData());
            });
        }
    </script>
 @endsection

 @section('main')



<section class="content">
    <div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">

                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{route('p_dashboard')}}">
                                <i class="fas fa-home"></i> داشبورد</a>
                        </li>
                        <li class="breadcrumb-item bcrumb-2">
                            <a href="javascript:void(0);">تنظیمات سایت</a>
                        </li>
                        <li class="breadcrumb-item active">
                            تنظیمات همگانی
                        {{$title}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        @if(LANGUAGES !=[])
                            <div id="langs">
                                <a href="{{route('p_options')}}" class="btn btn-info {{DEFAULT_LANG[0]==$lang ? 'active' : ''}}">{{DEFAULT_LANG[1]}}</a>
                                @foreach(LANGUAGES as $lll)
                                    <a href="{{route('p_options',[$lll[0]])}}" class="btn btn-info {{$lll[0] == $lang ? 'active' : ''}}">{{$lll[1]}}</a>
                                @endforeach
                            </div>
                        @endif
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($groups as $k=>$group)
                                @if(permission('editOptions'.$group->id))
                                    <li role="presentation">
                                        <a href="#tab{{$group->id}}" data-toggle="tab" class="{{$group->id==$default?'active show':''}}">
                                            {{$group->name}}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            <?php $iii=0; ?>
                            @foreach($groups as $k=>$group)
                                @if(permission('editOptions'.$group->id))
                                    <div role="tabpanel" class="tab-pane fade {{$group->id==$default?'active show':''}}" id="tab{{$group->id}}">
                                        <?php $iii++ ?>
                                        <form class="send_ajax" data-before="beforeSave" method="post" action="{{route('p_optionsSave')}}" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="groupId" value="{{$group->id}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            @foreach($options[$group->id] as $option)
                                                <div class="row" >
                                                    <div class="col-sm-12 col-md-4 col-lg-2" style="padding-top: 25px">{{$option->title}}</div>
                                                    <div class="col-sm-12 col-md-4 col-lg-5">
                                                        @if($option->type=='image')
                                                            <div class="row">
                                                                <div class="col-9 file-field input-field">
                                                                    <div>
                                                                        <input type="hidden" class="img_url" name="value_{{$option->id}}" value="{{$option->value}}">
                                                                        <button style="margin-bottom: 20px;float: right" id="id_{{$option->id}}" type="button" class="btn btn-outline-info btn_image">
                                                                            انتخاب
                                                                        </button>
{{--                                                                        <img src="{{url($option->value)}}">--}}
                                                                        <div style="clear: both"></div>
                                                                    </div>
                                                                    <div >
                                                                        <div style="display: none" class="progress shadow-style">
                                                                            <div
                                                                                class="progress-bar width-per-40"
                                                                                role="progressbar"
                                                                                aria-valuenow="40"
                                                                                aria-valuemin="0"
                                                                                aria-valuemax="100"
                                                                            >
                                                                                75%
                                                                            </div>
                                                                        </div>
                                                                        <p class="myAlertImage text-danger"></p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-3">
                                                                    <a href="{{url($option->value)}}" target="_blank">
                                                                        <img src="{{url(str_replace('/upl_','/64_64_upl_',$option->value))}}">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @elseif($option->type=='textarea')
                                                            <textarea class="form-control" name="value_{{$option->id}}">{{$option->value}}</textarea>
                                                        @elseif($option->type=='ckeditor')
                                                            <textarea class="form-control ckeditor" id="ckeditor{{$option->id}}" name="value_{{$option->id}}">{{$option->value}}</textarea>
                                                        @else
                                                            <input type="text" name="value_{{$option->id}}" value="{{$option->value}}"  class="form-control">
                                                        @endif
                                                    </div>
                                                    <div class="col-md-4 p-t-15">
                                                        <p style="color: #999">{{$option->desc??''}}</p>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <p class="myAlert"></p>
                                            <p class="mySuccess"></p>
                                            <button type="submit" class="btn btn-success ">ذخیره تنظیمات</button>
                                        </form>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<form id="form_image">
    @csrf
    <input id="file_image" class="d-none" type="file" name="image">
</form>

    @endsection
