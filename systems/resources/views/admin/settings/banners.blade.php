 @extends('layouts.admin')

 @section('title','بنرها')

 @section('css')
     <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
    <style>
        .errorsAction li {
            margin-right: 35px;
            list-style-type: square !important;
            color: red;
        }
        #langs .active{
            background-color: blue !important;
        }
        #tbody1 .actions a.btn{
            width: 25px !important;
            height: 25px !important;
        }
        .modal-body label{
            font-size: 13px;
            margin-bottom: 0px;
            padding-right: 10px !important;
        }
        .modal-body input{
            border: 1px solid darkgray !important;
            border-radius: 5px !important;
            height: 27px !important;
            padding-right: 10px !important;
            padding-left: 10px !important;
            box-sizing: border-box !important;
        }
        .modal-body textarea{
            border: 1px solid darkgray !important;
            border-radius: 5px !important;
            padding-right: 5px !important;
            padding-left: 5px !important;
            box-sizing: border-box !important;
        }
        .modal-body .form-group{
            margin-bottom: 10px;
        }
        .removed{
            display: none !important;
        }
    </style>
 @endsection
 @section('js')
     <script>

         $('#menuOptionsBanners').addClass('cvcd').parent().addClass('active').parent().parent().addClass('active');


     </script>
     <script src="{{url('assets/js/admin.js')}}"></script>
     <script src="{{url('assets/js/form.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
    <script>
        var thisBtn;


        function after_add(data,thisForm){
            if(data.res === 10){
                $('#tbody1').html(data.tbl);

                window.setTimeout(function () {
                    $('#modalAdd').modal('hide');
                },3000);
            }
        }


        $('#tbody1').on('click','.edited',function (e) {
            e.preventDefault();
            thisBtn=$(this);
            $('#modalEdit .mySuccess').hide();
            $('#modalEdit .myAlert').hide();
            $('#modalEdit [name=id]').val($(this).parent().data('id'));
            $('#modalEdit [name=secure]').val($(this).parent().data('secure'));
            $('#modalEdit [name=title]').val($(this).parents('tr').find('.title').text());
            $('#modalEdit [name=link]').val($(this).parent().data('link'));
            $('#modalEdit [name=image]').val('');
            $('#modalEdit .best_size').html($(this).parent().data('size'));

            $('#modalEdit').modal('show');
        });


        $('#tbody1').on('click','.imagee',function (){
            $('#modalImage').modal('show');
            $('#modalImage img').prop('src',$(this).data('img'));
        });

    </script>
 @endsection

 @section('main')



<section class="content">
    <div class="container-fluid">

        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">

                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{route('p_dashboard')}}">
                                <i class="fas fa-home"></i> داشبورد</a>
                        </li>
                        <li class="breadcrumb-item bcrumb-2">
                            <a href="javascript:void(0);">تنظیمات سایت</a>
                        </li>
                        <li class="breadcrumb-item active">
                            بنرها
                        {{$title}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div id="langs">
                            <a href="{{route('p_banner')}}" class="btn btn-info {{DEFAULT_LANG[0]==$lang ? 'active' : ''}}">{{DEFAULT_LANG[1]}}</a>
                            @foreach(LANGUAGES as $lll)
                                <a href="{{route('p_banner',[$lll[0]])}}" class="btn btn-info {{$lll[0] == $lang ? 'active' : ''}}">{{$lll[1]}}</a>
                            @endforeach

                        </div>
                    <?php App\Helpers\Helpers::showErrors() ?>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            @php($r=0)
                            @foreach($banners as $k=>$banner)
                                <li role="presentation">
                                    <a href="#tab{{$r}}" data-toggle="tab" class="{{$r<1?'active show':''}}">
                                        {{$k}}
                                    </a>
                                </li>
                                @php($r++)
                            @endforeach
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" >
                            <?php $iii=0; ?>
                                @php($r=0)
                            @foreach($banners as $k=>$banner)

                                <div role="tabpanel" class="tab-pane fade {{$r<1?'active show':''}}" id="tab{{$r}}">
                                   <table class="table">
                                       <thead>
                                       <tr>
                                           <th class="w5">ردیف</th>
                                           <th class="w30">جایگاه</th>
                                           <th class="w40">عنوان</th>
                                           <th class="w10">تصویر</th>
                                           <th class="w15">عملیات</th>
                                       </tr>
                                       </thead>
                                    @php($kk = 1)
                                       <tbody id="tbody1">

                                       {!! \App\Http\Controllers\admin\settings\BannerController::tbl($banner) !!}

                                       </tbody>
                                   </table>

                                </div>
                                    @php($r++)
                            @endforeach



                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="modal fade" id="modalEdit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ویرایش</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" class="send_ajax" data-after="after_add" id="formEdit" action="{{route('p_banner_edit')}}">
                    @csrf
                    <input type="hidden" name="lang" value="{{$lang}}">
                    <input type="hidden" name="id" >
                    <input type="hidden" name="secure" >
                    <div class="form-group">
                        <label>عنوان : </label>
                        <input type="text" name="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>لینک : </label>
                        <input type="text" name="link" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>تصویر : <sup>*</sup></label>
                        <input type="file" name="image" class="form-control">
                        <p>بهترین سایز :
                        <span style="direction: ltr;display: inline-block;" class="best_size"></span>
                            پیکسل
                        </p>
                    </div>

                    <p class="mySuccess"></p>
                    <p class="myAlert"></p>
                    <div class="form-group">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>

                        <button id="btnEdit" type="submit" class="btn btn-success">ذخیره</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modalImage">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">تصویر بنر</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <img style="max-width: 100%;">
            </div>
        </div>
    </div>
</div>



 @endsection
