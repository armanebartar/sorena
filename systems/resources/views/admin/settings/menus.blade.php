@extends('layouts.admin')

    @section('title')
        منوها
    @stop

    @section('css')
        <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
    @endsection
    @section('js')
        <script>

            $('#menuOptionsMenu{{$group}}').addClass('cvcd').parent().addClass('active').parent().parent().addClass('active');


        </script>
        <script src="{{url('assets/js/table.min.js')}}"></script>
        <script src="{{url('assets/js/admin.js')}}"></script>
        <script src="{{url('assets/js/form.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/dataTables.buttons.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.flash.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/jszip.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/pdfmake.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/vfs_fonts.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.html5.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.print.min.js')}}"></script>
        <script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>
        <script>
            $('select').formSelect();
            function afterAdd(data,thisForm){
                if(data.res == 10){
                    thisForm.find('[name=name]').val('');
                    thisForm.find('[name=link]').val('');
                    thisForm.find('[name=image]').val('');
                    thisForm.find('[name=sort]').val('1');
                    thisForm.find('[name=parent]').val('0');
                    $('#tbody1').html(data.tbl);
                    $('.parents').each(function () {
                        $(this).html(data.parentsTbl).formSelect() ;
                    });
                    $('select').formSelect();
                    window.setTimeout(function () {
                        $('#modalEdit').modal('hide');
                    },3000);
                }
            }
            // var btnText;
            // var thisBtn;
            // var dataForm;
            // $('.formAdd').on('submit',(function(e) {
            //     e.preventDefault();
            //     var formData = new FormData(this);
            //     var thisForm = $(this);
            //     thisForm.find('.mySuccess').hide();
            //     thisForm.find('.myAlert').hide();
            //
            //     $.ajax({
            //         type:'POST',
            //         url: $(this).attr('action'),
            //         data:formData,
            //         cache:false,
            //         contentType: false,
            //         processData: false,
            //         dataType: 'json',
            //         success:function(data){
            //             thisForm.find('[type=submit]').html(btnText).removeClass('act');
            //             thisForm.find('.mySuccess').show().html(data.mySuccess);
            //             thisForm.find('.myAlert').show().html(data.myAlert);
            //             if(data.res == 10){
            //                 thisForm.find('[name=name]').val('');
            //                 thisForm.find('[name=link]').val('');
            //                 thisForm.find('[name=image]').val('');
            //                 thisForm.find('[name=sort]').val('1');
            //                 thisForm.find('[name=parent]').val('0');
            //                 $('#tbody1').html(data.tbl);
            //                 $('.parents').each(function () {
            //                     $(this).html(data.parentsTbl).formSelect() ;
            //                 });
            //                 $('select').formSelect();
            //                 window.setTimeout(function () {
            //                     $('#modalEdit').modal('hide');
            //                 },3000);
            //             }
            //         },
            //         error: function(data){
            //             $("#submitAdd").html(btnText).removeClass('act');
            //         }
            //     });
            // }));

            // $(".formAdd [type=submit]").on("click", function(e) {
            //     e.preventDefault();
            //
            //     thisBtn=$(this);
            //     if(thisBtn.hasClass('act')){
            //         return;
            //     }
            //     btnText = thisBtn.html();
            //
            //     thisBtn.html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>').addClass('act');
            //     thisBtn.parents('form').submit();
            // });


            $('#tbody1').on('click','.removed',function (e) {
                e.preventDefault();
                $('#modalRemove .name').text($(this).parent().data('name'));
                $('#modalRemove .slug').text($(this).parent().data('slug'));
                $('#modalRemove .id').val($(this).parent().data('id'));
                $('#modalRemove .mySuccess').hide();
                $('#modalRemove .myAlert').hide();
                $('#modalRemove').modal('show');
            });

            function afterRemove(data,thisForm){
                if(data.res === 10){
                    $('#tbody1').html(data.tbl);
                    $('.parents').each(function () {
                        $(this).html(data.parentsTbl).formSelect() ;
                    });


                    window.setTimeout(function () {
                        $('#modalRemove [type=button]').trigger('click');
                    },3000);
                }
            }

//             $('#modalRemove [type=submit]').on('click',function (e) {
//                 e.preventDefault();
//                 $('#modalRemove .mySuccess').hide();
//                 $('#modalRemove .myAlert').hide();
//                 var thisBtn=$(this);
//                 if(thisBtn.hasClass('act')){
//                     return;
//                 }
//                 var btnText = thisBtn.html();
//
//                 thisBtn.html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>').addClass('act');
//                 dataForm=thisBtn.parents('form').serialize();
//
//                 $.ajax({
//                     url: thisBtn.parents('form').prop('action'),
//                     type: "POST",
//                     data: dataForm,
// //                    async:false,
//                     dataType: 'json',
//                     success: function (data) {
//                         thisBtn.html(btnText).removeClass('act');
//
//                         $('#modalRemove .mySuccess').show().html(data.mySuccess);
//                         $('#modalRemove .myAlert').show().html(data.myAlert);
//
//                        if(data.res === 10){
//                            $('#tbody1').html(data.tbl);
//                            $('.parents').each(function () {
//                                $(this).html(data.parentsTbl).formSelect() ;
//                            });
//
//
//                             window.setTimeout(function () {
//                                 $('#modalRemove [type=button]').trigger('click');
//                             },3000);
//                         }
//                     }
//                 });
//             });

            $('#tbody1').on('click','.edited',function (e) {
                e.preventDefault();

                $('#modalEdit .mySuccess').hide();
                $('#modalEdit .myAlert').hide();
                $('#modalEdit .name').val($(this).parent().data('name'));


                $('#modalEdit .parent option').each(function () {
                    $(this).prop('selected','false');
                });
                var thisPar = $(this).parent().data('parent');

                $('#modalEdit #parentSelect').show();
                if($(this).parent().data('childes') == 0) {
                    $('#modalEdit .parent [value=' + thisPar + ']').prop('selected', 'selected');
                    $('#modalEdit .parent').formSelect()
                    $('#modalRemove [type=button]').data('offset', $(this).offset().top);
                }else {
                    $('#modalEdit #parentSelect').hide();
                }
                // $('select').formSelect();


                $('#modalEdit .id').val($(this).parent().data('id'));
                $('#modalEdit').modal('show');
                $('#modalEdit .link').val($(this).parent().data('link'));
                $('#modalEdit .sort').val($(this).parent().data('sort'));
            });





        </script>
    @endsection

    @section('main')
        <style>
            #tbody1 .lang .btn{
                padding: 0px 5px !important;
                height: 30px !important;
                margin-right: 3px;
            }
            #langs .active{
                background-color: blue !important;
            }
        </style>
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>

                                <li class="breadcrumb-item active">منوها</li>
                            </ul>

                        </div>
                        <div id="langs" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <a href="{{route('p_menu',[DEFAULT_LANG[0],$group])}}" class="btn btn-info {{DEFAULT_LANG[0]==$lang ? 'active' : ''}}">{{DEFAULT_LANG[1]}}</a>
                            @foreach(LANGUAGES as $lll)
                                <a href="{{route('p_menu',[$lll[0],$group])}}" class="btn btn-info {{$lll[0] == $lang ? 'active' : ''}}">{{$lll[1]}}</a>
                            @endforeach

                        </div>

                    </div>

                </div>
                <div class="row clearfix">

                    <div class=" col-md-4  col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>افزودن</strong> منو جدید</h2>

                            </div>
                            <div class="body">
                                <form id="formAdd" class="send_ajax" data-after="afterAdd" method="post" action="{{route('p_menu_addSave',[$lang,$group])}}" enctype="multipart/form-data">
                                    @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="name"  class="form-control" type="text">
                                                <label class="form-label">عنوان <sup>*</sup></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="link"  class="form-control" type="text">
                                                <label class="form-label">لینک </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="sort"  class="form-control" type="number">
                                                <label class="form-label">اولویت </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="image" class="form-control" type="file">
{{--                                                <label class="form-label">تصویر </label>--}}
                                            </div>
                                            <p>بهترین سایز : 64*64 پیکسل</p>
                                        </div>
                                    </div>

                                    <div class="col-12" style="margin-top: 28px">
                                        <div class="form-group ">
                                            <div>
                                                <select class="parents" id="parentTbl" name="parent" >
                                                    {!! $parentsTbl !!}
                                                </select>
                                                <label style="position: relative;top: -90px;font-size: 16px">والد </label>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="mySuccess"></p>
                                    <p class="myAlert"></p>
                                    <div class="col-12">
                                        <button id="submitAdd" type="submit" class="btn btn-success">ذخیره منو</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class=" col-md-8  col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>لیست</strong> منوها</h2>

                            </div>
                            <div class="body">
                                <div class="table-responsive">

                                    <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                        <thead>
                                        <tr>
                                            <th class="w5">ردیف</th>
                                            <th class="w25">عنوان</th>
                                            @if(count(LANGUAGES) > 0)
                                                <th class="w25">زبان</th>
                                            @else
                                                <th class="w25">لینک</th>
                                            @endif
                                            <th class="w15">تصویر</th>
                                            <th class="w30">عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody1">

                                        {!! $tbl !!}

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Exportable Table -->
            </div>



            <div class="modal fade" id="modalRemove">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">حذف منو</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form method="post" class="send_ajax" data-after="afterRemove" action="{{route('p_menu_remove',[$lang,$group])}}">
                            @csrf
                            <input name="id" class="id" type="hidden">
                            <div class="modal-body">
                                <h5>آیا مطمئنید که قصد حذف کردن این منو را دارید؟</h5>
                                <p><br><b>عنوان : </b><span class="name"></span></p>

                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                                <button type="submit" class="btn btn-info waves-effect sub">بله، حذف شود.</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modalEdit">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ویرایش منو</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form method="post"  class="send_ajax" data-after="afterAdd" action="{{route('p_menu_editSave',[$lang,$group])}}" enctype="multipart/form-data">
                            @csrf
                            <input name="id" class="id" type="hidden">
                            <div class="modal-body">
                                <p><br><b>نام : </b><input name="name"  class="name form-control"></p>
                                <p><br><b>لینک : </b><input name="link"  class="link form-control"></p>
                                <p><br><b>اولویت : </b><input name="sort"  class="sort form-control"></p>
                                <p><br><b>تصویر : </b><input name="image" type="file"  class="slug form-control"></p>
                                <p  id="parentSelect"><br><b>والد : </b>
                                    <select id="parentTbl2" name="parent" class="parent parents form-control">
                                        {!! $parentsTbl !!}
                                    </select>
                                </p>


                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                                <button type="submit" class="btn btn-info waves-effect sub">ذخیره تغییرات</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </section>

    @endsection
