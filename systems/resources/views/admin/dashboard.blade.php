@extends('layouts.admin')

    @section('title','داشبورد')

     @section('css')

     @endsection

    @section('js')
        <script src="{{url('assets/js/chart.min.js')}}"></script>
        <script src="{{url('assets/js/admin.js')}}"></script>
        <script src="{{url('assets/js/pages/index.js')}}"></script>
        <script src="{{url('assets/js/pages/charts/jquery-knob.js')}}"></script>
        <script src="{{url('assets/js/pages/sparkline/sparkline-data.js')}}"></script>
        <script src="{{url('assets/js/pages/medias/carousel.js')}}"></script>
    @endsection


    @section('main')
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Widgets -->
                @if(permission('dashboardShow'))
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="counter-box text-center white">
                            <div class="text font-17 m-b-5">بازدید </div>
                            <h3 class="m-b-10">{{$allViews ?? 0}} نفر
                                <i class="material-icons col-green">trending_up</i>
                            </h3>
                            <div class="icon">
                                <div class="chart chart-bar" data-trend="[{{$allViewsArr ?? 0}}]"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="counter-box text-center white">
                            <div class="text font-17 m-b-5"> مشاهیر</div>
                            <h3 class="m-b-10">{{$geniusCount ?? 0}} نفر
                            </h3>
                            <p>تعداد <b>{{$geniusCountDeactive ?? 0}}</b> نخبه در انتظار تایید</p>

                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="counter-box text-center white">
                            <div class="text font-17 m-b-5">کامنتهای دریافتی</div>
                            <h3 class="m-b-10">{{$commentCount ?? 0}} کامنت
                            </h3>
                            <p>تعداد <b>{{$commentCountNotViewed ?? 0}}</b> کامنت رویت نشده</p>

                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="counter-box text-center white">
                            <div class="text font-17 m-b-5">تماس با ما</div>
                            <h3 class="m-b-10">{{$contactCount ?? 0}} کامنت
                                <i class="material-icons col-green">trending_up</i>
                            </h3>
                            <p>تعداد <b>{{$contactCountNotViewed ?? 0}}</b> کامنت رویت نشده</p>
                        </div>
                    </div>
                </div>
                <!-- #END# Widgets -->

                    @endif


            </div>
        </section>

    @endsection

