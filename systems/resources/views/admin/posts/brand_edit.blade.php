@extends('layouts.admin')

 @section('title')
    ویرایش برند
    @stop

 @section('css')
    <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    <style>
        #divAddImage>div{
            margin-bottom: 0;
        }
        .helperText{
            font-size: 10px;
            padding-top: 1px;
            color: #111;
        }
        label{
            font-weight: bold!important;
            color: #111!important;
            font-size: 11px !important;
        }
    </style>
 @stop
 @section('js')
     <script>
         $('#menuBrand{{ucfirst($brand->type->type)}}').addClass('active').parent().addClass('active').parent().parent().addClass('active');
     </script>
    <script src="{{url('assets/js/admin.js')}}"></script>
    <script src="{{url('assets/js/form.min.js')}}"></script>
    <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
    <script src="{{url('assets/js/bundles/ckeditor-full/ckeditor.js')}}"></script>

    <script>
        'use strict';
        var config = {};
        config.language = 'fa';
        //        config.uiColor = '#2f6dbb';
        config.font_names =
            "Tahoma;" +
            "Nazanin/Nazanin, B Nazanin, BNazanin;" +
            "Yekan/Yekan, BYekan, B Yekan, Web Yekan;" +
            "IranSans/IranSans, IranSansWeb;" +
            "Parastoo/Parastoo;" +
            "Arial/Arial, Helvetica, sans-serif;" +
            "Times New Roman/Times New Roman, Times, serif;";
        CKEDITOR.replace('ckeditor1',config);

        $('select').formSelect();
        let _token = $('meta[name="csrf-token"]').attr('content');


        function beforeAdd(){
            $('#ckeditor1').val(CKEDITOR.instances['ckeditor1'].getData());
        }
        function afterAdd(data,thisForm){
            if(data.res == 10){
                location.replace("{{route('p_posts_brands',[$brand->type->type])}}");
            }
        }


        $('.btn_thumb').on('click',function (e){
            e.preventDefault();
            $('#file_thumb').trigger('click');
        });

        let file_thumb = document.getElementById('file_thumb');
        let btn_thumb = document.getElementById('btn_thumb');
        let progress_thumb = document.getElementById('progress_thumb');
        let image_thumb = document.getElementById('image_thumb');

        file_thumb.addEventListener('change',function (){
            $('#myAlertThumb').hide();
            $('#progress_thumb').css('opacity','1');
            const xhr = new XMLHttpRequest();
            xhr.open('POST',"{{route('p_posts_brands_saveThumb')}}");
            let formData = new FormData(document.getElementById('form_thumb'));

            xhr.addEventListener('load',function (event){
                let res = JSON.parse(xhr.responseText);
                console.log(res);
                if(res.res === 10){
                    $('#image_thumb').prop('src',res.url);
                    $('[name=image]').val(res.fileName);
                    $('[name=path]').val(res.path);
                }else {
                    $('.myAlertThumb').show().text(res.myAlert);
                    window.setTimeout(function (){
                        $('.myAlertThumb').hide();
                    },3000);
                }
                $('#progress_thumb').css('opacity','0');
            });
            xhr.upload.addEventListener('progress',function (event){
                let percent = parseInt((event.loaded / event.total) * 100);
                $('#progress_thumb').attr('class',`progress-bar width-per-${percent}`);
                $('#progress_thumb').attr('aria-valuenow',percent);
                $('#progress_thumb').text(percent+" %");
            });
            xhr.send(formData);
        });


    </script>
 @stop

 @section('main')

     <style>
         .image_attach {
             position: relative;
             border: 1px solid #555;
             margin-bottom: 10px !important;
             padding: 5px;
             border-radius: 10px;
         }
         .image_attach .img_b{
             width: 100px;
         }
         .image_attach .rm_file_att_icon{
             width: 24px;
             position: absolute;
             cursor: pointer;
             left: 5px;
         }

          .select-wrapper{
              border: 1px solid #aaa;
              padding: 5px 7px 0 0;
          }
          .select-wrapper input{
              border: 0 !important;
          }
          .box-add-property{
              width: 400px;
              border: 1px solid #999;
              border-radius: 10px;
              position: absolute;
              right: -254px;
              background: floralwhite;
              z-index: 1;
              box-shadow: 2px 2px 8px;
              top: 113%;
              display: none;
          }
          .box-add-property button{
              padding: 0 7px;
              position: relative;
              top: 29px;
              left: 25px;
          }
          .btn-add-property{
              padding: 3px;
              position: relative;
              top: 7px;
              left: 23px;
              height: 30px;
          }
     </style>


    <section class="content">
        <form method="post" class="send_ajax" data-after="afterAdd" data-before="beforeAdd" action="{{route('p_posts_brands_edit_complete_save')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$brand->id}}">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>
                                <li class="breadcrumb-item bcrumb-2">
                                    <a href="{{route('p_posts_brands',[$brand->type->type])}}">برند های {{$brand->type->title}}</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    ویرایش برند
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    ویرایش برند
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group ">
                                            <div >
                                                <label class="form-label">عنوان برند <sup>*</sup></label>
                                                <input class="form-control" value="{{$brand->name}}" name="name" type="text">
                                            </div>
                                            <p class="helperText">
                                                عنوان  باید حداقل شامل ۳ کارکتر و حداکثر ۲۰۰ کارکتر باشد.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group form-float">
                                            <div >
                                                <label class="form-label ">عنوان فرعی (اسلاگ) </label>
                                                <input class="form-control" value="{{$brand->slug}}" name="slug" type="text">
                                                <p class="helperText">این مقدار در url صفحه قرار میگیرد</p>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">توضیحات مختصر </label>
                                                <textarea name="minContent" rows="5" class="form-control">{{$brand->minContent ?? ''}}</textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>توضیحات تکمیلی :‌ <label></label></strong>
                                </h2>

                            </div>
                            <div class="body">
                                <textarea name="desc" id="ckeditor1" contenteditable="true">{{$brand->desc ?? ''}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>تنظیمات سئو</strong>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">عنوان  </label>
                                                <input class="form-control" name="seo_title" value="{{$brand->seo_title ?? ''}}" type="text">
                                            </div>
                                            <p class="helperText">
                                                این عنوان در متا تگ تایتل قرار میگیرد
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">توضیحات  </label>
                                                <textarea name="seo_desc" rows="5" class="form-control">{{$brand->seo_desc ?? ''}}</textarea>
                                            </div>
                                            <p class="helperText">
                                                این مقدار در متاتگ دیسکریپشن قرار میگیرد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">کلمات کلیدی  </label>
                                                <textarea name="seo_keyword" rows="5" class="form-control">{{$brand->seo_keyword ?? ''}}</textarea>
                                            </div>
                                            <p class="helperText">
                                                هر کلمه کلیدی را در یک خط قرار دهید و یا بین کلمات کلیدی کاما بگذارید.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>تصویر</strong> شاخص
                                    <button type="button" id="btn_thumb" class="btn btn-outline-info btn_thumb">انتخاب</button>
                                </h2>
                            </div>
                            <div class="body">
                                <p class="myAlertThumb text-danger"></p>
                                <div id="progress_thumb" style="opacity: 0" class="progress shadow-style">
                                    <div class="progress-bar width-per-40" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100">75%</div>
                                </div>
                                <div class="file-field input-field">
                                    <img id="image_thumb" class="getImageFromMedia btn_thumb"
                                         style="margin-bottom: 60px;"
                                         src="{{is_file(ROOT.$brand->path.$brand->image) ? url($brand->path.$brand->image) : url('assets/images/add_image_index.png')}}">
                                    <input type="hidden" name="image" value="{{$brand->image}}">
                                    <input type="hidden" name="path" value="{{$brand->path}}">
                                </div>
                                <p class="myAlert"></p>
                                <p class="mySuccess"></p>
                                <button id="savePost"  data-numb="1" type="submit" class="btn btn-success btn-border-radius waves-effect"> ذخیره تغییرات</button>

                            </div>
                        </div>
                    </div>
{{--                    <div class="col-lg-8 col-md-6" style="min-height: 100%;padding-bottom: 30px">--}}
{{--                        <div class="card" style="height: 100%">--}}
{{--                            <div class="header">--}}
{{--                                <h2>--}}
{{--                                    <strong>تصاویر</strong> جانبی--}}
{{--                                    <button type="button" id="btn_images" class="btn btn-outline-info btn_images">انتخاب</button>--}}

{{--                                </h2>--}}
{{--                            </div>--}}
{{--                            <div class="body">--}}
{{--                                <div id="box_images" class="file-field input-field">--}}
{{--                                    @if($data['attaches'] != [])--}}
{{--                                        @foreach($data['attaches'] as $k=>$attach)--}}
{{--                                            <div id="image_attach100{{$k}}" class="image_attach row">--}}
{{--                                                <div class='col-md-8'>--}}
{{--                                                    <label>عنوان فایل : </label>--}}
{{--                                                    <input type="text" class="form-control" value="{{$attach->title}}" name="att_img_desc_{{$attach->id}}">--}}
{{--                                                </div>--}}
{{--                                                <div class='col-md-4'>--}}
{{--                                                    <a href="{{url($attach->path.$attach->file)}}" target="_blank">--}}
{{--                                                        <img class="img_b" src="{{url($attach->path.$attach->file)}}">--}}
{{--                                                    </a>--}}
{{--                                                    <img data-div="att_20" class="rm_file_att_icon" src="{{url('assets/images/remove.png')}}">--}}
{{--                                                    <input type="hidden" name="attach[]" value="{{$attach->id}}">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>


            </div>
        </form>
        <form id="form_thumb">
            @csrf
        <input value="{{$brand->type->type}}" type="hidden" name="type">
        <input id="file_thumb" class="d-none" type="file" name="thumb">
        </form>
        <input id="images" class="d-none" type="file" name="images[]" multiple>
    </section>

 @stop
