@extends('layouts.admin')

    @section('title')
        {{$special->name}}
        {{$meta['nameSingle']}}
    @stop

    @section('css')
        <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
        <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.css')}}" rel="stylesheet" />
        <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.theme.css')}}" rel="stylesheet" />
        <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery_ui_rtl.css')}}" rel="stylesheet" />
    @endsection
    @section('js')
        <script src="{{url('assets/js/table.min.js')}}"></script>
        <script src="{{url('assets/js/admin.js')}}"></script>
        <script src="{{url('assets/js/form.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/dataTables.buttons.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.flash.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/jszip.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/pdfmake.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/vfs_fonts.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.html5.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.print.min.js')}}"></script>
        <script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>
        <script src="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
        <script>
            $('select').formSelect();
            $.ajaxSetup({headers:{'X-CSRF-TOKEN':$('meta[name=csrf-token]').attr('content')}});
            $('#formAdd .name').val('');
            $('#formAdd #posts_name').val('');
            $('#formAdd [name=image]').val('');
            $('#formAdd [name=id]').val('');
            $.ajax({
                url: "{{route('p_posts_specials_posts',[$meta['type']])}}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $("#posts_name").autocomplete({
                        source: data,
                        select: function( event, ui ) {
                            $('#formAdd .name').val(ui.item.name);
                            $('#formAdd .id').val(ui.item.id);
                            $('#formAdd [name=image]').val('');
                        }
                    });
                }
            });

            function afterAdd(data,thisForm){
                if(data.res == 10){
                    thisForm.find('input[type=text]').val('');
                    $('#tbody1').html(data.tbl);

                    window.setTimeout(function () {
                        $('#modalEdit').modal('hide');
                        $('#modalRemove').modal('hide');
                    },3000)
                }
            }



            $('#tbody1').on('click','.removed',function (e) {
                e.preventDefault();
                $('#modalRemove .name').text($(this).parent().data('name'));
                $('#modalRemove .sp').val($(this).parent().data('sp'));
                $('#modalRemove .id').val($(this).parent().data('id'));
                $('#modalRemove .secure').val($(this).parent().data('secure'));
                $('#modalRemove .mySuccess').hide();
                $('#modalRemove .myAlert').hide();
                $('#modalRemove').modal('show');
            });


            $('#tbody1').on('click','.edited',function (e) {
                e.preventDefault();

                $('#modalEdit .mySuccess').hide();
                $('#modalEdit .myAlert').hide();
                $('#modalEdit .name').val($(this).parent().data('name'));

                $('#modalEdit .id').val($(this).parent().data('id'));
                $('#modalEdit .sp').val($(this).parent().data('sp'));
                $('#modalEdit .secure').val($(this).parent().data('secure'));
                $('#modalEdit').modal('show');

            });


            let thisBtnLang;
            $('#tbody1').on('click','.lang .btn',function (e) {
                e.preventDefault();
                $('#modalLang').modal('show');
                $('#modalLang .name').text($(this).data('title'));
                $('#modalLang [name=name]').val($(this).data('title2'));
                $('#modalLang [name=secure]').val($(this).data('secure'));
                $('#modalLang [name=id]').val($(this).data('id'));
                $('#modalLang [name=lang]').val($(this).data('lang'));
                $('#modalLang [name=sp]').val($(this).data('sp'));
                $('#modalLang .mySuccess').hide();
                $('#modalLang .myAlert').hide();
                thisBtnLang = $(this);
            });


            function afterLang(data,thisForm){
                if(data.res === 10){
                    thisBtnLang.removeClass('btn-info').addClass('btn-primary').data('title2',$('#modalLang [name=name]').val());
                    window.setTimeout(function () {
                        $('#modalLang').modal('hide');
                    },3000);
                }
            }
            function afterEditSp(data,thisForm){
                if(data.res === 10){
                    location.reload(true);
                }
            }


        </script>
    @endsection

    @section('main')
        <style>
            #tbody1 .lang .btn{
                padding: 0px 5px !important;
                height: 30px !important;
                margin-right: 3px;
            }
        </style>
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>
                                <li class="breadcrumb-item bcrumb-2">
                                    <a href="{{route($meta['baseRout'],[$meta['type']])}}">{{$meta['nameTotal']}}</a>
                                </li>
                                <li class="breadcrumb-item active">{{$special->name}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>{{$special->name}}</strong> </h2>
                                <p style="margin-top: 10px;" class="text-info">{{$special->desc}}</p>
                            </div>
                            <div class="body">

                                @php(session(['saveSpecProd'=>'']))
                                <form method="post" class="send_ajax" data-after="afterEditSp" action="{{route('p_shop_special_saveGroup')}}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$special->id}}">
                                    <input type="hidden" name="type" value="{{$meta['type']}}">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="myAlert"></p>
                                            <p class="mySuccess"></p>
                                        </div>
                                        <div class="col-md-3 myInput">
                                            <label>نام گروه : <sup>*</sup></label>
                                            <input type="text" name="name" class="form-control" value="{{$special->name??''}}">
                                        </div>
                                        <div class="col-md-3 myInput">
                                            <label>لینک : <sup>*</sup></label>
                                            <input type="text" name="link" class="form-control" value="{{$special->link??''}}">
                                        </div>
                                        <div class="col-md-3 myInput">
                                            <label>تصویر : <sup>*</sup></label>
                                            <input type="file" style="position: relative;top: 10px" name="image" class="form-control" value="{{$special->image??''}}">

                                        </div>
                                        <div class="col-md-1">

                                            <button type="submit" style="position: relative;top: 30px;padding: 0 10px;" class="btn btn-success">ذخیره</button>
                                        </div>
                                        <div class="col-md-2">
                                            @if(is_file(ROOT.$special->path.$special->image))
                                                <img style="height: 80px;float: left" src="{{url($special->path.$special->image)}}">
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class=" col-md-4  col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>افزودن</strong> مورد جدید
                                </h2>
                            </div>
                            <div class="body">
                                <form id="formAdd" class="send_ajax" data-after="afterAdd" method="post" action="{{route('p_posts_specials_addSave',[$meta['type']])}}" enctype="multipart/form-data">
                                    @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div class="">
                                                <label class="form-label">
                                                    {{$meta['nameTotal']}}
                                                    <sup>*</sup>


                                                </label>
                                                <input  id="posts_name" value="" class="form-control" type="text">

                                                <input type="hidden" class="id" name="post_id">
                                                <input type="hidden" name="special" value="{{$special->id}}">
                                                <input type="hidden" name="secure" value="{{\App\Models\Post::secure([$special->id,$meta['type']])}}">
                                                <p>
                                                    <small>حداقل یک کارکتر از عنوان {{$meta['nameSingle']}} مورد نظر را وارد نمایید.</small>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div class="">
                                                <label class="form-label">عنوان <sup>*</sup></label>
                                                <input name="name" value="{{old('name')}}" class="form-control name" type="text">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input name="image" class="form-control" type="file">
{{--                                                <label class="form-label">تصویر </label>--}}
                                            </div>
                                            <p><small>اگر تصویری انتخاب نشود تصویر {{$meta['nameSingle']}} در نظر گرفته می‌شود.</small></p>
                                        </div>
                                    </div>


                                    <p class="mySuccess"></p>
                                    <p class="myAlert"></p>
                                    <div class="col-12">
                                        <button id="submitAdd"  type="submit" class="btn btn-success">ذخیره </button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class=" col-md-8  col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>لیست</strong> {{$special->name}}</h2>

                            </div>
                            <div class="body">
                                <div class="table-responsive">

                                    <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                        <thead>
                                        <tr>
                                            <th class="w5">ردیف</th>
                                            <th class="w25">عنوان</th>
                                            @if(count(LANGUAGES) > 0)
                                                <th class="w25">زبان</th>
                                            @endif
                                            <th class="w15">تصویر</th>
                                            <th class="w30">عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody1">

                                        {!! $tbl !!}

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Exportable Table -->
            </div>



            <div class="modal fade" id="modalRemove">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">حذف گروه</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form class="send_ajax" data-after="afterAdd" method="post" action="{{route('p_posts_specials_remove',[$meta['type']])}}">
                            @csrf
                            <input name="id" class="id" type="hidden">
                            <input name="secure" class="secure" type="hidden">
                            <input name="sp" class="sp" type="hidden">
                            <div class="modal-body">
                                <h5>آیا مطمئنید که قصد حذف کردن این مورد را دارید؟</h5>
                                <p><br><b>عنوان : </b><span class="name"></span></p>
                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                                <button type="submit" class="btn btn-info waves-effect sub">بله، حذف شود.</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalLang">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ترجمه <span class="lang"></span></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form id="formAddLang1" class="send_ajax" data-after="afterLang" method="post" action="{{route('p_posts_specials_addLang',[$meta['type']])}}" enctype="multipart/form-data">
                            @csrf
                            <input name="id" class="id" type="hidden">
                            <input name="lang" class="lang" type="hidden">
                            <input name="sp" class="sp" type="hidden">
                            <input name="secure" class="secure" type="hidden">
                            <div class="modal-body">
                                <p><br><b>نام : </b><span class="name"></span></p>
                                <p><br><b>نام : </b><input name="name"  class=" form-control"></p>
                                <p><br><b>تصویر : </b><input name="image" type="file" class=" form-control"></p>
                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                                <button type="submit" id="btnLang"  class="btn btn-info waves-effect sub">ذخیره</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalEdit">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ویرایش گروه</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form method="post" class="send_ajax"  data-after="afterAdd" action="{{route('p_posts_specials_addSave',[$meta['type']])}}">
                            @csrf
                            <input name="id" class="id" type="hidden">
                            <input name="sp" class="sp" type="hidden">
                            <input name="secure" class="secure" type="hidden">
                            <input type="hidden" name="special" value="{{$special->id}}">
                            <div class="modal-body">
                                <p><br><b>نام : </b><input name="name"  class="name form-control"></p>
                                <p><br><b>تصویر : </b><input name="image" type="file"  class="slug form-control"></p>

                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                                <button type="submit" class="btn btn-info waves-effect sub">ذخیره تغییرات</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </section>

    @endsection
