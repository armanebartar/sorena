@extends('layouts.admin')

 @section('title')
    ویرایش دسته
    @stop

 @section('css')
    <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    <style>
        #divAddImage>div{
            margin-bottom: 0;
        }
        .helperText{
            font-size: 10px;
            padding-top: 1px;
            color: #111;
        }
        label{
            font-weight: bold!important;
            color: #111!important;
            font-size: 11px !important;
        }
    </style>
 @stop
 @section('js')
     <script>
         $('#menuCat{{ucfirst($cat->type->type)}}').addClass('active').parent().addClass('active').parent().parent().addClass('active');
     </script>
    <script src="{{url('assets/js/admin.js')}}"></script>
    <script src="{{url('assets/js/form.min.js')}}"></script>
    <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
    <script src="{{url('assets/js/bundles/ckeditor-full/ckeditor.js')}}"></script>

    <script>
        'use strict';
        var config = {};
        config.language = 'fa';
        //        config.uiColor = '#2f6dbb';
        config.font_names =
            "Tahoma;" +
            "Nazanin/Nazanin, B Nazanin, BNazanin;" +
            "Yekan/Yekan, BYekan, B Yekan, Web Yekan;" +
            "IranSans/IranSans, IranSansWeb;" +
            "Parastoo/Parastoo;" +
            "Arial/Arial, Helvetica, sans-serif;" +
            "Times New Roman/Times New Roman, Times, serif;";
        CKEDITOR.replace('ckeditor1',config);

        $('select').formSelect();
        let _token = $('meta[name="csrf-token"]').attr('content');


        function beforeAdd(){
            $('#ckeditor1').val(CKEDITOR.instances['ckeditor1'].getData());
        }
        function afterAdd(data,thisForm){
            if(data.res == 10){
                location.replace("{{route('p_posts_groups',[$cat->type->type])}}");
            }
        }


        $('.btn_thumb').on('click',function (e){
            e.preventDefault();
            $('#file_thumb').trigger('click');
        });

        let file_thumb = document.getElementById('file_thumb');
        let btn_thumb = document.getElementById('btn_thumb');
        let progress_thumb = document.getElementById('progress_thumb');
        let image_thumb = document.getElementById('image_thumb');

        file_thumb.addEventListener('change',function (){
            $('#myAlertThumb').hide();
            $('#progress_thumb').css('opacity','1');
            const xhr = new XMLHttpRequest();
            xhr.open('POST',"{{route('p_posts_group_saveThumb')}}");
            let formData = new FormData(document.getElementById('form_thumb'));

            xhr.addEventListener('load',function (event){
                let res = JSON.parse(xhr.responseText);
                console.log(res);
                if(res.res === 10){
                    $('#image_thumb').prop('src',res.url);
                    $('[name=image]').val(res.fileName);
                    $('[name=path]').val(res.path);
                }else {
                    $('.myAlertThumb').show().text(res.myAlert);
                    window.setTimeout(function (){
                        $('.myAlertThumb').hide();
                    },3000);
                }
                $('#progress_thumb').css('opacity','0');
            });
            xhr.upload.addEventListener('progress',function (event){
                let percent = parseInt((event.loaded / event.total) * 100);
                $('#progress_thumb').attr('class',`progress-bar width-per-${percent}`);
                $('#progress_thumb').attr('aria-valuenow',percent);
                $('#progress_thumb').text(percent+" %");
            });
            xhr.send(formData);
        });


        let propCode;
        @if($meta['type'] == 'product')
        $('#addProperty').on('click',function () {
            propCode = $(this).data('code') + 1;
            $(this).data('code',propCode);
            $('#property .body .notSet').detach();
            $('#property .body>div').append(
                "<div class=\"prop col-md-3\">\n" +
                "<em>X</em><input type='hidden' name='values[]' value='"+propCode+"'>" +
                "<input type='hidden' name='catId"+propCode+"' value='0'>" +
                "    <div class=\"row\">\n" +
                "        <div class=\"col-sm-10\">\n" +
                "            <div class=\"myInput\">\n" +
                "                <label>عنوان ویژگی : </label>\n" +
                "                <input type=\"text\" name='prop"+propCode+"' class=\"form-control\">\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"col-sm-1 divAddValue \">\n" +
                "            <i data-code='"+propCode+"' class=\"fa fa-plus-square\"></i>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>"
            );

        });
        @endif
        $('#property').on('click','.prop i.fa-plus-square',function () {
            propCode = $(this).data('code');
            $(this).parents('.prop').append(

                "<div class=\"myInput vall\">\n" +
                "   <input type=\"hidden\" value=\"0\" name=\"propId"+propCode+"[]\">\n" +
                "   <input type=\"text\" name=\"propTitle"+propCode+"[]\" placeholder=\"عنوان\" class=\"form-control\">\n" +
                "</div>"


            );
        });
        $('#property').on('click','.prop em',function () {
            $(this).parents('.prop').detach();
        });
        // $(function() {
        //     $("#datepicker1").persianDatepicker();
        //     $("#datepicker2").persianDatepicker();
        // });


        $('#addComment').on('click',function (e) {
            e.preventDefault();
            $('.comment.notSet').hide();
            $('#commentItems').append(
                "<div class=\"comment col-md-6\">\n" +
                "       <em>X</em>\n" +
                "       <div class=\"row\">\n" +
                "           <div class=\"col-sm-10\">\n" +
                "               <div class=\"myInput\">\n" +
                "                   <label>عنوان ویژگی کامنت : </label>\n" +
                "                   <input type=\"text\" value=\"\" name='comment[]' class=\"form-control\">\n" +
                "               </div>\n" +
                "           </div>\n" +
                "       </div>\n" +
                "   </div>"

            )
        });

        $('#commentItems').on('click','.comment>em',function () {
            $(this).parent().detach();
        });

        $('#addAttr').on('click',function (e) {
            e.preventDefault();
            $('.attr.notSet').hide();
            $('#attrs').append(
                "<div class=\"attr col-md-6\">\n" +
                "    <em>X</em>\n" +
                "    <div class=\"row\">\n" +
                "        <div class=\"col-sm-12\">\n" +
                "            <div class=\"myInput\">\n" +
                "                <label>عنوان خصوصیت : </label>\n" +
                "                <input type=\"text\" value=\"\" name='attr[]' class=\"form-control\">\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>"
            );

        });

        $('#attrs').on('click','.attr>em',function () {
            $(this).parent().detach();
        });

    </script>
 @stop

 @section('main')

     <style>
         .image_attach {
             position: relative;
             border: 1px solid #555;
             margin-bottom: 10px !important;
             padding: 5px;
             border-radius: 10px;
         }
         .image_attach .img_b{
             width: 100px;
         }
         .image_attach .rm_file_att_icon{
             width: 24px;
             position: absolute;
             cursor: pointer;
             left: 5px;
         }

          .select-wrapper{
              border: 1px solid #aaa;
              padding: 5px 7px 0 0;
          }
          .select-wrapper input{
              border: 0 !important;
          }
          .box-add-property{
              width: 400px;
              border: 1px solid #999;
              border-radius: 10px;
              position: absolute;
              right: -254px;
              background: floralwhite;
              z-index: 1;
              box-shadow: 2px 2px 8px;
              top: 113%;
              display: none;
          }
          .box-add-property button{
              padding: 0 7px;
              position: relative;
              top: 29px;
              left: 25px;
          }
          .btn-add-property{
              padding: 3px;
              position: relative;
              top: 7px;
              left: 23px;
              height: 30px;
          }
     </style>


    <section class="content">
        <form method="post" class="send_ajax" data-after="afterAdd" data-before="beforeAdd" action="{{route('p_posts_groups_edit_complete_save')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$cat->id}}">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>
                                <li class="breadcrumb-item bcrumb-2">
                                    <a href="{{route('p_posts_groups',[$cat->type->type])}}">دسته های {{$cat->type->title}}</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    ویرایش دسته
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    ویرایش دسته
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group ">
                                            <div >
                                                <label class="form-label">عنوان دسته <sup>*</sup></label>
                                                <input class="form-control" value="{{$cat->name}}" name="name" type="text">
                                            </div>
                                            <p class="helperText">
                                                عنوان  باید حداقل شامل ۳ کارکتر و حداکثر ۲۰۰ کارکتر باشد.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group form-float">
                                            <div >
                                                <label class="form-label ">عنوان فرعی (اسلاگ) </label>
                                                <input class="form-control" value="{{$cat->slug}}" name="slug" type="text">
                                                <p class="helperText">این مقدار در url صفحه قرار میگیرد</p>
                                            </div>

                                        </div>
                                    </div>
                                    @if($cat->lang == DEFAULT_LANG[0])
                                        <div class="col-md-4 col-xs-12 sel" style="padding-top: 25px">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <select class="groups"  name="parent" >
                                                        <option value="0">فاقد والد</option>
                                                        @foreach(\App\Models\Category::where([['group',$cat->group],['parent','0'],['parent_lang','0'],['id','<>',$cat->id]])->get() as $parent)
                                                            <option {{$parent->id==$cat->parent?'selected="selected"' : ''}} value="{{$parent->id}}">{{$parent->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <label class="form-label">والد

                                                    </label>
                                                </div>
                                                <div class="col-sm-1" style="position:relative;">
                                                    <button class="btn btn-success btn-add-property" type="button">
                                                        <i class="icon-plus"></i>
                                                    </button>
                                                    <div class="box-add-property">
                                                        <div class="row">
                                                            <div class="col-sm-10">
                                                                <label>عنوان والد جدید : </label>
                                                                <input type="text" class="form-control property" >
                                                                <input type="hidden" value="category" class="form-control prop-group" >
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <button
                                                                    type="button"
                                                                    class="btn btn-success save-prop"
                                                                    data-type="{{$cat->type->type}}"
                                                                    data-lang="{{$cat->lang}}"
                                                                    data-url="{{route('p_posts_saveProperty')}}"
                                                                >
                                                                    ذخیره
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">توضیحات مختصر </label>
                                                <textarea name="minContent" rows="5" class="form-control">{{$cat->minContent ?? ''}}</textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>توضیحات تکمیلی :‌ <label></label></strong>
                                </h2>

                            </div>
                            <div class="body">
                                <textarea name="desc" id="ckeditor1" contenteditable="true">{{$cat->desc ?? ''}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row clearfix">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>تنظیمات سئو</strong>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">عنوان  </label>
                                                <input class="form-control" name="seo_title" value="{{$cat->seo_title ?? ''}}" type="text">
                                            </div>
                                            <p class="helperText">
                                                این عنوان در متا تگ تایتل قرار میگیرد
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">توضیحات  </label>
                                                <textarea name="seo_desc" rows="5" class="form-control">{{$cat->seo_desc ?? ''}}</textarea>
                                            </div>
                                            <p class="helperText">
                                                این مقدار در متاتگ دیسکریپشن قرار میگیرد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">کلمات کلیدی  </label>
                                                <textarea name="seo_keyword" rows="5" class="form-control">{{$cat->seo_keyword ?? ''}}</textarea>
                                            </div>
                                            <p class="helperText">
                                                هر کلمه کلیدی را در یک خط قرار دهید و یا بین کلمات کلیدی کاما بگذارید.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>تصویر</strong> شاخص
                                    <button type="button" id="btn_thumb" class="btn btn-outline-info btn_thumb">انتخاب</button>
                                </h2>
                            </div>
                            <div class="body">
                                <p class="myAlertThumb text-danger"></p>
                                <div id="progress_thumb" style="opacity: 0" class="progress shadow-style">
                                    <div class="progress-bar width-per-40" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100">75%</div>
                                </div>
                                <div class="file-field input-field">
                                    <img id="image_thumb" class="getImageFromMedia btn_thumb"
                                         style="margin-bottom: 60px;"
                                         src="{{is_file(ROOT.$cat->path.$cat->image) ? url($cat->path.$cat->image) : url('assets/images/add_image_index.png')}}">
                                    <input type="hidden" name="image" value="{{$cat->image}}">
                                    <input type="hidden" name="path" value="{{$cat->path}}">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                @if($meta['is_product'])
                <div id="property" class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>
                                        ویژگیها :
                                        <button type="button" data-code="{{count($properties)}}" id="addProperty" style="line-height: 0;padding: 3px 8px;height: 31px;" class="btn btn-info">
                                            <i class="fa fa-plus" style="font-size:0.9rem"></i>
                                            افزودن ویژگی جدید
                                        </button>
                                    </strong>
                                    <small>ویژگیهای قابل انتخاب
                                        {{$meta['nameTotal']}}
                                        از قبیل رنگ و سایز را در این بخش اضافه نمایید تا در زمان تعریف
                                        {{$meta['nameSingle']}}
                                        برای این گروه بتوان از آن بهره برد.</small>
                                </h2>
                            </div>
                            <style>
                                .prop{
                                    background-color: lavender;
                                    padding: 5px;
                                    border-radius: 5px;
                                    margin-bottom: 15px;
                                    margin-left: 1%;
                                    flex: 0 0 24%;
                                    position: relative;
                                }
                                .divAddValue{
                                    position: relative;
                                }
                                .divAddValue>i{
                                    font-size: 2.5rem;
                                    position: absolute;
                                    top: 39%;right: 0;
                                    cursor: pointer;
                                }
                                .vall{
                                    padding: 1px 15px !important;
                                }
                                .vall input{
                                    text-align: center;
                                    height: 30px;
                                }
                                .prop em{
                                    position:absolute;
                                    left: 10px;
                                    color: red;
                                    cursor: pointer;
                                    z-index: 1000;
                                }
                            </style>
                            <div class="body ">
                                <div class="row">
                                    @if(count($properties)<1)
                                        <div class="prop notSet">
                                            هنوز هیچ ویژگی برای این گروه ثبت نشده است.
                                        </div>
                                    @else
                                        @foreach($properties as $k=>$v)
                                            <div class="prop col-md-3">
                                                <em>X</em>
                                                <input type='hidden' name='values[]' value='{{$k+1}}'>
                                                <input type='hidden' name='catId{{$k+1}}' value='{{$v->id}}'>
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <div class="myInput">
                                                            <label>عنوان ویژگی : </label>
                                                            <input type="text" value="{{$v->title}}" name='prop{{$k+1}}' class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1 divAddValue">
                                                        <i data-code='{{$k+1}}' class="fa fa-plus-square"></i>
                                                    </div>
                                                </div>
                                                @foreach($v->values as $pp)
                                                    <div class="myInput vall">
                                                        <input type="hidden" value="{{$pp->id}}" name="propId{{$k+1}}[]">
                                                        <input type="text" value="{{$pp->value}}" name="propTitle{{$k+1}}[]" placeholder="عنوان" class="form-control">
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($meta['by_attr'])
                <div id="attributes" class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>
                                        خصوصیات {{$meta['nameTotal']}} :
                                        <button type="button"  id="addAttr" style="line-height: 0;padding: 3px 8px;height: 31px;" class="btn btn-info">
                                            <i class="fa fa-plus" style="font-size:0.9rem"></i>
                                            افزودن خصوصیت جدید
                                        </button>
                                    </strong>
                                    <small>
                                        عناوین خصوصیت
                                        {{$meta['nameTotal']}}
                                        در اینجا تعریف میشود و در زمان ایجاد
                                        {{$meta['nameSingle']}}
                                        مقدار دهی میشوند.
                                    </small>
                                </h2>
                            </div>
                            <style>
                                .comment{
                                    background: aliceblue;
                                    padding: 5px;
                                    width: 100%;
                                }

                                #attrs em{
                                    color: red;
                                    cursor: pointer;
                                    float: left;
                                }
                            </style>
                            <div class="body ">
                                <div class="row" id="attrs">
                                    @if(count($attrs)<1)
                                        <div class="attr notSet">
                                            هنوز هیچ خصوصیتی برای
                                            {{$meta['nameTotal']}}
                                            این گروه ثبت نشده است.
                                        </div>
                                    @else
                                        @foreach($attrs as $k=>$v)
                                            <div class="attr col-md-6">
                                                <em>X</em>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="myInput">
                                                            <label>عنوان خصوصیت : </label>
                                                            <input type="text" value="{{$v}}" name='attr[]' class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                @endif

                <div id="comments" class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>
                                        ویژگیهای کامنت :
                                        <button type="button"  id="addComment" style="line-height: 0;padding: 3px 8px;height: 31px;" class="btn btn-info">
                                            <i class="fa fa-plus" style="font-size:0.9rem"></i>
                                            افزودن ویژگی جدید
                                        </button>
                                    </strong>
                                    <small>بر اساس آپشنهایی که در این بخش مشخص مینمایید، کاربران به
                                        {{$meta['nameTotal']}}
                                        این دسته امتیاز میدهند</small>
                                </h2>
                            </div>
                            <style>
                                .comment{
                                    background: aliceblue;
                                    padding: 5px;
                                    width: 100%;
                                }

                                #commentItems em{
                                    color: red;
                                    cursor: pointer;
                                    float: left;
                                }
                            </style>
                            <div class="body ">
                                <div class="row" id="commentItems">
                                    @if(count($comments)<1)
                                        <div class="comment notSet">
                                            هنوز هیچ ویژگی برای کامنتهای این گروه ثبت نشده است.
                                        </div>
                                    @else
                                        @foreach($comments as $k=>$v)
                                            <div class="comment col-md-6">
                                                <em>X</em>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="myInput">
                                                            <label>عنوان ویژگی کامنت : </label>
                                                            <input type="text" value="{{$v}}" name='comment[]' class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="body">

                                <p class="myAlert"></p>
                                <p class="mySuccess"></p>
                                <button id="savePost"  data-numb="1" type="submit" class="btn btn-success btn-border-radius waves-effect"> ذخیره تغییرات</button>

                            </div>
                        </div>
                    </div>
{{--                    <div class="col-lg-8 col-md-6" style="min-height: 100%;padding-bottom: 30px">--}}
{{--                        <div class="card" style="height: 100%">--}}
{{--                            <div class="header">--}}
{{--                                <h2>--}}
{{--                                    <strong>تصاویر</strong> جانبی--}}
{{--                                    <button type="button" id="btn_images" class="btn btn-outline-info btn_images">انتخاب</button>--}}

{{--                                </h2>--}}
{{--                            </div>--}}
{{--                            <div class="body">--}}
{{--                                <div id="box_images" class="file-field input-field">--}}
{{--                                    @if($data['attaches'] != [])--}}
{{--                                        @foreach($data['attaches'] as $k=>$attach)--}}
{{--                                            <div id="image_attach100{{$k}}" class="image_attach row">--}}
{{--                                                <div class='col-md-8'>--}}
{{--                                                    <label>عنوان فایل : </label>--}}
{{--                                                    <input type="text" class="form-control" value="{{$attach->title}}" name="att_img_desc_{{$attach->id}}">--}}
{{--                                                </div>--}}
{{--                                                <div class='col-md-4'>--}}
{{--                                                    <a href="{{url($attach->path.$attach->file)}}" target="_blank">--}}
{{--                                                        <img class="img_b" src="{{url($attach->path.$attach->file)}}">--}}
{{--                                                    </a>--}}
{{--                                                    <img data-div="att_20" class="rm_file_att_icon" src="{{url('assets/images/remove.png')}}">--}}
{{--                                                    <input type="hidden" name="attach[]" value="{{$attach->id}}">--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>


            </div>
        </form>
        <form id="form_thumb">
            @csrf
        <input value="{{$cat->type->type}}" type="hidden" name="type">
        <input id="file_thumb" class="d-none" type="file" name="thumb">
        </form>
        <input id="images" class="d-none" type="file" name="images[]" multiple>
    </section>

 @stop
