@extends('layouts.admin')

    @section('title')
        گروههای {{$meta['nameSingle']}}
    @stop

    @section('css')
        <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    @stop
    @section('js')
        <script src="{{url('assets/js/table.min.js')}}"></script>
        <script src="{{url('assets/js/admin.js')}}"></script>
        <script src="{{url('assets/js/form.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/dataTables.buttons.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.flash.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/jszip.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/pdfmake.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/vfs_fonts.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.html5.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.print.min.js')}}"></script>
        <script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>
        <script>
            $('select').formSelect();

            $('#tbody1').on('click','.removed',function (e) {
                e.preventDefault();
                $('#modalRemove .name').text($(this).parent().data('name'));
                $('#modalRemove .slug').text($(this).parent().data('slug'));
                $('#modalRemove .id').val($(this).parent().data('id'));
                $('#modalRemove .mySuccess').hide();
                $('#modalRemove .myAlert').hide();
                $('#modalRemove').modal('show');
            });
            function removeAfter(data,thisForm){
                if(data.res === 10){
                    $('#tbody1').html(data.tbl);
                    $('.parents').each(function () {
                        $(this).html(data.parentsTbl).formSelect() ;
                    });


                    window.setTimeout(function () {
                        thisForm.parents('').modal('hide');
                    },3000);
                }
            }

            let completeEdit = "{{route('p_posts_groups_edit_complete')}}";
            $('#tbody1').on('click','.edited',function (e) {
                e.preventDefault();

                $('#modalEdit [name=name]').val($(this).parent().data('name'));
                $('#modalEdit [name=slug]').val($(this).parent().data('slug'));
                $('#modalEdit [name=id]').val($(this).parent().data('id'));
                $('#modalEdit .completeEdit').prop('href',completeEdit+'/'+$(this).parent().data('id'));
                $('#modalEdit [name=parent] option').each(function () {
                    $(this).attr('selected','false');
                });

                $('#modalEdit #parentSelect').show();
                if($(this).parent().data('childes') == 0) {
                    $('#modalEdit [name=parent] [value=' + $(this).parent().data('parent') + ']').prop('selected', 'selected');
                    $('#modalEdit [name=parent]').formSelect();
                }else {
                    $('#modalEdit #parentSelect').hide();
                }

                $('#modalEdit').modal('show');
            });

            let thisBtnLang;
            let completeLang = "{{route('p_posts_groups_edit_lang')}}";
            $('#tbody1').on('click','.lang .btn',function (e) {
                e.preventDefault();
                $('#modalLang').modal('show');
                $('#modalLang .completeLang').prop('href',completeLang+'/'+$(this).data('id')+'/'+$(this).data('lang'));
                $('#modalLang .name').text($(this).data('title'));
                $('#modalLang .slug').text($(this).data('slug'));
                $('#modalLang [name=name]').val($(this).data('title2'));
                $('#modalLang [name=slug]').val($(this).data('slug2'));
                $('#modalLang [name=id]').val($(this).data('id'));
                $('#modalLang [name=lang]').val($(this).data('lang'));
                thisBtnLang = $(this);
            });

            function afterAdd(data,thisForm){
                if(data.res === 10){
                    $('#tbody1').html(data.tbl);
                    $('.parents').each(function () {
                        $(this).html(data.parentsTbl).formSelect() ;
                    });
                    $('select').formSelect();
                    thisForm.find('input').val('');
                    window.setTimeout(function () {
                        thisForm.parents('').modal('hide');
                    },3000);
                }
            }
            function afterAddLang(data,thisForm){
                if(data.res === 10){
                    $('#tbody1').html(data.tbl);
                    thisBtnLang.removeClass('btn-info').addClass('btn-primary')
                        .data('title2',$('#modalLang [name=name]').val())
                        .data('slug2',$('#modalLang [name=slug]').val());
                    thisForm.find('input').val('');
                    window.setTimeout(function () {
                        thisForm.parents('').modal('hide');
                    },3000);
                }
            }

        </script>
    @stop

    @section('main')
        <style>
            #tbody1 .lang .btn{
                padding: 0px 5px !important;
                height: 30px !important;
                margin-right: 3px;
            }
        </style>
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">
                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>
                                <li class="breadcrumb-item bcrumb-2">
                                    <a href="{{route($meta['baseRout'],[$meta['type']])}}">{{$meta['nameTotal']}}</a>
                                </li>
                                <li class="breadcrumb-item active">گروهبندی</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class=" col-md-4  col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>افزودن</strong> گروه جدید
                                </h2>
                            </div>
                            <div class="body">
                                <form id="formAdd" data-after="afterAdd" class="send_ajax" method="post" action="{{route('p_posts_groups_addSave',[$meta['type']])}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input name="name" value="{{old('name')}}" class="form-control" type="text">
                                                    <label class="form-label">نام گروه <sup>*</sup></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input name="slug" value="{{old('slug')}}" class="form-control" type="text">
                                                    <label class="form-label">نامک </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input name="image" class="form-control" type="file">
    {{--                                                <label class="form-label">تصویر </label>--}}
                                                </div>
                                                <p>
                                                    بهترین سایز :
                                                    {{$meta['image_sizes_cat'][count($meta['image_sizes_cat'])-1][0] ?? 0}}
                                                    *
                                                    {{$meta['image_sizes_cat'][count($meta['image_sizes_cat'])-1][1] ?? 0}}
                                                    پیکسل</p>
                                            </div>
                                        </div>

                                        <div class="col-12" style="margin-top: 28px">
                                            <div class="form-group ">
                                                <div>
                                                    <select class="parents" id="parentTbl" name="parent" >
                                                        {!! $parentsTbl !!}
                                                    </select>
                                                    <label style="position: relative;top: -90px;font-size: 16px">والد </label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="mySuccess"></p>
                                        <p class="myAlert"></p>
                                        <div class="col-12">
                                            <button id="submitAdd" type="submit" class="btn btn-success">ذخیره گروه</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class=" col-md-8  col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>لیست</strong> گروهها</h2>
                            </div>
                            <div class="body">
                                <div class="table-responsive">

                                    <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                        <thead>
                                        <tr>
                                            <th class="w5">ردیف</th>
                                            <th class="w25">نام گروه</th>
                                            <th class="w25">نامک</th>
                                            @if(count(LANGUAGES) > 0)
                                                <th class="w25">زبان</th>
                                            @endif
                                            <th class="w15">تصویر</th>
                                            <th class="w30">عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody1">
                                            {!! $tbl !!}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalRemove">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">حذف گروه</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form data-after="removeAfter" class="send_ajax" method="post" action="{{route('p_posts_groups_remove',[$meta['type']])}}">
                            @csrf
                            <input name="id" class="id" type="hidden">
                            <div class="modal-body">
                                <h5>آیا مطمئنید که قصد حذف کردن این گروه را دارید؟</h5>
                                <p>
                                    <b>نام : </b>
                                    <span class="name"></span>
                                </p>
                                <br>
                                <p>
                                    <b>نامک : </b>
                                    <span class="slug"></span>
                                </p>
                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                                <button type="submit" class="btn btn-info waves-effect ">بله، حذف شود.</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalLang">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ترجمه <span class="lang"></span></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>

                        <form class="send_ajax" data-after="afterAddLang" method="post" action="{{route('p_posts_groups_addLang',[$meta['type']])}}">
                            @csrf
                            <input name="id" class="id" type="hidden">
                            <input name="lang" class="lang" type="hidden">
                            <div class="modal-body">
                                <a href="" class="btn btn-primary completeLang" style="float: left;margin-bottom: 15px;">
                                    ترجمه کامل
                                </a>
                                <p><br><b>نام : </b><span class="name"></span></p>
                                <p><b>نامک : </b><span class="slug"></span></p>
                                <p><br><b>نام : </b><input name="name"  class=" form-control"></p>
                                <p><br><b>نامک : </b><input name="slug"  class=" form-control"></p>
                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                                <button type="submit" class="btn btn-info waves-effect sub">ذخیره</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalEdit">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ویرایش گروه</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form method="post" data-after="afterAdd" class="send_ajax" action="{{route('p_posts_groups_addSave',[$meta['type']])}}" enctype="multipart/form-data">
                            @csrf
                            <input name="id" class="id" type="hidden">
                            <div class="modal-body">
                                <a href="" class="btn btn-primary completeEdit" style="float: left;margin-bottom: 15px;">
                                    ویرایش کامل
                                </a>
                                <p><br><b>نام : </b><input name="name"  class="name form-control"></p>
                                <p><br><b>نامک : </b><input name="slug"  class="slug form-control"></p>
                                <p><br><b>تصویر : </b><input name="image" type="file"  class="slug form-control"></p>
                                <p  id="parentSelect"><br><b>گروه : </b>
                                    <select id="parentTbl2" name="parent" class="parent parents form-control">
                                        {!! $parentsTbl !!}
                                    </select>
                                </p>
                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                                <button type="submit" class="btn btn-info waves-effect sub">ذخیره تغییرات</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    @stop
