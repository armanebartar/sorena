@extends('layouts.admin')

    @section('title','برچسبها')

    @section('css')
        <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    @stop
    @section('js')
        <script src="{{url('assets/js/table.min.js')}}"></script>
        <script src="{{url('assets/js/admin.js')}}"></script>
        <script src="{{url('assets/js/form.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/dataTables.buttons.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.flash.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/jszip.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/pdfmake.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/vfs_fonts.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.html5.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.print.min.js')}}"></script>
        <script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>
        <script>
            $('select').formSelect();

            $('#tbody1').on('click','.removed',function (e) {
                e.preventDefault();
                $('#modalRemove .name').text($(this).parent().data('name'));
                $('#modalRemove .slug').text($(this).parent().data('slug'));
                $('#modalRemove .id').val($(this).parent().data('id'));
                $('#modalRemove .mySuccess').hide();
                $('#modalRemove .myAlert').hide();
                $('#modalRemove [type=button]').data('offset',$(this).offset().top);
                $('#btnModal').trigger('click');
            });

            let completeEdit = "{{route('p_posts_tags_edit_complete')}}";

            $('#tbody1').on('click','.edited',function (e) {
                e.preventDefault();
                $('#modalEdit .mySuccess').hide();
                $('#modalEdit .myAlert').hide();
                $('#modalEdit .completeEdit').prop('href',completeEdit+'/'+$(this).parent().data('id'));
                $('#modalEdit .name').val($(this).parent().data('name'));
                $('#modalEdit .slug').val($(this).parent().data('slug'));
                $('#modalEdit .id').val($(this).parent().data('id'));
                $('#modalRemove [type=button]').data('offset',$(this).offset().top);
                $('#btnModalEdit').trigger('click');
            });

            var thisBtnLang;
            let completeLang = "{{route('p_posts_tags_edit_lang')}}";
            $('#tbody1').on('click','.lang .btn',function (e) {
                e.preventDefault();
                $('#modalLang').modal('show');
                $('#modalLang .name').text($(this).data('title'));
                $('#modalLang .completeLang').prop('href',completeLang+'/'+$(this).data('id')+'/'+$(this).data('lang'));

                $('#modalLang .slug').text($(this).data('slug'));
                $('#modalLang [name=name]').val($(this).data('title2'));
                $('#modalLang [name=slug]').val($(this).data('slug2'));
                $('#modalLang [name=id]').val($(this).data('id'));
                $('#modalLang [name=lang]').val($(this).data('lang'));
                thisBtnLang = $(this);
            });
            function afterAdd(data,thisForm){
                if(data.res === 10){
                    $('#tbody1').html(data.tbl);
                    $('.parents').each(function () {
                        $(this).html(data.parentsTbl).formSelect() ;
                    });
                    $('select').formSelect();
                    thisForm.find('input').val('');
                    window.setTimeout(function () {
                        thisForm.parents('').modal('hide');
                    },3000);
                }
            }

            function afterAddLang(data,thisForm){
                if(data.res === 10){
                    $('#tbody1').html(data.tbl);
                    thisBtnLang.removeClass('btn-info').addClass('btn-primary')
                        .data('title2',$('#modalLang [name=name]').val())
                        .data('slug2',$('#modalLang [name=slug]').val());
                    thisForm.find('input').val('');
                    window.setTimeout(function () {
                        thisForm.parents('').modal('hide');
                    },3000);
                }
            }

        </script>
    @stop

    @section('main')
        <style>
            #tbody1 .lang .btn{
                padding: 0px 5px !important;
                height: 30px !important;
                margin-right: 3px;
            }
        </style>
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>

                                <li class="breadcrumb-item active">برچسبها</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class=" col-md-4  col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>افزودن</strong> برچسب جدید</h2>

                            </div>
                            <div class="body">
                                @if(permission('addTag'))
                                    <form id="formAdd" class="send_ajax" data-after="afterAdd" method="post" action="{{route('p_tags_addSave')}}">
                                       @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input class="form-control" name="name" type="text">
                                                    <label class="form-label">نام برچسب <sup>*</sup></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input class="form-control" name="slug" type="text">
                                                    <label class="form-label">نامک <sup>*</sup></label>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="mySuccess"></p>
                                        <p class="myAlert"></p>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success">ذخیره برچسب</button>
                                        </div>
                                    </div>
                                    </form>
                                @else
                                <p>در حال حاضر امکان افزودن برچسب برای شما میسر نمیباشد.</p>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class=" col-md-8  col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>لیست</strong> برچسبها</h2>

                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                    <style>
                                        #tbody1 td{
                                            font-size: 13px;
                                        }
                                        #tbody1 img{
                                            max-width: 100%;
                                            cursor: pointer;
                                        }
                                        #tbody1 .actions a{
                                            width: 18px !important;
                                            height: 18px !important;
                                            font-size: 10px;
                                        }
                                        .actShow span{
                                            font-size: 11px;
                                            padding: 0 14px;
                                        }
                                        ٫modalShowImage img{
                                            max-width: 100%;
                                        }
                                    </style>
                                    <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                        <thead>
                                        <tr>
                                            <th class="w5">ردیف</th>
                                            <th class="w25">نام برچسب</th>
                                            @if(count(LANGUAGES) > 0)
                                                <th class="w25">زبان</th>
                                            @else
                                                <th class="w25">نامک</th>
                                            @endif
                                            <th class="w30">عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody1">
                                            {!! $tbl !!}
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Exportable Table -->
            </div>


            <a id="btnModal" data-toggle="modal" href="#modalRemove"></a>
            <div class="modal fade" id="modalRemove">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">حذف برچسب</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form  class="send_ajax" data-after="afterAdd" method="post" action="{{route('p_tags_remove')}}">
                            @csrf
                            <input name="id" class="id" type="hidden">
                        <div class="modal-body">
                            <h5>آیا مطمئنید که قصد حذف کردن این برچسب را دارید؟</h5>
                            <p><br><b>نام : </b><span class="name"></span></p>
                            <p><b>نامک : </b><span class="slug"></span></p>
                            <p class="mySuccess"></p>
                            <p class="myAlert"></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                            <button type="submit" class="btn btn-info waves-effect sub">بله، حذف شود.</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <a id="btnModalEdit" data-toggle="modal" href="#modalEdit"></a>
            <div class="modal fade" id="modalEdit">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ویرایش برچسب</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form method="post" class="send_ajax" data-after="afterAdd" action="{{route('p_tags_addSave')}}">
                            {{csrf_field()}}
                            <input name="id" class="id" type="hidden">
                            <div class="modal-body">
                                <a href="" class="btn btn-primary completeEdit" style="float: left;margin-bottom: 15px;">
                                    ویرایش کامل
                                </a>
                                <p><br><b>نام : </b><input name="name"  class="name form-control"></p>
                                <p><br><b>نامک : </b><input name="slug"  class="slug form-control"></p>

                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                                <button type="submit" class="btn btn-info waves-effect sub">ذخیره تغییرات</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalLang">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ترجمه <span class="lang"></span></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <form class="send_ajax" data-after="afterAddLang" method="post" action="{{route('p_tags_addLang')}}">
                            {{csrf_field()}}
                            <input name="id" class="id" type="hidden">
                            <input name="lang" class="id" type="hidden">
                            <div class="modal-body">
                                <a href="" class="btn btn-primary completeLang" style="float: left;margin-bottom: 15px;">
                                    ترجمه کامل
                                </a>
                                <p><br><b>نام : </b><span class="name"></span></p>
                                <p><b>نامک : </b><span class="slug"></span></p>
                                <p><br><b>نام : </b><input name="name"  class=" form-control"></p>
                                <p><br><b>نامک : </b><input name="slug"  class=" form-control"></p>
                                <p class="mySuccess"></p>
                                <p class="myAlert"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                                <button type="submit" class="btn btn-info waves-effect sub">ذخیره</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



        </section>

    @stop
