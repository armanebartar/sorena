@extends('layouts.admin')

 @section('title')

    {{$mode=='add'?'افزودن '.$is_translate.$meta['nameSingle'].' جدید' : 'ویرایش '.$is_translate.$meta['nameSingle']}}
    @stop

 @section('css')
    <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    <link type="text/css" href="{{url('assets/js/bundles/jQuery-Datepicker-Plugin-For-Persian-Date-persianDatepicker/css/persianDatepicker-default.css')}}" rel="stylesheet" />
    <style>
        #divAddImage>div{
            margin-bottom: 0;
        }
        .helperText{
            font-size: 10px;
            padding-top: 1px;
            color: #111;
        }
        label{
            font-weight: bold!important;
            color: #111!important;
            font-size: 11px !important;
        }
    </style>
 @stop
 @section('js')
     <script>
         $('#{{$meta['idMenuName']}}').addClass('active').parent().addClass('active').parent().parent().addClass('active');
     </script>
    <script src="{{url('assets/js/admin.js')}}"></script>
    <script src="{{url('assets/js/form.min.js')}}"></script>
    <script src="{{url('assets/js/bundles/multiselect/js/jquery.multi-select.js')}}"></script>
    <script src="{{url('assets/js/bundles/ckeditor-full/ckeditor.js')}}"></script>
    <script src="{{url('assets/js/bundles/jQuery-Datepicker-Plugin-For-Persian-Date-persianDatepicker/js/persianDatepicker.min.js')}}"></script>

    <script>
        'use strict';
        var config = {};
        config.language = 'fa';
        //        config.uiColor = '#2f6dbb';
        config.font_names =
            "Tahoma;" +
            "Nazanin/Nazanin, B Nazanin, BNazanin;" +
            "Yekan/Yekan, BYekan, B Yekan, Web Yekan;" +
            "IranSans/IranSans, IranSansWeb;" +
            "Parastoo/Parastoo;" +
            "Arial/Arial, Helvetica, sans-serif;" +
            "Times New Roman/Times New Roman, Times, serif;";
        CKEDITOR.replace('ckeditor1',config);

        $('select').formSelect();
        let _token = $('meta[name="csrf-token"]').attr('content');
        let typee = "{{$meta['type']}}";
        let post_id = "{{$post->id}}";
        $(function() {
            $("#datepicker1").persianDatepicker();
            $("#datepicker2").persianDatepicker({
                formatDate: "YYYY/MM/DD hh:mm:ss"
            });
            $("#datepicker3").persianDatepicker({
                formatDate: "YYYY/MM/DD hh:mm:ss"
            });
        });

        function beforeAdd(){
            $('#ckeditor1').val(CKEDITOR.instances['ckeditor1'].getData());
        }
        function afterAdd(data,thisForm){
            if(data.res == 10){
                location.replace("{{route($meta['baseRout'],[$meta['type']])}}");
            }
        }


        $('.btn_thumb').on('click',function (e){
            e.preventDefault();
            $('#file_thumb').trigger('click');
        });

        let file_thumb = document.getElementById('file_thumb');
        let btn_thumb = document.getElementById('btn_thumb');
        let progress_thumb = document.getElementById('progress_thumb');
        let image_thumb = document.getElementById('image_thumb');

        file_thumb.addEventListener('change',function (){
            $('#myAlertThumb').hide();
            $('#progress_thumb').css('opacity','1');
            const xhr = new XMLHttpRequest();
            xhr.open('POST',"{{route('p_posts_saveThumb')}}");
            let formData = new FormData(document.getElementById('form_thumb'));

            xhr.addEventListener('load',function (event){
                let res = JSON.parse(xhr.responseText);
                console.log(res);
                if(res.res === 10){
                    $('#image_thumb').prop('src',res.url);
                    $('[name=image]').val(res.fileName);
                    $('[name=path]').val(res.path);
                }else {
                    $('.myAlertThumb').show().text(res.myAlert);
                    window.setTimeout(function (){
                        $('.myAlertThumb').hide();
                    },3000);
                }
                $('#progress_thumb').css('opacity','0');
            });
            xhr.upload.addEventListener('progress',function (event){
                let percent = parseInt((event.loaded / event.total) * 100);
                $('#progress_thumb').attr('class',`progress-bar width-per-${percent}`);
                $('#progress_thumb').attr('aria-valuenow',percent);
                $('#progress_thumb').text(percent+" %");
            });
            xhr.send(formData);
        });

        let images = document.getElementById('images');
        let btn_images = document.getElementById('btn_images');
        let icon_remove = "{{url('assets/images/remove.png')}}";
        $('.btn_images').on('click',function (e){
            e.preventDefault();
            $('#images').trigger('click');
        });

        images.addEventListener('change',function (){
            let files = images.files;
            let title = $('[name=title]').val();
            console.log(files);
            for (let m = 0; m < files.length ; m++){

                let formData = new FormData();
                formData.append('file'+m,files[m]);
                formData.append('_token',_token);
                formData.append('type',typee);
                formData.append('post_id',post_id);
                $('#box_images').append(`
                <div id="image_attach${m}" class="image_attach row">
                    <div id="progress_image${m}" class="progress shadow-style">
                        <div class="progress-bar width-per-1" role="progressbar" aria-valuenow="1"
                             aria-valuemin="0" aria-valuemax="100">1%</div>
                    </div>
                </div>
                `);

                const xhr = new XMLHttpRequest();
                xhr.open('POST',"{{route('p_posts_saveImageAtt')}}");
                xhr.addEventListener('load',function (event){
                    let res = JSON.parse(xhr.responseText);
                    $('#progress_image'+m).detach();
                    console.log(res);
                    if(res.res === 10){
                        $('#image_attach'+m).html(`
                        <div class='col-md-8'>
                            <label>عنوان فایل : </label>
                            <input type="text" class="form-control" value="${title}" name="att_img_desc_${res.id}">
                        </div>
                        <div class='col-md-4'>
                            <a href="${res.url}" target="_blank">
                                <img class="img_b" src="${res.url}">
                            </a>
                            <img data-div="att_20" class="rm_file_att_icon" src="${icon_remove}">
                            <input type="hidden" name="attach[]" value="${res.id}">
                        </div>

                        `);
                    }else {
                        $('#image_attach'+m).html("<p class='text-danger'>" + res.myAlert + "</p>");
                    }

                });
                xhr.upload.addEventListener('progress',function (event){
                    let percent = parseInt((event.loaded / event.total) * 100);
                    $('#progress_image'+m).attr('class',`progress-bar width-per-${percent}`);
                    $('#progress_image'+m).attr('aria-valuenow',percent);
                    $('#progress_image'+m).text(percent+" %");
                });
                xhr.send(formData);
            }
        });

        $('#box_images').on('click','.rm_file_att_icon',function (){
            let thisImg = $(this).parents('.image_attach');
            thisImg.css('backgroundColor','rgba(255,50,50,0.5)').hide(900);
            window.setTimeout(function (){
                thisImg.detach();
            },1500);
        });


        let filess = document.getElementById('files');
        let btn_attaches = document.getElementById('btn_attaches');
        $('.btn_attaches').on('click',function (e){
            e.preventDefault();
            $('#files').trigger('click');
        });

        filess.addEventListener('change',function (){
            let files = filess.files;
            let title = $('[name=title]').val();
            console.log(files,10);
            for (let m = 0; m < files.length ; m++){

                let formData = new FormData();
                formData.append('file'+m,files[m]);
                formData.append('_token',_token);
                formData.append('type',typee);
                formData.append('post_id',post_id);
                $('#box_attaches').append(`
                <div id="file_attach${m}" class="file_attach row">
                    <div id="progress_file${m}" class="progress shadow-style">
                        <div class="progress-bar width-per-1" role="progressbar" aria-valuenow="1"
                             aria-valuemin="0" aria-valuemax="100">1%</div>
                    </div>
                </div>
                `);

                const xhr = new XMLHttpRequest();
                xhr.open('POST',"{{route('p_posts_saveFileAtt')}}");
                xhr.addEventListener('load',function (event){
                    let res = JSON.parse(xhr.responseText);
                    $('#progress_image'+m).detach();
                    console.log(res);
                    if(res.res === 10){
                        $('#file_attach'+m).html(`
                        <div class='col-md-8'>
                            <label>عنوان فایل : </label>
                            <input type="text" class="form-control" value="${title}" name="att_img_desc_${res.id}">
                        </div>
                        <div class='col-md-4'>
                            <a href="${res.url}" target="_blank">
                                <i class="icon-doc"></i>
                            </a>
                            <img data-div="att_20" class="rm_file_att_icon" src="${icon_remove}">
                            <input type="hidden" name="attach[]" value="${res.id}">
                        </div>

                        `);
                    }else {
                        $('#file_attach'+m).html("<p class='text-danger'>" + res.myAlert + "</p>");
                    }
                });
                xhr.upload.addEventListener('progress',function (event){
                    let percent = parseInt((event.loaded / event.total) * 100);
                    $('#progress_file'+m).attr('class',`progress-bar width-per-${percent}`);
                    $('#progress_file'+m).attr('aria-valuenow',percent);
                    $('#progress_file'+m).text(percent+" %");
                });
                xhr.send(formData);
            }
        });

        $('#box_attaches').on('click','.rm_file_att_icon',function (){
            let thisFile = $(this).parents('.file_attach');
            thisFile.css('backgroundColor','rgba(255,50,50,0.5)').hide(900);
            window.setTimeout(function (){
                thisFile.detach();
            },1500);
        });

        @if($meta['by_attr'] || $meta['is_product'])
        $('.cat_index').on('change',function (){
            let this_cat_id = $(this).val();
            let this_post_id = $(this).val();

            $.ajax({
                url: "{{route('p_posts_get_attrs')}}",
                type: "POST",
                data: {id:this_cat_id,post_id:post_id},
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if(data.res == 10){
                        $('#tbodyAttrs').html(data.attrs);
                    }
                    $('#tbodyAttrs').html(data.attrs);
                    $('#divProps').html(data.props);
                    $('#stock_props').html(data.stock_props);
                    $('select').formSelect();
                }, fail: function () {
                    alert('عملیات با خطا مواجه گردید.');
                }
            });
        });
        window.setTimeout(function (){
            $('.cat_index').trigger('change');
        },1000);

        $('.add_attr').on('click',function (){
            $('#tbodyAttrs').append(`
            <tr>
                <td class="myInput"><input type="text" name="attrTitle[]" class="form-control"></td>
                <td class="myInput"><input type="text" name="attrDesc[]" class="form-control"></td>
                <td>

                    <i class="fa fa-trash-alt rm_attr text-danger" style="cursor: pointer"></i>
                </td>
            </tr>
            `).hide(100).show(600);
        });
        $('#tbodyAttrs').on('click','.rm_attr',function (){
            $(this).parents('tr').hide(600);
            window.setTimeout(function (){
                $(this).parents('tr').detach();
            },1000);
        });

        @endif
        @if($meta['is_product'])
        CKEDITOR.replace('proposal',config);
        CKEDITOR.replace('method_use',config);
        $('[name=discount_target]').on('change',function (){
            discount_target($(this));
        });
        function discount_target(this_select){
            if(this_select.val() == 'users')
                $('.discount_target_users').show();
            else
                $('.discount_target_users').hide();

            if(this_select.val() == 'plan')
                $('.discount_target_plans').show();
            else
                $('.discount_target_plans').hide();
        }
        $('[name=discount_limit]').on('change',function (){
            discount_limit($(this));
        });
        function discount_limit(this_timed){
            if(this_timed.prop("checked")){
                $('.discount_limit').show();
            }else
                $('.discount_limit').hide();

        }

        window.setTimeout(function (){
            $('.money').trigger('keyup');
            $('[name=discount_target]').trigger('change');
            $('[name=discount_limit]').trigger('change');
        },1000);

        $('#btn_discount_set').on('click',function (){
            let thisBtn = $(this);
            if (thisBtn.hasClass('act')) {
                return;
            }
            let btnText = thisBtn.html();
            thisBtn.html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>').addClass('act');

            let form_data = $('#send_ajax_id').serialize();
            $.ajax({
                url: "{{route('p_posts_discount_set')}}",
                type: "POST",
                data: form_data,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    thisBtn.html(btnText).removeClass('act');
                    if(data.res == 10){
                        $('#tbodyAttrs').html(data.attrs);
                        $('.mySuccess1').html(data.message);
                        $('#tbl_discounts').html(data.tbl);
                    }else
                        $('.myAlert1').html(data.message);

                    window.setTimeout(function (){
                        $('.mySuccess1').html('');
                        $('.myAlert1').html('');
                    },2500);

                }, fail: function () {
                    alert('عملیات با خطا مواجه گردید.');
                }
            });
        });
        $('#tbl_discounts').on('click','.deactive_discount',function (e){
            e.preventDefault();
            $('#modal-deactive-discount').modal('show');
            $('#modal-deactive-discount [name=id]').val($(this).data('id'));

        });
        function afterAct(data,thisForm){
            if(data.res === 10){
                $('#tbl_discounts').html(data.tbl);

                window.setTimeout(function () {
                    $('#modal-deactive-discount').modal('hide');
                },3000);
            }
        }


        $('#btn_stock_set').on('click',function (){
            let thisBtn = $(this);
            if (thisBtn.hasClass('act')) {
                return;
            }
            let btnText = thisBtn.html();
            thisBtn.html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>').addClass('act');

            let form_data = $('#send_ajax_id').serialize();
            $.ajax({
                url: "{{route('p_posts_stock_set')}}",
                type: "POST",
                data: form_data,
                cache: false,
                dataType: 'json',
                success: function (data) {
                    thisBtn.html(btnText).removeClass('act');
                    if(data.res == 10){
                        $('#tbody_stock').html(data.tbl);
                        $('.mySuccessStock').html(data.message);

                    }else
                        $('.myAlertStock').html(data.message);

                    window.setTimeout(function (){
                        $('.myAlertStock').html('');
                        $('.mySuccessStock').html('');
                    },2500);

                }, fail: function () {
                    alert('عملیات با خطا مواجه گردید.');
                }
            });
        });

        $('#tbody_stock').on('click','.rm_stock',function (e){
            e.preventDefault();
            $('#modal-rm-stock').modal('show');
            $('#modal-rm-stock [name=id]').val($(this).data('id'));

        });

        function afterStockRm(data,thisForm){
            if(data.res === 10){
                $('#tbody_stock').html(data.tbl);

                window.setTimeout(function () {
                    $('#modal-rm-stock').modal('hide');
                },3000);
            }
        }
        @endif
    </script>
 @stop

 @section('main')

     <style>
         .image_attach,.file_attach {
             position: relative;
             border: 1px solid #555;
             margin-bottom: 10px !important;
             padding: 5px;
             border-radius: 10px;
         }
         .image_attach .img_b,.file_attach .img_b{
             width: 100px;
         }
         .rm_file_att_icon{
             width: 24px;
             position: absolute;
             cursor: pointer;
             left: 5px;
         }
         .file_attach .icon-doc{
            font-size: 50px;
         }

          .select-wrapper{
              border: 1px solid #aaa;
              padding: 5px 7px 0 0;
          }
          .select-wrapper input{
              border: 0 !important;
          }
          .box-add-property{
              width: 400px;
              border: 1px solid #999;
              border-radius: 10px;
              position: absolute;
              right: -254px;
              background: floralwhite;
              z-index: 1;
              box-shadow: 2px 2px 8px;
              top: 113%;
              display: none;
          }
          .box-add-property button{
              padding: 0 7px;
              position: relative;
              top: 29px;
              left: 25px;
          }
          .btn-add-property{
              padding: 3px;
              position: relative;
              top: 7px;
              left: 23px;
              height: 30px;
          }
          #divProps label{
              display: block;
              padding: 15px 0 5px 0;
              border-bottom: 1px dotted;
          }
     </style>


    <section class="content">
        <form method="post" id="send_ajax_id" class="send_ajax" data-after="afterAdd" data-before="beforeAdd" action="{{route($meta['baseRout'].'_addSave',[$meta['type']])}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$post->id}}">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>
                                <li class="breadcrumb-item bcrumb-2">
                                    <a href="{{route($meta['baseRout'],[$meta['type']])}}">{{$meta['nameTotal']}}</a>
                                </li>
                                <li class="breadcrumb-item active">
                                    {{$mode=='add' ? 'افزودن '.$is_translate.$meta['nameSingle'].' جدید' : 'ویرایش '.$is_translate.$meta['nameSingle']}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    {!! $mode=='add'?'<strong>افزودن</strong> '.$is_translate.$meta['nameSingle'].' جدید' : '<strong>ویرایش</strong> '.$is_translate.$meta['nameSingle'] !!}
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-md-5 col-xs-12">
                                        <div class="form-group ">
                                            <div >
                                                <label class="form-label">عنوان {{$meta['nameSingle']}} <sup>*</sup></label>
                                                <input class="form-control" value="{{$post->title}}" name="title" type="text">
                                            </div>
                                            <p class="helperText">
                                                عنوان {{$meta['nameSingle']}} باید حداقل شامل ۳ کارکتر و حداکثر ۲۰۰ کارکتر باشد.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12">
                                        <div class="form-group form-float">
                                            <div >
                                                <label class="form-label ">عنوان فرعی (اسلاگ) </label>
                                                <input class="form-control" value="{{$post->subTitle}}" name="subTitle" type="text">
                                                <p class="helperText">این مقدار در url صفحه قرار میگیرد</p>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-12">
                                        <div>
                                            <div >
                                                <label class="form-label">تاریخ  انتشار </label>
                                                <input id="datepicker1" value="{{$post->date}}" name="date" style="width: 180px;text-align: center" type="text" class="form-control ltr">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row p-t-30 sel">

                                    @if($not_translate&&$meta['status_category'])

                                    <div class="col-md-6 col-xs-12 ">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <select class="groups cat_index cat_select"  name="group_index">
                                                    @foreach($cats as $cat)
                                                        <option {{$cat->id==$post->cat_id?'selected="selected"' : ''}} value="{{$cat->id}}">{{$cat->parentId==0?$cat->name:'_'.$cat->name}}</option>
                                                        @if($cat->childes!=[])
                                                            @foreach($cat->childes as $child)
                                                                <option {{in_array($child->id,$oldGroups)?'selected="selected"' : ''}} value="{{$child->id}}">{{$child->parentId==0?$child->name:'_'.$child->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <label class="form-label">
                                                    گروه‌ اصلی {{$meta['nameSingle']}}
                                                 <sup>*</sup>
                                                </label>
                                            </div>
                                            <div class="col-sm-1" style="position:relative;">
                                                <button class="btn btn-success btn-add-property" type="button">
                                                    <i class="icon-plus"></i>
                                                </button>
                                                <div class="box-add-property">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <label>عنوان گروه جدید : </label>
                                                            <input type="text" class="form-control property" >
                                                            <input type="hidden" value="category" class="form-control prop-group" >
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button
                                                                type="button"
                                                                class="btn btn-success save-prop cat"
                                                                data-type="{{$meta['type']}}"
                                                                data-lang="{{$lang}}"
                                                                data-url="{{route('p_posts_saveProperty')}}"
                                                            >
                                                                ذخیره
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12 ">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <select class="groups cat_select"  name="groups[]" multiple>
                                                    @foreach($cats as $cat)
                                                        <option {{in_array($cat->id,$oldGroups)?'selected="selected"' : ''}} value="{{$cat->id}}">{{$cat->parentId==0?$cat->name:'_'.$cat->name}}</option>
                                                        @if($cat->childes!=[])
                                                            @foreach($cat->childes as $child)
                                                                <option {{in_array($child->id,$oldGroups)?'selected="selected"' : ''}} value="{{$child->id}}">{{$child->parentId==0?$child->name:'_'.$child->name}}</option>
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <label class="form-label">گروه‌(های) {{$meta['nameSingle']}}
                                                    <sup>*</sup>
                                                </label>
                                            </div>
                                            <div class="col-sm-1" style="position:relative;">
                                                <button class="btn btn-success btn-add-property" type="button">
                                                    <i class="icon-plus"></i>
                                                </button>
                                                <div class="box-add-property">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <label>عنوان گروه جدید : </label>
                                                            <input type="text" class="form-control property" >
                                                            <input type="hidden" value="category" class="form-control prop-group" >
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button
                                                                type="button"
                                                                class="btn btn-success save-prop cat"
                                                                data-type="{{$meta['type']}}"
                                                                data-lang="{{$lang}}"
                                                                data-url="{{route('p_posts_saveProperty')}}"
                                                            >
                                                                ذخیره
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @endif
                                </div>

                                <div class="row p-t-30">
                                    @if($not_translate&&$meta['status_tag'])
                                    <div class="col-md-6 col-xs-12 sel">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <div class="form-group ">
                                                    <div >
                                                        <select  name="tags[]" multiple>
                                                            @foreach($tags as $tag)
                                                                <option {{in_array($tag->id,$oldTags)?'selected="selected"' : ''}} value="{{$tag->id}}">{{$tag->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label class="form-label">
                                                            برچسب ها
                                                            <sup>*</sup>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-1" style="position:relative;">
                                                <button class="btn btn-success btn-add-property" type="button">
                                                    <i class="icon-plus"></i>
                                                </button>
                                                <div class="box-add-property">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <label>عنوان برچسب جدید : </label>
                                                            <input type="text" class="form-control property" >
                                                            <input type="hidden" value="tag" class="form-control prop-group" >
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button
                                                                type="button"
                                                                class="btn btn-success save-prop"
                                                                data-type="{{$meta['type']}}"
                                                                data-lang="{{$lang}}"
                                                                data-url="{{route('p_posts_saveProperty')}}"
                                                            >
                                                                ذخیره
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    @endif
                                    @if($not_translate&&$meta['status_brand'])
                                    <div class="col-md-6 col-xs-12 sel">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                <div class="form-group ">
                                                    <div >
                                                        <select  name="brand" >
                                                            @foreach($brands as $brand)
                                                                <option {{$brand->id==$post->brand ? 'selected="selected"' : ''}} value="{{$brand->id}}">{{$brand->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <label class="form-label">
                                                            برند ها
                                                            <sup>*</sup>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-1" style="position:relative;">
                                                <button class="btn btn-success btn-add-property" type="button">
                                                    <i class="icon-plus"></i>
                                                </button>
                                                <div class="box-add-property">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <label>عنوان برند جدید : </label>
                                                            <input type="text" class="form-control property" >
                                                            <input type="hidden" value="brand" class="form-control prop-group" >
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button
                                                                type="button"
                                                                class="btn btn-success save-prop"
                                                                data-type="{{$meta['type']}}"
                                                                data-lang="{{$lang}}"
                                                                data-url="{{route('p_posts_saveProperty')}}"
                                                            >
                                                                ذخیره
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    @endif
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">توضیحات مختصر </label>
                                                <textarea name="minContent" rows="5" class="form-control">{{$post->minContent}}</textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                @if($not_translate && $meta['is_product'])
                    @php($plans = \App\Models\S_product::user_plans())
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card" style="z-index: 10">
                                <div class="header">
                                    <h2>
                                        تنظیمات فروشگاه :
                                    </h2>
                                </div>
                                <div class="body">
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation">
                                            <a href="#home_animation_1" data-toggle="tab" class="active show">تنظیمات محصول</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#prop_animation_1" data-toggle="tab">ویژگیها</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#profile_animation_1" data-toggle="tab">قیمتگذاری</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#messages_animation_1" data-toggle="tab">کمیسیون بازاریاب</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#settings_animation_1" data-toggle="tab">تخفیف</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#settings_animation_2" data-toggle="tab">موجودی و قیمت متغیر</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#method_use" data-toggle="tab">دستورالعمل مصرف</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#proposal" data-toggle="tab">توصیه های مهم</a>
                                        </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated flipInX active show" id="home_animation_1">
                                            <div class="row">
                                                <div class="col-md-3 d-none">
                                                    <label>نوع محصول : </label>
                                                    <select name="product_type" class="form-control">
                                                        <option selected value="physical">فیزیکی</option>
                                                        <option value="download">دانلودی</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>امتیاز : </label>
                                                    <input type="text" class="form-control money" value="{{$post->s_product ? $post->s_product->sell_rate : 0 }}" name="sell_rate">

                                                </div>
                                                <div class="col-md-3">
                                                    <label>هزینه حمل و نقل : </label>
                                                    <input type="text" class="form-control money" value="{{$post->s_product ? $post->s_product->transport : 0 }}" name="transport">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>واحد شمارش : </label>
                                                    <input type="text" class="form-control" value="{{$post->s_product ? $post->s_product->unit : '' }}" name="unit">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>وزن <sub>(گرم)</sub> : </label>
                                                    <input type="text" class="form-control money" value="{{$post->s_product ? $post->s_product->weight : 1 }}" name="weight">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>موجودی : <sup>*</sup></label>
                                                    <input type="text" class="form-control money" value="{{$post->s_product ? $post->s_product->stock : 1 }}" name="stock">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>نمایش موجودی : </label>
                                                    <input type="text" class="form-control money" value="{{$post->s_product ? $post->s_product->stock_show : 10 }}" name="stock_show">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>هشدار موجودی : </label>
                                                    <input type="text" class="form-control money" value="{{$post->s_product ? $post->s_product->stock_report : 5 }}" name="stock_report">
                                                </div>
                                            </div>
                                            <ol>
                                                <li>با هر خرید امتیاز وارد شده در این بخش به مشتری اعطا میگردد.</li>
                                                <li> اگر امتیاز وارد نشود، صفر درنظر گرفته میشود.</li>
                                                <li>اگر هزینه حمل و نقل را وارد نکنید صفر درنظر گرفته میشود.</li>
                                                <li> هزینه حمل و نقل برای اجناسی که هزینه حمل و نقل، ترنسفر، گمرک و ... دارند ثبت میشود و هزینه ارسال کالا بر روی سبد خرید اعمال میشود </li>
                                                <li> وزن کالا برای محاسبه وزن نهایی سبد و مبلغ ارسال مورد استفاده قرار میگیرد و اگر وارد نشود یک گرم درنظر گرفته میشود </li>
                                                <li>اگر موجودی محصول از تعداد وارد شده در فیلد نمایش موجودی کمتر باشد به کاربران سایت نمایش داده میشود</li>
                                                <li>اگر موجودی محصول از تعداد وارد شده در فیلد هشدار موجودی کمتر باشد به مدیر سایت جهت تکمیل موجودی هشدار داده میشود</li>
                                            </ol>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated flipInX" id="prop_animation_1">
                                            <b>ویژگیهای محصول</b>
                                            <div class="" id="divProps">
                                                {!! $props !!}
                                            </div>
                                            <ol>
                                                <li>فقط تیک ویژگیهایی از محصول که موجود است را بزنید</li>
                                                <li>در زمان فروش محصول، ویژگیهایی که تیک آن نخورده باشد به کاربر نمایش داده نمیشود</li>
                                            </ol>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated flipInX" id="profile_animation_1">
                                            <b>قیمتگذاری</b>
                                           <div class="row">
                                               <div class="col-md-3">
                                                   <label>نوع قیمت <sup>*</sup></label>
                                                   <select name="money_type" class="form-control">
                                                       @foreach(\App\Models\S_money::where('id','>',0)->get() as $mon)
                                                           <option {{$post->s_product && $post->s_product->money_id==$mon->id ? 'selected="selected"' : '' }} value="{{$mon->id}}">{{$mon->name}}</option>
                                                       @endforeach
                                                   </select>
                                               </div>
                                               <div class="col-md-3">
                                                   <label>قیمت  اصلی
                                                       <sup>*</sup>
                                                   </label>
                                                   <input type="text" value="{{$post->s_product && $post->s_product->real_price ? $post->s_product->real_price : 0 }}" name="real_price" class="form-control money">
                                               </div>
                                               @for($i=1;$i<4;$i++)
                                                   <div class="col-md-3">
                                                       <label>قیمت مشتریان پلن  {{$plans[$i][0]}}
                                                           {!! $i==1?'<sup>*</sup>':'' !!}
                                                       @php($nnm='price'.$i)
                                                       </label>
                                                       <input type="text" value="{{$post->s_product ? $post->s_product->{$nnm} : 0 }}" name="{{$nnm}}" class="form-control money">
                                                   </div>
                                               @endfor
                                           </div>
                                            <ol>
                                                <li>تمامی بخشهای این محصول که نیاز به قیمت دارد با همین نوع قیمت محاسبه میگردد.</li>
                                                <li>قیمت پلن {{$plans[1][0]}} الزامیست و اگر دو قیمت دیگر وارد نشود با قیمت برنزی پر می‌گردد.</li>
                                            </ol>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated flipInX" id="messages_animation_1">
                                            <b>کمیسیون بازاریاب برحسب درصد :</b>
                                           <div class="row">
                                               <div class="col-md-3">
                                                   <label>نوع محاسبه کمیسیون <sup>*</sup></label>
                                                   <select name="type_commission" class="form-control">
                                                       <option {{$post->s_commission && $post->s_commission->type=='percent' ? 'selected="selected"' : '' }} value="percent">درصدی</option>
                                                       <option {{$post->s_commission && $post->s_commission->type=='fix' ? 'selected="selected"' : '' }} value="fix">ثابت</option>
                                                   </select>
                                               </div>
                                               @for($i=1;$i<4;$i++)
                                                   <div class="col-md-3">
                                                       <label>
                                                           کمیسیون بازاریابان   {{$plans[$i][0]}}
                                                           {!! $i==1?'<sup>*</sup>':'' !!}
                                                           @php($nnm='commission'.$i)
                                                       </label>
                                                       <input type="text" value="{{$post->s_commission ? $post->s_commission->{$nnm} : 0 }}" name="{{$nnm}}" class="form-control money">
                                                   </div>
                                               @endfor
                                           </div>
                                            <ol>
                                                <li>اگر نوع محاسبه کمیسیون درصدی باشد، معادل درصد وارد شده بابت هر فروش به بازاریاب مربوطه کمیسیون تعلق میگیرد.</li>

                                                <li>اگر نوع کمیسیون ثابت انتخاب شود، به ازای هر فروش به بازاریاب مربوطه کمیسیون بر مبنای واحد پول انتخابی تعلق میگیرد</li>
                                                <li>وارد کردن کمیسیون بازریابان {{$plans[1][0]}} الزامیست و اگر دو مورد دیگر وارد نشود با مقدار اول پر میشود</li>
                                                <li>کمیسیون فروشنده بر مبنای قیمت نهایی محصول (پس از اعمال تخفیفات) و بدون در نظر گرفتن هزینه حمل و نقل محاسبه میشود</li>
                                            </ol>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="settings_animation_1">
                                            <b>تخفیف</b>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>نوع تخفیف</label>
                                                    <select class="form-control" name="discount_type">
                                                        <option value="fix">ثابت</option>
                                                        <option value="percent">درصد</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>میزان تخفیف</label>
                                                    <input type="text" name="discount_val" class="money form-control">
                                                </div>

                                                <div class="col-md-2">
                                                    <label>گروه هدف</label>
                                                    <select class="form-control" name="discount_target">
                                                        <option value="all">همه اعضا</option>
                                                        <option value="users">اعضای خاص</option>
                                                        <option value="plan">پلن خاص</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 discount_target_users">
                                                    <label>اعضای هدف</label>
                                                    <select class="form-control discount_users1" name="discount_users[]"  multiple>
                                                        @foreach(\App\Models\User::where('role',2)->get() as $item)
                                                            <option value="{{$item->id}}">{{$item->name}} {{$item->family}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-3 discount_target_plans">
                                                    <label>پلن هدف</label>
                                                    <select class="form-control" name="discount_plan">
                                                        @foreach($plans as $k=>$item)
                                                            <option value="{{$k}}">{{$item[0]}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label style="position: relative;top: 33px;cursor: pointer">
                                                        با تاریخ محدود
                                                        <input type="checkbox" style="opacity: 1;margin-right: 15px;" name="discount_limit">
                                                    </label>
                                                </div>
                                                <div class="col-md-3 discount_limit">
                                                    <label>
                                                        تاریخ شروع
                                                    </label>
                                                    <input id="datepicker2" style="direction: ltr" type="text" name="discount_start" class="form-control">
                                                </div>
                                                <div class="col-md-3 discount_limit">
                                                    <label>
                                                        تاریخ خاتمه
                                                    </label>
                                                    <input id="datepicker3" style="direction: ltr" type="text" name="discount_end" class="form-control">
                                                </div>
                                                <div class="col-md-3">
                                                    <p class="myAlert1 text-danger"></p>
                                                    <p class="mySuccess1 text-success"></p>
                                                    <button id="btn_discount_set" type="button" style="position: relative;top: 22px;" class="btn btn-success">
                                                        ثبت تخفیف
                                                    </button>
                                                </div>

                                            </div>
                                            <ol>
                                                <li>تخفیف ثبت شده، بر روی قیمت نهایی محصول با توجه به پلن کاربر اعمال میشود. و قیمت اصلی ملاک عمل نمیباشد</li>
                                                <li>اگر محصولی دارای چندین تخفیف فعال همتراز باشد، فقط جدیدترین تخفیف از نظر زمان ثبت اعمال میشود</li>
                                                <li>این تخفیف به همین محصول اعمال میشود و  مشتریان میتوانند کدتخفیفی به سبد خرید اعمال نمایند که در آن صورت این تخفیف نیز حفظ میگردد</li>
                                                <li>بالاترین اولویت تخفیف زمانیست که گروه هدف
                                                    <b>اعضای خاص</b>
                                                    باشد و پس از آن
                                                    <b>پلن خاص</b>
                                                    قرار دارد. یعنی اگر محصولی دارای چندین تخفیف فعال باشد، به یک کاربر  تخفیفی اعمال میشود که در لیست اعضای خاص باشد
                                                </li>
                                                <li>اگر تیک
                                                <b>با تاریخ محدود</b>
                                                    زده نشود تخفیف تا زمانیکه غیرفعال نشود و یا تخفیفی همرده یا بالاتر به این محصول اعمال نشود قابل استفاده است
                                                </li>
                                                <li>در مورد تخفیفهای زماندار ملاک پایان زمان تخفیف پرداخت مبلغ آن محصول است. یعنی اگر محصولی در محدوده زمانی تخفیف به سبد خرید اضافه شده باشد و پرداخت آن بعد از تاریخ خاتمه انجام شود، این تخفیف اعمال نخواهد شد.</li>
                                                <li>تخفیف به قیمتهای متغیر هم اعمال میشود</li>
                                            </ol>
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>نوع تخفیف</th>
                                                    <th>میزان تخفیف</th>
                                                    <th>بازه زمانی</th>
                                                    <th>گروه هدف</th>
                                                    <th>وضعیت</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbl_discounts">
                                                {!! $discounts !!}
                                                </tbody>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="settings_animation_2">
                                            <b>موجودی</b>
                                            <div id="stock_props" class="row ">
                                                {!! $stock_props !!}
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>قیمت  اصلی
                                                        <sup>*</sup>
                                                    </label>
                                                    <input type="text" value="0" name="var_real_price" class="form-control money">
                                                </div>
                                                @for($i=1;$i<4;$i++)
                                                    <div class="col-md-3">
                                                        <label>قیمت مشتریان پلن  {{$plans[$i][0]}}
                                                            {!! $i==1?'<sup>*</sup>':'' !!}
                                                            @php($nnm1='var_price'.$i)
                                                        </label>
                                                        <input type="text" value="0" name="{{$nnm1}}" class="form-control money">
                                                    </div>
                                                @endfor
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>موجودی</label>
                                                    <input type="text" name="this_stock" class="form-control money">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>توضیحات</label>
                                                    <input type="text" name="this_stock_desc" class="form-control">
                                                </div>
                                                <div class="col-md-3">
                                                    <p class="myAlertStock text-danger"></p>
                                                    <p class="mySuccessStock text-success"></p>
                                                    <button id="btn_stock_set" style="position: relative;top: 21px;" type="button" class="btn btn-outline-success save_stock">
                                                        ثبت متغیر
                                                    </button>
                                                </div>
                                            </div>
                                            <ol>
                                                <li>مواردی که در این بخش ایجاد می گردد به موجودی اصلی اضافه نمی کند .</li>
                                                <li>با هر فروش محصولی که با این موجودی ها تطابق داشته باشد علاوه بر اینکه از موجودی اصلی محصول کسر می گردد، از تعدادی که در این بخش وارد شده نیز کم می شود</li>
                                                <li>اگر موجودی محصولی با خصوصیاتی که در اینجا تعریف شده، به صفر برسد، مشتریان نمی توانند آنرا به سبد خرید اضافه نمایند </li>
                                                <li>توضیحات وارد شد در این بخش به کاربر در زمان انتخاب محصول نمایش داده می شود</li>
                                            </ol>
                                            <table class="table-striped table">
                                                <thead>
                                                <tr>
                                                    <th>ویژگیها</th>
                                                    <th>موجودی</th>
                                                    <th>قیمت اصلی</th>
                                                    <th>قیمت پلنها</th>
                                                    <th>توضیحات</th>
                                                    <th>#</th>
                                                </tr>
                                                </thead>
                                                <tbody id="tbody_stock">{!! $stock_tbl !!}</tbody>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="method_use">
                                            <div>
                                                <label for="">عنوان بخش دستورالعمل</label>
                                                <input type="text" name="method_use_title" class="form-control" value="{{$options['method_use_title']}}">
                                            </div>
                                            <div>
                                                <label for="">توضیحات بخش دستورالعمل</label>
                                                <textarea name="method_use" id="method_use">{!! $options['method_use'] !!}</textarea>
                                            </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane animated flipInX" id="proposal">
                                            <div>
                                                <label for="">عنوان بخش توصیه‌ها</label>
                                                <input type="text" name="proposal_title" class="form-control" value="{{$options['proposal_title']}}">
                                            </div>
                                            <div>
                                                <label for="">توضیحات بخش توصیه‌ها</label>
                                                <textarea name="proposal" id="proposal">{!! $options['proposal'] !!}</textarea>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if($meta['by_attr'])
                <div class="row clearfix attrs">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>خصوصیات {{$meta['nameSingle']}} : </strong>
                                    <i class="fa fa-plus-circle text-success add_attr" style="cursor: pointer"></i>
                                </h2>
                            </div>
                            <div class="body">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th class="w15">عنوان</th>
                                        <th class="w75">توضیحات</th>
                                        <th class="w25">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyAttrs">
                                    {!! $attrs !!}
                                    <tr>
                                        <td class="myInput"><input type="text" name="attrTitle[]" class="form-control"></td>
                                        <td class="myInput"><input type="text" name="attrDesc[]" class="form-control"></td>
                                        <td>

                                            <i class="fa fa-trash-alt rm_attr text-danger" style="cursor: pointer"></i>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif


                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>توضیحات تکمیلی : <label><sup>*</sup></label></strong>
                                </h2>

                            </div>
                            <div class="body">
                                <textarea name="contentPost" id="ckeditor1" contenteditable="true">{{$post->content}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row clearfix">
                    <div class=" col-md-8 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>تنظیمات سئو</strong>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">عنوان  </label>
                                                <input class="form-control" name="seo_title" value="{{$options['seo_title']}}" type="text">
                                            </div>
                                            <p class="helperText">
                                                این عنوان در متا تگ تایتل قرار میگیرد
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">توضیحات  </label>
                                                <textarea name="seo_desc" rows="5" class="form-control">{{$options['seo_desc']}}</textarea>
                                            </div>
                                            <p class="helperText">
                                                این مقدار در متاتگ دیسکریپشن قرار میگیرد.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group form-float">
                                            <div>
                                                <label class="form-label">کلمات کلیدی  </label>
                                                <textarea name="seo_keyword" rows="5" class="form-control">{{$options['seo_keyword']}}</textarea>
                                            </div>
                                            <p class="helperText">
                                                هر کلمه کلیدی را در یک خط قرار دهید و یا بین کلمات کلیدی کاما بگذارید.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>تصویر</strong> شاخص
                                    <button type="button" id="btn_thumb" class="btn btn-outline-info btn_thumb">انتخاب</button>
                                </h2>
                            </div>
                            <div class="body">
                                <p class="myAlertThumb text-danger"></p>
                                <div id="progress_thumb" style="opacity: 0" class="progress shadow-style">
                                    <div class="progress-bar width-per-40" role="progressbar" aria-valuenow="40"
                                         aria-valuemin="0" aria-valuemax="100">75%</div>
                                </div>
                                <div class="file-field input-field">
                                    <img id="image_thumb" class="getImageFromMedia btn_thumb"
                                         style="margin-bottom: 60px;"
                                         src="{{is_file(ROOT.$post->path.$post->image) ? url($post->path.$post->image) : url('assets/images/add_image_index.png')}}">
                                    <input type="hidden" name="image" value="{{$post->image}}">
                                    <input type="hidden" name="path" value="{{$post->path}}">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">

                    <div class=" col-md-6" style="min-height: 100%;padding-bottom: 30px">
                        <div class="card" style="height: 100%">
                            <div class="header">
                                <h2>
                                    <strong>تصاویر</strong> جانبی
                                    <button type="button" id="btn_images" class="btn btn-outline-info btn_images">انتخاب</button>

                                </h2>
                            </div>
                            <div class="body">
                                <div id="box_images" class="file-field input-field">
                                    @if($attaches != [])
                                        @foreach($attaches as $k=>$attach)
                                            @continue($attach->type != 'image')
                                            <div id="image_attach100{{$k}}" class="image_attach row">
                                                <div class='col-md-8'>
                                                    <label>عنوان فایل : </label>
                                                    <input type="text" class="form-control" value="{{$attach->title}}" name="att_img_desc_{{$attach->id}}">
                                                </div>
                                                <div class='col-md-4'>
                                                    <a href="{{url($attach->path.$attach->file)}}" target="_blank">
                                                        <img class="img_b" src="{{url($attach->path.$attach->file)}}">
                                                    </a>
                                                    <img data-div="att_20" class="rm_file_att_icon" src="{{url('assets/images/remove.png')}}">
                                                    <input type="hidden" name="attach[]" value="{{$attach->id}}">
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class=" col-md-6" style="min-height: 100%;padding-bottom: 30px">
                        <div class="card" style="height: 100%">
                            <div class="header">
                                <h2>
                                    <strong>فایلهای</strong> ضمیمه
                                    <button type="button" id="btn_attaches" class="btn btn-outline-info btn_attaches">انتخاب</button>

                                </h2>
                            </div>
                            <div class="body">
                                <div id="box_attaches" class="file-field input-field">
                                    @if($attaches != [])
                                        @foreach($attaches as $k=>$attach)
                                            @continue($attach->type == 'image')
                                            <div id="file_attach100{{$k}}" class="file_attach row">
                                                <div class='col-md-8'>
                                                    <label>عنوان فایل : </label>
                                                    <input type="text" class="form-control" value="{{$attach->title}}" name="att_img_desc_{{$attach->id}}">
                                                </div>
                                                <div class='col-md-4'>
                                                    <a href="{{url($attach->path.$attach->file)}}" target="_blank">
                                                        <i class="icon-doc"></i>
                                                    </a>
                                                    <img data-div="att_20" class="rm_file_att_icon" src="{{url('assets/images/remove.png')}}">
                                                    <input type="hidden" name="attach[]" value="{{$attach->id}}">
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row"  style="padding: 20px;margin-bottom: 15px">
                                    <div class="col-12">
                                        <p class="myAlert"></p>
                                        <p class="mySuccess"></p>
                                        <button id="savePost"  data-numb="1" type="submit" class="btn btn-success btn-border-radius waves-effect"> ذخیره {{$is_translate}}{{$meta['nameSingle']}}</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
        <form id="form_thumb">
            @csrf
        <input value="{{$meta['type']}}" type="hidden" name="type">
        <input id="file_thumb"  accept="image/*" class="d-none" type="file" name="thumb">
        </form>
        <input id="images" accept="image/*" class="d-none" type="file" name="images[]" multiple>
        <input id="files" class="d-none" type="file" name="files[]" multiple>

        @if($not_translate && $meta['is_product'])
        <div class="modal fade" id="modal-deactive-discount" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">غیرفعال کردن تخفیف</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" class="send_ajax" data-after="afterAct" action="{{route('p_posts_discount_deactive')}}">
                        @csrf
                        <input type="hidden" name="post" value="{{$post->id}}" >
                        <input type="hidden" name="secure" value="{{secure($post->id)}}">
                        <input type="hidden" name="id" >
                    <div class="modal-body">
                        <h5>آیا مطمئنید که قصد غیرفعال کردن این تخفیف را دارید؟</h5>
                        <p>توجه داشته باشید که این عملیات قابل برگشت نمیباشد و در صورت نیاز مجدد به این تخفیف باید مجددا آنرا تعریف نمایید.</p>
                        <p class="myAlert"></p>
                        <p class="mySuccess"></p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                        <button type="submit" class="btn btn-primary">بله غیرفعال شود</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-rm-stock" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">حذف وجودی</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" class="send_ajax" data-after="afterStockRm" action="{{route('p_posts_stock_remove')}}">
                        @csrf
                        <input type="hidden" name="post" value="{{$post->id}}" >
                        <input type="hidden" name="secure" value="{{secure($post->id,'mobile')}}">
                        <input type="hidden" name="id" >
                    <div class="modal-body">
                        <h5>آیا مطمئنید که قصد حذف کردن این موجودی را دارید؟</h5>
                        <p class="myAlert"></p>
                        <p class="mySuccess"></p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                        <button type="submit" class="btn btn-primary">بله حذف شود</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
            @endif
    </section>

 @stop
