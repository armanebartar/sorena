@extends('layouts.admin')


    @section('title')
        {{$meta['title']}} {{$titre}}
    @endsection

    @section('css')
        <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
    @endsection
    @section('js')
        <script>
            $('#{{$meta['idMenuName']}}').addClass('cvcd').parent().addClass('active').parent().parent().addClass('active');
        </script>
        <script src="{{url('assets/js/table.min.js')}}"></script>
        <script src="{{url('assets/js/admin.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/dataTables.buttons.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.flash.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/jszip.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/pdfmake.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/vfs_fonts.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.html5.min.js')}}"></script>
        <script src="{{url('assets/js/bundles/export-tables/buttons.print.min.js')}}"></script>


        {{--        <script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>--}}
        <script>

            $('#tableExport').DataTable({
                dom: 'Bfrtip',
                "displayLength": 25,
                "scrollX": true,
                paginate:false,
                lengthChange:false,
                info:false,
                filter:false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            $('#tbody1').on('click','img',function () {
                $('#modalShowImage').modal('show');
                $('#modalShowImage img').prop('src',$(this).data('source'));
            });
            var tr_id;
            $('#tbody1').on('click','.actions .btn-danger',function () {

                $('#modalRemoveNews').modal('show');
                $("#modalRemoveNews .title").text($(this).parent().parent().data('title'));
                $("#modalRemoveNews .id").val($(this).parent().parent().data('id'));
                tr_id = 'tr_'
                tr_id += $(this).parents('tr').data('id');
            });

            function afterRemove(data,thisForm){
                if(data.res === 10){
                    $('#tbody1 #'+tr_id).detach();

                    window.setTimeout(function () {
                        $('#modalRemoveNews').modal('hide');
                    },3000);
                }
            }
            function afterAct(data,thisForm){
                if(data.res === 10){
                    $('#tbody1 #'+tr_id+' .actShow').html(data.newBtn);

                    window.setTimeout(function () {
                        $('#modalActiveNews').modal('hide');
                    },3000);
                }
            }




            $('#tbody1').on('click','.activated',function () {
                $('#modalActiveNews').modal('show');
                $("#modalActiveNews .title").text($(this).parent().parent().data('title'));
                $("#modalActiveNews .titre").text($(this).data('titre'));
                $("#modalActiveNews .id").val($(this).parent().parent().data('id'));
                $("#modalActiveNews [name=status]").val($(this).data('new'));
                $('#modalActiveNews .mySuccess').hide();
                $('#modalActiveNews .myAlert').hide();

                tr_id = 'tr_'
                tr_id += $(this).parents('tr').data('id');
            });



            $('#tbody1').on('click','.changeLangByGoogle',function (e) {
                e.preventDefault();
                $('#modalTransGoogle').modal('show');
                $("#modalTransGoogle .title").text($(this).parent().parent().data('title'));
                $("#modalTransGoogle .link").prop('href',$(this).data('link'));
            });


        </script>
    @endsection

    @section('main')
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul class="breadcrumb breadcrumb-style ">

                                <li class="breadcrumb-item bcrumb-1">
                                    <a href="{{route('p_dashboard')}}">
                                        <i class="fas fa-home"></i>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        داشبورد
                                    </a>
                                </li>
                                <li class="breadcrumb-item bcrumb-2">
                                    <a href="javascript:void(0);">{{$meta['nameTotal']}}</a>
                                </li>
                                <li class="breadcrumb-item active">{{$meta['title']}} {{$titre}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Exportable Table -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <strong>لیست</strong> {{$meta['nameTotal']}} {{$titre}}</h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li>
                                                <a style="{{$sort=='id'?'color:black;font-weight:800':''}}" href="{{route($meta['baseRout'],[$meta['type'],'id'])}}"> پیش فرض</a>
                                            </li>
                                            <li>
                                                <a style="{{$sort=='myPost'?'color:black;font-weight:800':''}}" href="{{route($meta['baseRout'],[$meta['type'],'myPost'])}}"> {{$meta['nameTotal']}} من</a>
                                            </li>
                                            <li>
                                                <a style="{{$sort=='active'?'color:black;font-weight:800':''}}" href="{{route($meta['baseRout'],[$meta['type'],'active'])}}"> فقط فعال ها</a>
                                            </li>

                                            <li>
                                                <a style="{{$sort=='deactive'?'color:black;font-weight:800':''}}" href="{{route($meta['baseRout'],[$meta['type'],'deactive'])}}"> فقط غیرفعال ها</a>
                                            </li>
                                            <li>
                                                <a style="{{$sort=='deactive'?'color:black;font-weight:800':''}}" href="{{route($meta['baseRout'],[$meta['type'],'draft'])}}"> پیش نویس ها</a>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                    <style>
                                        #tbody1 td{
                                            font-size: 13px;
                                        }
                                        #tbody1 img{
                                            max-width: 100%;
                                            cursor: pointer;
                                        }
                                        #tbody1 .actions a{
                                            width: 18px !important;
                                            height: 18px !important;
                                            font-size: 10px;
                                        }
                                        .actShow span{
                                            font-size: 11px;
                                            padding: 0 14px;
                                        }
                                        .modalShowImage img{
                                            max-width: 100%;
                                        }
                                        .modal-body b{
                                            font-size: 14px;
                                            font-weight: 800;
                                            color: #333;
                                        }
                                        #tbody1 .lang .btn{
                                            padding: 0px 5px !important;
                                            height: 30px !important;
                                        }
                                    </style>
                                    <table style="min-width: 800px" id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                        <thead>
                                        <tr>
                                            <th class="w5">ردیف</th>
                                            <th class="w30">عنوان {{$meta['nameSingle']}}</th>
                                            @if($meta['type'] == 'recall')
                                            <th class="w10">کدخبر</th>
                                            @endif
                                            <th class="w15">گروه</th>
                                            <th class="w10">وضعیت</th>
                                            <th class="w15">تاریخ انتشار</th>
                                            <th class="w10">تصویر شاخص</th>
                                            <th class="w15">عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody1">
                                            {!! $tbl !!}
                                        </tbody>
                                        {{--<tfoot>--}}
                                        {{--<tr>--}}
                                            {{--<th class="w5">ردیف</th>--}}
                                            {{--<th class="w30">عنوان {{$meta['nameSingle']}}</th>--}}
                                            {{--<th class="w15">گروه</th>--}}
                                            {{--<th class="w10">وضعیت</th>--}}
                                            {{--<th class="w15">تاریخ انتشار</th>--}}
                                            {{--<th class="w10">تصویر شاخص</th>--}}
                                            {{--<th class="w15">عملیات</th>--}}
                                        {{--</tr>--}}
                                        {{--</tfoot>--}}
                                    </table>
                                </div>
                                <div class="row" >
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-8">{!! \App\Helpers\Helpers::pagination($page,$total,$maxRow,$targetPage) !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Exportable Table -->
            </div>


            <div class="modal fade" id="modalTransGoogle">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">ترجمه با گوگل ترنسلیت</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h5>آیا مطمئنید که قصد ترجمه این مطلب با گوگل را دارید؟</h5><br>
                            <p><b>عنوان {{$meta['nameSingle']}} : </b> &nbsp;&nbsp;<span class="title"></span></p>
                            <br>
                            <p style="color: red">با توجه به اینکه ترجمه بصورت اتوماتیک انجام میشود، اکیدا توصیه میشود حتما ترجمه دریافتی را بازبینی نمایید. </p>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                            <a href="" class="btn btn-info waves-effect link">بله، ترجمه شود.</a>
                        </div>

                    </div>
                </div>
            </div>



            <div class="modal fade" id="modalActiveNews">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"><span class="titre">فعال</span> کردن {{$meta['nameSingle']}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h5>آیا مطمئنید که قصد <span class="titre">فعال</span> کردن این {{$meta['nameSingle']}} را دارید؟</h5><br>
                            <p><b>عنوان {{$meta['nameSingle']}} : </b> &nbsp;&nbsp;<span class="title"></span></p>
                            <p class="mySuccess"></p>
                            <p class="myAlert"></p>
                        </div>
                        <form class="send_ajax" data-after="afterAct" method="post" action="{{route($meta['baseRout'].'_activated',[$meta['type']])}}">
                            {{csrf_field()}}
                            <input class="id" type="hidden" name="id">
                            <input  type="hidden" name="status" value="publish">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                            <button type="submit" class="btn btn-info waves-effect">بله، <span style="top: 0" class="titre">فعال</span> شود.</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="modalShowImage">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <img src="" />
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" data-dismiss="modal">بستن</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalRemoveNews">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">حذف {{$meta['nameSingle']}}</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <h5>آیا مطمئنید که قصد حذف کردن این {{$meta['nameSingle']}} را دارید؟</h5><br>
                            <p><b>عنوان {{$meta['nameSingle']}} : </b> &nbsp;&nbsp;<span class="title"></span></p>

                        </div>
                        <form method="post" class="send_ajax" data-after="afterRemove" action="{{route($meta['baseRout'].'_remove',[$meta['type']])}}">
                            <p class="mySuccess"></p>
                            <p class="myAlert"></p>
                            @csrf
                            <input class="id" type="hidden" name="id">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                            <button type="submit" class="btn btn-info waves-effect">بله، حذف شود.</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>


        </section>

    @endsection
