 @extends('layouts.admin')

 @section('title','لیست اعضای سایت')

 @section('css')
     <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />

     <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
     <style>
         #tbody1 td{
             font-size: 13px;
         }
         #tbody1 img{
             width: 50px;
             height: 50px;
             cursor: pointer;
             border-radius: 50%;
         }
         #tbody1 .actions a{
             width: 18px !important;
             height: 18px !important;
             font-size: 10px;
         }
         .actShow span{
             font-size: 11px;
             padding: 0 14px;
         }
         .modalShowImage img{
             max-width: 100%;

         }

     </style>
 @endsection
 @section('js')
     <script src="{{url('assets/js/table.min.js')}}"></script>
     <script src="{{url('assets/js/admin.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/dataTables.buttons.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/buttons.flash.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/jszip.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/pdfmake.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/vfs_fonts.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/buttons.html5.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/buttons.print.min.js')}}"></script>
     <script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>
     <script>

         var thisBtn;
         var mode;
         $('#tbody1').on('click','img',function () {
             $('#modalShowImage').modal('show');
             $('#modalShowImage [type=button]').data('offset',$(this).offset().top);
             $('#modalShowImage img').prop('src',$(this).data('source'));
         });



         $('#tbody1').on('click','.activated',function () {
             $('#modalActive').modal('show');
             thisBtn = $(this);
             $('#modalActive .myAlert').hide();
             $('#modalActive .mySuccess').hide();

             $("#modalActive .name").text($(this).parent().parent().data('name'));
             $("#modalActive .id").val($(this).parent().parent().data('id'));
             $("#modalActive .row").val($(this).parent().parent().data('row'));
             $("#modalActive .st").text($(this).data('st'));
             $("#modalActive [name=status]").val($(this).data('status'));
         });



         function afterActivate(data,thisForm){
            if(data.res === 10){
                 thisBtn.parents('tr').replaceWith(data.tr);
                 window.setTimeout(function () {
                     $('#modalActive').modal('hide');
                 },3000);
             }
         }




         $('#addUser').on('click',function () {
             $('#modalAdd').modal('show');
             $('#modalAdd .pass').show();
             mode = 'add';

             $('#modalAdd .myAlert').hide();
             $('#modalAdd .mySuccess').hide();
             $('#modalAdd .name').val('');
             $('#modalAdd .family').val('');
             $('#modalAdd .mobile').val('');
             $('#modalAdd .tel').val('');
             $('#modalAdd .address').val('');
             $('#modalAdd .company').val('');
             $('#modalAdd .postt').val('');


             $('#modalAdd .id').val('0');
             $('#modalAdd .pass input').val('');
             $('#modalAdd .pass p').hide();
             $("#modalAdd h4").text('افزودن کاربر جدید');
         });



         function afterAdd(data,thisForm){
             if(data.res === 10){

                 if(mode == 'add'){
                     $('#tbody1').append(data.tr);
                 }else {
                     thisBtn.parents('tr').replaceWith(data.tr);
                 }

                 window.setTimeout(function () {
                     $('#modalAdd').modal('hide');
                 },3000);
             }
         }


         $('#tbody1').on('click','.btn-info',function (e) {
             e.preventDefault();
             $('#modalAdd').modal('show');

             thisBtn = $(this);
             $('#modalAdd .myAlert').hide();
             $('#modalAdd .mySuccess').hide();
             mode = 'edit';
             $('#modalAdd .name').val($(this).parent().parent().data('fname'));
             $('#modalAdd .row').val($(this).parent().parent().data('row'));
             $('#modalAdd .family').val($(this).parent().parent().data('lname'));
             $('#modalAdd .mobile').val($(this).parent().parent().data('mobile'));
             $('#modalAdd .company').val($(this).parent().parent().data('company'));
             $('#modalAdd .postt').val($(this).parent().parent().data('postt'));
             $('#modalAdd .tel').val($(this).parent().parent().data('tel'));
                $('#modalAdd .id').val($(this).parent().parent().data('id'));
             $('#modalAdd .address').val($(this).parent().parent().data('address'));
             $('#modalAdd .pass input').val('');
             @if( !permission('userPass'))
                $('#modalAdd .pass').hide();
             @endif
             $('#modalAdd .pass p').show();
             $("#modalAdd h4").text('ویرایش کاربر');



         });


         $('#tbody1').on('click','.btn-danger',function (e) {
             e.preventDefault();
             thisBtn = $(this);

             $('#modalRemove').modal('show');
             $('#modalRemove .name').text($(this).parent().parent().data('name'));
             $('#modalRemove .id').val($(this).parent().parent().data('id'));
             thisBtn = $(this);
         });

        function afterRemove(data,thisForm){
            if(data.res == 10){
                thisBtn.parents('tr').detach();
                var idRemove = 1;
                $('.sorting_1').each(function () {
                    $(this).text(idRemove);
                    idRemove++;
                });
                window.setTimeout(function () {
                    $('#modalRemove').modal('hide');
                },3000);
            }
        }


     </script>
 @endsection

 @section('main')

     <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <div class="row">
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul class="breadcrumb breadcrumb-style ">

                             <li class="breadcrumb-item bcrumb-1">
                                 <a href="{{route('p_dashboard')}}">
                                     <i class="fas fa-home"></i>
                                     &nbsp;&nbsp;&nbsp;&nbsp;
                                     داشبورد
                                 </a>
                             </li>
                             <li class="breadcrumb-item active">لیست اعضای سایت</li>
                         </ul>
                     </div>
                 </div>
             </div>
             <!-- Exportable Table -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header">
                             <h2>
                                 <strong>لیست</strong> اعضای سایت
                                 @if(permission('userAdd'))
                                 &nbsp;&nbsp;&nbsp;&nbsp;
                                 <button id="addUser" type="button" class="btn btn-outline-success btn-border-radius">افزودن کاربر جدید</button>
                                     @endif
                             </h2>

                         </div>
                         <div class="body">
                             <div class="table-responsive">

                                 <table id="tableExport" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                     <thead>
                                     <tr>
                                         <th class="w5">ردیف</th>
                                         <th class="w15">نام و نام خانوادگی </th>
                                         <th class="w10">شرکت</th>
                                         <th class="w10">سمت</th>
                                         <th class="w10">موبایل</th>
                                         <th class="w10">تلفن</th>
                                         <th class="w15">وضعیت</th>
                                         <th class="w10"> عضویت</th>
                                         <th class="w10">مشاور </th>
                                         <th class="w15">عملیات</th>
                                     </tr>
                                     </thead>
                                     <tbody id="tbody1">
                                        {!! $tbl !!}
                                     </tbody>

                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Exportable Table -->
         </div>

         <div class="modal fade" id="modalAdd">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                         <h4 class="modal-title">افزودن کاربر جدید</h4>
                     </div>
                     <form method="post" id="formAdd" class="send_ajax" data-after="afterAdd" action="{{route('p_users_addSave')}}">
                         @csrf
                         <input type="hidden" class="id" name="id">
                         <input type="hidden" class="row" name="row">

                     <div class="modal-body">
                         <div class="row">
                             <div class="col-sm-12">
                                 <div class="form-group form-float">
                                     <div class="">
                                         <label class="form-label">نام : <sub>*</sub></label>
                                         <input name="name" class="form-control name" type="text">
                                     </div>
                                 </div>
                             </div>
                             <div class="col-sm-12">
                                 <div class="form-group form-float">
                                     <div class="">
                                         <label class="form-label">نام خانوادگی : <sub>*</sub></label>
                                         <input name="family" class="form-control family" type="text">

                                     </div>
                                 </div>
                             </div>

                             <div class="col-sm-12">
                                 <div class="form-group form-float">
                                     <div class="">
                                         <label class="form-label">موبایل : <sub>*</sub></label>
                                         <input name="mobile" class="form-control mobile" type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-sm-12">
                                 <div class="form-group form-float">
                                     <div class="">
                                         <label class="form-label">شرکت یا سازمان  :</label>
                                         <input name="company" class="form-control company" type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-sm-12">
                                 <div class="form-group form-float">
                                     <div class="">
                                         <label class="form-label">سمت در شرکت  :</label>
                                         <input name="postt" class="form-control postt" type="text">
                                     </div>
                                 </div>
                             </div>

                             <div class="col-sm-12">
                                 <div class="form-group form-float">
                                     <div class="">
                                         <label class="form-label">تلفن  :</label>
                                         <input name="tel" class="form-control tel" type="text">
                                     </div>
                                 </div>
                             </div>
                             <div class="col-sm-12">
                                 <div class="form-group form-float">
                                     <div class="">
                                         <label class="form-label">آدرس : </label>
                                         <textarea name="address" class="form-control address" type="text"></textarea>
                                     </div>
                                 </div>
                             </div>

                             <div class="col-sm-12 pass">
                                 <div class="form-group form-float">
                                     <div class="">
                                         <label class="form-label ">پسورد : </label>
                                         <input name="pass" class="form-control pass" type="text">
                                     </div>
                                     <p><small>اگر این فیلد خالی رها شود پسورد قبلی حفظ می‌گردد.</small></p>
                                 </div>
                             </div>

                         </div>
                         <p class="mySuccess"></p>
                         <p class="myAlert"></p>
                     </div>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                         <button type="submit" id="btnAdd" class="btn btn-info waves-effect">ذخیره کاربر</button>
                     </div>
                     </form>
                 </div>
             </div>
         </div>

         <div class="modal fade" id="modalActive">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h4 class="modal-title"><span class="st"></span> کردن کاربر</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     </div>
                     <div class="modal-body">
                         <h5>آیا مطمئنید که قصد <span class="st"></span> کردن این کاربر را دارید؟</h5>
                         <p><b>نام و نام خانوادگی : </b> &nbsp;&nbsp;<span class="name"></span></p>
                         <p class="mySuccess"></p>
                         <p class="myAlert"></p>
                     </div>
                     <form method="post" class="send_ajax" data-after="afterActivate" id="formAct" action="{{route('p_users_activated')}}">
                         @csrf
                         <input class="id" type="hidden" name="id">
                         <input  type="hidden" name="status" value="1">
                         <input type="hidden" class="row" name="row">
                         <p class="myAlert"></p>
                         <p class="mySuccess"></p>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                         <button type="submit" id="btnAct"  class="btn btn-info waves-effect">
                             بله، <span style="position: relative;top: 0px;" class="st"></span> شود.
                         </button>
                     </div>
                     </form>
                 </div>
             </div>
         </div>



         <div class="modal fade" id="modalShowImage">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h4 class="modal-title"></h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     </div>
                     <div class="modal-body">
                         <img src="" />
                     </div>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                     </div>
                 </div>
             </div>
         </div>


         <div class="modal fade" id="modalRemove">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h4 class="modal-title">حذف کاربر</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     </div>
                     <div class="modal-body">
                         <h5>آیا مطمئنید که قصد حذف کردن این کاربر را دارید؟</h5>
                         <p><b>نام و نام خانوادگی : </b> &nbsp;&nbsp;<span class="name"></span></p>

                     </div>
                     <form method="post" class="send_ajax" data-after="afterRemove" id="formRemove" action="{{route('p_users_remove')}}">
                         @csrf
                         <input class="id" type="hidden" name="id">
                         <input type="hidden" class="row" name="row">
                         <p class="mySuccess"></p>
                         <p class="myAlert"></p>
                         <div class="modal-footer">
                             <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                             <button type="submit" id="btnRemove" class="btn btn-info waves-effect">بله، حذف شود.</button>
                         </div>
                     </form>
                 </div>
             </div>
         </div>




     </section>


 @endsection
