@extends('layouts.admin_auth')
 @section('title','ورود')
 @section('js')
     <script>
         $('#changeCaptcha').on('click',function () {

             $('#changeCaptcha .cap').prop('src',$('#changeCaptcha .cap').prop('src')+1);
         });
     </script>
 @endsection
 @section('main')
     <style>
         .errorsAction li{
             color: red;
             list-style-type: square !important;
             padding-bottom: 5px;
         }

#changeCaptcha{cursor: pointer}

     </style>
     <div class="limiter">
         <div class="container-login100 page-background">
             <div class="wrap-login100">

                 <form class="login100-form " method="post" action="{{route('loginAdmin2')}}">
                     {{csrf_field()}}
					<span class="login100-form-logo">
						<img alt="" src="{{url('assets/images/loading.png')}}">
					</span>
                     <span class="login100-form-title p-b-34 p-t-27">
						ورود
					</span>
                     <?php App\Helpers\Helpers::showErrors(1,'col-xs-12 ') ?>
                     <div class="wrap-input100 validate-input" data-validate="Enter username">
                         <input class="input100" type="text" name="username" placeholder="Username">
                         <i class="material-icons focus-input1001">person</i>
                     </div>
                     <div class="wrap-input100 validate-input" data-validate="Enter password">
                         <input class="input100" type="password" name="pass" placeholder="Password">
                         <i class="material-icons focus-input1001">lock</i>
                     </div>
                     <div class="wrap-input100 validate-input" data-validate="Enter password">
                         <div class="row" style="padding:15px 15px 0">
                             <div class="col-5">
                                 <input class="input100" type="text" name="captcha" placeholder="captcha">
                                 <i class="material-icons focus-input1001">lock</i>
                             </div>
                             <div class="col-7" id="changeCaptcha">
                                 {{--<a href="#" id="changeCaptcha">--}}
                                     <img src="{{url('assets/images/refresh-icon.png')}}">
                                     <img class="cap col-xs-5" src="{{route('captcha',['cpLoginAdmin']).'?i='.rand(1,1000)}}">
                                 {{--</a>--}}
                             </div>
                         </div>
                     </div>
                     <div class="contact100-form-checkbox">
                         <div class="form-check">
                             <label class="form-check-label">
                                 <input class="form-check-input" type="checkbox" value=""> مرا به خاطر بسپار
                                 <span class="form-check-sign">
									<span class="check"></span>
								</span>
                             </label>
                         </div>
                     </div>
                     <div class="container-login100-form-btn">
                         <button type="submit" class="login100-form-btn">
                             ورود
                         </button>
                     </div>
                     <div class="text-center p-t-50">
                         <a class="txt1" href="forgot-password.html">
                             رمز عبور را فراموش کرده اید؟
                         </a>
                     </div>
                 </form>
             </div>
         </div>
     </div>

 @endsection
