@extends('layouts.admin')

@section('title','تنظیم سطوح دسترسی')

@section('css')

    <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />

@endsection
@section('js')
    <script>
        $('#menuItemsStaffList').addClass('cvcd').parent().addClass('active');
    </script>
    <script src="{{url('assets/js/admin.js')}}"></script>
    <script src="{{url('assets/js/form.min.js')}}"></script>
    <script>
        $('i.erroe_dis').on('click',function () {
            var thisI=$(this);
            $('.desc p').each(function () {
                $(this).fadeTo(100,0);
            });
            thisI.parent().parent().find('p').fadeTo(900,0.8);
            window.setTimeout(function () {
                thisI.parent().parent().find('p').fadeTo(300,0);
            },8000);
        });
        $('.desc p').on('click',function () {
            $(this).fadeTo(400,0);
        });
        $('.switchParent').on('change',function () {
            if($(this).prop('checked')){
                $(this).parents('.gr').find('.item').each(function () {
                    $(this).prop('checked','checked');
                });
            }else {
                $(this).parents('.gr').find('.item').each(function () {
                    $(this).prop('checked',false);
                });
            }
        });
        var checkAll;
        var parentGr;
        $('.item').on('change',function () {
             checkAll = true;
            parentGr = $(this).parents('.gr');
            parentGr.find('.item').each(function () {
                if(!$(this).prop('checked')){
                    checkAll = false;
                }
            });
            if(checkAll){
                parentGr.find('.switchParent').prop('checked','checked');
            }else{
                parentGr.find('.switchParent').prop('checked',false);
            }
        });

        $('.gr').each(function () {
            checkAll = true;
            parentGr = $(this);
            parentGr.find('.item').each(function () {
                if(!$(this).prop('checked')){
                    checkAll = false;
                }
            });
            if(checkAll){
                parentGr.find('.switchParent').prop('checked','checked');
            }else{
                parentGr.find('.switchParent').prop('checked',false);
            }

        });
    </script>
@endsection

@section('main')


    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">

                            <li class="breadcrumb-item bcrumb-1">
                                <a href="{{route('p_dashboard')}}">
                                    <i class="fas fa-home"></i>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    داشبورد
                                </a>
                            </li>
                            <li class="breadcrumb-item bcrumb-2">
                                <a href="{{route('p_staff')}}">پرسنل سایت</a>
                            </li>
                            <li class="breadcrumb-item active">سطح دسترسی {{$user->name.' ' .$user->family}}</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <style>
                            .group1 i{
                                color : #b39cdb;
                                cursor: pointer;
                                transition: 1s;
                            }
                            .group1 i:hover{
                                color : #673ab7;
                            }
                            .group1 .desc p{
                                border: 1px solid #673ab7;
                                color: #6235b2;
                                background-color: #b39cdb;
                            }
                            .group1 .desc span{
                                background-color:#b39cdb;
                                border-top:1px solid #673ab7;
                                border-right:1px solid #673ab7;
                            }
                            .group1 h5{
                                color: #673ab7;
                                border-bottom: 2px solid #673ab7;
                            }

                            .group2 i{
                                color : #7fcac3;
                                cursor: pointer;
                                transition: 1s;
                            }
                            .group2 i:hover{
                                color : #009688;
                            }
                            .group2 .desc p{
                                border: 1px solid #009688;
                                color: #009688;
                                background-color: #7fcac3;
                            }
                            .group2 .desc span{
                                background-color:#7fcac3;
                                border-top:1px solid #009688;
                                border-right:1px solid #009688;
                            }
                            .group2 h5{
                                color: #009688;
                                border-bottom: 2px solid #009688;
                            }

                            .group3 i{
                                color : #ffab90;
                                cursor: pointer;
                                transition: 1s;
                            }
                            .group3 i:hover{
                                color : #ff5722;
                            }
                            .group3 .desc p{
                                border: 1px solid #ff5722;
                                color: #ff5722;
                                background-color: #ffab90;
                            }
                            .group3 .desc span{
                                background-color:#ffab90;
                                border-top:1px solid #ff5722;
                                border-right:1px solid #ff5722;
                            }
                            .group3 h5{
                                color: #ff5722;
                                border-bottom: 2px solid #ff5722;
                            }


                            h5{
                                padding-bottom: 9px;
                                margin-bottom: 15px;
                                display: inline-block;
                            }
                            .desc{
                                margin-bottom: 0;
                                vertical-align: top;
                                height: 43px;
                                overflow: visible
                            }
                            .desc p{
                                top: -3px;
                                padding: 10px;
                                border-radius: 5px;
                                position: relative;
                                opacity:0;
                                margin-bottom: 0;
                                max-width: 90% !important;
                            }
                            .desc span{
                                display: inline-block;
                                width: 10px;
                                height: 10px;
                                position: absolute;
                                top: 8px;
                                right: -6px;
                                transform: rotate(45deg);
                            }
                        </style>
                        <form method="post" action="{{route('p_staff_permissionsSave')}}" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$userId}}">
                            <div class="body">
                                <div class="row">
                                    @php
                                    $calsses = ['switch-col-deep-purple','switch-col-teal','switch-col-deep-orange'];
                                    @endphp
                                    @foreach($permissions as $k=>$perr)
                                    <div class="col-12 gr group{{($k%3)+1}}">
                                        <h5>دسترسی‌های بخش {{$perr[0]->gr_name}} :

                                            <div class="switch " style="position: relative;top: 14px;">
                                                <label>
                                                    انتخاب همه
                                                    <input class="switchParent" name="{{'gr'.$perr[0]->group}}" type="checkbox">
                                                    <span class="lever {{$calsses[$k%3]}}"></span>
                                                </label>
                                            </div>
                                        </h5>
                                        @foreach($perr as $per)
                                            <div class="row" style="padding-bottom: 0;margin-bottom: 0">
                                                <div class="col-3">
                                                    {{$per->name}}
                                                </div>
                                                <div class="col-1">
                                                    <div class="switch">
                                                        @php
                                                            $check = '';
                                                        if(isset($userPer[$per->slug])&& $userPer[$per->slug][0])
                                                            $check = 'checked=""';
                                                        @endphp
                                                        <label>
                                                            <input class="item" {{isset($userPer[$per->slug])&&$userPer[$per->slug][0]?'checked=""':''}} name="{{$per->id}}" type="checkbox">
                                                            <span class="lever {{$calsses[$k%3]}}"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-1" style="text-align: left">
                                                    <i class="material-icons erroe_dis">error</i>
                                                </div>
                                                <div class="col-7 desc">

                                                    <p style="display: inline-block">
                                                        <span></span>
                                                        {{$per->description}}</p>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>

                                    @endforeach

                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success">ذخیره دسترسی ها</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>

            </div>


        </div>
        </form>
    </section>

    @endsection
