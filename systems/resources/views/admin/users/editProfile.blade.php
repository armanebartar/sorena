 @extends('layouts.admin')

 @section('title','ویرایش پروفایل')

 @section('css')
     <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
    <style>
        .errorsAction li {
            margin-right: 35px;
            list-style-type: square !important;
            color: red;
        }
        .myAlert{
            display: block;
        }
        .mySuccess{
            display: block;
        }
    </style>
 @endsection
 @section('js')
     <script>
         $('#menuItemHidden').addClass('cvcd').parent().addClass('active');
     </script>
     <script src="{{url('assets/js/app.min.js')}}"></script>
     <script src="{{url('assets/js/admin.js')}}"></script>
    <script>


    </script>
 @endsection

 @section('main')



<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">

                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{route('p_dashboard')}}">
                                <i class="fas fa-home"></i> داشبورد</a>
                        </li>
                        <li class="breadcrumb-item bcrumb-2">
                            <a href="javascript:void(0);">پروفایل</a>
                        </li>
                        <li class="breadcrumb-item active">ویرایش اطلاعات</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">

                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation">
                                <a href="#home_with_icon_title" data-toggle="tab" class="{{$tab!='pass'?'active show':''}}">
                                    ویرایش اطلاعات کاربری
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#profile_with_icon_title" data-toggle="tab" class="{{$tab=='pass'?'active show':''}}">
                                    {{--<i class="material-icons">face</i>--}}
                                    تغییر پسورد
                                </a>
                            </li>

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade {{$tab!='pass'?'active show':''}}" id="home_with_icon_title">
                                {{--<b style="color: darkviolet;margin-bottom: 20px;display: block">ویرایش اطلاعات کاربری</b>--}}

                                <?php App\Helpers\Helpers::showErrors(1,'col-md-8') ?>
                                <form method="post" action="{{route('p_profile_editSave')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}

                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                        <b>نام :‌</b>
                                    </div>
                                    <div class="col-sm-7 col-md-6 col-lg-5">
                                        <input type="text" name="name" value="{{$level?old('name'):auth('admin')->user()->name}}" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                        <b>نام خانوادگی :‌</b>
                                    </div>
                                    <div class="col-sm-7 col-md-6 col-lg-5">
                                        <input type="text" name="family" value="{{$level?old('family'):auth('admin')->user()->family}}" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                        <b>ایمیل :‌</b>
                                    </div>
                                    <div class="col-sm-7 col-md-6 col-lg-5">
                                        <input type="text" name="email" value="{{$level?old('email'):auth('admin')->user()->email}}" class="form-control ltr">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                        <b>موبایل :‌</b>
                                    </div>
                                    <div class="col-sm-7 col-md-6 col-lg-5">
                                        <input type="text" name="mobile" value="{{$level?old('mobile'):auth('admin')->user()->mobile}}" class="form-control ltr">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                        <b>تلفن :‌</b>
                                    </div>
                                    <div class="col-sm-7 col-md-6 col-lg-5">
                                        <input type="text" name="tel" value="{{$level?old('tel'):auth('admin')->user()->tel}}" class="form-control ltr">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                        <b>آدرس :‌</b>
                                    </div>
                                    <div class="col-sm-7 col-md-6 col-lg-5">
                                        <textarea name="address" class="form-control">{{$level?old('address'):auth('admin')->user()->address}}</textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                        <b>تصویر پروفایل  :‌</b>
                                    </div>
                                    <div class="col-sm-7 col-md-6 col-lg-5">
                                        <input type="file" name="image" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                        <b>&nbsp;</b>
                                    </div>
                                    <div class="col-sm-7 col-md-6 col-lg-5">
                                        <input type="submit"  class="btn btn-success" value="ذخیره تغییرات">
                                    </div>
                                </div>
                                </form>

                            </div>


                            <div role="tabpanel" class="tab-pane fade {{$tab=='pass'?'active show':''}}" id="profile_with_icon_title">
                                <p class="myAlert">{{session('err_changePass')}}</p>
                                <p class="mySuccess">{{session('suc_changePass')}}</p>
                                @php(session(['err_changePass'=>'','suc_changePass'=>'']))

                                <form method="post" action="{{route('p_profile_changePass')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}

                                    <div class="row">
                                        <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                            <b>پسورد جدید :‌</b>
                                        </div>
                                        <div class="col-sm-7 col-md-6 col-lg-5">
                                            <input type="password" name="pass" value="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                            <b>تکرار پسورد جدید :‌</b>
                                        </div>
                                        <div class="col-sm-7 col-md-6 col-lg-5">
                                            <input type="password" name="pass2" value="" class="form-control">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 col-md-3 col-lg-2" style="padding-top: 14px;font-size: 16px;">
                                            <b>&nbsp;</b>
                                        </div>
                                        <div class="col-sm-7 col-md-6 col-lg-5">
                                            <input type="submit"  class="btn btn-success" value="ذخیره تغییرات">
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>


    @endsection
