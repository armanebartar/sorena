 @extends('layouts.admin_auth')
  @section('title','ثبت نام')
  @section('js')

  @endsection
 @section('main')
     <div class="limiter">
         <div class="container-login100 page-background">
             <div class="wrap-login100" style="width: 800px">
                 <style>
                     .errorsAction li{
                         color: red;
                         list-style-type: square !important;
                         padding-bottom: 5px;
                     }

                 </style>


                 <form class="login100-form validate-form1" method="post" action="{{route('registerSave')}}">
                     {{csrf_field()}}
					<span class="login100-form-logo">
						<img alt="" src="{{url('assets/images/loading.png')}}">
					</span>
                     <span class="login100-form-title p-b-34 p-t-27">
						ثبت نام
					</span>
                     <?php App\Helpers\Helpers::showErrors(1,'col-xs-12 ') ?>
                     <div class="row">
                         <div class="col-lg-6 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="نام باید حداقل دو کارکتر داشته باشد.">
                                 <input class="input100 checkValid" type="text" name="name" value="{{old('name')}}" placeholder="نام ">
                                 <i class="material-icons focus-input1001">person</i>
                             </div>
                         </div>
                         <div class="col-lg-6 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="نام خانوادگی باید حداقل دو کارکتر داشته باشد">
                                 <input class="input100 checkValid" type="text" name="family" value="{{old('family')}}" placeholder="نام خانوادگی">
                                 <i class="material-icons focus-input1001">person_pin</i>
                             </div>
                         </div>
                         <div class="col-lg-6 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="کد ملی را بدون فاصله و خط تیره و بصورت لاتین وارد کنید.">
                                 <input class="input100 checkValid" type="text" name="melli" value="{{old('melli')}}" placeholder="کدملی ">
                                 <i class="material-icons focus-input1001"> fingerprint</i>
                             </div>
                         </div>

                         <div class="col-lg-6 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="موبایل را با اعداد لاتین و بدین صورت وارد نمایید : 09123456789">
                                 <input class="input100 checkValid" type="text" name="mobile" value="{{old('mobile')}}" placeholder="موبایل ">
                                 <i class="material-icons focus-input1001">phone_iphone</i>
                             </div>
                         </div>
                         <div class="col-lg-6 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="نام خانوادگی را وارد کنید">
                                 <input class="input100" type="text" name="tel" value="{{old('tel')}}" placeholder="تلفن ثابت">
                                 <i class="material-icons focus-input1001"> contact_phone</i>
                             </div>
                         </div>
                         <div class="col-lg-6 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="ایمیل  را وارد کنید">
                                 <input class="input100" type="text" name="email" value="{{old('email')}}" placeholder="ایمیل ">
                                 <i class="material-icons focus-input1001">contact_mail</i>
                             </div>
                         </div>
                         <div class="col-12 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="آدرس را وارد کنید">
                                 <input class="input100" type="text" name="address" value="{{old('address')}}" placeholder="آدرس">
                                 <i class="material-icons focus-input1001">location_on</i>
                             </div>
                         </div>
                         <div class="col-lg-6 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="رمز عبور را وارد کنید">
                                 <input class="input100" type="password" name="pass" placeholder="رمز عبور">
                                 <i class="material-icons focus-input1001">vpn_key</i>
                             </div>
                         </div>
                         <div class="col-lg-6 p-t-20">
                             <div class="wrap-input100 validate-input" data-validate="دوباره رمز عبور را وارد کنید">
                                 <input class="input100" type="password" name="pass2" placeholder="تکرار رمز عبور">
                                 <i class="material-icons focus-input1001">vpn_key</i>
                             </div>
                         </div>
                     </div>

                     <div class="container-login100-form-btn">
                         <button type="submit" class="login100-form-btn">
                             ثبت نام
                         </button>
                     </div>
                 </form>
                     <div class="text-center p-t-50">
                         <a class="txt1" href="sign-in.html">
                             شما در حال حاضر عضو هستید؟
                         </a>
                     </div>
                 </form>
             </div>
         </div>
     </div>

 @endsection