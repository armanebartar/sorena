@extends('layouts.admin')

@section('title','دستورات آرتیسان')

@section('css')

    <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />

@endsection
@section('js')

    <script src="{{url('assets/js/admin.js')}}"></script>
    <script src="{{url('assets/js/form.min.js')}}"></script>
    <script>

    </script>
@endsection

@section('main')


    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">

                            <li class="breadcrumb-item bcrumb-1">
                                <a href="{{route('p_articles')}}">
                                    <i class="fas fa-home"></i>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    داشبورد
                                </a>
                            </li>
                            <li class="breadcrumb-item bcrumb-2">
                                <a href="javascript:void(0);">پنل توسعه دهنده</a>
                            </li>
                            <li class="breadcrumb-item active">دستورات آرتیسان</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <style>
                                .btnCommand a{
                                    margin-bottom: 5px;
                                    padding: 2px 5px;
                                }
                            </style>
                                <div class="row btnCommand">
                                    <div class="col-12">
                                        <a href="{{route('p_dev_artisanCommand',['command'=>'cacheClear'])}}" class="btn">پاک کردن کش</a>
                                        <a href="{{route('p_dev_artisanCommand',['command'=>'viewClear'])}}" class="btn">پاک کردن ویوها</a>
                                        <a href="{{route('p_dev_artisanCommand',['command'=>'routeCache'])}}" class="btn">کش کردن روتها</a>
                                        <a href="{{route('p_dev_artisanCommand',['command'=>'routeClear'])}}" class="btn">پاک کردن روتهای کش شده</a>
                                        <a href="{{route('p_dev_artisanCommand',['command'=>'configClear'])}}" class="btn">پاک کردن تنظیمات</a>
                                        <a href="{{route('p_dev_artisanCommand',['command'=>'configCatch'])}}" class="btn">کش کردن تنظیمات</a>
                                        <a href="{{route('p_dev_artisanCommand',['command'=>'optimize'])}}" class="btn">حالت بهینه</a>
                                    </div>

                                </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <form method="post" action="{{route('p_dev_saveController')}}">
                                {{csrf_field()}}
                            <div class="row">
                                <div class="col-3">
                                    ساخت کنترلر :
                                </div>
                                <div class="col-7">
                                    <input type="text" name="controller" value="{{old('controller')}}" class="form-control ltr" placeholder="panel/Test or panel/test or panel/TestController or panel\Test">
                                </div>
                                <div class="col-2">
                                    <input type="submit" class="btn" value="ذخیره">
                                </div>
                            </div>
                            </form>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <form method="post" action="{{route('p_dev_saveModel')}}">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-3">
                                        ساخت مدل :
                                    </div>
                                    <div class="col-7">
                                        <input type="text" name="model" value="{{old('model')}}" class="form-control ltr" placeholder="test or Test">
                                    </div>
                                    <div class="col-2">
                                        <input type="submit" class="btn" value="ذخیره">
                                    </div>
                                </div>
                            </form>
                        </div>
                        </form>
                    </div>
                </div>
            </div>








        </div>
    </section>

    @endsection