
<script>
    var myTr;
    $(function() {
        pageNotFactor = false;
        $("#datepicker1").persianDatepicker();
        $("#datepicker2").persianDatepicker();
    });
    $.ajaxSetup({headers:{'X-CSRF-TOKEN':$('meta[name=csrf-token]').attr('content')}});
    $('#tableExport1')
        .on( 'page.dt',   function () {
            window.setTimeout(function () {
                sep();
            },100)
        } ).DataTable({
        dom: 'Bfrtip',
        "displayLength":25,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    var _c = true;
    var _n = 0;
    var _p = 0;
    window.setInterval(function () {
        if(_p < 3) {
            if (_c === false) {
                _n++;
                if (_n > 6) {
                    _n = 0;
                    _c = true;
                }
            }
            if (_c && $('#clock-box clock').text() === '04:59:00') {
                _p++;
                $('#row_IRO1LSMD0001 #cell_SymbolFa span').click();
                $('#send_order_txtCount').val('10');
                $('#send_order_txtPrice').val('3000');
                $('#send_order_btnSendOrder').click();

            }
        }
    },50);

    var _c = true;var _n = 0;window.setInterval(function () {if(_c === false){_n++;if(_n>6){_n=0;_c = true;}}if(_c && $('#clock-box clock').text() === '04:59:00'){$('#row_IRO1LSMD0001 #cell_SymbolFa span').click();$('#send_order_txtCount').val('10');$('#send_order_txtPrice').val('3000');$('#send_order_btnSendOrder').click();}},50);

    $('.submitAjax').on('click',function (e) {
        e.preventDefault();
        var thisBtn = $(this);
        var thisBtnText = thisBtn.html();
        var thisForm = thisBtn.parents('form');
        thisForm.find('.myAlert').hide().html('');
        thisForm.find('.mySuccess').hide().html('');
        if(thisBtn.hasClass('act'))
            return ;
        thisBtn.addClass('act').html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');
        var thisFormData = thisForm.serialize();
        var thisUrl = thisForm.prop('action');
        $.ajax({
            url: thisUrl,
            type: "POST",
            data: thisFormData,
//                    async:false,
            dataType: 'json',
            success: function (data) {

                thisBtn.removeClass('act').html(thisBtnText);
                thisForm.find('.myAlert').show().text(data.myAlert);
                thisForm.find('.mySuccess').show().text(data.mySuccess);

                if(data.res === 10){
                    myTr.replaceWith(data.tr);
                    sep();
                    window.setTimeout(function () {
                        thisForm.find('.myAlert').hide().html('');
                        thisForm.find('.mySuccess').hide().html('');
                        thisForm.parents('.modal').modal('hide');
                    },2500)
                }


            }

        });
    });


    $('#modalShowFactor .detailsBtn').on('click',function () {
        $(' #modalShowFactor .details').toggle(500);
    });






    @if(permission('factorTransport'))
    $('#tbody1').on('click','.actions .goToPeyk',function (e) {
        e.preventDefault();
        $('#modalAddBijak').modal('show');
        $('#modalAddBijak [name=id]').val($(this).parent().data('id'));
        myTr = $(this).parents('tr');
    });
    @endif




    @if(permission('factorsCheckout'))
    $('#tbody1').on('click','.cash',function (e) {
        e.preventDefault();
        $('#modalCash').modal('show');
        thisTr = $(this).parents('tr');
        $('#modalCash .name').text(thisTr.data('name'));
        $('#modalCash .myAlert').hide().text('');
        $('#modalCash .mySuccess').hide().text('');
        $('#modalCash .company').text(thisTr.data('company'));
        $('#modalCash .factor').text(thisTr.data('id'));
        $('#modalCash [name=factorId]').val(thisTr.data('id'));
    });

    $('#tbody1').on('click','.credit',function (e) {
        e.preventDefault();
        $('#modalCredit').modal('show');
        thisTr = $(this).parents('tr');
        $('#modalCredit .myAlert').hise().text('');
        $('#modalCredit .mySuccess').hise().text('');
        $('#modalCredit .name').text(thisTr.data('name'));
        $('#modalCredit .company').text(thisTr.data('company'));
        $('#modalCredit .factor').text(thisTr.data('id'));
        $('#modalCredit [name=factorId]').val(thisTr.data('id'));
    });

    $('.btnChangCheckout').on('click',function (e) {
        e.preventDefault();

        var thisBtn = $(this);
        var thisBtnText = thisBtn.html();
        if(thisBtn.hasClass('act'))
            return ;
        thisBtn.addClass('act').html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');
        var thisFormData = thisBtn.parents('form').serialize();
        $.ajax({
            url: thisBtn.parents('form').prop('action'),
            type: "POST",
            data: thisFormData,
//                    async:false,
            dataType: 'json',
            success: function (data) {

                thisBtn.removeClass('act').html(thisBtnText);
                thisBtn.parents('form').find('.myAlert').show().text(data.myAlert);
                thisBtn.parents('form').find('.mySuccess').show().text(data.mySuccess);
                // $(' #modalUnpaid #home .myAlert').show().text(data.myAlert);
                // $(' #modalUnpaid #home .mySuccess').show().text(data.mySuccess);

                if(data.res === 10){

                    thisTr.find('.checkout button').addClass(thisBtn.data('add')).removeClass(thisBtn.data('rm')).text(thisBtn.data('text'));
                    window.setTimeout(function () {
                        thisBtn.parents('.modal').modal('hide');
                    },2500);
                }
                sep();
            }
        });

    });


    @endif

        @if(permission('factorDelivery'))
    $('#tbody1').on('click','.delivery',function (e) {
        e.preventDefault();
        $('#modalDelivery').modal('show');
        thisTr = $(this).parents('tr');
        $('#modalDelivery .name').text(thisTr.data('name'));
        $('#modalDelivery .myAlert').hide().text('');
        $('#modalDelivery .mySuccess').hide().text('');
        $('#modalDelivery .company').text(thisTr.data('company'));
        $('#modalDelivery .factor').text(thisTr.data('id'));
        $('#modalDelivery [name=factorId]').val(thisTr.data('id'));
    });


    $('#modalDelivery [type=submit]').on('click',function (e) {
        e.preventDefault();

        var thisBtn = $(this);
        if(thisBtn.hasClass('act'))
            return ;
        thisBtn.addClass('act').html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');
        var thisFormData = thisBtn.parents('form').serialize();
        $.ajax({
            url: thisBtn.parents('form').prop('action'),
            type: "POST",
            data: thisFormData,
//                    async:false,
            dataType: 'json',
            success: function (data) {

                thisBtn.removeClass('act').html('بله، تحویل شد.');
                thisBtn.parents('form').find('.myAlert').show().text(data.myAlert);
                thisBtn.parents('form').find('.mySuccess').show().text(data.mySuccess);
                // $(' #modalUnpaid #home .myAlert').show().text(data.myAlert);
                // $(' #modalUnpaid #home .mySuccess').show().text(data.mySuccess);

                if(data.res === 10){

                    thisTr.find('.delivery').hide();
                    thisTr.find('.status_user').html('تحویل مشتری');
                    window.setTimeout(function () {
                        thisBtn.parents('.modal').modal('hide');
                    },2500);
                }
                sep();
            }
        });

    });
        @endif


        @if(permission('factorPackage'))
    $('#tbody1').on('click','.actions .btn-primary',function (e) {
        e.preventDefault();
       $('#modalPackaging').modal('show');
            $('#modalPackaging .fact').text($(this).parent().data('id'));
            $('#modalPackaging [name=id]').val($(this).parent().data('id'));
            $('#modalPackaging [name=sms]').prop('checked','checked');
            myTr = $(this).parents('tr');

    });

    @endif


    {{--$('#tbody1').on('click','.btnShowMsg',function () {--}}

    {{--    var thisBtn = $(this);--}}
    {{--    factor_id = $(this).data('id');--}}

    {{--    $.ajax({--}}
    {{--        url:"{{route('p_factors_getMessages')}}",--}}
    {{--        type: 'post',--}}
    {{--        dataType: "json",--}}
    {{--        data: "id="+ factor_id,--}}
    {{--        success: function (data) {--}}
    {{--            if(data.message === 1){--}}
    {{--                alert('عملیات با خطا مواجه گردید.');--}}
    {{--                return;--}}
    {{--            }--}}
    {{--            thisBtn.find('span').text('0');--}}
    {{--            $('#btnModalShowMsg').trigger('click');--}}
    {{--            $(".msg_history").html(data.message);--}}
    {{--        }, error: function() {--}}
    {{--            swal({--}}
    {{--                position: 'top-end',--}}
    {{--                type: 'error',--}}
    {{--                title: 'عملیات با خطا مواجه گردید ',--}}
    {{--                showConfirmButton: true,--}}
    {{--                timer: 4500,--}}
    {{--                animation: true--}}
    {{--            });--}}
    {{--        }--}}
    {{--    });--}}
    {{--});--}}


    {{--$('.write_msg').on('keypress',function (e) {--}}
    {{--    if(e.charCode == 13){--}}
    {{--        $('.msg_send_btn').trigger('click');--}}
    {{--    }--}}
    {{--});--}}

    {{--$('.msg_send_btn').on('click', function () {--}}

    {{--    var msg =$('.write_msg').val();--}}
    {{--    $('.template p.msgUser').text(msg);--}}
    {{--    var html = $('.template').html();--}}
    {{--    $('.msg_history').append(html);--}}
    {{--    $('.write_msg').val('');--}}
    {{--    $('.notMessageFound').hide();--}}

    {{--    $.ajax({--}}
    {{--        url:"{{route('p_factors_sendMessage')}}",--}}
    {{--        type: 'post',--}}
    {{--        dataType: "json",--}}
    {{--        data: {--}}
    {{--            "id": factor_id,--}}
    {{--            "msg": msg,--}}
    {{--        },--}}
    {{--        success: function (data) {--}}
    {{--            if(data.messages !== 'no'){--}}
    {{--                $(".msg_history").html(data.messages);--}}
    {{--            }--}}
    {{--        }, error: function() {--}}
    {{--            swal({--}}
    {{--                position: 'top-end',--}}
    {{--                type: 'error',--}}
    {{--                title: 'عملیات با خطا مواجه گردید ',--}}
    {{--                showConfirmButton: true,--}}
    {{--                timer: 4500,--}}
    {{--                animation: true--}}
    {{--            });--}}
    {{--        }--}}

    {{--    });--}}

    {{--});--}}

    {{--window.setInterval(function () {--}}
    {{--    $.ajax({--}}
    {{--        url:"{{route('p_factors_getNewMessages')}}",--}}
    {{--        type: 'post',--}}
    {{--        dataType: "json",--}}
    {{--        success: function (data) {--}}
    {{--            data.map(function (num) {--}}
    {{--                $('.cl_'+num).find('.btnShowMsg span').text('1');--}}
    {{--            })--}}
    {{--        }--}}

    {{--    });--}}
    {{--},5000);--}}






    $.ajax({
        url: "{{route('p_factors_getProducts')}}",
        type: "POST",
        dataType: 'json',
        success: function (data) {

            $("#findProduct").autocomplete({
                source: data,
                select: function( event, ui ) {
                    var nameP = ui.item.name + '\n' + ui.item.subName + '\n' + ui.item.irc;
                    var priceP = ui.item.price;
                    if($("#findProduct").data('row') != 0) {
                        $('.cl_' + $("#findProduct").data('row')).find('textarea').val(nameP);
                        $('.cl_' + $("#findProduct").data('row')).find('.priceField input').val(priceP);
                    }else{
                        var rowww = 1;
                        $('#tbody3 tr').each(function () {
                            rowww++;
                        });
                        var rand = Math.floor(Math.random() * 10000);
                        var n_r = "<tr class=\"cl_"+rand+"\">";
                        n_r += "<td>"+rowww+"</td>";
                        n_r += "<td class=\"prodNameEdit\">";
                        n_r += "<textarea class=\"form-control inp\" name=\"new_name[]\">"+nameP+"</textarea>";
                        n_r += "<i data-id=\""+rand+"\" class=\"fas fa-search\"></i></td>";
                        n_r += "<td class=\"priceField\"><input type=\"text\" class=\"form-control inp justNumber\" name=\"new_price[]\" value=\""+priceP+"\"></td>";
                        n_r += "<td class=\"numbField\"><input class=\"form-control inp\" type=\"text\" name=\"new_numb[]\" value=\"1\"></td>";
                        n_r += "<td class=\"tot number\">0</td>";
                        n_r += "</tr>";

                        $('#tbody3').append(n_r);
                    }

                    setPrice();
                    $('.closeModalFind').trigger('click');

                }
            });
        }
    });

    $('#btnAddNewProduct').on('click',function () {
        $('#btnModalFind').trigger('click');
        $("#findProduct").data('row','0').val('');
    });

    $('#tbody3').on('click','.prodNameEdit i',function () {
        $('#btnModalFind').trigger('click');
        $("#findProduct").data('row',$(this).data('id')).val('');
    });



    $('#tbody1').on('click','.bijak',function (e) {
        e.preventDefault();
        $('#modalShowBijak').modal('show');
        var bijakId = $(this).data('id');
        $.ajax({
            url: "{{route('p_factors_getBijak')}}",
            data : {'id' : + bijakId},
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#modalShowBijak .modal-body').html(data.bijak);

            }
        });

    });


    $('#tbody1').on('click','tr',function () {
        var thisTr = $(this);
        $('#tbody1 tr').each(function () {
            $(this).removeClass('actt');
        });
        thisTr.addClass('actt');

    });

    $('#formFilter [type=submit]').on('click',function () {
        $(this).html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');

    });


    $('#tbody1').on('click','.paid',function () {
        $('#modalPaid').modal('show');
        var thisTr = $(this).parents('tr');
        $('#modalPaid .name').text(thisTr.data('name'));
        $('#modalPaid .mobile').text(thisTr.data('mobile'));
        $('#modalPaid .company').text(thisTr.data('company'));
        $('#modalPaid .numb').text(thisTr.data('id'));
        $('#modalPaid .date').text(thisTr.data('date'));
        $('#modalPaid .amount').text(thisTr.data('amount'));

        var transId = $(this).parents('tr').data('trans');
        $.ajax({
            url: "{{route('p_factors_getTrans')}}",
            type: "POST",
            data : {id:transId},
            dataType: 'json',
            success: function (data) {
                $('#modalPaid .code span').text(data.code);
                $('#modalPaid .price span').text(data.price);
                $('#modalPaid .tracking span').text(data.tracking);
                $('#modalPaid .date1 span').text(data.date1);
                $('#modalPaid .date2 span').text(data.date2);
                $('#modalPaid .method span').text(data.method);
                $('#modalPaid .stat span').text(data.stat);

                sep();
            }
        });


    });

    var thisTr
    $('#tbody1').on('click','.unpaid',function () {
        $('#modalUnpaid').modal('show');
        thisTr = $(this).parents('tr');
        $('#modalUnpaid #menu1 [name=user]').val(thisTr.data('user'));
        $('#modalUnpaid #menu1 [name=factor]').val(thisTr.data('id'));
        $('#modalUnpaid #home [name=factor]').val(thisTr.data('id'));
        $('#modalUnpaid #home [name=trans]').val('0');
        $('#modalUnpaid .name').text(thisTr.data('name'));
        $('#modalUnpaid .mobile').text(thisTr.data('mobile'));
        $('#modalUnpaid .company').text(thisTr.data('company'));
        $('#modalUnpaid .numb').text(thisTr.data('id'));
        $('#modalUnpaid .date').text(thisTr.data('date'));
        $('#modalUnpaid .amount').text(thisTr.data('amount'));

        $(' #modalUnpaid #menu1 .myAlert').hide().text('');
        $(' #modalUnpaid #menu1 .mySuccess').hide().text('');
        $(' #modalUnpaid #menu1 [name=price]').val('');
        $(' #modalUnpaid #menu1 [name=time_pay]').val('');
        $(' #modalUnpaid #menu1 [name=code]').val('');
        $(' #modalUnpaid #menu1 [name=details]').val('');

        $(' #modalUnpaid #home .myAlert').hide().text('');
        $(' #modalUnpaid #home .mySuccess').hide().text('');

        var userId = $(this).parents('tr').data('user');
        $.ajax({
            url: "{{route('p_factors_getUserTrans')}}",
            type: "POST",
            data : {id:userId},
            dataType: 'json',
            success: function (data) {
                $('#modalUnpaid #home tbody').html(data.tbody);
                sep();
            }
        });
    });

    $('#modalUnpaid #home').on('click','tr',function () {
        var thisTrr = $(this);
        $('#modalUnpaid #home [name=trans]').val($(this).data('id'));
        $('#modalUnpaid #home tr').each(function () {
            $(this).removeClass('acttt');
        });
        thisTrr.addClass('acttt');
    });

    $('.btnAddTransToFactor').on('click',function (e) {
        e.preventDefault();
        var thisBtn = $(this);
        if(thisBtn.hasClass('act'))
            return ;
        thisBtn.addClass('act').html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');
        var thisFormData = thisBtn.parents('form').serialize();
        $.ajax({
            url: thisBtn.parents('form').prop('action'),
            type: "POST",
            data: thisFormData,
//                    async:false,
            dataType: 'json',
            success: function (data) {

                thisBtn.removeClass('act').html('ذخیره ');
                thisBtn.parents('form').find('.myAlert').show().text(data.myAlert);
                thisBtn.parents('form').find('.mySuccess').show().text(data.mySuccess);
                // $(' #modalUnpaid #home .myAlert').show().text(data.myAlert);
                // $(' #modalUnpaid #home .mySuccess').show().text(data.mySuccess);

                if(data.res === 10){
                    thisTr.data('trans',data.transId);
                    thisTr.find('.btn-pay button').addClass('btn-success paid').removeClass('btn-danger unpaid').text('پرداخت شده');
                    window.setTimeout(function () {
                        $('#modalUnpaid').modal('hide');
                    },2500);
                }
                sep();
            }
        });
    });



    window.setInterval(function () {
            $.ajax({
                url: "{{route('p_factors_checkOrder')}}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if (data.res === 10) {
                        var x = document.getElementById("audio1");
                        x.play();
                        $('.headTitle').html(res.filter);
                        $('#tbody1').html(res.tbl);
                    }
                }
            });
    },60000);

    sep();
</script>
