<a id="btn_modalShowFactor" data-toggle="modal" href="#modalShowFactor"></a>


@if(permission('factorTransport'))
<a id="btnModalAddBijak" data-toggle="modal" href="#modalAddBijak"></a>
<div class="modal fade" id="modalAddBijak">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">تحویل باربری</h4>
            </div>
            <form method="post" action="{{route('p_factors_saveBijak')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" class="id" name="id" value="">

                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <div>
                                <label>نام باربری :  </label>
                                <input name="name" class="form-control" value="" type="text">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="typeBijak">
                                <label>نوع حمل و نقل :  </label>
                                <select class="form-control" name="type">
                                    <option>باربری</option>
                                    <option>پیک</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div>
                                <label>زمان تحویل به باربری :  </label>
                                <input name="time" class="form-control" value="" type="text">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div>
                                <label>تلفن :  </label>
                                <input name="tel" class="form-control" value="" type="text">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div>
                                <label>موبایل :  </label>
                                <input name="mobile" class="form-control" value="" type="text">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="typeBijak">
                                <label>تصویر بارنامه :  </label>
                                <input name="image" class="form-control" value="" type="file">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="typeBijak">
                                <label>توضیحات :  </label>
                                <textarea name="desc" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="typeBijak">
                                <label>برای کاربر پیامک اطلاع رسانی ارسال شود ؟
                                    <input style="opacity: 1" type="checkbox" name="sendSms">
                                </label>

                            </div>
                        </div>
                    </div>
                    <p class="myAlert"></p>
                    <p class="mySuccess"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                    <button type="submit" class="btn btn-info waves-effect submitAjax">ذخیره بیجک</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endif



<a id="btnModalShowBijak" data-toggle="modal" href="#modalShowBijak"></a>
<div class="modal fade" id="modalShowBijak">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">مشاهده بیجک</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">اوکی</button>

            </div>

        </div>
    </div>
</div>

@if(permission('factorsCheckout'))
<div class="modal fade" id="modalCash">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">تغییر نحوه پرداخت</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="{{route('p_factors_change_checkout')}}">
                @csrf
                <input type="hidden" name="factorId">
                <input type="hidden" name="newCheckout" value="credit">

                <div class="modal-body">
                    <h5>آیا مطمئنید که قصد دارید نحوه پرداخت این فاکتور بصورت اعتباری شود ؟ </h5>
                    <p><b>نام مشتری : </b><span class="name"></span></p>
                    <p><b>سازمان  : </b><span class="company"></span></p>
                    <p><b>کد فاکتور : </b><span class="factor"></span></p>

                    <p class="myAlert"></p>
                    <p class="mySuccess"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                    <button type="submit" data-add="btn-warning credit" data-rm="btn-success cash" data-text="اعتباری" class="btn btn-info waves-effect btnChangCheckout">بله، اعتباری شود.</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade " id="modalCredit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">تغییر نحوه پرداخت</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="{{route('p_factors_change_checkout')}}">
                @csrf
                <input type="hidden" name="factorId">
                <input type="hidden" name="newCheckout" value="cash">

                <div class="modal-body">
                    <h5>آیا مطمئنید که قصد دارید نحوه پرداخت این فاکتور بصورت نقدی شود ؟ </h5>
                    <p><b>نام مشتری : </b><span class="name"></span></p>
                    <p><b>سازمان  : </b><span class="company"></span></p>
                    <p><b>کد فاکتور : </b><span class="factor"></span></p>
                    <p class="myAlert"></p>
                    <p class="mySuccess"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                    <button type="submit" data-add="btn-success cash" data-rm="btn-warning credit" data-text="نقدی" class="btn btn-info waves-effect btnChangCheckout">بله، نقدی شود.</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endif

@if(permission('factorDelivery'))
<div class="modal fade " id="modalDelivery">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">تحویل به مشتری</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="{{route('p_factors_delivery')}}">
                @csrf
                <input type="hidden" name="factorId">

                <div class="modal-body">
                    <h5>آیا مطمئنید که این سفارش تحویل مشتری گردیده است ؟</h5>
                    <p><b>نام مشتری : </b><span class="name"></span></p>
                    <p><b>سازمان  : </b><span class="company"></span></p>
                    <p><b>کد فاکتور : </b><span class="factor"></span></p>
                    <p class="myAlert"></p>
                    <p class="mySuccess"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>
                    <button type="submit" class="btn btn-info waves-effect">بله، تحویل شد.</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endif

@if(permission('factorPackage'))

    <div class="modal fade" id="modalPackaging">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">تایید پرداخت</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <h5>آیا مطمئنید که این فاکتور از نظر پرداخت مشکلی ندارد؟</h5>
                    <p>
                        فاکتور شماره :
                        <span class="fact"></span>
                    </p>
                    <ul>
                        <li>اگر پرداخت این فاکتور نقدی لحاظ شده، از بانک و حساب مربوطه استعلام بگیرید. </li>
                        <li>اگر پرداخت بصورت اعتباری لحاظ شده مطمئن شوید که اشتباهی رخ نداده باشد.</li>
                    </ul>

                </div>
                <form method="post" action="{{route('p_factors_packaging',[0])}}">
                    {{csrf_field()}}
                    <input class="id" type="hidden" name="id">
                    <p>
                        <label style="padding-right: 50px">
                            <input style="opacity: 1;position: relative;" type="checkbox" name="sms">
                            پیامک اطلاع رسانی به کاربر ارسال شود؟
                        </label>
                    </p>
                    <div class="modal-footer">
                        <p class="myAlert"></p>
                        <p class="mySuccess"></p>
                        <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                        <button type="submit" class="btn btn-info waves-effect submitAjax">بله، مطمئنم.</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif



<div class="modal fade" id="modalPaid">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">اطلاعات تراکنش</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row" style="border:1px solid #bbb;padding: 5px 0;">
                    <div class="col-lg-4 name" style="padding-bottom: 8px;"></div>
                    <div class="col-lg-4 mobile"></div>
                    <div class="col-lg-4 company"></div>
                    <div class="col-lg-4">
                        <b>کد فاکتور :</b>
                        <span class="numb"></span>
                    </div>
                    <div class="col-lg-4 date"></div>
                    <div class="col-lg-4 amount number"></div>
                </div>

                <div class="transData" style="border:1px solid #ccc;margin-top: 5px;padding: 5px 10px;">
                    <p class="code">
                        <b>کد تراکنش : </b>
                        <span></span>
                    </p>
                    <p class="price">
                        <b>مبلغ : </b>
                        <span class="number"></span>
                    </p>
                    <p class="tracking">
                        <b>کد پیگیری : </b>
                        <span></span>
                    </p>
                    <p class="date1">
                        <b>تاریخ ثبت : </b>
                        <span></span>
                    </p>
                    <p class="date2">
                        <b>تاریخ تراکنش : </b>
                        <span></span>
                    </p>
                    <p class="method">
                        <b>روش پرداخت : </b>
                        <span></span>
                    </p>
                    <p class="stat">
                        <b>وضعیت : </b>
                        <span></span>
                    </p>

                </div>
            </div>



        </div>
    </div>
</div>

<div class="modal fade" id="modalUnpaid">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">انتخاب تراکنش</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row" style="border:1px solid #bbb;padding: 5px 0;">
                    <div class="col-lg-4 name" style="padding-bottom: 8px;"></div>
                    <div class="col-lg-4 mobile"></div>
                    <div class="col-lg-4 company"></div>
                    <div class="col-lg-4">
                        <b>کد فاکتور :</b>
                        <span class="numb"></span>
                    </div>
                    <div class="col-lg-4 date"></div>
                    <div class="col-lg-4 amount number"></div>
                </div>

                <div class="transData" style="border:1px solid #ccc;margin-top: 5px;padding: 5px 10px;">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home">انتخاب تراکنش </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">ثبت تراکنش جدید</a>
                        </li>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="home" class="container tab-pane active" style="padding: 0;width: 100%;">
                            <p>بر روی ردیف مربوط به این فاکتور کلیک نمایید و سپس دکمه ذخیره را بزنید.</p>
                            <table>
                                <thead>
                                <tr style="background-color: #bbb;color: #fff;">
                                    <td>کد</td>
                                    <td>مبلغ</td>
                                    <td>تاریخ ثبت</td>
                                    <td>فاکتور</td>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                            <form method="post" action="{{route('p_factors_addTransToFactor')}}">
                                <p class="myAlert"></p>
                                <p class="mySuccess"></p>
                                @csrf
                                <input type="hidden" name="factor">
                                <input type="hidden" name="trans">
                                <button id="btnAddTransToFactor" class="btn btn-success btnAddTransToFactor">ذخیره</button>

                            </form>
                        </div>
                        <div id="menu1" class="container tab-pane fade" style="padding: 0;width: 100%;">
                            <form method="post" action="{{route('p_factors_addTrans_andToFactor')}}">
                                @csrf
                                <input type="hidden" name="factor">
                                <input type="hidden" name="user">
                                <div>
                                    <label>روش پرداخت :</label>
                                    <select class="form-control" name="method_way">
                                        <option>روش پرداخت</option>

                                    </select>
                                </div>
                                <div>
                                    <label style="position: relative;top: 18px">مبلغ : </label>
                                    <input type="text" value="" name="price" class="form-control">
                                </div>
                                <div>
                                    <label style="position: relative;top: 18px">زمان پرداخت :</label>
                                    <input type="text" value="" name="time_pay" class="form-control">
                                </div>
                                <div>
                                    <label style="position: relative;top: 18px">کد پیگیری :</label>
                                    <input type="text" value="" name="code" class="form-control">
                                </div>
                                <div>
                                    <label>توضیحات : </label>
                                    <textarea class="form-control" name="details">{{old('details')}}</textarea>
                                </div>
                                <p class="myAlert"></p>
                                <p class="mySuccess"></p>
                                <button id="btnAddTransAndToFactor" class="btn btn-success btnAddTransToFactor">ذخیره</button>

                            </form>
                        </div>

                    </div>


                </div>
            </div>



        </div>
    </div>
</div>
