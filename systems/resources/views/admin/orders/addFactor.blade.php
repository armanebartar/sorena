@extends('layouts.admin')

 @section('title')
    افزودن فاکتور جدید
    @stop

 @section('css')

{{--    <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />--}}
{{--    <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />--}}
{{--    <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />--}}
    <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />

 @stop
 @section('js')
     <script>
         $('#list_orders').addClass('active').parent().addClass('active').parent().parent().addClass('active');
     </script>
     <script src="{{url('assets/js/table.min.js')}}"></script>
     <script src="{{url('assets/js/admin.js?i=3')}}"></script>
     <link href="{{url('')}}/dist/js/select2-develop/dist/css/select2.min.css" rel="stylesheet" />
     <script src="{{url('')}}/dist/js/select2-develop/dist/js/select2.full.min.js"></script>

     <script>
         // $(select).select();
         $(document).ready(function() {
             // $('.my_select2').select2();
         });
         function ToRial(str) {
             str = str.toString().replace(/\,/g, '');
             var objRegex = new RegExp('(-?[0-9]+)([0-9]{3})');
             while (objRegex.test(str)) {
                 str = str.replace(objRegex, '$1,$2');
             }
             return str;
         }


         $.ajaxSetup({headers:{'X-CSRF-TOKEN':$('meta[name=csrf-token]').attr('content')}});

         $('#select_user').on('change',function (){
             $('#customerDetails').hide();
             let user_id =  $('#select_user').val();
             if(user_id === '0')
                 return;
             $.ajax({
                 url: "{{route('p_factors_get_user_data')}}",
                 type: "POST",
                 data: {user_id},
//                    async:false,
                 dataType: 'json',
                 success: function (data) {
                     if(data.res == 10){
                         $('#customerDetails').html(data.tbl);
                         $('#customerDetails').show();
                         return;
                     }
                     alert('کاربر یافت نشد.');
                 }
             });
         });

         $('#select_user').trigger('change');

         $('#add_row').on('click',function (){
             let row = 1;
             $('#tbody2 tr').each(function (){
                 row++;
             });
             $('#tbody2').append(`
             <tr>
                <td class="radif">${row}</td>
                <td class="product"><input type="text" name="name[]" class="form-control"></td>
                <td class="unit"><input type="text" name="unit[]" class="form-control"></td>
                <td class="price"><input type="text" name="price[]" class="form-control nnn money"></td>
                <td class="numb"><input type="text" name="numb[]" class="form-control nnn money"></td>
                <td class="disc"><input type="text" name="disc[]" class="form-control nnn money"></td>
                <td class="total"></td>
            </tr>
             `);
         });
         $('#add_row').trigger('click');
         let tblContainer= $('#tblContainer');
         tblContainer.on('change keyup','.nnn',calc)
         tblContainer.on('keypress keyup','.money',function (event) {
             $(this).css({'direction':'ltr','text-align':'left'});
             $(this).val($(this).val().replace(/[^0-9\.]/g,''));
             if ((event.which != 46 || $(this).val().indexOf('.') != -1) && !((event.which > 47 && event.which < 58) || (event.which > 1775 && event.which < 1786))) {
                 event.preventDefault();
             }
             $(this).val(ToRial($(this).val()));
         });
         function calc(){
             let allTotal = 0;
             $('#tbody2 tr').each(function (){
                 let tot = ($(this).find('.price input').val().replace(/[^0-9\.]/g,'') - $(this).find('.disc input').val().replace(/[^0-9\.]/g,'')) * $(this).find('.numb input').val().replace(/[^0-9\.]/g,'') ;
                 $(this).find('.total').html(ToRial(tot));
                 allTotal += tot;
             });
             $('.totalFactor').html(ToRial(allTotal));
             $('.finalFactor').html(ToRial(allTotal - $('[name=discount]').val().replace(/[^0-9\.]/g,'')));

         }

         $('#form_add_factor').on('submit',function (e){
             e.preventDefault();
             let thisBtn = $('#submitForm');
             $('.myAlert').hide();
             $('.mySuccess').hide();
             if(thisBtn.hasClass('act'))
                 return;
             thisBtn.addClass('act').html('در حال بررسی ...');
             let this_data =  $('#form_add_factor').serialize();
             $.ajax({
                 url: $('#form_add_factor').prop('action'),
                 type: "POST",
                 data: this_data,
//                    async:false,
                 dataType: 'json',
                 success: function (data) {
                     thisBtn.removeClass('act').html('ذخیره سفارش');
                     $('.myAlert').html(data.myAlert).show();
                     $('.mySuccess').html(data.mySuccess).show();
                 }
             });
         });
     </script>
 @stop

 @section('main')



    <section class="content">
        <form id="form_add_factor" method="post" action="{{route('p_factors_addSave')}}" enctype="multipart/form-data">
            {{csrf_field()}}

        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">

                            <li class="breadcrumb-item bcrumb-1">
                                <a href="{{route('p_dashboard')}}">
                                    <i class="fas fa-home"></i>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    داشبورد
                                </a>
                            </li>
                            <li class="breadcrumb-item bcrumb-2">
                                <a href="javascript:void(0);">فاکتورها</a>
                            </li>
                            <li class="breadcrumb-item active">افزودن فاکتور</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>افزودن</strong> فاکتور جدید</h2>
                            <style>
                                .rmRole{font-size: 16px;display: inline-block;border: 1px solid red;padding: 7px 2px 5px 2px;border-radius: 50%;cursor: pointer;}
                                .errorsAction li{
                                    border: 1px solid red;
                                    padding: 15px;
                                    margin: 10px;
                                    color: red;
                                }

                                .warningsAction li{
                                    border: 1px solid orangered;
                                    padding: 15px;
                                    margin: 10px;
                                    color: orangered;
                                }
                                .tot,.priceField{
                                    padding-right:20px ;
                                    padding-left: 20px;
                                }
                                .numbField{
                                    padding-right:30px ;
                                    padding-left: 30px;
                                }
                                .fl{
                                    border: 1px solid #ccc !important;
                                    border-radius: 5px !important;
                                    padding: 0 10px 0 10px !important;
                                    text-align: left !important;
                                    max-width: 130px;
                                }
                                .priceField input{
                                    width: 100px !important;
                                }
                                .numbField input{
                                    width: 81px !important;
                                }
                                .duplicated{
                                    background-color: #eab8c4;
                                    transition: 0.8s;
                                }
                                #tbody2 textarea{
                                    min-height: 80px;
                                    padding: 5px;
                                    border: 1px solid #ccc;
                                    border-radius: 5px;
                                }

                                .removeProduct{
                                    position: absolute;
                                    left: 22px;
                                    top: 40px;
                                    color: red;
                                    font-size: 20px;
                                    cursor: pointer;
                                }
                                #tbody2 td{
                                    padding: 15px 10px;
                                }
                                #tbody2 td.radif{
                                    text-align: center;
                                }

                            </style>

                            <?php App\Helpers\Helpers::showErrors() ?>
                            <?php App\Helpers\Helpers::showWarnings() ?>

                        </div>


                        <div class="body">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group ">
                                        <div class="form-line">
                                            <label style="position: relative;top: 0px;" class="form-label">نام مشتری <sup>*</sup></label>
                                            <select  class="form-control my_select2 " style="width: 100% !important;z-index: 100;opacity: 1;left: 0;" id="select_user" name="user_id">
                                                <option value="0">انتخاب کاربر</option>
                                                @foreach(\App\Models\User::where('role',2)->get() as $user)
                                                    <option value="{{$user->id}}">{{$user->name}} {{$user->family}} {{$user->company}} {{$user->mobile}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group ">
                                        <div class="form-line">
                                            <label style="position: relative;top: 0px;" class="form-label">سربرگ فاکتور <sup>*</sup></label>
                                            <select class="form-control  " style="width: 100% !important;z-index: 100;opacity: 1;left: 0;" id="user_target" name="comp">
                                                <option value="1">تجهیزات پزشکی فردوسی</option>
                                                <option value="2">تجهیزگران پویای امید سلامت</option>

                                            </select>
                                        </div>

                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-12" id="customerDetails" style="display: none;">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <button type="button" id="add_row" class="btn btn-info ">افزودن ردیف</button>
                            <div class="row"  id="tblContainer">
                                <table id="tbl1" style="border:1px solid gray;margin-top: 5px">
                                    <thead>
                                    <tr style="background: gray;color: white">
                                        <th style="width: 5%;padding: 5px">ردیف</th>
                                        <th style="width: 40%;padding: 5px">نام محصول</th>
                                        <th style="width: 7%;padding: 5px">واحد شمارش</th>
                                        <th style="width: 15%;padding: 5px">قیمت واحد</th>
                                        <th style="width: 8%;padding: 5px;text-align: center">تعداد</th>
                                        <th style="width: 10%;padding: 5px;text-align: center">تخفیف واحد</th>
                                        <th style="width: 15%;padding: 5px">قیمت کل</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbody2" data-row="1">

                                    </tbody>
                                    <tfoot>
                                    <tr style="border-bottom:0">
                                        <td colspan="3" style="padding: 5px;font-size: 14px">&nbsp;</td>
                                        <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">جمع کل : </td>
                                        <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number totalFactor"></b></td>
                                    </tr>
                                    <tr style="border-bottom:0">
                                        <td colspan="3" style="padding: 5px;font-size: 14px">&nbsp;</td>
                                        <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">تخفیف : </td>
                                        <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><input type="text" class="rial fl nnn money" name="discount" value="0"> </td>
                                    </tr>
                                    <tr style="border-bottom:0">
                                        <td colspan="3" style="padding: 5px;font-size: 14px">&nbsp;</td>
                                        <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">قابل پرداخت : </td>
                                        <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number finalFactor"></b></td>
                                    </tr>
                                    <tr style="border-bottom:0">
                                        <td colspan="4" style="padding: 5px;font-size: 14px">&nbsp;</td>

                                        <td colspan="3" style="padding:30px 5px;border-bottom:1px dashed #e0e0e0;">
                                            <p class="myAlert text-danger"></p>
                                            <p class="mySuccess text-success"></p>
                                            <button id="submitForm" data-numb="0" type="submit" class="btn btn-success btn-border-radius waves-effect">ذخیره فاکتور</button>
                                        </td>
                                    </tr>

                                    </tfoot>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        </form>
    </section>

    @endsection
