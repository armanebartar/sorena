 @extends('layouts.admin')

 @section('title','لیست فاکتورها')

 @section('css')
     <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
     <link type="text/css" href="{{url('assets/js/bundles/jQuery-Datepicker-Plugin-For-Persian-Date-persianDatepicker/css/persianDatepicker-default.css')}}" rel="stylesheet" />

     <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
     <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.theme.css')}}" rel="stylesheet" />
     <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery_ui_rtl.css')}}" rel="stylesheet" />
     <script src="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/external/jquery/jquery.js')}}"></script>
     <script src="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
{{--     <link href="{{url('assets/pages/factors.css')}}" rel="stylesheet" />--}}




 @endsection
 @section('js')
     <script>
         $('#list_orders').addClass('active').parent().addClass('active').parent().parent().addClass('active');
     </script>
     <script src="{{url('assets/js/table.min.js')}}"></script>
     <script src="{{url('assets/js/admin.js?i=3')}}"></script>
     <script src="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/dataTables.buttons.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/buttons.flash.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/jszip.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/pdfmake.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/vfs_fonts.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/buttons.html5.min.js')}}"></script>
     <script src="{{url('assets/js/bundles/export-tables/buttons.print.min.js')}}"></script>
     <script src="{{url('assets/js/pages/tables/jquery-datatable.js')}}"></script>
     <script src="{{url('assets/js/bundles/jQuery-Datepicker-Plugin-For-Persian-Date-persianDatepicker/js/persianDatepicker.min.js')}}"></script>

     <script>
         $.ajaxSetup({headers:{'X-CSRF-TOKEN':$('meta[name=csrf-token]').attr('content')}});
         $('#tableExport1').on( 'page.dt',   function () {
                 window.setTimeout(function () {
                     sep();
                 },100)
             } ).DataTable({
             dom: 'Bfrtip',
             "displayLength":25,
             buttons: [
                 'copy', 'csv', 'excel', 'pdf', 'print'
             ]
         });
         function sep() {
             $(".number").each(function () {
                 $(this).text(ToRial($(this).text()));
             })
         }
         function ToRial(str) {
             str = str.toString().replace(/\,/g, '');
             var objRegex = new RegExp('(-?[0-9]+)([0-9]{3})');
             while (objRegex.test(str)) {
                 str = str.replace(objRegex, '$1,$2');
             }
             return str;
         }

         function printDiv(divID) {
             let order_id = 10;
             let divContents = document.getElementById(divID).innerHTML;
             let a = window.open('', '', 'height=500, width=500');
             a.document.write('<html><head><title>order_'+order_id+'</title></head>');
             a.document.write('<body > <small>سفارش شماره '+order_id+' <small><br>');
             a.document.write(divContents);
             a.document.write('</body></html>');
             a.document.close();
             a.print();
             // a.close();
         }

         function setPrice() {
             let total = 0;

             $('#tbody3 tr').each(function () {
                 let pr = $(this).find('.priceField input').val().replace(',','').replace(',','').replace(',','').replace(',','').replace(',','');
                 let nu = $(this).find('.numbField input').val().replace(',','').replace(',','').replace(',','').replace(',','').replace(',','');
                 let disc = $(this).find('.discField input').val().replace(',','').replace(',','').replace(',','').replace(',','').replace(',','') * nu;
                 let tot = (pr * nu) - disc;

                 $(this).find('.tot').text(tot);
                 total += tot;
             });
             $('#modalEditFactor .totalFactor').text(total);
             let discount = Number($('#modalEditFactor [name=discount]').val().replace(',','').replace(',','').replace(',','').replace(',','').replace(',',''));
             discount = discount?discount:0;
             let transs = Number($('#modalEditFactor [name=transs]').val().replace(',','').replace(',','').replace(',','').replace(',','').replace(',',''));
             transs = transs?transs:0;
             let finalFactor = total - discount + transs;
             $('#modalEditFactor .finalFactor').text(finalFactor);
             sep();
         }

         var factor_id;
         $('#tbody1').on('click','.actions .showed',function (e) {
             e.preventDefault();
             var factId = $(this).data('id');
             $.ajax({
                 url: "{{route('p_factors_getFactorDetails')}}",
                 type: "POST",
                 data: 'id='+factId,
//                    async:false,
                 dataType: 'json',
                 success: function (data) {
                    if(data.res == 10){
                         $('#modalShowFactor').modal('show');
                         $(' #tbody2').html(data.tbl);
                         $(' #modalShowFactor .date').html(data.date);
                         $(' #modalShowFactor .name').html(data.name);
                         $(' #modalShowFactor .mobile').html(data.mobile);
                         $(' #modalShowFactor .company').html(data.company);
                         $(' #modalShowFactor .numbId').html(data.numbId);
                         $(' #modalShowFactor .address').html(data.address);
                         $(' #modalShowFactor .total').html(data.total_all);
                         $(' #modalShowFactor .desc_factor').html(data.desc);
                         $(' #modalShowFactor .final').html(data.final);
                         $(' #modalShowFactor .discount').html(data.discount_all);
                         sep();
                         return;
                     }
                     alert('فاکتور یافت نشد.');
                 }
             });
         });

         @if(permission('factorsSmsNotStock'))
         $('#tbody1').on('click','.await',function (e) {
             e.preventDefault();
             $('#modalWait').modal('show');
             $('#modalWait [name=name]').val($(this).data('name'));
             $('#modalWait [name=mobile]').val($(this).data('mobile'));
             $('#modalWait [name=numb]').val($(this).parent().data('id'));
             $('#modalWait .numb').text($(this).parent().data('id'));
             $('#modalWait .myAlert').text('');
             $('#modalWait .mySuccess').text('');
             $('#modalWait [name=product]').val('');
         });

         $('#modalWait .btn-success').on('click',function (e) {
             e.preventDefault();
             if($(this).hasClass('act'))
                 return;
             $(this).addClass('act').html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');
             var thisBtn = $(this);
             var dataForm = $(this).parents('form').serialize();
             $.ajax({
                 url: $(this).parents('form').prop('action'),
                 type: "POST",
                 data : dataForm,
                 dataType: 'json',
                 success: function (data) {

                     thisBtn.removeClass('act').html('ارسال پیامک');
                     $('#modalWait .mySuccess').html(data.mySuccess).show();
                     $('#modalWait .myAlert').html(data.myAlert).show();

                     if(data.res === 10){
                         window.setTimeout(function () {
                             $('#modalWait').modal('hide');
                         },2500);
                     }


                 }
             });
         });
         @endif


         var thisBtnEdit;
         @if(permission('factorConfirmFirst') || permission('factorConfirmLast') || permission('factorTemporary'))
         $('#tbody1').on('click','.actions .btn-info',function (e) {
             e.preventDefault();
             myTr = $(this).parents('tr');
             $(' #modalEditFactor .myAlert').hide().text('');
             $(' #modalEditFactor .mySuccess').hide().text('');
             thisBtnEdit = $(this);
             let factId = $(this).parent().data('id');
             $.ajax({
                 url: "{{route('p_factors_getFactorDetailsEdit')}}",
                 type: "POST",
                 data: 'id='+factId,
//                    async:false,
                 dataType: 'json',
                 success: function (data) {
                     if(data.res == 10){
                         $('.factorConfirmLast').hide();
                         $('.factorConfirmFirst').hide();
                         $('.factorTemporary').hide();
                         if(data.factStatus < 1) {
                             $('.factorConfirmFirst').show();
                             $('.factorTemporary').show();
                         }
                         else{
                             $('.factorConfirmLast').show();
                             @if(permission('factorConfirmLast'))
                             $('.factorTemporary').show();
                             @endif
                         }


                         $('#modalEditFactor').modal('show');
                         $(' #tbody3').html(data.tbl);
                         $(' #modalEditFactor .name').html(data.name);
                         $(' #modalEditFactor .date').html(data.date);
                         $(' #modalEditFactor .mobile').html(data.mobile);
                         $(' #modalEditFactor .company').html(data.company);
                         $(' #modalEditFactor .total').html(data.total);
                         $(' #modalEditFactor .address1').html(data.address);
                         $(' #modalEditFactor .final').html(data.final);
                         $(' #modalEditFactor .details').hide();

                         $(' #modalEditFactor .desc').html(data.desc);

                         $(' #modalEditFactor .final').html(data.final);
                         $(' #modalEditFactor [name=discount]').val(data.discount);
                         $(' #modalEditFactor [name=transs]').val(data.transs);
                         $(' #modalEditFactor .numbId').html(data.numbId);
                         $('#modalEditFactor [name=factor_id]').val(data.factId);
                         $('#modalEditFactor [name=address]').val(data.address);
                         $('#modalEditFactor [name=desc]').val("");
                         setPrice();
                         sep();
                         return;
                     }
                     alert('فاکتور یافت نشد.');
                 }
             });
         });

         $('#modalEditFactor').on('change keyup keypress blur','.inp',function () {
             setPrice();
         });

         $('#modalEditFactor .btn_order_change_order').on('click',function (e) {
             e.preventDefault();
             let thisBtn = $(this);
             if(thisBtn.hasClass('act'))
                 return ;
             var txt_btn = thisBtn.html();
             thisBtn.addClass('act').html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');
             let thisFormData = thisBtn.parents('form').serialize();
             $.ajax({
                 url: "{{route('p_factors_editSave')}}",
                 type: "POST",
                 data: thisFormData,
//                    async:false,
                 dataType: 'json',
                 success: function (data) {

                     thisBtn.removeClass('act').html(txt_btn);
                     $(' #modalEditFactor .myAlert').show().text(data.myAlert);
                     $(' #modalEditFactor .mySuccess').show().text(data.mySuccess);


                 }
             });

         });

         @endif

         @if(permission('factorTemporary'))
         $('#modalEditFactor #interim').on('click',function (e) {
             e.preventDefault();
             let thisBtn = $(this);
             if(thisBtn.hasClass('act'))
                 return ;
             thisBtn.addClass('act').html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');
             let thisFormData = thisBtn.parents('form').serialize();
             $.ajax({
                 url: "{{route('p_factors_editSaveInterim')}}",
                 type: "POST",
                 data: thisFormData,
//                    async:false,
                 dataType: 'json',
                 success: function (data) {

                     thisBtn.removeClass('act').html('ذخیره موقت');
                     $(' #modalEditFactor .myAlert').show().text(data.myAlert);
                     $(' #modalEditFactor .mySuccess').show().text(data.mySuccess);

                 }
             });

         });

         @endif

         @if(permission('factorsRemove'))
         $('#tbody1').on('click','.actions .btn-danger',function (e) {
             e.preventDefault();
             $('#modalRemove').modal('show');
             $('#modalRemove [name=id]').val($(this).parent().data('id'));
             myTr = $(this).parents('tr');
         });
         $('#btn_remove_order').on('click',function (e) {
             e.preventDefault();
             let thisBtn = $(this);
             if(thisBtn.hasClass('act'))
                 return ;
             let btn_txt = thisBtn.html();
             thisBtn.addClass('act').html('<i class="fa fa-spinner fa-spin" style="font-size:24px;"></i>');
             let thisFormData = thisBtn.parents('form').serialize();
             $.ajax({
                 url: "{{route('p_factors_remove')}}",
                 type: "POST",
                 data: thisFormData,
//                    async:false,
                 dataType: 'json',
                 success: function (data) {
                     thisBtn.removeClass('act').html(btn_txt);
                     $(' #modalRemove .myAlert').show().text(data.myAlert);
                     $(' #modalRemove .mySuccess').show().text(data.mySuccess);
                     if(data.res == 10){
                         myTr.detach();
                         window.setTimeout(function (){
                             $('#modalRemove').modal('hide');
                         },2000);
                     }
                 }
             });

         });
         @endif

         $('#btn_add_row_edit').on('click',function (){
             let rowww = 1;
             $('#tbody3 tr').each(function (){
                 rowww++;
             });
             $('#tbody3').append(`
             <tr class="cl_24">
                 <td class="td_row">${rowww}<i class="far fa-times-circle remove_row_edited"></i></td>
                 <td class="prodNameEdit">
                 <textarea class="form-control inp" name="namee[]"></textarea>
                 <i data-id="0" class="fas fa-search"></i>
                 </td>
                 <td class="unitField" style="padding: 5px"><input type="text" class="form-control" style="max-width: 77%" name="unitt[]" value=""></td>

                 <td class="priceField" style="padding: 5px"><input type="text" class="form-control inp justNumber" name="pricee[]" value=""></td>
                 <td class="numbField" style="padding: 5px;text-align: center"><input class="form-control inp" type="text" name="numbb[]" value="1"></td>
                 <td class="discField" style="padding: 5px;text-align: center"><input class="form-control inp" type="text" name="discc[]" value="0"></td>
                 <td class="tot number" style="padding: 5px">0</td>
             </tr>
             `);
         });
         $('#tbody3 ').on('click','.td_row i.remove_row_edited',function (){
             $(this).parents('tr').detach();
         });

     </script>

{{--     @include('admin.orders.factors_js')--}}

 @endsection

 @section('main')

     <style>
         #modalEditFactor .inp{
             max-width: 75%;
             text-align: left;
         }
         #modalEditFactor .prodNameEdit .inp{
             text-align: right;
             max-width: 100%;
         }
         #menu1 .btn-group{
             border: none !important;
         }
         #tbody1 .btnShowMsg {
             position: relative;
         }
         #tbody1 .btn-pay,#tbody1 .checkout{
             text-align: center;
         }
         #tbody1 .btn-pay button,#tbody1 .checkout button {
             padding: 2px 7px;
             height: 24px;
             line-height: unset;
             font-size: 11px;
             border-radius: 5px;
         }
         #tbody1 .btnShowMsg span{
             position: absolute;
             top: 6px ;
             left: 3px;
         }
         #tbody1 td{
             font-size: 13px;
         }
         #tbody1 img{
             width: 50px;
             height: 50px;
             cursor: pointer;
             border-radius: 50%;
         }
         #tbody1 .actions a{
             width: 12px !important;
             height: 12px !important;
             font-size: 10px;
         }
         #tbody1 .actions a i{
             position: relative;
             top: -1px;
             left: 1px;
         }
         .actShow span{
             font-size: 11px;
             padding: 0 14px;
         }
         .modalShowImage img{
             max-width: 100%;

         }
         #containerMseg{max-width:100%; margin:auto;width: 100%}
         img{ max-width:100%;}
         #containerMseg .inbox_people {
             background: #f8f8f8 none repeat scroll 0 0;
             float: left;
             overflow: hidden;
             width: 40%; border-right:1px solid #c4c4c4;
         }
         #containerMseg .inbox_msg {
             border: 1px solid #c4c4c4;
             clear: both;
             overflow: hidden;
         }
         #containerMseg .top_spac{ margin: 20px 0 0;}


         #containerMseg .recent_heading {float: left; width:40%;}
         #containerMseg .srch_bar {
             display: inline-block;
             text-align: right;
             width: 60%;
         }
         #containerMseg .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

         #containerMseg .recent_heading h4 {
             color: #05728f;
             font-size: 21px;
             margin: auto;
         }
         #containerMseg .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
         #containerMseg .srch_bar .input-group-addon button {
             background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
             border: medium none;
             padding: 0;
             color: #707070;
             font-size: 18px;
         }
         #containerMseg .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

         #containerMseg .chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
         #containerMseg .chat_ib h5 span{ font-size:13px; float:right;}
         #containerMseg .chat_ib p{ font-size:14px; color:#989898; margin:auto}
         .chat_img {
             float: left;
             width: 11%;
         }
         #containerMseg .chat_ib {
             float: left;
             padding: 0 0 0 15px;
             width: 88%;
         }

         #containerMseg .chat_people{ overflow:hidden; clear:both;}
         #containerMseg .chat_list {
             border-bottom: 1px solid #c4c4c4;
             margin: 0;
             padding: 18px 16px 10px;
         }
         #containerMseg .inbox_chat { height: 550px; overflow-y: scroll;}

         #containerMseg .active_chat{ background:#ebebeb;}

         #containerMseg .incoming_msg_img {
             display: inline-block;
             width: 6%;
         }
         #containerMseg .received_msg {
             display: inline-block;
             padding: 0 0 0 10px;
             vertical-align: top;
             width: 92%;
         }
         #containerMseg .received_withd_msg p {
             background: #ebebeb none repeat scroll 0 0;
             border-radius: 3px;
             color: #646464;
             font-size: 14px;
             margin: 0;
             padding: 8px 10px 5px 12px;
             width: 100%;
             text-align: right;
         }
         #containerMseg .time_date {
             color: #747474;
             display: block;
             font-size: 12px;
             margin: 0 0 10px 0;
         }
         #containerMseg .received_withd_msg { width: 57%;}
         #containerMseg .mesgs {
             float: left;
             padding: 30px 15px 0 25px;
             width: 100%;
             padding-right: 0px;
             padding-left: 0px;
         }

         #containerMseg .sent_msg p {
             background: #05728f none repeat scroll 0 0;
             border-radius: 3px;
             font-size: 14px;
             margin: 0; color:#fff;
             padding: 8px 10px 5px 12px;
             width:100%;

         }
         #containerMseg .outgoing_msg{
             overflow:hidden; margin:0px 0 0px;padding-right: 10px;
         }
         #containerMseg .sent_msg {
             float: right;
             width: 46%;

         }
         #containerMseg .input_msg_write input {
             background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
             border: medium none;
             color: #4c4c4c;
             font-size: 15px;
             min-height: 48px;
             width: 100%;
             outline: none;
         }

         #containerMseg .type_msg {
             border-top: 1px solid #c4c4c4;position: relative;
             padding-right: 39px;
             outline: none;
         }
         #containerMseg .msg_send_btn {
             background: #05728f none repeat scroll 0 0;
             border: medium none;
             border-radius: 50%;
             color: #fff;
             cursor: pointer;
             font-size: 17px;
             height: 33px;
             position: absolute;
             right: 0;
             top: 11px;
             width: 33px;
             outline: none;
             top: 7px;
             right: 3px;
         }
         #containerMseg .messaging { padding: 0 0 50px 0;}
         #containerMseg .msg_history {
             height: 250px;
             overflow-y: auto;
         }
         #chat{
             max-width: 70%;
         }

         #containerMseg .incoming_msg{
             text-align: left;
             direction: ltr;
         }
         .notMessageFound{
             text-align: center;
         }
         .notMessageFound span{
             background:
                 #ddd;
             padding: 10px !important;
         }

         .ui-autocomplete{
             z-index: 10000000000000000000000000 !important;
         }

         #modalUnpaid #home .acttt{
             background-color: lightseagreen;
         }
         .transData b{
             display: inline-block;
             width: 23%;
         }
         #tbody1 .actt{
             background-color: #eefe;
         }
         .prodNameEdit{
             padding: 5px;position: relative
         }
         .prodNameEdit:hover i{
             opacity: 1;
         }
         .prodNameEdit i{
             position:absolute;
             color: orange;
             opacity: 0.1;
             transition: 0.5s;
             top: 10px;
             left: 10px;
             cursor: pointer;
         }
         .prodNameEdit textarea{
             min-height: 73px
         }
         #wraperStatSelect div.btn-group{
             border: none !important;
         }

         #modalAddBijak label{
             font-size: 13px;
             margin-bottom: 0px;
             position: relative;
             top: 9px;
         }
         .typeBijak div{
             border: none !important;
         }
         .typeBijak label{
             margin-bottom: 15px !important;
         }
         #modalAddBijak .col-sm-12{
             padding-top: 15px;
         }
         #tbody3 .td_row {
             position: relative;
             padding: 5px;
             text-align: center;
         }
         #tbody3 .td_row i.remove_row_edited{
             position: absolute;
             top: 1px;
             right: 1px;
             color: red;
             cursor: pointer;
         }
     </style>

     <section class="content">
         <div class="container-fluid">
             <div class="block-header">
                 <div class="row">
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                         <ul class="breadcrumb breadcrumb-style ">

                             <li class="breadcrumb-item bcrumb-1">
                                 <a href="{{route('p_dashboard')}}">
                                     <i class="fas fa-home"></i>
                                     &nbsp;&nbsp;&nbsp;&nbsp;
                                     داشبورد
                                 </a>
                             </li>
                             <li class="breadcrumb-item bcrumb-2">
                                 <a href="javascript:void(0);">فاکتورها</a>
                             </li>
                             <li class="breadcrumb-item active">لیست فاکتور</li>
                         </ul>
                         <p style="float: left;padding-top: 13px;color: #444;font-weight: bold;">
                             امروز :&nbsp;&nbsp<i style="float: inherit">{{jdate('Y-m-d',time())}}</i>
                         </p>
                     </div>
                 </div>
             </div>



                 <div class="block-header" style="margin-top: 10px">
                     <div class="row">
                         <div class="col-md-12">
                             <a class="btn {{$status=='all'?'btn-primary':'btn-info'}}" href="{{route('p_factors',['all'])}}">همه</a>
                             <a class="btn {{$status==1?'btn-primary':'btn-info'}}" href="{{route('p_factors',[1])}}">در انتظار تایید اولیه</a>
                             <a class="btn {{$status==2?'btn-primary':'btn-info'}}" href="{{route('p_factors',[2])}}">در انتظار تایید نهایی </a>
                             <a class="btn {{$status==3?'btn-primary':'btn-info'}}" href="{{route('p_factors',[3])}}">در انتظار تایید مشتری</a>
                             <a class="btn {{$status==4?'btn-primary':'btn-info'}}" href="{{route('p_factors',[4])}}">تایید نهایی</a>
                             <a class="btn {{$status==-1?'btn-primary':'btn-info'}}" href="{{route('p_factors',['-1'])}}">رد شده</a>
                         </div>
                     </div>
                 </div>


             <!-- Exportable Table -->
             <div class="row clearfix">
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="card">
                         <div class="header headTitle">
                             <h2>
                                 {!! $filter !!}

                                 @if(permission('factorAdd'))
                                 &nbsp;&nbsp;&nbsp;&nbsp;
                                 <a  href="{{route('p_factors_add')}}" class="btn btn-outline-success btn-border-radius">افزودن فاکتور جدید</a>
                                     @endif
                             </h2>

                         </div>
                         <div class="body">
                             <div class="table-responsive">

                                 <table id="tableExport1" class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                                     <thead>
                                     <tr>
                                         <th class="d-none">ردیف</th>
                                         <th class="w10">کد فاکتور </th>
                                         <th class="w15">نام مشتری</th>
                                         <th class="w10">وضعیت</th>
                                         <th class="w10">آخرین تغییر</th>
                                         <th class="w10">تاریخ</th>
                                         <th class="w15">عملیات</th>
                                     </tr>
                                     </thead>
                                     <tbody id="tbody1">
                                    {!! $tbl !!}
                                     </tbody>

                                 </table>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <!-- #END# Exportable Table -->
         </div>

{{--        @include('admin.orders.factors_modal')--}}

         <style>
             #tbody1 td{
                 padding:  2px;
                 border-left: 1px solid #eee;
                 font-size: 11px;
                 color: black;
             }
             #tbody1 img{
                 width: 50px;
                 height: 50px;
                 cursor: pointer;
                 border-radius: 50%;
             }
             #tbody1 td a{

                 font-size: 12px;
             }

             .modalShowImage img{
                 max-width: 100%;

             }
             .btn-circle i{
                 font-size: 13px;
             }
             .btn-pay .btn{
                 background-color: rgba(1,1,1,0) !important;
                 padding: 0 4px !important;
                 line-height: 5px !important;
                 height: 20px !important
             }
             .btn-pay .btn-danger{
                 border: 1px solid #fb483a !important;
                 color: #fb483a !important;
             }
             .btn-pay .btn-success{
                 border: 1px solid #18ce0f !important;
                 color: #18ce0f !important;
             }
             .checkout .btn{
                 background-color: rgba(1,1,1,0) !important;
                 padding: 0 4px !important;
                 line-height: 5px !important;
                 height: 20px !important
             }
             .checkout .btn-danger{
                 border: 1px solid #fb483a !important;
                 color: #fb483a !important;
             }
             .checkout .btn-warning{
                 border: 1px solid #ff9600 !important;
                 color: #ff9600 !important;
             }
             .checkout .btn-success{
                 border: 1px solid #18ce0f !important;
                 color: #18ce0f !important;
             }

         </style>


     </section>
     <div class="template d-none">
         <div class="outgoing_msg">
             <div class="sent_msg">
                 <p class="msgUser"></p>
                 <span class="time_date"> <?php echo jdate('h:m', time());?>  |    <?php echo jdate('Y/m/d', time())?></span> </div>
         </div>
     </div>

     <div class="modal fade" id="modalFinf">
         <div class="modal-dialog" >
             <div class="modal-content">
                 <div class="modal-header" style="display: none">
                     <button type="button" class="close closeModalFind" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title text-center"></h4>
                 </div>
                 <div class="modal-body">
                     <div>
                         <label>نام محصول :</label>
                         <input type="text" name="prod" class="form-control" id="findProduct">
                     </div>
                 </div>

             </div>
         </div>
     </div>


     <div class="modal fade" id="modalShowFactor">
         <div class="modal-dialog modal-lg" style="max-width: unset;width: 90%">
             <div class="modal-content" style="max-width: unset;width: 100%">
                 <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title">جزئیات فاکتور</h4>
                 </div>
                 <div class="modal-body" id="tblPrint">
                     <table style="border:1px solid gray;direction: rtl;text-align: right;width: 100%">
                         <tr style="border-bottom: 0">
                             <td style="width: 33.33%;padding: 0">
                                 &nbsp;
                             </td>
                             <td style="width: 33.33%;padding: 5px">
                                 <h5 style="text-align: center;position: relative;top: -17px;">بسمه تعالی</h5>
                             </td>
                             <td style="width: 33.33%;padding: 16px 66px 2px 5px;">
                                 <p style="margin-bottom: 0"><b>تاریخ :</b> <span class="date"></span></p>
                                 <p><b>شماره :</b> <span class="numbId"></span></p>
                             </td>
                         </tr>
                         <tr style="border-bottom: 0">
                             <td style="width: 33.33%;padding: 5px">
                                 <b>نام مشتری :</b>
                                 <span class="name"></span>
                             </td>
                             <td style="width: 33.33%;padding: 5px">
                                 <b>تلفن تماس :</b>
                                 <span class="mobile"></span>
                             </td>
                             <td style="width: 33.33%;padding: 5px">
                                 <b>شرکت / سازمان :</b>
                                 <span class="company"></span>
                             </td>
                         </tr>
                         <tr style="border-bottom: 0">
                             <td colspan="3" style="padding: 5px">
                                 <b>آدرس :</b>
                                 <span class="address"></span>
                             </td>
                         </tr>
                     </table>
                     <table style="border:1px solid gray;margin-top: 5px;direction: rtl;text-align: right">
                         <thead>
                         <tr style="background: gray;color: white">
                             <th style="width: 7%;padding: 5px">ردیف</th>
                             <th style="width: 40%;padding: 5px">نام محصول</th>
                             <th style="width: 10%;padding: 5px">واحد شمارش</th>
                             <th style="width: 15%;padding: 5px">قیمت واحد</th>
                             <th style="width: 18%;padding: 5px;text-align: center">تعداد</th>
                             <th style="width: 18%;padding: 5px;text-align: center">تخفیف</th>
                             <th style="width: 20%;padding: 5px">قیمت کل</th>
                         </tr>
                         </thead>
                         <tbody id="tbody2">

                         </tbody>
                         <tfoot>

                         <tr style="border-bottom:0">
                             <td colspan="3" rowspan="3" style="padding: 5px;font-size: 14px">توضیحات  : <span class="desc_factor"></span></td>
                             <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">جمع کل : </td>
                             <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number total"></b></td>
                         </tr>
                         <tr style="border-bottom:0">
                             <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">تخفیف : </td>
                             <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number discount"></b> </td>
                         </tr>
                         <tr style="border-bottom:0">


                             <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">قابل پرداخت : </td>
                             <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number final"></b></td>
                         </tr>

                         </tfoot>

                     </table>

                 </div>

                 <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                     <button type="button"  onclick="javascript:printDiv('tblPrint')" class="btn btn-primary">پرینت فاکتور</button>
                 </div>
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->


     @if(permission('factorsSmsNotStock'))
         <div class="modal fade" id="modalWait">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h4 class="modal-title">عدم موجودی</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     </div>
                     <div class="modal-body">
                         <form method="post" action="{{route('p_factors_await')}}">
                             @csrf
                             <input type="hidden" name="numb">
                             <input type="hidden" name="mobile">

                             <div style="border: 1px solid #999;padding: 8px;border-radius: 8px;">
                                 <p>
                                     <input type="text" name="name" style="width: 150px;border: 1px solid rgb(119, 119, 119) !important;border-radius: 5px;height: 29px;direction: rtl;padding-right: 5px;">
                                     عزیز
                                 </p>
                                 <p>
                                     سفارش شماره
                                     <b class="numb">651</b>
                                     شما را دریافت کردیم ولی بدلیل عدم موجودی
                                     <input type="text" name="product" placeholder="نام محصولی که موجود نیست"  style="width: 150px;border: 1px solid rgb(119, 119, 119) !important;border-radius: 5px;height: 29px;direction: rtl;padding-right: 5px;">
                                     قادر به تایید آن نیستیم. به محض تکمیل موجودی، سفارشتان را تایید و از همین طریق اطلاع رسانی مینماییم.
                                 </p>
                                 <p>از صبر و شکیبایی شما سپاسگزاریم.</p>
                                 <br>
                                 <p>«تجهیزات پزشکی فردوسی» </p>
                             </div>
                             <p class="myAlert"></p>
                             <p class="mySuccess"></p>
                             <button type="button" class="btn btn-success">ارسال پیامک</button>
                         </form>

                     </div>

                 </div>
             </div>
         </div>
     @endif

     @if(permission('factorConfirmFirst') || permission('factorConfirmLast') || permission('factorTemporary'))

         <div class="modal fade" id="modalEditFactor">
             <div class="modal-dialog modal-lg" style="max-width: unset;width: 90%">
                 <form method="post" action="{{route('p_factors_editSave')}}" style="width: 100%">
                     @csrf
                     <input type="hidden" name="factor_id">

                     <div class="modal-content" style="max-width: unset;width: 100%">
                         <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                             <h4 class="modal-title">ویرایش فاکتور</h4>
                         </div>
                         <div class="modal-body" id="tblPrint">
                             <table style="border:1px solid gray">
                                 <tr style="border-bottom: 0">
                                     <td style="width: 33.33%;padding: 0">
                                         &nbsp;
                                     </td>
                                     <td style="width: 33.33%;padding: 5px">
                                         <h5 style="text-align: center;position: relative;top: -17px;">بسمه تعالی</h5>
                                     </td>
                                     <td style="width: 33.33%;padding: 16px 66px 2px 5px;">
                                         <p style="margin-bottom: 0"><b>تاریخ :</b> <span class="date"></span></p>
                                         <p><b>شماره :</b> <span class="numbId"></span></p>
                                     </td>
                                 </tr>
                                 <tr style="border-bottom: 0">
                                     <td style="width: 33.33%;padding: 5px">
                                         <b>نام مشتری :</b>
                                         <span class="name"></span>
                                     </td>
                                     <td style="width: 33.33%;padding: 5px">
                                         <b>تلفن تماس :</b>
                                         <span class="mobile"></span>
                                     </td>
                                     <td style="width: 33.33%;padding: 5px">
                                         <b>شرکت / سازمان :</b>
                                         <span class="company"></span>
                                     </td>
                                 </tr>
                                 <tr style="border-bottom: 0">
                                     <td colspan="3" style="padding: 5px">
                                         <b>آدرس :</b>
                                         <input type="text" style="width: 85%" name="address" class="form-control">
                                     </td>
                                 </tr>
                             </table>
                             <div class="details" style="text-align: center;border: 1px solid#777;margin-top: 6px;padding: 10px;">
                                 <img class="image" src="" style="max-width: 100%">
                                 <p class="desc"></p>
                             </div>
                             <p style="margin: 15px 0 -5px 0 ">


                             </p>
                             <button type="button" class="btn btn-info" id="btn_add_row_edit">افزودن سطر جدید</button>
                             <table style="border:1px solid gray;margin-top: 5px">
                                 <thead>
                                 <tr style="background: gray;color: white">
                                     <th style="width: 7%;padding: 5px">ردیف</th>
                                     <th style="width: 40%;padding: 5px">نام محصول</th>
                                     <th style="width: 8%;padding: 5px"> واحد شمارش</th>
                                     <th style="width: 15%;padding: 5px">قیمت واحد</th>
                                     <th style="width: 18%;padding: 5px;text-align: center">تعداد</th>
                                     <th style="width: 18%;padding: 5px;text-align: center">تخفیف واحد</th>
                                     <th style="width: 20%;padding: 5px">قیمت کل با کسر تخفیف</th>
                                 </tr>
                                 </thead>
                                 <tbody id="tbody3">

                                 </tbody>
                                 <tfoot>
                                 <tr style="border-bottom:0">
                                     <td colspan="3" rowspan="2" style="padding: 5px;font-size: 14px">توضیحات  : <span class="desc_factor"></span></td>
                                     <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">جمع کل : </td>
                                     <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number total totalFactor"></b></td>
                                 </tr>
                                 <tr style="border-bottom:0">

                                     <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">تخفیف : </td>
                                     <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number discount"></b>
                                         <input type="text" name="discount" class="form-control inp">
                                     </td>
                                 </tr>
                                 <tr style="border-bottom:0">
                                     <td colspan="3" rowspan="2" style="padding: 5px;font-size: 14px">آدرس  : <span class="address1"></span></td>
                                     <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">هزینه حمل : </td>
                                     <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number transs"></b>
                                         <input type="text" name="transs" class="form-control inp">
                                     </td>
                                 </tr>
                                 <tr style="border-bottom:0">
                                     <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">قابل پرداخت : </td>
                                     <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number final finalFactor"></b></td>
                                 </tr>

                                 </tfoot>

                             </table>
                             <div>
                                 <label>توضیحات : </label>
                                 <textarea class="form-control" style="width: 90%" name="desc"></textarea>
                             </div>
                             <div style="padding: 15px">
                                 <label>سربرگ فاکتور : </label>
                                 <select name="comp" style="display: block;border: 1px solid #555;">
                                     <option value="1">تجهیزات پزشکی فردوسی</option>
                                     <option value="2">تجهیزگران پویای امید سلامت</option>
                                 </select>
                             </div>
                             <div id="transportOnModalEdit" style="border:1px solid gray;margin-top: 5px">

                             </div>
                             <p class="myAlert"></p>
                             <p class="mySuccess"></p>
                         </div>
                         <div class="modal-footer">
                             @if(permission('factorTemporary'))
                                 <button type="button" id="interim" class="btn btn-info factorTemporary btnSaveFactorEdit ">ذخیره موقت</button>
                             @endif
                             <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
                             @if(permission('factorConfirmFirst'))
                                 <button type="submit" class="btn btn-primary factorConfirmFirst btnSaveFactorEdit btnFirstEdit btn_order_change_order">تایید اولیه</button>
                             @endif
                             @if(permission('factorConfirmLast'))
                                 <button type="submit"  class="btn btn-primary factorConfirmLast btnSaveFactorEdit btn_order_change_order">تایید نهایی</button>
                             @endif
                             {{--                    <a  class="btn btn-primary btnConfirmFactorEdit">ارسال به بخش بسته بندی</a>--}}
                         </div>
                     </div>
                 </form>
             </div>
         </div>
     @endif

     @if(permission('factorsRemove'))
         <div class="modal fade" id="modalRemove">
             <div class="modal-dialog">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h4 class="modal-title">حذف فاکتور</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     </div>
                     <div class="modal-body">
                         <h5>آیا مطمئنید که قصد حذف کردن این فاکتور را دارید؟</h5>

                     </div>
                     <form method="post" action="{{route('p_factors_remove')}}">
                         @csrf
                         <input class="id" type="hidden" name="id">
                         <p class="myAlert"></p>
                         <p class="mySuccess"></p>
                         <div class="modal-footer">
                             <button type="button" class="btn btn-default" data-dismiss="modal">فعلا نه</button>
                             <button type="submit" id="btn_remove_order" class="btn btn-info waves-effect ">بله، حذف شود.</button>
                         </div>
                     </form>
                 </div>
             </div>
         </div>
     @endif

 @endsection
