@extends('layouts.admin')

 @section('title')
    ویرایش فاکتور
    @endsection

 @section('css')

    <link href="{{url('assets/css/app.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/js/bundles/materialize-rtl/materialize-rtl.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/form.min.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet" />
    <link href="{{url('assets/css/styles/all-themes.css')}}" rel="stylesheet" />
    <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.css')}}" rel="stylesheet" />
    <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.theme.css')}}" rel="stylesheet" />
    <link href="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery_ui_rtl.css')}}" rel="stylesheet" />
    <script src="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>

 @endsection
 @section('js')
     <script src="{{url('assets/js/admin.js')}}"></script>
     <script src="{{url('assets/js/bundles/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
     <script>
         $.ajaxSetup({headers:{'X-CSRF-TOKEN':$('meta[name=csrf-token]').attr('content')}});
         $("#customerName").val('');
         $("#productName").val('');
         $('[name=user_id]').val('0');

         $("#clearProductField").on('click',function () {
             $("#productName").val('');
             $("#clearProductField").hide();
         });

         $.ajax({
             url: "{{route('p_factors_getProducts')}}",
             type: "POST",
             dataType: 'json',
             success: function (data) {

                 $("#productName").autocomplete({
                     source: data,
                     select: function( event, ui ) {
                         // $("#clearProductField").show();

                         var duplicated= false;
                         $('#tbody2 tr').each(function () {
                             if($(this).hasClass(ui.item.rr)){
                                 var dulicatedTr = $(this);
                                 dulicatedTr.addClass('duplicated');
                                 window.setTimeout(function () {
                                     dulicatedTr.removeClass('duplicated');
                                 },2000);
                                 duplicated = true;
                             }
                         });
                         window.setTimeout(function () {
                             $("#productName").val('');
                         },500);
                         if(duplicated)
                             return;
                         var numbPr = $('#submitForm').data('numb');
                         numbPr++;
                         $('#submitForm').data('numb',numbPr);
                         var newRow = "<tr style='position: relative' class='"+ui.item.rr+"'>";
                         newRow += "<td class='r'></td>";
                         // newRow += "<td>"+ui.item.value+"<br>"+ui.item.subName+"<br>"+ui.item.irc+"</td>";
                         newRow += "<td><input type='hidden' name='productId[]' value='"+ui.item.id+"' ><input type='hidden' name='codes[]' value='"+ui.item.code+"' ><textarea name='productName[]'>"+ui.item.name+'\n'+ui.item.subName+'\n'+ui.item.irc+"</textarea> </td>";
                         newRow += "<td class='priceField'><input type=\"text\" class=\"form-control rial fl\" value=\""+ui.item.price+"\" name=\"price[]\"></td>";
                         newRow += "<td class='numbField'><input type=\"text\" class=\"form-control rial fl\" value=\"1\" name=\"numb[]\"><i class=\"material-icons removeProduct\"> clear</i></td>";
                         newRow += "<td class='tot number'>"+ui.item.price+"</td>";
                         newRow += "</tr>";

                         $('#tblContainer').show();
                         $('#tbody2').append(newRow);
                         var row = 1;
                         $('#tbody2 tr').each(function () {
                             $(this).find('.r').text(row++);
                         });
                         setPrice();
                     }
                 });
             }
         });

         $('#tbl1').on('change keyup keypress blur','.fl',function () {

             setPrice();
         });

         $('#productName').on('keypress',function (e) {

             if(e.charCode == 13){
                 e.preventDefault();
                 var numbPr = $('#submitForm').data('numb');
                 numbPr++;
                 $('#submitForm').data('numb',numbPr);
                 var newRow = "<tr style='position: relative' class='1000000'>";
                 newRow += "<td class='r'></td>";
                 // newRow += "<td>"+ui.item.value+"<br>"+ui.item.subName+"<br>"+ui.item.irc+"</td>";
                 newRow += "<td><input type='hidden' name='productId[]' value='0' ><input type='hidden' name='codes[]' value='0' ><textarea name='productName[]'>"+$(this).val()+"</textarea> </td>";
                 newRow += "<td class='priceField'><input type=\"text\" class=\"form-control rial fl\" value=\"0\" name=\"price[]\"></td>";
                 newRow += "<td class='numbField'><input type=\"text\" class=\"form-control rial fl\" value=\"1\" name=\"numb[]\"><i class=\"material-icons removeProduct\"> clear</i></td>";
                 newRow += "<td class='tot number'>0</td>";
                 newRow += "</tr>";

                 $(this).val('');
                 $('#tblContainer').show();
                 $('#tbody2').append(newRow);
                 var row = 1;
                 $('#tbody2 tr').each(function () {
                     $(this).find('.r').text(row++);
                 });
                 setPrice();
             }
         });


         $('#submitForm').on('click',function (e) {
             var numbPrrr = $('#submitForm').data('numb');
             if($('[name=user_id]').val() < 1){
                 alert('نام مشتری را وارد نکرده اید.');
                 e.preventDefault();
             }else if(numbPrrr < 1){
                 alert('برای ثبت فاکتور حداقل یک محصول باید انتخاب شود.');
                 e.preventDefault();
             }
         });

         $('#tbl1').on('click','.removeProduct',function () {
             var numbPr2 = $('#submitForm').data('numb');
             numbPr2++;
             $('#submitForm').data('numb',numbPr2);
             $(this).parent().parent().detach();
             setPrice();
         });

         function setPrice() {
             var total = 0;

             $('#tbody2 tr').each(function () {
                 var pr = $(this).find('.priceField input').val().replace(',','').replace(',','').replace(',','').replace(',','').replace(',','');
                 var nu = $(this).find('.numbField input').val().replace(',','').replace(',','').replace(',','').replace(',','').replace(',','');;
                 var tot = pr * nu;

                 $(this).find('.tot').text(tot);
                 total += tot;
             });
             $('.totalFactor').text(total);
             var discount = $('[name=discount]').val().replace(',','').replace(',','').replace(',','').replace(',','').replace(',','');;
             var finalFactor = total - discount;
             $('.finalFactor').text(finalFactor);
             sep();
         }


     </script>
 @endsection

 @section('main')



    <section class="content">
        <form method="post" action="{{route('p_factors_addSave')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="user_id" value="0">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul class="breadcrumb breadcrumb-style ">

                            <li class="breadcrumb-item bcrumb-1">
                                <a href="{{route('p_dashboard')}}">
                                    <i class="fas fa-home"></i>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    داشبورد
                                </a>
                            </li>
                            <li class="breadcrumb-item bcrumb-2">
                                <a href="javascript:void(0);">فاکتورها</a>
                            </li>
                            <li class="breadcrumb-item active">ویرایش فاکتور</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <strong>ویرایش</strong> فاکتور </h2>
                            <style>
                                .rmRole{font-size: 16px;display: inline-block;border: 1px solid red;padding: 7px 2px 5px 2px;border-radius: 50%;cursor: pointer;}
                                .errorsAction li{
                                    border: 1px solid red;
                                    padding: 15px;
                                    margin: 10px;
                                    color: red;
                                }

                                .warningsAction li{
                                    border: 1px solid orangered;
                                    padding: 15px;
                                    margin: 10px;
                                    color: orangered;
                                }
                                .tot,.priceField{
                                    padding-right:20px ;
                                    padding-left: 20px;
                                }
                                .numbField{
                                    padding-right:30px ;
                                    padding-left: 30px;
                                }
                                .fl{
                                    border: 1px solid #ccc !important;
                                    border-radius: 5px !important;
                                    padding: 0 10px 0 10px !important;
                                    text-align: left !important;
                                    max-width: 130px;
                                }
                                .priceField input{
                                    width: 100px !important;
                                }
                                .numbField input{
                                    width: 81px !important;
                                }
                                .duplicated{
                                    background-color: #eab8c4;
                                    transition: 0.8s;
                                }
                                #tbody2 textarea{
                                    min-height: 80px;
                                    padding: 5px;
                                    border: 1px solid #ccc;
                                    border-radius: 5px;
                                }

                                .removeProduct{
                                    position: absolute;
                                    left: 22px;
                                    top: 40px;
                                    color: red;
                                    font-size: 20px;
                                    cursor: pointer;
                                }

                            </style>

                            <?php App\Helpers\Helpers::showErrors() ?>
                            <?php App\Helpers\Helpers::showWarnings() ?>

                        </div>


                        <div class="body">
                            <div class="row">
                                <div class="col-12" id="customerDetails" >
                                    @php($user = $factor->rel_user)
                                    <div class="row">
                                        <div class="col-4">
                                            <b>نام : </b>
                                            <span class="name">{{$user->name.' '.$user->family}}</span>
                                        </div>
                                        <div class="col-4">
                                            <b>شرکت : </b>
                                            <span class="company">{{$user->company}}</span>
                                        </div>
                                        <div class="col-4">
                                            <b>تلفن تماس : </b>
                                            <span class="mobile">{{$user->mobile}}</span>
                                        </div>
                                        <div class="col-12">
                                            <b>آدرس : </b>
                                            <span class="address"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">

                                <div class="col-12 col-xs-12">

                                </div>
                            </div>
                            <div class="row"  id="tblContainer">
                                <div class="{{is_file($factor->image)?'col-lg-8':'col-lg-12'}}">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input class="form-control" value="" id="productName" name="title" type="text">
                                            <label class="form-label">نام محصول <sup>*</sup></label>
                                            <i id="clearProductField"  style="display: none;position: relative;top: -29px;left: 23px;color: red;font-size: 20px;cursor: pointer;" class="material-icons"> clear</i>
                                        </div>
                                        <p class="helperText">
                                            یک یا چند کارکتر از نام کالا را وارد نمایید.
                                        </p>

                                    </div>
                                    <div style="overflow-x: scroll">
                                        <table id="tbl1" style="border:1px solid gray;margin-top: 5px;width: 900px">
                                            <thead>
                                                <tr style="background: gray;color: white">
                                                    <th style="width: 7%;padding: 5px">ردیف</th>
                                                    <th style="width: 40%;padding: 5px">نام محصول</th>
                                                    <th style="width: 15%;padding: 5px">قیمت واحد</th>
                                                    <th style="width: 18%;padding: 5px;text-align: center">تعداد</th>
                                                    <th style="width: 20%;padding: 5px">قیمت کل</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbody2" data-row="1">
                                                @foreach($factor->rel_details as $k=>$val)
                                                    <tr style="position: relative" class="19">
                                                        <td class="r">{{$k+1}}</td>
                                                        <td>
                                                            <input type="hidden" name="productId[]" value="19">
                                                            <input type="hidden" name="codes[]" value="0">
                                                            <textarea name="productName[]"></textarea>
                                                        </td>
                                                        <td class="priceField">
                                                            <input type="text" class="form-control rial fl" value="0" name="price[]">
                                                        </td>
                                                        <td class="numbField">
                                                            <input type="text" class="form-control rial fl" value="1" name="numb[]">
                                                            <i class="material-icons removeProduct"> clear</i>
                                                        </td>
                                                        <td class="tot number">0</td>
                                                    </tr>

                                            @endforeach
                                            <tfoot>
                                            <tr style="border-bottom:0">
                                                <td colspan="3" style="padding: 5px;font-size: 14px">&nbsp;</td>
                                                <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">جمع کل : </td>
                                                <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number totalFactor"></b></td>
                                            </tr>
                                            <tr style="border-bottom:0">
                                                <td colspan="3" style="padding: 5px;font-size: 14px">&nbsp;</td>
                                                <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">تخفیف : </td>
                                                <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><input type="text" class="rial fl" name="discount" value="0"> </td>
                                            </tr>
                                            <tr style="border-bottom:0">
                                                <td colspan="3" style="padding: 5px;font-size: 14px">&nbsp;</td>
                                                <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">قابل پرداخت : </td>
                                                <td style="padding: 5px;border-bottom:1px dashed #e0e0e0;"><b class="number finalFactor"></b></td>
                                            </tr>
                                            <tr style="border-bottom:0">
                                                <td colspan="3" style="padding: 5px;font-size: 14px">&nbsp;</td>
                                                <td style="padding: 5px;font-size: 14px;border-right:1px solid #e0e0e0;border-bottom:1px dashed #e0e0e0;">&nbsp; </td>
                                                <td style="padding:30px 5px;border-bottom:1px dashed #e0e0e0;">
                                                    <button id="submitForm" data-numb="0" type="submit" class="btn btn-success btn-border-radius waves-effect">ذخیره فاکتور</button>
                                                </td>
                                            </tr>

                                            </tfoot>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-lg-4" style="padding-top: 90px">
                                    <img src="{{url($factor->image)}}" style="width: 100%;">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
        </form>
    </section>

    @endsection
