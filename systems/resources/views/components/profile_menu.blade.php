<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 sticky-sidebar">
    <div class="profile-sidebar dt-sl">
        <div class="dt-sl dt-sn mb-3">
            <div class="profile-sidebar-header dt-sl">
                <div class="profile-avatar float-right">
                    <img src="{{is_file(ROOT.auth()->user()->image)?
url(auth()->user()->image):
url($opt['profile_userImage'])}}" alt="">
                </div>
                <div class="profile-header-content mr-3 mt-2 float-right">
                    <span class="d-block profile-username">{{auth()->user()->name.' '.auth()->user()->family}}</span>
                    <span class="d-block profile-phone">{{auth()->user()->mobile}}</span>
                </div>
                {{--                                <div class="profile-point mt-3 mb-2 dt-sl">--}}
                {{--                                    <span class="float-right label-profile-point">امتیاز شما:</span>--}}
                {{--                                    <span class="float-left value-profile-point">120</span>--}}
                {{--                                </div>--}}
                <div class="profile-link mt-2 dt-sl">
                    <div class="row">
                        <div class="col-6 text-center">
                            <a href="{{route('profile_changePass')}}">
                                <i class="mdi mdi-lock-reset"></i>
                                <span class="d-block">
                                    {{$opt['profile_menu_pass']}}
                                    {{--                                    تغییر رمز--}}
                                </span>
                            </a>
                        </div>
                        <div class="col-6 text-center">
                            <a href="{{route('profile_personalInfo')}}">
                                <i class="mdi mdi-account-edit-outline"></i>
                                <span class="d-block">
                                    {{$opt['profile_menu_edit']}}
                                    {{--                                    ویرایش اطلاعات--}}
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dt-sl dt-sn mb-3 text-center">
            <a href="{{route('profile_affiliate')}}">
                <img src="{{url($opt['profile_affImage'])}}" class="img-fluid" alt="">
            </a>
        </div>
        <div class="dt-sl dt-sn mb-3">

            <div class="profile-menu-section dt-sl">
                <div class="label-profile-menu mt-2 mb-2">
                    <span>
                        {{$opt['profile_yourAccount']}}
                        {{--                        حساب کاربری شما--}}
                    </span>
                </div>
                <div class="profile-menu">
                    <ul>
                        <li>
                            <a href="{{route('profile')}}" class="menu_profile">
                                <i class="mdi mdi-account-circle-outline"></i>
                                {{$opt['profile_prof']}}
                                {{--                                پروفایل--}}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('profile_awards')}}" class="menu_award">
                                <i class="mdi mdi-trophy-award"></i>
                                جوایز و امتیازات
                            </a>
                        </li>
                        <li>
                            <a class="menu_orders" href="{{route('profile_orders')}}">
                                <i class="mdi mdi-basket"></i>
                                {{$opt['profile_orders']}}
                                {{--                                همه سفارش ها--}}
                            </a>
                        </li>
{{--                        <li>--}}
{{--                            <a href="#">--}}
{{--                                <i class="mdi mdi-backburger"></i>--}}
{{--                                درخواست مرجوعی--}}
{{--                            </a>--}}
{{--                        </li>--}}
                        <li>
                            <a class="menu_favorites" href="{{route('profile_favorites')}}">
                                <i class="mdi mdi-heart-outline"></i>
                                {{$opt['profile_faves']}}
                                {{--                                لیست علاقمندی ها--}}
                          </a>
                        </li>
                        <li>
                            <a class="menu_comments" href="{{route('profile_comments')}}">
                                <i class="mdi mdi-glasses"></i>
                                {{$opt['profile_naghd']}}
                                {{--                                نقد و نظرات--}}
                            </a>
                        </li>
                        <li>
                            <a class="menu_addresses" href="{{route('profile_addresses')}}">
                                <i class="mdi mdi-sign-direction"></i>
                                {{$opt['profile_addresss']}}
                                {{--                                آدرس ها--}}
                            </a>
                        </li>
                        <li>
                            <a class="menu_visits" href="{{route('profile_visits')}}">
                                <i class="mdi mdi-eye-outline"></i>
                                {{$opt['profile_lastSeenes']}}
                                {{--                                بازدیدهای اخیر--}}
                            </a>
                        </li>
                        <li>
                            <a class="menu_aff" href="{{route('profile_affiliate')}}">
                                <i class="mdi mdi-currency-eur"></i>
                                {{$opt['profile_affesss']}}
                                {{--                                کسب درآمد یورویی--}}
                            </a>
                        </li>
                        <li>
                            <a class="menu_edit" href="{{route('profile_personalInfo')}}">
                                <i class="mdi mdi-account-edit-outline"></i>
                                {{$opt['profile_menuInfo']}}
                                {{--                                اطلاعات شخصی--}}
                            </a>
                        </li>
                        <li>
                            <a class="menu_pass" href="{{route('profile_changePass')}}">
                                <i class="mdi mdi-lock-reset"></i>
                                {{$opt['profile_menuPass']}}
                                {{--                                تغییر پسورد--}}
                            </a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}">
                                <i class="mdi mdi-account-edit-outline"></i>
                                {{$opt['profile_menuExit']}}
                                {{--                                خروج--}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

