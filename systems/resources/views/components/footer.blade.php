<footer class="main-footer dt-sl">
    <div class="back-to-top">
        <a href="#">
            <span class="icon">
                <i class="mdi mdi-chevron-up"></i>
            </span>
            <span>{{$opt['go_to_top']}}</span>
        </a>
    </div>
    <div class="container main-container">
        <div class="footer-services">
            <div class="row">
                @for($i=1;$i<=5;$i++)
                    <div class="service-item col">
                        <a href="{{$opt['footer_icon'.$i.'_link']}}" target="_blank">
                            <img src="{{url($opt['footer_icon'.$i.'_image'])}}"
                                 alt="{{$opt['footer_icon'.$i.'_title']}}">
                        </a>
                        <p>{{$opt['footer_icon'.$i.'_title']}}</p>
                    </div>
                @endfor

            </div>
        </div>
        <div class="footer-widgets">
            <div class="row">
                @for($i=1;$i<=3;$i++)
                    <div class="col-12 col-sm-6 col-lg-3 hidden-sm">
                        <div class="widget-menu widget card">
                            <header class="card-header">
                                <h3 class="card-title">{{$opt['footer_menu'.$i]}}</h3>
                            </header>
                            <ul class="footer-menu">
                                @foreach(\App\Models\Menu::getByGroup(AB_lang(),$i+1) as $menu)
                                    <li>
                                        <a href="{{$menu->link}}">{{$menu->name}}</a>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                @endfor

                <div class="col-12 col-md-6 col-lg-3">
                    <div class="newsletter">
                        <p>{{$opt['footer_email']}}</p>
                        <form id="SubscribeNewsletter" action="{{route('subscribe')}}" method="post">
                            @csrf
                            <input type="text" class="form-control" placeholder="{{$opt['footer_email2']}}"
                                   name="email">
                            <input type="submit" class="btn btn-primary" value="{{$opt['footer_email3']}}">
                            <p class="myAlert"></p>
                            <p class="mySuccess"></p>
                        </form>
                    </div>
                    <div class="socials">
                        <p>{{$opt['footer_social']}}</p>
                        <div class="footer-social">
                            <ul class="text-center">
                                <li><a href="{{$opt['instagram']}}"><i class="mdi mdi-instagram"></i></a></li>
                                <li><a href="{{$opt['telegram']}}"><i class="mdi mdi-telegram"></i></a></li>
                                <li><a href="{{$opt['facebook']}}"><i class="mdi mdi-facebook"></i></a></li>
                                <li><a href="{{$opt['twitter']}}"><i class="mdi mdi-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="info">
            <div class="row">
                <div class="col-12 text-right">
                    <span>{{$opt['footer_titre']}}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="description">
        <div class="container main-container">
            <div class="row">
                <div class="site-description col-12 col-lg-7">
                    <h1 class="site-title">{{$opt['footer_title']}}</h1>
                    <p>{{$opt['footer_desc']}} </p>
                </div>
                @for($i=1;$i<=2;$i++)
                    <a href="{{$opt['footer_symbol_link'.$i]}}" title="{{$opt['footer_symbol'.$i]}}" target="_blank">
                        <img src="{{url($opt['footer_image'.$i])}}" alt="{{$opt['footer_symbol'.$i]}}">
                    </a>
                @endfor
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container main-container">
            <p>
                {{$opt['footer_copy']}}
            </p>
        </div>
    </div>
</footer>
