<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\VarDumper\VarDumper;

if (!function_exists('dump')) {
    /**
     * @author Nicolas Grekas <p@tchwork.com>
     */
    function dump(mixed $var, mixed ...$moreVars): mixed
    {
        VarDumper::dump($var);

        foreach ($moreVars as $v) {
            VarDumper::dump($v);
        }

        if (1 < func_num_args()) {
            return func_get_args();
        }

        return $var;
    }
}

if (!function_exists('dd')) {
    /**
     * @return never
     */
    function dd(...$vars): void
    {
        if (!in_array(\PHP_SAPI, ['cli', 'phpdbg'], true) && !headers_sent()) {
            header('HTTP/1.1 500 Internal Server Error');
        }

        foreach ($vars as $v) {
            VarDumper::dump($v);
        }

        exit(1);
    }
}


if (!function_exists('permission')) {
    function permission($permissionName,$removeCatch=false)
    {
        $result = 0 ;
        if(Auth::guard('admin')->check())
            $result = \App\Models\User::getPermission(Auth::guard('admin')->user()->id,$permissionName,$removeCatch)[0];

        return $result;

    }
}

if (!function_exists('forbidden')) {
    function forbidden($permissionName,$removeCatch=false)
    {
        return !\App\Models\User::checkPermissionPage($permissionName,$removeCatch);

    }
}

if (!function_exists('myOption')) {
    function myOption($optionName)
    {
        $userId=0;
        if(Auth::guard('admin')->check())
            $userId = Auth::guard('admin')->user()->id;
        elseif (Auth::check())
            $userId = Auth::user()->id;

        return \App\Models\User::getUserOptions($userId,$optionName);

    }
}

if (!function_exists('addLog')) {
    function addLog($subject,$inputs=[],$errors=[])
    {
        LogActivity::addToLog($subject,$inputs,$errors);

    }
}

if (!function_exists('sep')) {
    function sep($number = 0){
        $number = fa2la($number);
        return is_numeric($number) ?  number_format($number) : 0;
    }
}
if (!function_exists('fa2la')) {
    function fa2la($string,$otherSep=[]){
        $string = str_replace(',','',$string);
        $otherSep = (array) $otherSep;
        if(is_array($otherSep) && count($otherSep)>0)
            foreach ($otherSep as $item)
                $string = str_replace($item,'',$string);
        $persian_num = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        $latin_num = range(0, 9);

        $string = str_replace($persian_num, $latin_num, $string);

        return $string;
    }
}

if (!function_exists('isLogin')) {
    function isLogin(){
        return session('user_id',function(){
            if(auth()->check()){
                session(['user_id'=>auth()->user()->id,['user'=>[
                    'name'=>auth()->user()->name,
                    'family'=>auth()->user()->family,
                    'mobile'=>auth()->user()->mobile,
                ]]]);
                return auth()->user()->id;
            }
            return 0;
        });
    }
}

if (!function_exists('secure')) {
    function secure($id,$field='id'){
        if(\auth('admin')->check())
            $otherData = \auth('admin')->user()->$field ?? 'dehnavi';
        elseif (auth()->check())
            $otherData = \auth()->user()->$field ?? 'dehnavi';
        return \App\Models\User::hashPass($id.$otherData);
    }
}




