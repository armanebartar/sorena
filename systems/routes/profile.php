<?php
Route::get('profile/orders/{status?}', '\App\Http\Controllers\profile\ProfileController@profile')->name('profile')->middleware('auth');
Route::get('profile/profile_order/{id}/{secure}', '\App\Http\Controllers\profile\ProfileController@profile_order')->name('profile_order_detail')->middleware('auth');


Route::get('profile/personalInfo', '\App\Http\Controllers\profile\ProfileController@personalInfo')->name('profile_personalInfo')->middleware('auth');
Route::post('profile/personalInfo/save', '\App\Http\Controllers\profile\ProfileController@personalInfoSave')->name('profile_personalInfoSave')->middleware('auth');
Route::post('profile/get_data', '\App\Http\Controllers\profile\ProfileController@get_data')->name('profile_get_data')->middleware('auth');
Route::post('profile/order/details', '\App\Http\Controllers\profile\ProfileController@order_details')->name('profile_order_details')->middleware('auth');
Route::post('profile/order/confirm_user', '\App\Http\Controllers\profile\ProfileController@confirm_user')->name('profile_confirm_user')->middleware('auth');

Route::get('profile/changePass', '\App\Http\Controllers\profile\ProfileController@changePass')->name('profile_changePass')->middleware('auth');
Route::get('profile_awards', '\App\Http\Controllers\profile\ProfileController@awards')->name('profile_awards')->middleware('auth');
Route::post('profile/changePass/save', '\App\Http\Controllers\profile\ProfileController@changePassSave')->name('profile_changePassSave')->middleware('auth');
Route::get('profile/affiliate', '\App\Http\Controllers\profile\ProfileController@affiliate')->name('profile_affiliate')->middleware('auth');
Route::post('profile/addAccount', '\App\Http\Controllers\profile\ProfileController@addAccount')->name('profile_addAccount')->middleware('auth');
Route::post('profile/transfer', '\App\Http\Controllers\profile\ProfileController@transfer')->name('profile_transfer')->middleware('auth');
Route::get('profile/favorites', '\App\Http\Controllers\profile\ProfileController@favorites')->name('profile_favorites')->middleware('auth');
Route::post('profile/favorites/remove', '\App\Http\Controllers\profile\ProfileController@favorite_remove')->name('profile_favorite_remove')->middleware('auth');
Route::get('profile/orders', '\App\Http\Controllers\profile\ProfileController@orders')->name('profile_orders')->middleware('auth');
Route::get('profile/order/{id}', '\App\Http\Controllers\profile\ProfileController@order')->name('profile_order')->middleware('auth');
Route::get('comments', '\App\Http\Controllers\profile\ProfileController@comments')->name('profile_comments')->middleware('auth');
Route::get('addresses', '\App\Http\Controllers\profile\ProfileController@addresses')->name('profile_addresses')->middleware('auth');
Route::post('addresses/add', '\App\Http\Controllers\profile\ProfileController@add_addresses')->name('profile_addresses_add')->middleware('auth');
Route::post('addresses/remove', '\App\Http\Controllers\profile\ProfileController@remove_addresses')->name('profile_addresses_remove')->middleware('auth');
Route::get('visits', '\App\Http\Controllers\profile\ProfileController@visits')->name('profile_visits')->middleware('auth');
Route::get('addComment/{product_id}', '\App\Http\Controllers\profile\ProfileController@addComment')->name('profile_addComment')->middleware('auth');
Route::get('addComment/{product_id}', '\App\Http\Controllers\profile\ProfileController@addComment')->name('profile_addComment')->middleware('auth');
