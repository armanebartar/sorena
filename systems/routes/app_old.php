<?php
//home
Route::get('u/{secure}', '\App\Http\Controllers\app\AppController@user_login_sms')->name('user_login_sms');
Route::get('o/{id}/{secure}', '\App\Http\Controllers\app\AppController@user_order_sms')->name('user_order_sms');
Route::get('app', '\App\Http\Controllers\app\AppController@home')->name('app');
Route::get('app/home', '\App\Http\Controllers\app\AppController@home');
Route::get('app/about', '\App\Http\Controllers\app\AppController@about')->name('app_about');
Route::get('app/contact', '\App\Http\Controllers\app\AppController@contact')->name('app_contact');

//basket
Route::get('app/add_basket', '\App\Http\Controllers\app\AppController@add_basket')->name('app_add_basket');
Route::post('app/add_basket/save', '\App\Http\Controllers\app\AppController@add_basket_save')->name('add_basket_save');

//profile
Route::get('app/confirm_mobile', '\App\Http\Controllers\app\AppController@confirm_mobile')->name('app_confirm_mobile');
Route::post('app/confirm_mobile/save', '\App\Http\Controllers\app\AppController@confirm_mobile_save')->name('app_confirm_mobile_save');
Route::get('app/profile', '\App\Http\Controllers\app\AppController@profile')->name('app_profile');
Route::post('app/profile/save', '\App\Http\Controllers\app\AppController@profile_save')->name('app_profile_save');


//register
Route::get('app/register', '\App\Http\Controllers\app\AppController@register')->name('app_register');
Route::get('app/forgot', '\App\Http\Controllers\app\AppController@forgot')->name('app_forgot');
Route::post('app/forgoted', '\App\Http\Controllers\app\AppController@forgoted')->name('app_forgoted');
Route::get('app/login', '\App\Http\Controllers\app\AppController@login')->name('app_login');
Route::post('app/logined', '\App\Http\Controllers\app\AppController@logined')->name('app_logined');
Route::get('app/logout', '\App\Http\Controllers\app\AppController@logout')->name('app_logout');

//admin
Route::get('app/admin/orders', '\App\Http\Controllers\app\AppController@admin_orders')->name('app_admin_orders');
Route::get('app/admin/order/{status}', '\App\Http\Controllers\app\AppController@admin_order')->name('app_admin_order');
Route::get('app/admin/order_details/{id}/{status}', '\App\Http\Controllers\app\AppController@admin_order_details')->name('app_admin_order_details');
Route::post('app/admin/save_change_order', '\App\Http\Controllers\app\AppController@admin_save_change_order')->name('app_admin_save_change_order');
Route::any('app/register_admin2', '\App\Http\Controllers\app\AppController@register_admin')->name('app_register_admin2');
Route::any('app/register_admin/save', '\App\Http\Controllers\app\AppController@register_admin_save')->name('app_register_admin_save');
Route::any('app/admin/add_order', '\App\Http\Controllers\app\AppController@admin_add_order')->name('app_admin_add_order');
Route::any('app/admin/add_order/save', '\App\Http\Controllers\app\AppController@admin_add_order_save')->name('app_admin_add_order_save');
Route::any('app/admin/add_order/temporary', '\App\Http\Controllers\app\AppController@admin_add_order_temporary')->name('app_admin_add_order_temporary');
Route::any('app/get_user_address', '\App\Http\Controllers\app\AppController@get_user_address')->name('app_get_user_address');


//user
Route::get('app/user/orders', '\App\Http\Controllers\app\AppController@user_orders')->name('app_user_orders');
Route::get('app/user/order/{status}', '\App\Http\Controllers\app\AppController@user_order')->name('app_user_order');
Route::get('app/user/order_details/{id}/{status}', '\App\Http\Controllers\app\AppController@user_order_details')->name('app_user_order_details');
Route::post('app/user/order_confirm', '\App\Http\Controllers\app\AppController@user_order_confirm')->name('app_user_order_confirm');
Route::post('app/user/save_change_order', '\App\Http\Controllers\app\AppController@user_save_change_order')->name('app_user_save_change_order');
Route::any('app/user/add_order/temporary', '\App\Http\Controllers\app\AppController@user_add_order_temporary')->name('app_user_add_order_temporary');
Route::any('app/user/cancel_order/{id}/{secure}', '\App\Http\Controllers\app\AppController@user_cancel_order')->name('app_user_user_cancel_order');
