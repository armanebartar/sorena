<?php
Route::get('','\App\Http\Controllers\admin\users\LoginController@loginAdmin')->name('home');
Route::get('loginAdmin3', '\App\Http\Controllers\admin\users\LoginController@loginAdmin');
Route::get('loginadmin', '\App\Http\Controllers\admin\users\LoginController@loginAdmin');
Route::get('loginAdmin', '\App\Http\Controllers\admin\users\LoginController@loginAdmin');
Route::get('loginadmin', '\App\Http\Controllers\admin\users\LoginController@loginAdmin');
Route::get('admin', '\App\Http\Controllers\admin\users\LoginController@loginAdmin')->name('loginAdmin');
Route::post('loginAdmin/login', '\App\Http\Controllers\admin\users\LoginController@loginAdminLogin')->name("loginAdmin2");

Route::prefix('panel/posts')->group(function () {
    Route::get('/s/{type}/{sort?}/{page?}', '\App\Http\Controllers\admin\posts\PostsController@index')->name("p_posts")->middleware("admin");
    Route::get('add/{type}/{id?}/{lang?}', '\App\Http\Controllers\admin\posts\PostsController@add')->name("p_posts_add")->middleware("admin");
    Route::post('addSave/{type}', '\App\Http\Controllers\admin\posts\PostsController@addSave')->name("p_posts_addSave")->middleware("admin");
    Route::get('edit/{type}/{id}/{lang?}', '\App\Http\Controllers\admin\posts\PostsController@add')->name("p_posts_edit")->middleware("admin");
    Route::post('editSave/{type}', '\App\Http\Controllers\admin\posts\PostsController@editSave')->name("p_posts_editSave")->middleware("admin");
    Route::post('remove/{type}', '\App\Http\Controllers\admin\posts\PostsController@remove')->name("p_posts_remove")->middleware("admin");
    Route::post('activated/{type}', '\App\Http\Controllers\admin\posts\PostsController@activated')->name("p_posts_activated")->middleware("admin");
    Route::post('saveThumb', '\App\Http\Controllers\admin\posts\PostsController@saveThumb')->name("p_posts_saveThumb");
    Route::post('saveImageAtt', '\App\Http\Controllers\admin\posts\PostsController@saveImageAtt')->name("p_posts_saveImageAtt");
    Route::post('saveFileAtt', '\App\Http\Controllers\admin\posts\PostsController@saveFileAtt')->name("p_posts_saveFileAtt");
    Route::post('saveAtt', '\App\Http\Controllers\admin\posts\PostsController@saveAtt')->name("p_posts_saveAtt");
    Route::post('get_attrs', '\App\Http\Controllers\admin\posts\PostsController@get_attrs')->name("p_posts_get_attrs");
    Route::post('discount_set', '\App\Http\Controllers\admin\posts\PostsController@discount_set')->name("p_posts_discount_set");
    Route::post('discount_deactive', '\App\Http\Controllers\admin\posts\PostsController@discount_deactive')->name("p_posts_discount_deactive");
    Route::post('stock_set', '\App\Http\Controllers\admin\posts\PostsController@stock_set')->name("p_posts_stock_set");
    Route::post('stock_remove', '\App\Http\Controllers\admin\posts\PostsController@stock_remove')->name("p_posts_stock_remove");

    Route::post('saveProperty', '\App\Http\Controllers\admin\posts\PostsController@saveProperty')->name("p_posts_saveProperty");

    Route::get('translate/add/{type}/{id}/{lang}/{secret}', '\App\Http\Controllers\admin\posts\PostsController@addTranslate')->name("p_translate_add")->middleware("admin");
//    Route::get('translate/add/{type}/{id}/{lang}/{secret}', '\App\Http\Controllers\admin\posts\TranslateController@addTranslate')->name("p_translate_add")->middleware("admin");
//    Route::post('translate/addSave/{type}', '\App\Http\Controllers\admin\posts\TranslateController@addSaveTranslate')->name("p_translate_addSave")->middleware("admin");
    Route::get('translate/ByGoogle/{postId}/{lang}/{type}/{secure}', '\App\Http\Controllers\admin\posts\TranslateController@translateByGoogle')->name("p_translate_byGoogle")->middleware("admin");

    Route::get('groups/{type}', '\App\Http\Controllers\admin\posts\CategoryController@categories')->name("p_posts_groups")->middleware("admin");
    Route::post('groups/addSave/{type}', '\App\Http\Controllers\admin\posts\CategoryController@addSaveGroup')->name("p_posts_groups_addSave")->middleware("admin");
    Route::post('groups/remove/{type}', '\App\Http\Controllers\admin\posts\CategoryController@removeGroup')->name("p_posts_groups_remove")->middleware("admin");
    Route::post('groups/editSave/{type}', '\App\Http\Controllers\admin\posts\CategoryController@editSaveGroup')->name("p_posts_groups_editSave")->middleware("admin");
    Route::post('groups/addLang/{type}', '\App\Http\Controllers\admin\posts\CategoryController@addLang')->name("p_posts_groups_addLang")->middleware("admin");
    Route::get('groups/edit_complete/{id?}', '\App\Http\Controllers\admin\posts\CategoryController@edit_complete')->name("p_posts_groups_edit_complete")->middleware("admin");
    Route::get('groups/edit_lang/{id?}/{lang?}', '\App\Http\Controllers\admin\posts\CategoryController@edit_lang')->name("p_posts_groups_edit_lang")->middleware("admin");
    Route::post('groups/edit_complete_save', '\App\Http\Controllers\admin\posts\CategoryController@edit_complete_save')->name("p_posts_groups_edit_complete_save")->middleware("admin");
    Route::post('groups/saveThumb', '\App\Http\Controllers\admin\posts\CategoryController@saveThumb')->name("p_posts_group_saveThumb")->middleware("admin");

    Route::get('brands/{type}', '\App\Http\Controllers\admin\posts\BrandController@index')->name("p_posts_brands")->middleware("admin");
    Route::post('brands/addSave/{type}', '\App\Http\Controllers\admin\posts\BrandController@addSave')->name("p_posts_brands_addSave")->middleware("admin");
    Route::post('brands/remove/{type}', '\App\Http\Controllers\admin\posts\BrandController@remove')->name("p_posts_brands_remove")->middleware("admin");
    Route::post('brands/editSave/{type}', '\App\Http\Controllers\admin\posts\BrandController@editSave')->name("p_posts_brands_editSave")->middleware("admin");
    Route::post('brands/addLang/{type}', '\App\Http\Controllers\admin\posts\BrandController@addLang')->name("p_posts_brands_addLang")->middleware("admin");
    Route::get('brands/edit_complete/{id?}', '\App\Http\Controllers\admin\posts\BrandController@edit_complete')->name("p_posts_brands_edit_complete")->middleware("admin");
    Route::get('brands/edit_lang/{id?}/{lang?}', '\App\Http\Controllers\admin\posts\BrandController@edit_lang')->name("p_posts_brands_edit_lang")->middleware("admin");
    Route::post('brands/edit_complete_save', '\App\Http\Controllers\admin\posts\BrandController@edit_complete_save')->name("p_posts_brands_edit_complete_save")->middleware("admin");
    Route::post('brands/saveThumb', '\App\Http\Controllers\admin\posts\BrandController@saveThumb')->name("p_posts_brands_saveThumb")->middleware("admin");

    Route::get('tags', '\App\Http\Controllers\admin\posts\TagsController@index')->name("p_tags")->middleware("admin");
    Route::post('tags/addSave', '\App\Http\Controllers\admin\posts\TagsController@addSave')->name("p_tags_addSave")->middleware("admin");
    Route::post('tags/remove', '\App\Http\Controllers\admin\posts\TagsController@remove')->name("p_tags_remove")->middleware("admin");
    Route::post('tags/editSave', '\App\Http\Controllers\admin\posts\TagsController@editSave')->name("p_tags_editSave")->middleware("admin");
    Route::post('tags/addLang', '\App\Http\Controllers\admin\posts\TagsController@addLang')->name("p_tags_addLang")->middleware("admin");
    Route::get('tags/edit_complete/{id?}', '\App\Http\Controllers\admin\posts\TagsController@edit_complete')->name("p_posts_tags_edit_complete")->middleware("admin");
    Route::get('tags/edit_lang/{id?}/{lang?}', '\App\Http\Controllers\admin\posts\TagsController@edit_lang')->name("p_posts_tags_edit_lang")->middleware("admin");


    Route::post('tags/edit_complete_save', '\App\Http\Controllers\admin\posts\TagsController@edit_complete_save')->name("p_posts_tags_edit_complete_save")->middleware("admin");
    Route::post('tags/saveThumb', '\App\Http\Controllers\admin\posts\TagsController@saveThumb')->name("p_posts_tag_saveThumb")->middleware("admin");


    Route::post('files/addSave', '\App\Http\Controllers\admin\posts\MediaController@addSave')->name("p_files_addSave")->middleware("admin");
    Route::post('files/getImages/{start?}', '\App\Http\Controllers\admin\posts\MediaController@getImages')->name("p_files_getImages")->middleware("admin");

    Route::get('specials/{type?}/{id?}/{secure?}', '\App\Http\Controllers\admin\posts\SpecialController@index')->name("p_posts_specials")->middleware("admin");
    Route::post('specials/posts/{type}', '\App\Http\Controllers\admin\posts\SpecialController@posts')->name("p_posts_specials_posts")->middleware("admin");
    Route::post('specials/addSave/{type}', '\App\Http\Controllers\admin\posts\SpecialController@addSaveGroup')->name("p_posts_specials_addSave")->middleware("admin");
    Route::post('specials/remove/{type}', '\App\Http\Controllers\admin\posts\SpecialController@removeGroup')->name("p_posts_specials_remove")->middleware("admin");
    Route::post('specials/editSave/{type}', '\App\Http\Controllers\admin\posts\SpecialController@editSaveGroup')->name("p_posts_specials_editSave")->middleware("admin");
    Route::post('specials/addLang/{type}', '\App\Http\Controllers\admin\posts\SpecialController@addLang')->name("p_posts_specials_addLang")->middleware("admin");
    Route::post('saveGroup', '\App\Http\Controllers\admin\posts\SpecialController@saveGroup')->name("p_shop_special_saveGroup")->middleware("admin");
});


// ADMIN -> DASHBOARD
Route::prefix('panel')->group(function () {
//    Route::get('', function () {dd('test');})->name("p_dashboard")->middleware("admin");
    Route::get('', '\App\Http\Controllers\admin\DashboardController@index')->name("p_dashboard")->middleware("admin");
    Route::get('dashboard', function (){return redirect()->route('p_dashboard');});
    Route::post('getNotify', '\App\Http\Controllers\admin\DashboardController@getNotify')->name("p_getNotify")->middleware("admin");
    Route::post('p_upload_image', '\App\Http\Controllers\admin\DashboardController@p_upload_image')->name("p_upload_image")->middleware("admin");
    Route::get('ftpHome', '\App\Http\Controllers\admin\DashboardController@ftpHome')->name("p_ftpHome")->middleware("admin");
    Route::post('ftpSave', '\App\Http\Controllers\admin\DashboardController@ftpSave')->name("p_ftpSave")->middleware("admin");
    Route::post('uploadImage', '\App\Http\Controllers\admin\DashboardController@uploadImage')->name("p_uploadImage")->middleware("admin");
    Route::post('uploadImageByUrl', '\App\Http\Controllers\admin\DashboardController@uploadImageByUrl')->name("p_uploadImageByUrl")->middleware("admin");
    Route::post('uploadFile', '\App\Http\Controllers\admin\DashboardController@uploadFile')->name("p_uploadFile")->middleware("admin");
    Route::post('uploadFilePost', '\App\Http\Controllers\admin\ArticlesController@uploadFile')->name("p_uploadFilePost")->middleware("admin");

});

// ADMIN -> OPTIONS
Route::prefix('panel/options')->group(function () {
    Route::get('/l/{lang?}', '\App\Http\Controllers\admin\settings\OptionsController@index')->name("p_options")->middleware("admin");
    Route::post('save', '\App\Http\Controllers\admin\settings\OptionsController@save')->name("p_optionsSave")->middleware("admin");
    Route::post('save_image', '\App\Http\Controllers\admin\settings\OptionsController@save_image')->name("p_options_save_image")->middleware("admin");

});

Route::prefix('panel/banner')->group(function () {
    Route::get('banners/{lang?}', '\App\Http\Controllers\admin\settings\BannerController@home')->name("p_banner")->middleware("admin");
    Route::post('add', '\App\Http\Controllers\admin\settings\BannerController@add')->name("p_banner_add")->middleware("admin");
    Route::post('edit', '\App\Http\Controllers\admin\settings\BannerController@edit')->name("p_banner_edit")->middleware("admin");
    Route::post('remove', '\App\Http\Controllers\admin\settings\BannerController@remove')->name("p_banner_remove")->middleware("admin");
});


Route::prefix('panel/slider')->group(function () {
    Route::get('slides/{lang?}', '\App\Http\Controllers\admin\settings\SliderController@slider')->name("p_slider")->middleware("admin");
    Route::post('addSlider', '\App\Http\Controllers\admin\settings\SliderController@addSlider')->name("p_slider_add")->middleware("admin");
    Route::post('editSlider', '\App\Http\Controllers\admin\settings\SliderController@editSlider')->name("p_slider_edit")->middleware("admin");
    Route::post('removeSlider', '\App\Http\Controllers\admin\settings\SliderController@removeSlider')->name("p_slider_remove")->middleware("admin");


});
Route::prefix('panel/m_comments')->group(function () {
    Route::get('list/{lang?}', '\App\Http\Controllers\admin\settings\M_commentsController@list')->name("p_m_comments")->middleware("admin");
    Route::post('addSlider', '\App\Http\Controllers\admin\settings\M_commentsController@add')->name("p_m_comments_add")->middleware("admin");
    Route::post('editSlider', '\App\Http\Controllers\admin\settings\M_commentsController@edit')->name("p_m_comments_edit")->middleware("admin");
    Route::post('removeSlider', '\App\Http\Controllers\admin\settings\M_commentsController@remove')->name("p_m_comments_remove")->middleware("admin");


});

Route::prefix('panel/menu')->group(function () {
    Route::get('l/{lang?}/{group?}', '\App\Http\Controllers\admin\settings\MenuController@index')->name("p_menu")->middleware("admin");
    Route::post('addSave/{lang}/{group?}', '\App\Http\Controllers\admin\settings\MenuController@addSave')->name("p_menu_addSave")->middleware("admin");
    Route::post('remove/{lang?}/{group?}', '\App\Http\Controllers\admin\settings\MenuController@remove')->name("p_menu_remove")->middleware("admin");
    Route::post('editSave/{lang?}/{group?}', '\App\Http\Controllers\admin\settings\MenuController@editSave')->name("p_menu_editSave")->middleware("admin");
});

// ADMIN -> USERS
Route::prefix('panel/users')->group(function () {
    Route::get('', '\App\Http\Controllers\admin\users\UsersController@index')->name("p_users")->middleware("admin");
    Route::post('activated', '\App\Http\Controllers\admin\users\UsersController@activated')->name("p_users_activated")->middleware("admin");
    Route::post('addSave', '\App\Http\Controllers\admin\users\UsersController@addSave')->name("p_users_addSave")->middleware("admin");
    Route::post('remove', '\App\Http\Controllers\admin\users\UsersController@remove')->name("p_users_remove")->middleware("admin");
    Route::post('show', '\App\Http\Controllers\admin\users\UsersController@show')->name("p_users_show")->middleware("admin");

});
Route::prefix('panel/factors')->group(function () {
    Route::get('/list/{status?}', '\App\Http\Controllers\admin\orders\FactorsController@index')->name("p_factors")->middleware("admin");
    Route::any('/getFactorDetails', '\App\Http\Controllers\admin\orders\FactorsController@getFactorDetails')->name("p_factors_getFactorDetails")->middleware("admin");
    Route::post('await', '\App\Http\Controllers\admin\orders\FactorsController@await')->name("p_factors_await");
    Route::post('editSaveInterim', '\App\Http\Controllers\admin\orders\FactorsController@editSaveInterim')->name("p_factors_editSaveInterim")->middleware("admin");
    Route::post('remove', '\App\Http\Controllers\admin\orders\FactorsController@remove')->name("p_factors_remove")->middleware("admin");
    Route::post('get_user_data', '\App\Http\Controllers\admin\orders\FactorsController@get_user_data')->name("p_factors_get_user_data")->middleware("admin");
    Route::get('add', '\App\Http\Controllers\admin\orders\FactorsController@add')->name("p_factors_add")->middleware("admin");
    Route::post('addSave', '\App\Http\Controllers\admin\orders\FactorsController@addSave')->name("p_factors_addSave")->middleware("admin");


    Route::post('/filter', '\App\Http\Controllers\admin\orders\FactorsController@filter')->name("p_factors_filter")->middleware("admin");
    Route::post('/filter2', '\App\Http\Controllers\admin\orders\FactorsController@filter2')->name("p_factors_filter2")->middleware("admin");
    Route::any('/getFactorDetailsEdit', '\App\Http\Controllers\admin\orders\FactorsController@getFactorDetailsEdit')->name("p_factors_getFactorDetailsEdit")->middleware("admin");
     Route::get('edit/{id}', '\App\Http\Controllers\admin\orders\FactorsController@edit')->name("p_factors_edit")->middleware("admin");
    Route::post('editSave', '\App\Http\Controllers\admin\orders\FactorsController@editSave')->name("p_factors_editSave")->middleware("admin");
    Route::any('getCustomers', '\App\Http\Controllers\admin\orders\FactorsController@getCustomers')->name("p_factors_getCustomers")->middleware("admin");
    Route::any('getMessages', '\App\Http\Controllers\admin\orders\FactorsController@getMessages')->name("p_factors_getMessages")->middleware("admin");
    Route::any('sendMessage', '\App\Http\Controllers\admin\orders\FactorsController@sendMessage')->name("p_factors_sendMessage")->middleware("admin");
    Route::any('getNewMessages', '\App\Http\Controllers\admin\orders\FactorsController@getNewMessages')->name("p_factors_getNewMessages")->middleware("admin");
    Route::any('packaging/{id?}', '\App\Http\Controllers\admin\orders\FactorsController@packaging')->name("p_factors_packaging")->middleware("admin");
    Route::post('saveBijak', '\App\Http\Controllers\admin\orders\FactorsController@saveBijak')->name("p_factors_saveBijak")->middleware("admin");
    Route::post('getProducts/{id?}', '\App\Http\Controllers\admin\orders\FactorsController@getProducts')->name("p_factors_getProducts");
    Route::post('getBijak', '\App\Http\Controllers\admin\orders\FactorsController@getBijak')->name("p_factors_getBijak");
    Route::post('getRemind', '\App\Http\Controllers\admin\orders\FactorsController@getRemind')->name("p_factors_getRemind");
    Route::post('getTrans', '\App\Http\Controllers\admin\orders\FactorsController@getTrans')->name("p_factors_getTrans");
    Route::post('getUserTrans', '\App\Http\Controllers\admin\orders\FactorsController@getUserTrans')->name("p_factors_getUserTrans");
    Route::post('addTransToFactor', '\App\Http\Controllers\admin\orders\FactorsController@addTransToFactor')->name("p_factors_addTransToFactor");
    Route::post('addTrans_andToFactor', '\App\Http\Controllers\admin\orders\FactorsController@addTrans_andToFactor')->name("p_factors_addTrans_andToFactor");
    Route::post('change_checkout', '\App\Http\Controllers\admin\orders\FactorsController@change_checkout')->name("p_factors_change_checkout");
    Route::post('delivery', '\App\Http\Controllers\admin\orders\FactorsController@delivery')->name("p_factors_delivery");
    Route::post('checkOrder', 'panel\DashboardController@checkOrder')->name("p_factors_checkOrder")->middleware("admin");

//    Route::get('edit/{id}', '\App\Http\Controllers\admin\orders\FactorsController@edit')->name("p_factors_edit")->middleware("admin");
//    Route::post('editSave', '\App\Http\Controllers\admin\orders\FactorsController@editSave')->name("p_factors_editSave")->middleware("admin");
//    Route::post('activated', '\App\Http\Controllers\admin\orders\FactorsController@activated')->name("p_factors_activated")->middleware("admin");
//    Route::post('activated', '\App\Http\Controllers\admin\orders\FactorsController@activated')->name("p_factors_activated")->middleware("admin");
});


// ADMIN -> COMMENTS
Route::prefix('panel/Comments')->group(function () {
    Route::get('c/{type?}/{page?}', '\App\Http\Controllers\admin\comments\CommentsController@index')->name("p_comments")->middleware("admin");
//    Route::get('contact/{page?}', '\App\Http\Controllers\admin\comments\CommentsController@index')->name("p_comments")->middleware("admin");
    Route::get('userComments/{userId}/{type?}', '\App\Http\Controllers\admin\comments\CommentsController@userComments')->name("p_comments_userComments")->middleware("admin");
//    Route::get('contacts/{postId}/{type?}', '\App\Http\Controllers\admin\comments\CommentsController@contact')->name("p_comments_contact")->middleware("admin");
    Route::post('seen', '\App\Http\Controllers\admin\comments\CommentsController@seen')->name("p_comments_seen")->middleware("admin");
    Route::post('answer', '\App\Http\Controllers\admin\comments\CommentsController@answer')->name("p_comments_answer")->middleware("admin");
    Route::post('addSave', '\App\Http\Controllers\admin\comments\CommentsController@activated')->name("p_comments_activated")->middleware("admin");
    Route::post('remove', '\App\Http\Controllers\admin\comments\CommentsController@remove')->name("p_comments_remove")->middleware("admin");
    Route::post('editSave', '\App\Http\Controllers\admin\comments\CommentsController@editSave')->name("p_comments_editSave")->middleware("admin");
});

Route::prefix('panel/contacts')->group(function () {
    Route::get('c/', '\App\Http\Controllers\admin\comments\ContactsController@index')->name("p_contacts")->middleware("admin");
    Route::get('userComments/{userId}/{type?}', '\App\Http\Controllers\admin\comments\ContactsController@userComments')->name("p_contacts_userComments")->middleware("admin");
    Route::post('seen', '\App\Http\Controllers\admin\comments\ContactsController@seen')->name("p_contacts_seen")->middleware("admin");
    Route::post('addSave', '\App\Http\Controllers\admin\comments\ContactsController@activated')->name("p_contacts_activated")->middleware("admin");
    Route::post('remove', '\App\Http\Controllers\admin\comments\ContactsController@remove')->name("p_contacts_remove")->middleware("admin");
});





// ADMIN -> STAFF
Route::prefix('panel/staff')->group(function () {
    Route::get('', '\App\Http\Controllers\admin\users\StaffController@index')->name("p_staff")->middleware("admin");
    Route::post('activated', '\App\Http\Controllers\admin\users\StaffController@activated')->name("p_staff_activated")->middleware("admin");
    Route::post('addSave', '\App\Http\Controllers\admin\users\StaffController@addSave')->name("p_staff_addSave")->middleware("admin");
    Route::post('remove', '\App\Http\Controllers\admin\users\StaffController@remove')->name("p_staff_remove")->middleware("admin");
    Route::get('permissions/{userId}', '\App\Http\Controllers\admin\users\StaffController@permissions')->name("p_staff_permissions")->middleware("admin");
    Route::post('permissionsSave', '\App\Http\Controllers\admin\users\StaffController@permissionsSave')->name("p_staff_permissionsSave")->middleware("admin");

});

// ADMIN -> PROFILE
Route::prefix('panel/profile')->group(function () {
    Route::get('edit', '\App\Http\Controllers\admin\users\ProfileController@edit')->name("p_profile_edit")->middleware("admin");
    Route::get('pass', '\App\Http\Controllers\admin\users\ProfileController@pass')->name("p_profile_pass")->middleware("admin");
    Route::post('editSave', '\App\Http\Controllers\admin\users\ProfileController@editSave')->name("p_profile_editSave")->middleware("admin");
    Route::post('changePass', '\App\Http\Controllers\admin\users\ProfileController@changePass')->name("p_profile_changePass")->middleware("admin");

});






// DEVELOPER
Route::prefix('panel/developer')->group(function () {
    Route::get('', '\App\Http\Controllers\admin\DeveloperController@index')->name("p_dev")->middleware("admin");
    Route::post('saveController', '\App\Http\Controllers\admin\DeveloperController@saveController')->name("p_dev_saveController")->middleware("admin");
    Route::post('saveModel', '\App\Http\Controllers\admin\DeveloperController@saveModel')->name("p_dev_saveModel")->middleware("admin");
    Route::get('artisan/{command}', '\App\Http\Controllers\admin\DeveloperController@artisanCommand')->name("p_dev_artisanCommand")->middleware("admin");
    Route::get('options', '\App\Http\Controllers\admin\DeveloperController@options')->name("p_dev_options")->middleware("admin");
    Route::post('optionsSave', '\App\Http\Controllers\admin\DeveloperController@optionsSave')->name("p_dev_optionsSave")->middleware("admin");
    Route::post('groupOptionSave', '\App\Http\Controllers\admin\DeveloperController@groupOptionSave')->name("p_dev_groupOptionSave")->middleware("admin");
    Route::get('addPostType', '\App\Http\Controllers\admin\DeveloperController@addPostType')->name("p_dev_addPostType")->middleware("admin");
    Route::post('addPostTypeSave', '\App\Http\Controllers\admin\DeveloperController@addPostTypeSave')->name("p_dev_addPostTypeSave")->middleware("admin");
});
