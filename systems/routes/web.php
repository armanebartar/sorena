<?php

use Illuminate\Support\Facades\Route;

include_once "auth.php";
include_once "admin.php";
include_once "profile.php";
include_once "app.php";


Route::get('','\App\Http\Controllers\HomeController@home')->name('home');
Route::post('send_verify_app','\App\Http\Controllers\HomeController@send_verify_app')->name('send_verify_app');
Route::get('home',function (){
    return redirect()->route('home');
});
Route::get('brand/{type}/{id}/{slug?}/{page?}','\App\Http\Controllers\HomeController@brand')->name('brand');

Route::get('category/{type}/{id}/{slug?}/{page?}','\App\Http\Controllers\PostController@category')->name('category');
Route::get('academy/{id}/{slug?}/{page?}','\App\Http\Controllers\PostController@academy')->name('academy');
Route::get('shop/{id}/{slug?}/{page?}','\App\Http\Controllers\PostController@shop')->name('shop');
Route::get('news/{id}/{slug?}/{page?}','\App\Http\Controllers\PostController@news')->name('news');

Route::get('product/{id}/{slug?}/{lang?}', '\App\Http\Controllers\PostController@shop_show')->name('shop_show');
Route::get('training/{id}/{slug?}/{lang?}', '\App\Http\Controllers\PostController@academy_show')->name('academy_show');
Route::get('news_show/{id}/{slug?}/{lang?}', '\App\Http\Controllers\PostController@news_show')->name('news_show');
Route::get('show/{type}/{id}/{slug?}/{lang?}', '\App\Http\Controllers\PostController@show_post')->name('show_post');
Route::post('get_post_stock', '\App\Http\Controllers\PostController@get_post_stock')->name('get_post_stock');

Route::post('search_product', '\App\Http\Controllers\PostController@search_product')->name('search_product');

Route::post('addToFavorite', '\App\Http\Controllers\HomeController@addToFavorite')->name('addToFavorite');
Route::post('add_to_basket', '\App\Http\Controllers\BasketController@add_to_basket')->name('add_to_basket');
Route::post('add_to_basket_ajax', '\App\Http\Controllers\BasketController@add_to_basket_ajax')->name('add_to_basket_ajax');
Route::post('update_basket_ajax', '\App\Http\Controllers\BasketController@update_basket_ajax')->name('update_basket_ajax');
Route::post('get_basket_details_ajax', '\App\Http\Controllers\BasketController@get_basket_details_ajax')->name('get_basket_details_ajax');
Route::post('get_basket_details', '\App\Http\Controllers\BasketController@get_basket_details')->name('get_basket_details');

Route::get('basket_show', '\App\Http\Controllers\BasketController@basket_show')->name('basket_show');

Route::post('basket_address', '\App\Http\Controllers\AddressController@basket_address')->name('basket_address');
Route::get('basket_address', '\App\Http\Controllers\AddressController@basket_address_get')->name('basket_address');
Route::post('basket_add_address', '\App\Http\Controllers\AddressController@basket_add_address')->name('basket_add_address');
Route::post('basket_edit_address', '\App\Http\Controllers\AddressController@basket_edit_address')->name('basket_edit_address');
Route::post('basket_change_address', '\App\Http\Controllers\AddressController@basket_change_address')->name('basket_change_address');
Route::post('basket_remove_address', '\App\Http\Controllers\AddressController@basket_remove_address')->name('basket_remove_address');
Route::post('getCities', '\App\Http\Controllers\HomeController@getCities')->name('getCities');

Route::get('basket_payment', '\App\Http\Controllers\PaymentController@basket_payment')->name('basket_payment');
Route::any('payment', '\App\Http\Controllers\PaymentController@payment')->name('payment');
Route::any('payment_callback/{gateway}/{code}/{secure}', '\App\Http\Controllers\PaymentController@payment_callback')->name('payment_callback');


//old routes
Route::any('subscribe', 'HomeController@subscribe')->name('subscribe');
Route::post('comment_save','\App\Http\Controllers\CommentController@comment_save')->name('comment_save');
Route::get('search/{page?}','\App\Http\Controllers\HomeController@search')->name('search');
Route::get('search2/{search}/{page?}','\App\Http\Controllers\HomeController@search2')->name('search2');
Route::get('about_us','\App\Http\Controllers\HomeController@about_us')->name('about_us');
Route::get('contact_us','\App\Http\Controllers\HomeController@contact_us')->name('contact_us');



Route::any('search1', 'HomeController@search1')->name('search1');
Route::post('searchAjax', 'HomeController@searchAjax')->name('searchAjax');
