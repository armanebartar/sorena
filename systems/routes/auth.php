<?php

Route::get('captcha/{captchaName}/{count?}/{level?}','\App\Http\Controllers\HomeController@captcha')->name('captcha');

Route::prefix('register')->group(function () {
    Route::get('', '\App\Http\Controllers\Auth\RegisterController@index')->name("register");
    Route::post('', '\App\Http\Controllers\Auth\RegisterController@save')->name("register");
    Route::get('verify', '\App\Http\Controllers\Auth\RegisterController@verify')->name("register_verify");
    Route::post('verify', '\App\Http\Controllers\Auth\RegisterController@verify_save')->name("register_verify");
    Route::get('resend', '\App\Http\Controllers\Auth\RegisterController@resend')->name("register_resend");
});
Route::prefix('login')->group(function () {
    Route::get('', '\App\Http\Controllers\Auth\LoginController@index')->name("login");
    Route::post('', '\App\Http\Controllers\Auth\LoginController@login')->name("login");
});

Route::prefix('forgot')->group(function () {
    Route::get('', '\App\Http\Controllers\Auth\ForgotController@index')->name("forgot");
    Route::post('', '\App\Http\Controllers\Auth\ForgotController@checkMobile')->name("forgot");
    Route::get('verify', '\App\Http\Controllers\Auth\ForgotController@verifyPage')->name("forgot_verify");
    Route::post('verify', '\App\Http\Controllers\Auth\ForgotController@checkCode')->name("forgot_verify");
});

Route::any('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name("logout");
