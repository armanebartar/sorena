<?php
namespace App\Helpers;

use App\Models\Factor;
use App\Models\Payment;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class Helpers{
    private static $_mims = [
        'image'=>['image/jpeg','image/png','image/gif','image/xpng','image/bmp','image/jpg'],
        'pdf'=>['application/pdf'],
        'mpeg'=>['video/mpeg'],
        'mp3'=>['audio/mpeg'],
        'mp4'=>['video/mp4'],
        'excel'=>['application/vnd.ms-excel','application/msexcel','application/x-msexcel','application/x-ms-excel','application/x-excel','application/x-dos_ms_excel','application/xls','application/x-xls','application/vnd.ms-excel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
        'ppt'=>['application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','application/vnd.openxmlformats-officedocument.presentationml.template','application/vnd.openxmlformats-officedocument.presentationml.slideshow','application/vnd.ms-powerpoint.addin.macroEnabled.12','application/vnd.ms-powerpoint.presentation.macroEnabled.12','application/vnd.ms-powerpoint.template.macroEnabled.12','application/vnd.ms-powerpoint.slideshow.macroEnabled.12'],
        'doc'=>['application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.openxmlformats-officedocument.wordprocessingml.template','application/vnd.ms-word.document.macroEnabled.12'],
        'csv'=>['text/csv','application/csv','application/x-csv','text/comma-separated-values','text/x-comma-separated-values','text/tab-separated-values'],
        'json'=>['application/json','application/x-javascript','text/javascript','text/x-javascript','text/x-json'],
        'rar'=>['application/x-rar-compressed','application/octet-stream'],
        'txt'=>['text/plain'],
        'xml'=>['text/xml','application/xml'],
        'zip'=>['application/zip','application/x-zip-compressed','application/octet-stream','multipart/x-zip'],
    ];
    public static function sendSms($mobile,$message){
        return Sms::send($mobile,$message);
    }
    public static function sendSmsFast($mobile,$template_id,$parameters){
       return Sms::send_fast($mobile,$template_id,$parameters);
    }

    public static function db_secure_key($table,$min,$max){
        $code = Helpers::randString($min,$max);
        if($table == 'payment') {
            if (Payment::where('code', $code)->count() > 0) {
                $code = Helpers::db_secure_key($table,$min, $max);
            }
        }elseif ($table == 'factor'){
            if (Factor::where('secret_key', $code)->count() > 0) {
                $code = Helpers::db_secure_key($table,$min, $max);
            }
        }

        return $code;
    }
    public static function sendEmail($mobile,$message){


            ini_set('max_execution_time', 300);
            $userEmail = "mohdeh971@gmail.com";
            Mail::send('Emails.ChangeUserPass',
                array(
                    'Token' => 'test',
                    'ID' => '10'
                ),
                function ($message) use ($userEmail) {
                    $message->from('holandkala1@gmail.com', "فروشگاه اینترنتی هلندکالا");
                    $message->sender('holandkala1@gmail.com', "فروشگاه اینترنتی هلندکالا");
                    $message->subject("فروشگاه اینترنتی هلندکالا | بازیابی رمز عبور");
                    $message->to($userEmail,"فروشگاه اینترنتی هلند   | بازیابی رمز عبور");
                });

    }

    public static function captcha($sessionName,$count = 2,$dificalty = 1,$bg = [255 , 255 , 255]){
        $count = $count<2 ? 2 : $count;
        $chars = array();
        for($i=0;$i<$count;$i++){
            if($dificalty == 1) {
                do {
                    $ascii = rand(48, 90);
                } while ($ascii > 57);
                $chars[$i] = chr($ascii);
            }elseif($dificalty == 2){
                do {
                    $ascii = rand(48, 90);
                } while ($ascii < 65);
                $chars[$i] = chr($ascii);
            }else{
                do {
                    $ascii = rand(48, 90);
                } while ($ascii > 57 and $ascii < 65);
                $chars[$i] = chr($ascii);
            }
        }
        $code='';
        for ($i=0;$i<$count;$i++)
            $code .= $chars[$i] ;

        session([$sessionName=>strtolower($code)]);
//        $r->session()->put('myCaptcha',strtolower($code));
        $image = imagecreate($count*24 , 40);
        $green = imagecolorallocate($image , $bg[0] , $bg[1] , $bg[2]);
        for($i=0;$i<$count;$i++) {
            $angle = rand(-20 , 20);
            $size = rand(17, 22);
            $space = rand(18 , 22);
            $height = rand(27 , 33);
            $color = imagecolorallocate($image , rand(0 , 255) , rand(0 , 100) , rand(0 , 255));
            imagefttext($image , $size , $angle , $i * $space + 5 , $height , $color, ROOT."assets/fonts/ariblk.ttf", $chars[$i]);
            imageline($image , rand(0 , 119) , rand(0 , 39) , rand(0 , 119) , rand(0 , 39) , $color);
        }
        imagepng($image);
    }

    public static function checkFileAjax($FILE,$path,$size=10,$mimeTypes=['pdf','excel']){
        $type = $FILE['type'];
//        dd($type);
        if($FILE['size'] > ($size * 1024*1024))
            return ['res'=>1,'myAlert'=>'حجم فایل زیاد است','mySuccess'=>''];
        $mimeTypes = $mimeTypes=='all' ? ['image','pdf','mpeg','mp3','mp4','excel','ppt','doc','csv','json','rar','txt','xml','zip'] : $mimeTypes;

        $mime = '';
        foreach ($mimeTypes as $mimeType){
            if($mime=='' && isset(self::$_mims[$mimeType])){
                foreach (self::$_mims[$mimeType] as $mim)
                    if($mim == $type){
                        $mime = $mimeType;
                        break;
                    }
            }
        }
        if($mime == '')
            return ['res'=>1,'myAlert'=>'فایل انتخابی فرمت مناسبی ندارد.','mySuccess'=>''];


        $file_type = strtolower(substr($FILE['name'],strrpos($FILE['name'],'.')+1));
        $name = 'upl_' . time() . Helpers::randString(2,10) . '.' . $file_type;

        move_uploaded_file($FILE['tmp_name'],ROOT.$path.$name);
        $thisFile = $path.$name;
        if(!is_file(ROOT.$thisFile))
            return ['res'=>1,'myAlert'=>'عملیات با شکست مواجه گردید','mySuccess'=>''];
        return ['res'=>10,'file'=>$thisFile,'url'=>url($thisFile),'fileName'=>$name,'path'=>$path,'mime'=>$mime];

    }
    public static function checkImageAjax($request,$fieldName,$path,$size=10,$mime='image'){

        if(!$request->hasFile($fieldName))
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک تصویر انتخاب نمایید'];

        $file = Helpers::checkAndSaveImage($request,$fieldName ,$path,[[64,64],[255,255]],$size*1024);
        if($file == 'notFileFound')
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک فایل انتخاب نمایید..'];

        if($file == 'isLong')
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'فایل انتخابی حجیم است. حداکثر حجم مجاز : '.$size.' مگابایت'];

        if($file == 'notImage')
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'فایل انتخابی یک عکس معتبر نیست'];


        $thisFile = $path.$file;
        if(!is_file(ROOT.$thisFile))
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'عملیات با خطا مواجه گردید.'];
        return ['res'=>10,'file'=>$thisFile,'url'=>url($thisFile),'fileName'=>$file,'path'=>$path,'thumb'=>url($path.'64_64_'.$file)];
    }

    public static function checkAndSaveImage($request,$fieldName,$path,$sizes=[[64,64],[200,200]],$maxSize=500,$oldImageName=null){
        if($request->hasFile($fieldName)){

            $fields[$fieldName]='image|mimetypes:image/jpeg,image/png|mimes:jpeg,png,jpg,JPEG,PNG,JPG';
            $validation = Validator::make($request->all(), $fields);
            if (!$validation->fails()) {
                $fields[$fieldName]='max:'.$maxSize;
                $validation = Validator::make($request->all(), $fields);
                if (!$validation->fails()) {
                    $file = $request->file($fieldName);
                    $name = 'upl_' . time() . rand(1, 100) . '.' . strtolower($file->getClientOriginalExtension());

                    $path = trim($path, '/');

                    if (!is_dir($path))
                        mkdir($path);
                    $path .= '/';

                    if (!is_null($oldImageName) && is_file($path . $oldImageName)) {
                        unlink($path . $oldImageName);
                        foreach ($sizes as $k => $size) {
                            if (is_file($path . $size[0] . '_' . $size[1] . '_' . $oldImageName))
                                unlink($path . $size[0] . '_' . $size[1] . '_' . $oldImageName);
                        }
                    }

                    $request->{$fieldName}->move(ROOT.$path, $name);

                    if(is_array($sizes))
                    foreach ($sizes as $k => $size) {
                        if(isset($size[0]) && isset($size[1])) {
                            Image::make($path . $name)->resize($size[0], $size[1])->save($path . $size[0] . '_' .$size[1] . '_' . $name);
                        }
                    }
                    return $name;
                }else
                    return 'isLong';
            }else
                return 'notImage';
        }else
            return 'notFileFound';
    }
    public static function user_agent_parser($agent){
        include_once __DIR__.'/user_agent/parser.php';
        return user_agent_parser($agent);
    }

    public static function checkAndSaveFile($file,$path='pathType',$mimeTypes=['image','pdf'],$maxSize=500,$thumbnailSizes=[64,64]){

//        dd($file,json_decode($file),1);
        if(!method_exists($file,'getClientSize'))
            return 'notFileValid';
        $mimeTypesValid = self::$_mims;

        $mimeTypes = $mimeTypes=='all' ? ['image','pdf','mpeg','mp3','mp4','excel','ppt','doc','csv','json','rar','txt','xml','zip'] : $mimeTypes;
        $mimeTypes = (array) $mimeTypes;

        $types = [];
        foreach ($mimeTypes as $mimeType){
            $mimeType = strtolower($mimeType);
            if(isset($mimeTypesValid[$mimeType]))
                $types[$mimeType] = $mimeTypesValid[$mimeType];
        }

        if($types == []) {
            echo  'این میم تایپ(ها) تعریف نشده است.';
            dd($mimeTypes);
        }


            if($file->getClientSize()/1024 > $maxSize)
                return ['isLongFile',$file->getClientOriginalName()];

            $fileType = '';
            foreach ($types as $kkk=>$ttt){
                if(in_array($file->getClientMimeType(),$ttt))
                    $fileType = $kkk;
            }
            if($fileType == '')
                return ['mimeNotValid',$file->getClientOriginalName()];

            $name = 'upl_' . time() . Helpers::randString(2,10) . '.' . strtolower($file->getClientOriginalExtension());



            $path = $path=='pathType' || $path=='' ? 'files/'.$fileType : $path;
            $path = trim($path,'/');
            if(!is_dir($path))
                mkdir($path);
            $path.='/';

            $file->move($path, $name);

            if ($fileType == 'image') {
                Image::make($path . $name)->resize($thumbnailSizes[0], $thumbnailSizes[1])->save($path . $thumbnailSizes[0].'_' . $name);
            }
            return [$name,$fileType,$path];

//        }else
//            return 'notFileFound';
    }

    public static function showErrors($removeError = true,$cols='col-md-6 col-lg-5'){
        if($errors = session('errors', false)){
            echo  '<ul class= "errorsAction '.$cols.'">';
            foreach($errors as $error){
                echo "<li>$error</li>";
            }
            echo  "</ul>";
        }
        if($removeError)
            session(['errors'=> []]);
    }

    public static function showWarnings($removeWarnings = true){
        if($warnings = session('warnings', false)){
            echo  '<ul class= "warningsAction col-md-6 col-lg-5">';
            foreach($warnings as $warning){
                echo "<li>$warning</li>";
            }
            echo  "</ul>";
        }
        if($removeWarnings)
            session(['errors'=> []]);
    }

    public static function pagination($page,$total_pages,$limit,$targetpage='#',$calsses=[],$adjacents=2){
        if ($page == 0) $page = 1;
        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total_pages/$limit) ;
        $lpm1 = $lastpage - 1;

        $pagination = '';
        $ulClass = $calsses['ul'] ?? '';
        $liClass = $calsses['li'] ?? '';
        $aClass = $calsses['a'] ?? '';
        $activeClass = $calsses['active'] ?? '';
        if($lastpage > 1) {
            $pagination .= "<ul  class=\"{$ulClass}\">";
            //کلید قبلی
            if ($page > 1)
                $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$prev\">&laquo;</a></li>";
            else
                $pagination .= "<li class='{$liClass}'><a class='{$aClass}' class=\"disabled\" href=\"#\">&laquo;</a></li>";

            //صفحات
            if ($lastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='{$liClass} {$activeClass}'><a class='{$aClass}' href=\"#\">$counter</a></li>";
                    else
                        $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$counter\">$counter</a></li>";
                }
            }
            elseif ($lastpage > 5 + ($adjacents * 2)) {

                if ($page < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page)
                            $pagination .= "<li class='{$liClass} {$activeClass}'><a class='{$aClass}' href=\"#\">$counter</a></li>";
                        else
                            $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$counter\">$counter</a></li>";
                    }
                    $pagination .= "<li class=\"spacePagination {$liClass}\"> <a class='{$aClass}' class=\"disabled\" href=\"#\"> ..... </a> </li>";
                    $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$lpm1\">$lpm1</a></li>";
                    $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$lastpage\">$lastpage</a></li>";
                } elseif ($lastpage - ($adjacents * 2) > $page &&   $page > ($adjacents * 2))
                {
                    $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/1\">1</a></li>";
                    $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/2\">2</a></li>";
                    $pagination .= "<li class=\"{$liClass}spacePagination\"> <a class='{$aClass}' class=\"disabled\" href=\"#\"> ..... </a> </li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page)
                            $pagination .= "<li class='{$liClass} {$activeClass}'><a class='{$aClass}' href=\"#\">$counter</a></li>";
                        else
                            $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$counter\">$counter</a></li>";

                    }
                    $pagination .= "<li class=\"spacePagination {$liClass}\"> <a class='{$aClass}' class=\"disabled\" href=\"#\"> ..... </a> </li>";
                    $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$lpm1\">$lpm1</a></li>";
                    $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$lastpage\">$lastpage</a></li>";
                }

                else
                {
                    $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/1\">1</a></li>";
                    $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/2\">2</a></li>";
                    $pagination .= "<li class=\"{$liClass}\"> ... </li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page)
                            $pagination .= "<li class='{$liClass} {$activeClass}'><a class='{$aClass}' href=\"#\">$counter</a></li>";
                        else
                            $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$counter\">$counter</a></li>";
                    }
                }
            }

            //کلید بعدی
            if ($page < $counter - 1)
                $pagination .= "<li class='{$liClass}'><a class='{$aClass}' href=\"$targetpage/$next\">&raquo;</a></li>";
            else
                $pagination .= "<li class='{$liClass}'><a class='{$aClass}' class='disabled' href=\"#\">&raquo;</a></li>";
            $pagination .= '</ul>';


        }
        return $pagination;
    }

    public static function randString($min,$max){
        $len=rand($min,$max);
        $rand='';
        for($i=0;$i<$len;$i++){
            do{
                $ascii = rand(48 , 90);
            }while($ascii > 57 and $ascii < 65);
            $rand.=chr($ascii);
        }
        return $rand;
    }

    public static function translate($text,$source='fa', $target='en') {

        $response 		= self::requestTranslation($source, $target, $text);
        if (is_string($response) && strpos($response,'Your client issued a request that was too large'))
            return "Your client issued a request that was too large";
        $translation 	= self::getSentencesFromJSON($response);
        return $translation;
    }

    private static function requestTranslation($source, $target, $text) {
        $url = "https://translate.google.com/translate_a/single?client=at&dt=t&dt=ld&dt=qca&dt=rm&dt=bd&dj=1&hl=es-ES&ie=UTF-8&oe=UTF-8&inputm=2&otf=2&iid=1dd3b944-fa62-4b55-b330-74909a99969e";
        $fields = array(
            'sl' => urlencode($source),
            'tl' => urlencode($target),
            'q' => urlencode($text)
        );

        $fields_string = "";
        foreach($fields as $key=>$value) {
            $fields_string .= $key.'='.$value.'&';
        }

        rtrim($fields_string, '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'AndroidTranslate/5.3.0.RC02.130475354-53000263 5.1 phone TRANSLATE_OPM5_TEST_1');

        $result = curl_exec($ch);

        curl_close($ch);
        return $result;
    }

    private static function getSentencesFromJSON($json) {
        $sentencesArray = json_decode($json, true);

        $sentences = "";
        if(is_array($sentencesArray) && count($sentencesArray))
        foreach ($sentencesArray["sentences"] as $s) {
            $sentences .= $s["trans"];
        }
        return $sentences;
    }

    public static function shortLink($url, $wish = ''){

            $data = http_build_query(
                array(
                    'url' => $url,
                    'wish' => $wish,
                    'language' => 'fa',
                )
            );
            $http = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $data
                )
            );
            $context = stream_context_create($http);
            $result = file_get_contents('http://api.yon.ir/', FALSE, $context);
            $arrayMessage= json_decode($result, true);
            if ($arrayMessage['status'] == "true")
            {
                return 'http://yon.ir/'.$arrayMessage['output'];
            }
            else
            {
                return false;
            }

    }

    public static function checkBirthDay($birthDay){

        if(preg_match('#^\d{4}/\d{1,2}/\d{1,2}$#is',$birthDay)){
            $b = explode('/',$birthDay);
            if(jdate('Ymd','','','','en')<$b[0].$b[1].$b[2])
                return 'notOver';
            if(count($b) == 3){
                if($b[0] > 1255 && $b[0]<= jdate('Y','','','','en')){
                    if($b[1]>0 && $b[1]<13){
                        if($b[2]>0 && $b[2]<32){
                            if($b[1]<7)
                                return 'ok';
                            elseif ($b[2]<31)
                                return 'ok';
                        }
                        return 'dayInvalid';
                    }else
                        return 'mountInvalid';
                }else
                    return 'yearInvalid';
            }
        }
        return 'invalid';
    }

}
