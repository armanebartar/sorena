<?php

$dompdf = new \Dompdf\Dompdf();
$dompdf->loadHtml('<h1>Welcome to NetParadis.com</h1>');

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();
