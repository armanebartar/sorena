<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'whichbrowser/parser',
        'dev' => true,
    ),
    'versions' => array(
        'cache/adapter-common' => array(
            'pretty_version' => '1.3.0',
            'version' => '1.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../cache/adapter-common',
            'aliases' => array(),
            'reference' => '8788309be72aa7be69b88cdc0687549c74a7d479',
            'dev_requirement' => true,
        ),
        'cache/array-adapter' => array(
            'pretty_version' => '1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../cache/array-adapter',
            'aliases' => array(),
            'reference' => '7658acf46b35a23b7be13e50a2792049e1c678c4',
            'dev_requirement' => true,
        ),
        'cache/hierarchical-cache' => array(
            'pretty_version' => '1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../cache/hierarchical-cache',
            'aliases' => array(),
            'reference' => 'dedffd0a74f72c1db76e57ce29885836944e27f3',
            'dev_requirement' => true,
        ),
        'cache/tag-interop' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../cache/tag-interop',
            'aliases' => array(),
            'reference' => 'b062b1d735357da50edf8387f7a8696f3027d328',
            'dev_requirement' => true,
        ),
        'doctrine/annotations' => array(
            'pretty_version' => '1.13.3',
            'version' => '1.13.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/annotations',
            'aliases' => array(),
            'reference' => '648b0343343565c4a056bfc8392201385e8d89f0',
            'dev_requirement' => true,
        ),
        'doctrine/cache' => array(
            'pretty_version' => '1.13.0',
            'version' => '1.13.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/cache',
            'aliases' => array(),
            'reference' => '56cd022adb5514472cb144c087393c1821911d09',
            'dev_requirement' => true,
        ),
        'doctrine/collections' => array(
            'pretty_version' => '1.8.0',
            'version' => '1.8.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/collections',
            'aliases' => array(),
            'reference' => '2b44dd4cbca8b5744327de78bafef5945c7e7b5e',
            'dev_requirement' => true,
        ),
        'doctrine/common' => array(
            'pretty_version' => '2.13.3',
            'version' => '2.13.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/common',
            'aliases' => array(),
            'reference' => 'f3812c026e557892c34ef37f6ab808a6b567da7f',
            'dev_requirement' => true,
        ),
        'doctrine/deprecations' => array(
            'pretty_version' => 'v1.0.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/deprecations',
            'aliases' => array(),
            'reference' => '0e2a4f1f8cdfc7a92ec3b01c9334898c806b30de',
            'dev_requirement' => true,
        ),
        'doctrine/event-manager' => array(
            'pretty_version' => '1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/event-manager',
            'aliases' => array(),
            'reference' => '95aa4cb529f1e96576f3fda9f5705ada4056a520',
            'dev_requirement' => true,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '1.4.4',
            'version' => '1.4.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'reference' => '4bd5c1cdfcd00e9e2d8c484f79150f67e5d355d9',
            'dev_requirement' => true,
        ),
        'doctrine/instantiator' => array(
            'pretty_version' => '1.4.1',
            'version' => '1.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/instantiator',
            'aliases' => array(),
            'reference' => '10dcfce151b967d20fde1b34ae6640712c3891bc',
            'dev_requirement' => true,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '1.2.3',
            'version' => '1.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'reference' => 'c268e882d4dbdd85e36e4ad69e02dc284f89d229',
            'dev_requirement' => true,
        ),
        'doctrine/persistence' => array(
            'pretty_version' => '1.3.8',
            'version' => '1.3.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/persistence',
            'aliases' => array(),
            'reference' => '7a6eac9fb6f61bba91328f15aa7547f4806ca288',
            'dev_requirement' => true,
        ),
        'doctrine/reflection' => array(
            'pretty_version' => '1.2.3',
            'version' => '1.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/reflection',
            'aliases' => array(),
            'reference' => '1034e5e71f89978b80f9c1570e7226f6c3b9b6fb',
            'dev_requirement' => true,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.5.0',
            'version' => '7.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => 'b50a2a1251152e43f6a37f0fa053e730a67d25ba',
            'dev_requirement' => true,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.2',
            'version' => '1.5.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => 'b94b2807d85443f9719887892882d0329d1e2598',
            'dev_requirement' => true,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.4.3',
            'version' => '2.4.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => '67c26b443f348a51926030c83481b85718457d3d',
            'dev_requirement' => true,
        ),
        'icomefromthenet/reverse-regex' => array(
            'pretty_version' => 'v0.0.6.3',
            'version' => '0.0.6.3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../icomefromthenet/reverse-regex',
            'aliases' => array(),
            'reference' => '085c56fd31ce5daf7a2dd1337073fc75aaa4e5f9',
            'dev_requirement' => true,
        ),
        'myclabs/deep-copy' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/deep-copy',
            'aliases' => array(),
            'reference' => '14daed4296fae74d9e3201d2c4925d1acb7aa614',
            'dev_requirement' => true,
        ),
        'patchwork/utf8' => array(
            'pretty_version' => 'v1.3.3',
            'version' => '1.3.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../patchwork/utf8',
            'aliases' => array(),
            'reference' => 'e1fa4d4a57896d074c9a8d01742b688d5db4e9d5',
            'dev_requirement' => true,
        ),
        'phar-io/manifest' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/manifest',
            'aliases' => array(),
            'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
            'dev_requirement' => true,
        ),
        'phar-io/version' => array(
            'pretty_version' => '3.2.1',
            'version' => '3.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/version',
            'aliases' => array(),
            'reference' => '4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
            'dev_requirement' => true,
        ),
        'php-coveralls/php-coveralls' => array(
            'pretty_version' => 'v2.5.3',
            'version' => '2.5.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-coveralls/php-coveralls',
            'aliases' => array(),
            'reference' => '9d8243bbf0e053333692857c98fab7cfba0d60a9',
            'dev_requirement' => true,
        ),
        'phpunit/php-code-coverage' => array(
            'pretty_version' => '7.0.15',
            'version' => '7.0.15.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-code-coverage',
            'aliases' => array(),
            'reference' => '819f92bba8b001d4363065928088de22f25a3a48',
            'dev_requirement' => true,
        ),
        'phpunit/php-file-iterator' => array(
            'pretty_version' => '2.0.5',
            'version' => '2.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-file-iterator',
            'aliases' => array(),
            'reference' => '42c5ba5220e6904cbfe8b1a1bda7c0cfdc8c12f5',
            'dev_requirement' => true,
        ),
        'phpunit/php-text-template' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-text-template',
            'aliases' => array(),
            'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
            'dev_requirement' => true,
        ),
        'phpunit/php-timer' => array(
            'pretty_version' => '2.1.3',
            'version' => '2.1.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-timer',
            'aliases' => array(),
            'reference' => '2454ae1765516d20c4ffe103d85a58a9a3bd5662',
            'dev_requirement' => true,
        ),
        'phpunit/php-token-stream' => array(
            'pretty_version' => '4.0.4',
            'version' => '4.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-token-stream',
            'aliases' => array(),
            'reference' => 'a853a0e183b9db7eed023d7933a858fa1c8d25a3',
            'dev_requirement' => true,
        ),
        'phpunit/phpunit' => array(
            'pretty_version' => '8.5.30',
            'version' => '8.5.30.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/phpunit',
            'aliases' => array(),
            'reference' => '4fd448df9affda65a5faa58f8b93087d415216ce',
            'dev_requirement' => true,
        ),
        'psr/cache' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => '213f9dbc5b9bfbc4f8db86d2838dc968752ce13b',
            'dev_requirement' => false,
        ),
        'psr/cache-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '^1.0',
            ),
        ),
        'psr/container' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
            'dev_requirement' => true,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => true,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => true,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => true,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'ef29f6d262798707a9edd554e2b82517ef3a9376',
            'dev_requirement' => true,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '1.0|2.0|3.0',
            ),
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'dev_requirement' => true,
        ),
        'psr/simple-cache-implementation' => array(
            'dev_requirement' => true,
            'provided' => array(
                0 => '^1.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => true,
        ),
        'sebastian/code-unit-reverse-lookup' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/code-unit-reverse-lookup',
            'aliases' => array(),
            'reference' => '1de8cd5c010cb153fcd68b8d0f64606f523f7619',
            'dev_requirement' => true,
        ),
        'sebastian/comparator' => array(
            'pretty_version' => '3.0.5',
            'version' => '3.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/comparator',
            'aliases' => array(),
            'reference' => '1dc7ceb4a24aede938c7af2a9ed1de09609ca770',
            'dev_requirement' => true,
        ),
        'sebastian/diff' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/diff',
            'aliases' => array(),
            'reference' => '14f72dd46eaf2f2293cbe79c93cc0bc43161a211',
            'dev_requirement' => true,
        ),
        'sebastian/environment' => array(
            'pretty_version' => '4.2.4',
            'version' => '4.2.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/environment',
            'aliases' => array(),
            'reference' => 'd47bbbad83711771f167c72d4e3f25f7fcc1f8b0',
            'dev_requirement' => true,
        ),
        'sebastian/exporter' => array(
            'pretty_version' => '3.1.5',
            'version' => '3.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/exporter',
            'aliases' => array(),
            'reference' => '73a9676f2833b9a7c36968f9d882589cd75511e6',
            'dev_requirement' => true,
        ),
        'sebastian/global-state' => array(
            'pretty_version' => '3.0.2',
            'version' => '3.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/global-state',
            'aliases' => array(),
            'reference' => 'de036ec91d55d2a9e0db2ba975b512cdb1c23921',
            'dev_requirement' => true,
        ),
        'sebastian/object-enumerator' => array(
            'pretty_version' => '3.0.4',
            'version' => '3.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-enumerator',
            'aliases' => array(),
            'reference' => 'e67f6d32ebd0c749cf9d1dbd9f226c727043cdf2',
            'dev_requirement' => true,
        ),
        'sebastian/object-reflector' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-reflector',
            'aliases' => array(),
            'reference' => '9b8772b9cbd456ab45d4a598d2dd1a1bced6363d',
            'dev_requirement' => true,
        ),
        'sebastian/recursion-context' => array(
            'pretty_version' => '3.0.1',
            'version' => '3.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/recursion-context',
            'aliases' => array(),
            'reference' => '367dcba38d6e1977be014dc4b22f47a484dac7fb',
            'dev_requirement' => true,
        ),
        'sebastian/resource-operations' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/resource-operations',
            'aliases' => array(),
            'reference' => '31d35ca87926450c44eae7e2611d45a7a65ea8b3',
            'dev_requirement' => true,
        ),
        'sebastian/type' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/type',
            'aliases' => array(),
            'reference' => '0150cfbc4495ed2df3872fb31b26781e4e077eb4',
            'dev_requirement' => true,
        ),
        'sebastian/version' => array(
            'pretty_version' => '2.0.1',
            'version' => '2.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/version',
            'aliases' => array(),
            'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
            'dev_requirement' => true,
        ),
        'squizlabs/php_codesniffer' => array(
            'pretty_version' => '3.7.1',
            'version' => '3.7.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../squizlabs/php_codesniffer',
            'aliases' => array(),
            'reference' => '1359e176e9307e906dc3d890bcc9603ff6d90619',
            'dev_requirement' => true,
        ),
        'symfony/config' => array(
            'pretty_version' => 'v6.0.11',
            'version' => '6.0.11.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/config',
            'aliases' => array(),
            'reference' => '956d4ec5df274dda91a4cedfccc2bfd063f6f649',
            'dev_requirement' => true,
        ),
        'symfony/console' => array(
            'pretty_version' => 'v6.0.14',
            'version' => '6.0.14.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'reference' => '1f89cab8d52c84424f798495b3f10342a7b1a070',
            'dev_requirement' => true,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
            'dev_requirement' => true,
        ),
        'symfony/filesystem' => array(
            'pretty_version' => 'v6.0.13',
            'version' => '6.0.13.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/filesystem',
            'aliases' => array(),
            'reference' => '3adca49133bd055ebe6011ed1e012be3c908af79',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.26.0',
            'version' => '1.26.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '6fd1b9a79f6e3cf65f9e679b23af304cd9e010d4',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-intl-grapheme' => array(
            'pretty_version' => 'v1.26.0',
            'version' => '1.26.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-grapheme',
            'aliases' => array(),
            'reference' => '433d05519ce6990bf3530fba6957499d327395c2',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.26.0',
            'version' => '1.26.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '219aa369ceff116e673852dce47c3a41794c14bd',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.26.0',
            'version' => '1.26.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9344f9cb97f3b19424af1a21a3b0e75b0a7d8d7e',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php81' => array(
            'pretty_version' => 'v1.26.0',
            'version' => '1.26.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php81',
            'aliases' => array(),
            'reference' => '13f6d1271c663dc5ae9fb843a8f16521db7687a1',
            'dev_requirement' => true,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'reference' => 'd78d39c1599bd1188b8e26bb341da52c3c6d8a66',
            'dev_requirement' => true,
        ),
        'symfony/stopwatch' => array(
            'pretty_version' => 'v6.0.13',
            'version' => '6.0.13.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/stopwatch',
            'aliases' => array(),
            'reference' => '7554fde6848af5ef1178f8ccbdbdb8ae1092c70a',
            'dev_requirement' => true,
        ),
        'symfony/string' => array(
            'pretty_version' => 'v6.0.14',
            'version' => '6.0.14.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/string',
            'aliases' => array(),
            'reference' => '3db7da820a6e4a584b714b3933c34c6a7db4d86c',
            'dev_requirement' => true,
        ),
        'symfony/yaml' => array(
            'pretty_version' => 'v4.4.45',
            'version' => '4.4.45.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/yaml',
            'aliases' => array(),
            'reference' => 'aeccc4dc52a9e634f1d1eebeb21eacfdcff1053d',
            'dev_requirement' => true,
        ),
        'theseer/tokenizer' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../theseer/tokenizer',
            'aliases' => array(),
            'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
            'dev_requirement' => true,
        ),
        'whichbrowser/parser' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
