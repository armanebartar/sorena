<?php
namespace App\Helpers;

use App\Models\Factor;
use App\Models\Payment;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class Zarin{
    private static $_merchent = "72622f69-6a8f-4a36-863a-282f742bd5e8";
    public static function req($amount,$callback,$desc,$mobile,$email){
        $email = "info@gmail.com";
        $amount*=10;
        $data = array("merchant_id" => self::$_merchent,
            "amount" => $amount,
            "callback_url" => $callback,
            "description" => $desc,
            "metadata" => [
                "email" => $email,
                "mobile"=>$mobile
            ],
        );
        $jsonData = json_encode($data);
        $ch = curl_init('https://api.zarinpal.com/pg/v4/payment/request.json');
        curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonData)
        ));

        $result = curl_exec($ch);
        $err = curl_error($ch);
        $result = json_decode($result, true, JSON_PRETTY_PRINT);
        curl_close($ch);


        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            if (empty($result['errors'])) {
                if ($result['data']['code'] == 100) {
                    header('Location: https://www.zarinpal.com/pg/StartPay/' . $result['data']["authority"]);
                }
            } else {
                return 'Error Code: ' . $result['errors']['code'] .'<br>'.'message: ' .  $result['errors']['message'];
            }
        }
        return "عملیات با خظا مواجه گردید";
    }
}
