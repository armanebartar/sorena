<?php
require 'vendor/autoload.php';
function user_agent_parser($agent){
    $result = new WhichBrowser\Parser($agent);
    return [
        'os'=> $result->os->toString(),
        'engin'=> $result->engine->toString(),
        'device'=> $result->device->toString(),
        'device2'=> trim($result->device->type.' '.$result->device->manufacturer.' '.$result->device->model.' '.$result->device->series),
        ];
}
