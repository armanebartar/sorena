<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Category;
use App\Models\City;
use App\Models\Factor_detail;
use App\Models\Favorite;
use App\Helpers\Helpers;
use App\Models\Post;
use App\Models\Post_cat;
use App\Models\S_money;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;




class AddressController extends Controller{



    public function basket_address(Request $request){
        $r = $request->all();
        $factor = AB_get_this_basket();
        $details = $factor->details ?? [];

        foreach ($details as $detail){
            $numb = isset($r['quantity'.$detail->id]) ? fa2la($r['quantity'.$detail->id]) : 0;

            if(is_numeric($numb) && $numb>0) {
                $detail->numb = $numb;
                $detail->save();
            } else{
                Factor_detail::destroy($detail->id);
            }
        }
        return $this->basket_address_get();
    }
    public function basket_address_get(){
        $data = AB_basket_update();
        if(count($data['details'])<1){
            return redirect()->route('basket_show');
        }

        if(!auth()->check()){
            session(['alert-danger'=>'ابتدا باید به سایت وارد شوید.']);
            session(['backLogin'=>route('basket_address')]);
            return redirect()->route('login');
        }
        $data = array_merge($data,$this->get_address($data['factor']));



        $this->set_factor_address($data['thisAddress']->id,$data['factor']);


        $data['opt'] = AB_get_options();
        return  view('basket_address',$data);
    }

    private function get_address($factor){
        $result['addresses'] = [];
        foreach (Address::where([['user_id',Auth::user()->id],['status',1]])->orderBy('id','DESC')->get() as $address)
            $result['addresses'][$address->id] = $address;

        if(is_numeric($factor->address_id) && isset($result['addresses'][$factor->address_id]))
            $result['thisAddress'] = $result['addresses'][$factor->address_id];
        elseif ($result['addresses'] != []){
            foreach ($result['addresses'] as $datum) {
                $result['thisAddress'] = $datum;
                break;
            }
        }else{
            $result['thisAddress'] = (object)[
                'id'=>0,
                'name'=>'',
                'mobile'=>'',
                'post_code'=>'',
                'state'=>'',
                'city'=>'',
                'address'=>''];
        }
        return $result;
    }

    public function basket_add_address(Request $request){
        if(!auth()->check())
            return ['res'=>1,'myAlert'=>'نشست فعلی شما منقضی شده. ابتدا وارد حساب خود شوید','mySuccess'=>''];
        if(strlen($request->name) < 3)
            return ['res'=>1,'myAlert'=>'وارد کردن نام الزامیست.','mySuccess'=>''];
        if(strlen($request->mobile) < 3)
            return ['res'=>1,'myAlert'=>'وارد کردن موبایل الزامیست.','mySuccess'=>''];
        if(strlen($request->state) < 2 || State::where('name',$request->state)->count() < 1)
            return ['res'=>1,'myAlert'=>'انتخاب استان الزامیست.','mySuccess'=>''];
        if(strlen($request->city) < 2 || City::where('name',$request->city)->count() < 1)
            return ['res'=>1,'myAlert'=>'انتخاب شهر الزامیست.','mySuccess'=>''];
        if(strlen($request->address) < 3)
            return ['res'=>1,'myAlert'=>'وارد کردن آدرس دقیق الزامیست.','mySuccess'=>''];
        if(strlen($request->postCode) < 3)
            return ['res'=>1,'myAlert'=>'وارد کردن کدپستی الزامیست.','mySuccess'=>''];

        $address = new Address();
        $address->user_id = Auth::user()->id;
        $address->name = $request->name ;
        $address->mobile = $request->mobile ;
        $address->state = $request->state ;
        $address->city = $request->city ;
        $address->address = $request->address ;
        $address->post_code = $request->postCode ;
        $address->status = 1;
        $address->save();


        return $this->addressData($address,10,'','آدرس با موفقیت ذخیره شد.');
    }

    private function set_factor_address($new_address_id,$factor=''){
        $factor = $factor=='' ? AB_get_this_basket() : $factor;
        if($factor->address_id != $new_address_id){
            $factor->address_id = $new_address_id;
            $factor->save();
        }
    }

    public function basket_edit_address(Request $request){
        if(!auth()->check())
            return ['res'=>1,'myAlert'=>'نشست فعلی شما منقضی شده. ابتدا وارد حساب خود شوید','mySuccess'=>''];
        if(strlen($request->address) < 3)
            return ['res'=>1,'myAlert'=>'وارد کردن آدرس دقیق الزامیست.','mySuccess'=>''];
        if(strlen($request->postCode) < 3)
            return ['res'=>1,'myAlert'=>'وارد کردن کدپستی الزامیست.','mySuccess'=>''];
        if(!$address = Address::where([['id',$request->id],['user_id',Auth::user()->id]])->first())
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید','mySuccess'=>''];

        $olds = json_decode($address->olds,1) ?? [];
        $olds[]=['address'=>$address->address,'post_code'=>$request->postCode];

        $address->olds = json_encode($olds);
        $address->address = $request->address ;
        $address->post_code = $request->postCode ;
        $address->status = 1;
        $address->save();



        return $this->addressData($address,10,'','آدرس با موفقیت ذخیره شد.');
    }

    public function basket_change_address(Request $request){
        if(!auth()->check())
            return ['res'=>1,'myAlert'=>'نشست فعلی شما منقضی شده. ابتدا وارد حساب خود شوید','mySuccess'=>''];
        if(!$address = Address::where([['id',$request->id],['user_id',Auth::user()->id]])->first())
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید','mySuccess'=>''];
        return $this->addressData($address);
    }

    private function addressData($address,$res=10,$alert='',$success=''){
        $others = '';
        foreach (Address::where([['user_id',Auth::user()->id],['status',1]])->get() as $value){
            $others .= $this->make_address_row($value,$address->id);
        }
        $this->set_factor_address($address->id);
        return [
            'res'=>$res,
            'myAlert'=>$alert,
            'mySuccess'=>$success,
            'others'=>$others,
            'selected'=>$this->make_selected_address($address)
        ];
    }

    public function make_address_row($value,$address_id){
        $checked = $value->id==$address_id ? "checked" : "";
        $txt = $value->id==$address_id ? 'سفارش به این آدرس ارسال میشود':'ارسال سفارش به این آدرس';
        $classAct = $value->id==$address_id ? 'act':'';

        return  "
        <div class=\"col-xxl-6 col-lg-12 col-md-6 box_address_wrapp\">
            <div class=\"delivery-address-box {$classAct}\" style='cursor:pointer;'>
                <div>
                    <div class=\"form-check\">
                        <input {$checked} class=\"form-check-input\" value='{$value->id}' type=\"radio\" name=\"jack\" id=\"flexRadioDefault1\">
                    </div>
                    <ul class=\"delivery-address-detail\">
                        <li>
                            <h4 class=\"fw-500\">
                            <i class=\"fa-solid fa-location-dot\"></i>
                           {$value->state}, {$value->city}, {$value->address} -
                           کدپستی :
                           {$value->post_code}
                            </h4>
                        </li>
                        <li>
                            <div class=\"text-content\">
                                <p style='display: inline-block;width: 35%'>
                                    <span class=\"text-title\"> گیرنده :</span>{$value->name}
                                </p>
                                <p style='display: inline-block;width: 35%'>
                                    <span class=\"text-title\">تلفن:</span>{$value->mobile}
                                </p>
                            </div>
                        </li>
                    </ul>
                    <button style='position: absolute;bottom: -12px;left: 1px;padding: 3px;color: #0ca486;font-size: 1rem;' class=\"btn btn-info edit_address\"
                                                data-id=\"{$value->id}\"
                                                data-name=\"{$value->name}\"
                                                data-mobile=\"{$value->mobile}\"
                                                data-state=\"{$value->state}\"
                                                data-city=\"{$value->city}\"
                                                data-code=\"{$value->post_code}\"
                                                data-address=\"{$value->address}\">
                                            ویرایش
                                        </button>
                </div>
            </div>
        </div>

            ";
    }

    public function make_selected_address($thisAddress){
        return "
         <li class=\"checkout-contact-item\">
            گیرنده:
            <span class=\"full-name\">{$thisAddress->name}</span>
            <a class=\"checkout-contact-btn-edit edit_address\"
               data-id=\"{$thisAddress->id}\"
               data-name=\"{$thisAddress->name}\"
               data-mobile=\"{$thisAddress->mobile}\"
               data-state=\"{$thisAddress->state}\"
               data-city=\"{$thisAddress->city}\"
               data-code=\"{$thisAddress->post_code}\"
               data-address=\"{$thisAddress->address}\"
            >
                اصلاح این آدرس
            </a>
        </li>
        <li class=\"checkout-contact-item\">
            <div class=\"checkout-contact-item checkout-contact-item-mobile\">
                شماره تماس:
                <span class=\"mobile-phone\">{$thisAddress->mobile}</span>
            </div>
            <div class=\"checkout-contact-item-message\">
                کد پستی:
                <span class=\"post-code\">{$thisAddress->post_code}</span>
            </div>
            <br>
            استان
            <span class=\"state\">{$thisAddress->state}</span>
            ، ‌شهر
            <span class=\"city\">{$thisAddress->city}</span>
            ،
            <span class=\"address-part\">{$thisAddress->address}</span>
        </li>
        ";
    }
}
