<?php

namespace App\Http\Controllers\app;

use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;


use App\Models\Factor;
use App\Models\Factor_detail;

use App\Models\User;
use Faker\Extension\Helper;
use http\Exception\BadConversionException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;


class UserController extends Controller{
    public function user_orders(){
        if(!\auth()->check()) {
            session(['alert-danger'=>'لطفا ابتدا وارد حساب کاربریتان شوید']);
            return redirect()->route('app_login');
        }
        return $this->make_view('app.user_orders',[]);
    }

    public function user_order($status){
        if(!\auth()->check()) {
            session(['alert-danger'=>'لطفا ابتدا وارد حساب کاربریتان شوید']);
            return redirect()->route('app_login');
        }
        $data['titre'] = '';
        if($status == 1)
            $data['titre'] = ' در انتظار تایید مدیر سیستم';
        if($status == 3)
            $data['titre'] = ' در انتظار تایید من ';
        if($status == 4)
            $data['titre'] = ' تایید شده';
        if($status == -1)
            $data['titre'] = ' رد شده';
        if($status == 7)
            $data['titre'] = ' ارسال شده';
        $data['status'] = $status;
        $data['orders'] = Factor::with(['user'])->where([['status',$status],['user_id',auth()->user()->id]])->orderBy('updated_at','DESC')->skip(0)->take(20)->get();
        return $this->make_view('app.user_order',$data);
    }

    public function user_order_details($id,$status){
        if(!\auth()->check()) {
            session(['alert-danger'=>'لطفا ابتدا وارد حساب کاربریتان شوید']);
            return redirect()->route('app_login');
        }


        if(!$data['order']=Factor::with(['user','details'])->where('id',$id)->first()){

            session(['alert-danger'=>'سفارش مورد نظر یافت نشد']);
            return redirect()->route('app_user_order',[$status]);
        }
        if(\auth()->user()->id != $data['order']->user_id){

            session(['alert-danger'=>'سفارش متعلق به شما نمیباشد']);
            return redirect()->route('app_user_order',[$status]);
        }

        $data['status'] = $status;
        return $this->make_view('app.user_order_details',$data);
    }

    public function user_order_confirm(Request $request){
        if($request->secure != secure($request->id)){
            return ['res'=>1,'message'=>'کلید امنیتی منقضی شده'];

        }
        if(!$order=Factor::with(['user','details'])->where('id',$request->id)->first()){
            return ['res'=>1,'message'=>'سفارش یافت نشد'];

        }
        if($order->user_id != \auth()->user()->id){
            return ['res'=>1,'message'=>'سفارش متعلق به شما نیست'];

        }
        if($order->expire < time()){

            $order->status = -1;
            $order->save();
            return ['res'=>1,'message'=>'سفارش شما منقضی شده است'];
        }
        $r = $request->all();
        foreach ($order->details as $detail){
            $numb = $r['numb_'.$detail->id] ?? $detail->numb;
            $numb = fa2la($numb);
            if(!isset($r['numb_' . $detail->id]) || $numb < 1){
                Factor_detail::destroy($detail->id);
            }else {
                $detail->numb = $numb;
                $detail->save();
            }
        }
        $order->status = 4;
        $order->save();

        return ['res'=>10,'message'=>'سفارش شما با موفقیت تایید گردید'];

    }

//    public function user_save_change_order(Request $request){
//        if(!\auth()->check() || \auth()->user()->role!=1)
//            return ['res'=>1,'message'=>'شما مجاز به انجام اینکار نیستید'];
//        if($request->secure != secure($request->id))
//            return ['res'=>1,'message'=>'کلید امنیتی شما منقضی شده'];
//
//        if(!$order=Factor::with(['user','details'])->where('id',$request->id)->first()){
//            return ['res'=>1,'message'=>'سفارش یافت نشد !'];
//        }
//        $r = $request->all();
//        foreach ($order->details as $detail){
//            $numb = $r['numb_'.$detail->id] ?? $detail->numb;
//            if($numb < 1){
//                Factor_detail::destroy($detail->id);
//            }else {
//                $detail->product_name = $r['name_' . $detail->id] ?? $detail->product_name;
//                $detail->numb = $numb;
//                $detail->unit = $r['unit_' . $detail->id] ?? $detail->unit;
//                $detail->price = fa2la($r['price_' . $detail->id]) ?? $detail->price;
//
//                $detail->save();
//            }
//        }
//        $order->expire = AB_lifetime_order();
//        $order->status = 3;
//        $order->save();
//
//       return ['res'=>10,'message'=>'تغییرات با موفقیت اعمال شد'];
//    }

    public function user_cancel_order($id,$secure){
        if($secure!= secure($id.'5')){
            return ['res'=>1,'message'=>'کلید امنیتی منقضی شده'];

        }
        if(!$order=Factor::with(['user','details'])->where('id',$id)->first()){
            session(['alert-danger'=>'سفارش یافت نشد']);
        }else if($order->user_id != \auth()->user()->id){
            session(['alert-danger'=>'سفارش متعلق به شما نیست']);
        }else{
            $status = $order->status;
            $order->status = -1;
            $order->save();
            session(['alert-success'=>'سفارش با موفقیت لغو گردید']);
        }

        return back();
    }

//    private function make_view($view,$data){
//        $app = new AppController();
//        return $app->make_view($view,$data);
//    }
}
