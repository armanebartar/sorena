<?php

namespace App\Http\Controllers\app;

use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class RegisterController extends Controller{

    public function register(){
        $data['titre'] = 'ثبت نام';
        return $this->make_view('app.forgot',$data);
    }

    public function forgot(){
        $data['titre'] = 'بازیابی کلمه عبور';
        return $this->make_view('app.forgot',$data);
    }

    public function forgoted(Request $request){
        $mobile = fa2la($request->mobile);
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(strlen($mobile)<2)
            return ['res'=>1,'message'=>'وارد کردن موبایل الزامیست'];
        if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
            return ['res'=>1,'message'=>'لطفا موبایل خود را بصورت صحیح وارد نمایید.'];

        $code = rand(10000,99999);
        $user_login_sms = 'sorenamed.ir';
        if($user = User::where('mobile',$mobile)->first()){
            $user->secure = Helpers::randString(4,6);
            $user->save();
            $user_login_sms = route('user_login_sms',[$user->secure]);
        }

        Sms::send_fast($mobile,'70661',['Username'=>$mobile,'Pass'=>$code,'Verify'=> $user_login_sms]);

        session(['app_code_register'=>[
            'code'=>$code,
            'mobile'=>$mobile,
            'numb'=>0,
            'time'=>time()
        ]]);
        return ['res'=>10,'message'=>'پسورد جدید برایتان پیامک شد'];
    }

    public function login(){
        return $this->make_view('app.login',[]);
    }

    public function logined(Request $request){
        $mobile = fa2la($request->mobile);
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(strlen($mobile)<2)
            return ['res'=>1,'message'=>'وارد کردن موبایل الزامیست'];
        if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
            return ['res'=>1,'message'=>'لطفا موبایل خود را بصورت صحیح وارد نمایید.'];
        if(strlen($request->password) < 4)
            return ['res'=>1,'message'=>'لطفا رمز عبور خود را بصورت صحیح وارد نمایید.'];
        if(!$user = User::where('mobile',$mobile)->orWhere('username',$mobile)->first())
            return ['res'=>2,'message'=>'موبایل وارد شده در سیستم یافت نشد'];
        if($user->password != User::hashPass(fa2la($request->password)))
            return ['res'=>1,'message'=>'رمز عبور وارد شده صحیح نیست'];
        Auth::loginUsingId($user->id,true);
        return ['res'=>10,'message'=>'با موفقیت به حساب خود وارد شدید'];
    }

    public function logout(){
        if(\auth()->check())
            Auth::logout();
        if(\auth('admin')->check())
            Auth::guard('admin')->logout();
        return redirect()->route('app');
    }


//    private function make_view($view,$data){
//        $app = new AppController();
//        return $app->make_view($view,$data);
//    }


}
