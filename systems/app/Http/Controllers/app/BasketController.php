<?php

namespace App\Http\Controllers\app;

use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;


use App\Models\Factor;
use App\Models\Factor_detail;

use App\Models\User;
use Faker\Extension\Helper;
use http\Exception\BadConversionException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;


class BasketController extends Controller
{

    public function add_basket(){
        $data = [];
        return $this->make_view('app.add_basket',$data);
    }

    public function add_basket_save(Request $request){
        if(!auth()->check()){
            $mobile = fa2la($request->mobile);
            if(strlen($mobile)<2)
                return ['res'=>1,'message'=>'لطفا موبایل خود را وارد نمایید.'];
            $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
            if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
                return ['res'=>1,'message'=>'لطفا موبایل خود را بصورت صحیح وارد نمایید.'];

        }else
            $mobile = auth()->user()->mobile;
        $products = [];
        $r = $request->all();
        if(isset($request->name) and count($request->name))
            foreach ($request->name as $k=>$item){
                if(strlen($item)>1){
                    $numb = isset($r['numb']) && isset($r['numb'][$k]) && is_numeric(fa2la($r['numb'][$k])) ? fa2la($r['numb'][$k]) : 1;
                    $unit = isset($r['unit']) && isset($r['unit'][$k]) ? $r['unit'][$k] : '';
                    $products[]=['name'=>$item,'numb'=>$numb,'unit'=>$unit];
                }
            }
        if($products == [])
            return ['res'=>2,'message'=>'حداقل یک محصول را باید وارد نمایید'];


        $factor = new Factor();
        $factor->user_id = auth()->check() ? auth()->user()->id : 0;
        $factor->user_token = $this->user_token();
        $factor->user_mobile = $mobile;
        $factor->status = 1;
        $factor->secret_key = Helpers::randString(20,30);
        $factor->desc = $request->desc;
        $factor->address_static = $request->address;
        $factor->time = time();
        $factor->expire = AB_lifetime_invoice();
        $factor->save();
        if(auth()->check() && \auth()->user()->address != $request->address){
            $user = User::where('id',\auth()->user()->id)->first();
            $user->address = $request->address;
            $user->save();
        }

        foreach ($products as $product){
            $detail = new Factor_detail();
            $detail->factor_id = $factor->id;
            $detail->post_id = 0;
            $detail->real_price = 0;
            $detail->price = 0;
            $detail->discount = 0;
            $detail->discount_id = 0;
            $detail->numb = $product['numb'];
            $detail->product_name = $product['name'];
            $detail->props = '';
            $detail->details = '';
            $detail->stock_id = 0;
            $detail->price_type = '';
            $detail->unit = $product['unit'];
            $detail->save();
        }

        session(['alert-success'=>'سفارش شما با موفقیت ثبت گردید.']);
        if(!auth()->check()){
            $code = rand(10000,99999);
            if(!$user = User::where('mobile',$mobile)->first()){
                $user = new User();
                $user->name = '';
                $user->family = '';
                $user->username = $mobile;
                $user->mobile = $mobile;
                $user->company = '';
                $user->company_post = '';
                $user->desc = '';
                $user->password = User::hashPass($code);

                $user->role = 2;
                $user->grade = 1;
                $user->status = 1;
                $user->importer = 0;
                $user->secure = Helpers::randString(4,6);
                $user->save();
            }
            $user_login_sms = route('user_login_sms',[$user->secure]);
            Sms::send_fast($mobile,'70618',['Code'=>$code,'Verify'=>$user_login_sms]);
            session(['app_code_register'=>[
                'code'=>$code,
                'mobile'=>$mobile,
                'numb'=>0,
                'time'=>time()
            ]]);
        }
        session(['user_add_order_temporary'=>[]]);
        return ['res'=>10];
    }



    public function admin_add_order(){
        if(!AB_perm('app_admin_add_order'))
            return redirect()->route('app');
        $data = [];
        return $this->make_view('app.admin_add_order',$data);
    }

    public function admin_add_order_save(Request $request){
        if(!AB_perm('app_admin_add_order'))
            return ['res'=>1,'message'=>'شما مجاز به انجام این عملیات نیستید'];

        $products = [];
        $r = $request->all();
        if(!$user = User::where('id',$request->user_id)->first())
            return ['res'=>1,'message'=>'لطفا مشتری مورد نظر را انتخاب نمایید'];
        if(!$user->secure || strlen($user->secure)<3){
            if(strlen($user->address) < 3)
                $user->address = $request->address;
            $user->secure = Helpers::randString(4,6);
            $user->save();
        }elseif(strlen($user->address) < 3 && strlen($request->address)>1){
            $user->address = $request->address;
            $user->save();
        }
        if(isset($request->name) and count($request->name))
            foreach ($request->name as $k=>$item){
                if(strlen($item)>1){
                    $numb = isset($r['numb']) && isset($r['numb'][$k]) && is_numeric(fa2la($r['numb'][$k])) ? fa2la($r['numb'][$k]) : 1;
                    $price = isset($r['price']) && isset($r['price'][$k]) && is_numeric(fa2la($r['price'][$k])) ? fa2la($r['price'][$k]) : 1;
                    $unit = isset($r['unit']) && isset($r['unit'][$k]) ? fa2la($r['unit'][$k]) : 'عدد';
                    $products[]=['name'=>$item,'numb'=>$numb,'unit'=>$unit,'price'=>$price];
                }
            }
        if($products == [])
            return ['res'=>2,'message'=>'حداقل یک محصول را باید وارد نمایید'];

        $factor = new Factor();
        $factor->importer = auth()->user()->id;
        $factor->user_id = $user->id;
        $factor->user_token = $user->secure;
        $factor->user_mobile = $user->mobile;
        $factor->status = AB_perm('app_admin_confirm_order') ? 3 : 1;
        $factor->secret_key = Helpers::randString(20,30);
        $factor->desc = $request->desc;
        $factor->time = time();
        $factor->company = in_array(fa2la($request->name_company),[1,2]) ? fa2la($request->name_company) : 1;
        $factor->expire = AB_lifetime_invoice();
        $factor->address_static = $request->address;
        $factor->save();

        foreach ($products as $product){
            $detail = new Factor_detail();
            $detail->factor_id = $factor->id;
            $detail->post_id = 0;
            $detail->real_price = 0;
            if(AB_perm('app_admin_add_order_by_price'))
                $detail->price = $product['price'];
            $detail->discount = 0;
            $detail->discount_id = 0;
            $detail->numb = $product['numb'];
            $detail->product_name = $product['name'];
            $detail->props = '';
            $detail->details = '';
            $detail->stock_id = 0;
            $detail->price_type = '';
            $detail->unit = $product['unit'];

            $detail->save();
        }

        if(AB_perm('app_admin_confirm_order'))
            Sms::send_fast($user->mobile,70748,['Numb'=>$factor->id,'Verify'=>'o/'.$factor->id.'/'.$user->secure]);
        session(['alert-success'=>'سفارش  با موفقیت ثبت گردید.']);
        session(['admin_add_order_temporary'=>[]]);
        session(['user_add_order_temporary'=>[]]);
        return ['res'=>10];
    }

    public function admin_add_order_temporary(Request $request){
        session(['admin_add_order_temporary'=>$request->all()]);
        return ['res'=>10];
    }

    public function user_add_order_temporary(Request $request){
        session(['user_add_order_temporary'=>$request->all()]);
        return ['res'=>10];
    }

    public function get_user_address(Request $request){
        if($user = User::where('id',$request->user_id)->first())
            return  ['address'=>$user->address];
        return ['address'=>''];
    }
//    private function make_view($view,$data){
//        $app = new AppController();
//        return $app->make_view($view,$data);
//    }
}
