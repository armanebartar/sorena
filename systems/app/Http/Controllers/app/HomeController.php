<?php

namespace App\Http\Controllers\app;


use App\Http\Controllers\Controller;
use App\Models\Factor;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller{
    public function user_login_sms($secure){
        if($user = User::where('secure',$secure)->first()){
            Auth::loginUsingId($user->id);
        }
        return redirect()->route('app');
    }

    public function user_order_sms($id,$secure){
        if($user = User::where('secure',$secure)->first()){
            Auth::loginUsingId($user->id);
            if($order = Factor::where('id',$id)->first()){
                return redirect()->route('app_user_order_details',[$id,$order->status]);
            }
        }
        return redirect()->route('app');
    }

    public function home(){

        return $this->make_view('app.home',[]);
    }

    public function about(){
        return $this->make_view('app.about',[]);
    }

    public function contact(){
        return $this->make_view('app.contact',[]);
    }
    public function catalog($name = 'home'){
        $data['link'] = url("digi_catalogs/".$name);
        $data['name'] = 'کاتالوگ دیجیتال';
        return $this->make_view('app.catalog',$data);
    }


//    private function make_view($view,$data){
//        $app = new AppController();
//        return $app->make_view($view,$data);
//    }
}
