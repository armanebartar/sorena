<?php

namespace App\Http\Controllers\app;

use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;


use App\Models\Factor;
use App\Models\Factor_detail;

use App\Models\User;
use Faker\Extension\Helper;
use http\Exception\BadConversionException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;


class AdminController extends Controller{
    public function admin_orders () {

        if(!AB_perm('app_admin_orders'))
            return redirect()->route('app');

        return $this->make_view('app.admin_orders',[]);
    }

    public function admin_order($status){

        if(!AB_perm('app_admin_orders'))
            return redirect()->route('app');
        $data['titre'] = '';
        if($status == 1)
            $data['titre'] = ' در انتظار تایید';
        if($status == 3)
            $data['titre'] = ' در انتظار تایید مشتری';
        if($status == 4)
            $data['titre'] = ' تایید شده';
        if($status == -1)
            $data['titre'] = ' رد شده';
        if($status == 7)
            $data['titre'] = ' ارسال شده';
        $data['status'] = $status;
        $data['orders'] = Factor::with(['user'])->where('status',$status)->orderBy('updated_at','DESC')->skip(0)->take(50)->get();
        return $this->make_view('app.admin_order',$data);
    }

    public function admin_order_details($id,$status){
        if(!AB_perm('app_admin_orders'))
            return redirect()->route('app');

        if(!$data['order']=Factor::with(['user','details'])->where('id',$id)->first()){
            session(['alert-danger'=>'سفارش مورد نظر یافت نشد']);
            return redirect()->route('app_admin_order',[$status]);
        }
        $data['status'] = $status;
        return $this->make_view('app.admin_order_details',$data);
    }

    public function admin_save_change_order(Request $request){
        if(!AB_perm('app_admin_orders'))
            return ['res'=>1,'message'=>'شما مجاز به انجام اینکار نیستید'];
        if($request->secure != secure($request->id))
            return ['res'=>1,'message'=>'کلید امنیتی شما منقضی شده'];

        if(!$order=Factor::with(['user','details'])->where('id',$request->id)->first()){
            return ['res'=>1,'message'=>'سفارش یافت نشد !'];
        }
        $r = $request->all();
        foreach ($order->details as $detail){
            $numb = $r['numb_'.$detail->id] ?? $detail->numb;
            $numb = fa2la($numb);
            if(!isset($r['name_' . $detail->id]) || $numb < 1){
                Factor_detail::destroy($detail->id);
            }else {
                $detail->product_name = $r['name_' . $detail->id] ?? $detail->product_name;
                $detail->numb = $numb;
                $detail->unit = $r['unit_' . $detail->id] ?? $detail->unit;
                if(AB_perm('app_admin_add_order_by_price'))
                    $detail->price = fa2la($r['price_' . $detail->id]) ?? $detail->price;

                $detail->save();
            }
        }
        $order->expire = AB_lifetime_order();
        if(AB_perm('app_admin_confirm_order')&&$order->status<3)
            $order->status = 3;
        $order->company = in_array(fa2la($request->name_company),[1,2]) ? fa2la($request->name_company) : 1;
        $order->address_static = $request->address;
        $order->desc = $request->desc;
        $order->save();

        if(isset($r['namee']) && is_array($r['namee']) && count($r['namee'])>0){
            foreach ($r['namee'] as $k=>$item){
                $numb = isset($r['numbb'][$k])&&is_numeric(fa2la($r['numbb'][$k])) ? fa2la($r['numbb'][$k]) : 0;
                $price = isset($r['pricee'][$k])&&is_numeric(fa2la($r['pricee'][$k])) ? fa2la($r['pricee'][$k]) : 0;
                $unit = isset($r['unitt'][$k]) ? fa2la($r['unitt'][$k]) : 'عدد';
                if($numb>0 && $price>0 && strlen($item)>0){
                    $det = new Factor_detail();
                    $det->factor_id = $order->id;
                    $det->post_id = 0;
                    $det->real_price = $price;
                    $det->price = $price;
//                    $det->discount = $disc;
                    $det->discount_id = 0;
                    $det->numb = $numb;
                    $det->product_name = $item;
                    $det->unit = $unit;
                    $det->save();
                }
            }
        }

        if(Factor_detail::where('factor_id',$order->id)->count() < 1){
            Factor::destroy($order->id);
            return ['res' => 10, 'message' => 'فاکتور از سیستم حذف گردید'];
        }else {
            $mobile = $order->user ? $order->user->mobile : $order->user_mobile;

           if(AB_perm('app_admin_confirm_order')){
                if ($order->user) {
                    if (!$order->user->secure || strlen($order->user->secure) < 3) {
                        $order->user->secure = Helpers::randString(4, 6);
                        $order->user->save();
                    }
                    Sms::send_fast($mobile, 70748, ['Numb' => $order->id, 'Verify' => 'o/' . $order->id . '/' . $order->user->secure]);
                } else
                    Sms::send_fast($mobile, 27314, ['Numb' => $order->id]);
            }


            return ['res' => 10, 'message' => 'تغییرات با موفقیت اعمال شد'];
        }

    }

    public function register_admin(){
        if(!AB_perm('app_register_admin'))
            return redirect()->route('app');

        return $this->make_view('app.register_admin',[]);
    }

    public function register_admin_save(Request $request){
        if(!AB_perm('app_register_admin'))
            return ['res'=>1,'message'=>'شما مجاز به انجام اینکار نیستید'];
        $mobile = fa2la($request->mobile);
        if(strlen($mobile)<2)
            return ['res'=>1,'message'=>'لطفا موبایل  را وارد نمایید.'];
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
            return ['res'=>1,'message'=>'لطفا موبایل وارد شده صحیح نیست.'];

        $pass = strlen($request->pass)>3 ? fa2la($request->pass) : rand(10000,99999);
        if($user = User::where('mobile',$mobile)->first()) {

            $user->secure = Helpers::randString(4,6);
            $user->save();

            $user_login_sms = route('user_login_sms',[$user->secure]);
            Sms::send_fast($mobile,'70661',['Username'=>$mobile,'Pass'=>$pass,'Verify'=>$user_login_sms]);
            return ['res' => 10, 'message' => 'کاربر قبلا در سایت عضو شده'];
        }
        if(strlen($mobile)<5)
            return ['res'=>1,'message'=>'موبایل نامعتبر است'];

        $user = new User();
        $user->name = $request->name;
        $user->family = $request->family;
        $user->username = $mobile;
        $user->mobile = $mobile;
        $user->company = $request->company;
        $user->company_post = $request->company_post;
        $user->desc = $request->desc;
        $user->importer = \auth()->check() ? \auth()->user()->id : 0;
        $user->password = User::hashPass($pass);

        $user->role = 2;
        $user->grade = 1;
        $user->status = 1;

        $user->secure = Helpers::randString(4,6);
        $user->save();
        $user_login_sms = route('user_login_sms',[$user->secure]);
        Sms::send_fast($mobile,'70661',['Username'=>$mobile,'Pass'=>$pass,'Verify'=>$user_login_sms]);
        return ['res'=>10,'message'=>'کاربر با موفقیت ذخیره گردید'];
    }

//    private function make_view($view,$data){
//        $app = new AppController();
//        return $app->make_view($view,$data);
//    }
}
