<?php

namespace App\Http\Controllers\app;

use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;


use App\Models\Factor;
use App\Models\Factor_detail;

use App\Models\Log_aff;
use App\Models\User;
use Faker\Extension\Helper;
use http\Exception\BadConversionException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;


class StatisticsController extends Controller{
    public function statistics(){
        if(!AB_perm('app_statistics'))
            return redirect()->route('app');


        return $this->make_view('app.admin_statistics',[]);
    }

    public function user_inputs($first){
        if(!AB_perm('app_statistics'))
            return redirect()->route('app');

        $data['first'] = $first;
        $data['devices'] = $this->make_inputs(0,$first);
        return $this->make_view('app.admin_statistics_inputs',$data);
    }

    public function get_inputs(Request $request){
        return $this->make_inputs($request->page,$request->first);

    }
    private function make_inputs($page,$first){
        $take = 25;
        $count = 0;
        $start = $page*$take;
        if($first)
            $views = Log_aff::where([['aff_id',\auth()->user()->id],['first',1]])->orderBy('id','DESC')->skip($start)->take($take)->get();
        else
            $views = Log_aff::where([['aff_id',\auth()->user()->id]])->orderBy('id','DESC')->skip($start)->take($take)->get();
        $result = '';
        foreach ($views as $view){
            $count++;
            $data = jdate('Y-m-d H:i',strtotime($view->created_at));
            $os = Helpers::user_agent_parser($view->agent);

            $device = strlen($os['device'])>2 ? $os['device'] : $os['device2'];
            $result.="
            <div class=\"section mt-2\">
                <div class=\"card\">
                    <div class=\"card-body\" style=\"padding: 5px 16px\">
                        <p>تاریخ بازدید : <span class='date'>{$data}</span></p>
                        <p>سیستم عامل : {$os['os']}</p>
                        <p>دیوایس : {$device}</p>
                    </div>
                </div>
            </div>
            ";
        }

        return ['devices'=>$result,'count'=>$count];
    }

    public function user_registers($self){
        if(!AB_perm('app_statistics'))
            return redirect()->route('app');

        $data['self'] = $self;
        $data['users'] = $this->make_registers(0,$self);
        return $this->make_view('app.admin_statistics_registers',$data);
    }

    public function get_registers(Request $request){
        return $this->make_registers($request->page,$request->self1,$request->search);

    }
    private function make_registers($page,$self,$search=''){
        $take = 10;
        $count = 0;
        $start = $page*$take;
        $where = $self ? [['importer',\auth()->user()->id]] : [['importer_aff',\auth()->user()->id]];
        if(strlen($search)>1){
            $where1 = $where;
            $where1[]=['name','like','%'.$search.'%'];
            $where2 = $where;
            $where2[]=['family','like','%'.$search.'%'];
            $where3 = $where;
            $where3[]=['company','like','%'.$search.'%'];
            $where4 = $where;
            $where4[]=['mobile','like','%'.fa2la($search).'%'];
//            $where1 = array_merge($where[0],['name','like','%'.$search.'%']);
//            $where2 = array_merge($where[0],['family','like','%'.$search.'%']);
//            $where3 = array_merge($where[0],['company','like','%'.$search.'%']);
//            $where4 = array_merge($where[0],['mobile','like','%'.fa2la($search).'%']);

            $registers = User::where($where1)->orWhere($where2)->orWhere($where3)->orWhere($where4)->orderBy('id','DESC')->skip($start)->take($take)->get();
        }else
            $registers = User::where($where)->orderBy('id','DESC')->skip($start)->take($take)->get();

        $result = '';
        foreach ($registers as $user){
            $count++;
            $data = jdate('Y-m-d H:i',strtotime($user->created_at));
            $company = trim(trim($user->company.' - '.$user->company_post),'-');
            $result.="
            <div class=\"section mt-2\">
                <div class=\"card\">
                    <div class=\"card-body\" style=\"padding: 5px 16px\">
                        <p>نام : {$user->name} {$user->family}</p>
                        <p>سازمان : {$company}</p>
                        <p>عضویت : <span class='date'>{$data}</span></p>
                        <p>موبایل : <a style='margin-left: 35px' href=\"tel:{$user->mobile}\">{$user->mobile}</a>
                        <a href=\"sms:{$user->mobile}\">ارسال پیامک</a></p>
                    </div>
                </div>
            </div>
            ";
        }

        return ['users'=>$result,'count'=>$count];
    }



}
