<?php

namespace App\Http\Controllers\app;

use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;


use App\Models\Factor;
use App\Models\Factor_detail;

use App\Models\User;
use Faker\Extension\Helper;
use http\Exception\BadConversionException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;


class AppController extends Controller
{
    public function home(){

        return $this->make_view('app.home',[]);
    }

    public function add_basket(){
//        if(!auth()->check())
//            return redirect()->route('app_login');
        $data = [];

        return $this->make_view('app.add_basket',$data);
    }

    public function add_basket_save(Request $request){
        if(!auth()->check()){
            $mobile = fa2la($request->mobile);
            if(strlen($mobile)<2)
                return ['res'=>1,'message'=>'لطفا موبایل خود را وارد نمایید.'];
            $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
            if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
                return ['res'=>1,'message'=>'لطفا موبایل خود را بصورت صحیح وارد نمایید.'];

        }else
            $mobile = auth()->user()->mobile;
        $products = [];
        $r = $request->all();
        if(isset($request->name) and count($request->name))
        foreach ($request->name as $k=>$item){
            if(strlen($item)>1){
                $numb = isset($r['numb']) && isset($r['numb'][$k]) && is_numeric(fa2la($r['numb'][$k])) ? fa2la($r['numb'][$k]) : 1;
                $unit = isset($r['unit']) && isset($r['unit'][$k]) ? $r['unit'][$k] : '';
                $products[]=['name'=>$item,'numb'=>$numb,'unit'=>$unit];
            }
        }
        if($products == [])
            return ['res'=>2,'message'=>'حداقل یک محصول را باید وارد نمایید'];

        $factor = new Factor();
        $factor->user_id = auth()->check() ? auth()->user()->id : 0;
        $factor->user_token = $this->user_token();
        $factor->user_mobile = $mobile;
        $factor->status = 1;
        $factor->secret_key = Helpers::randString(20,30);
        $factor->desc = $request->desc;
        $factor->address_static = $request->address;
        $factor->time = time();
        $factor->expire = time()+10000;
        $factor->save();
        if(auth()->check() && \auth()->user()->address != $request->address){
            $user = User::where('id',\auth()->user()->id)->first();
            $user->address = $request->address;
            $user->save();
        }

        foreach ($products as $product){
            $detail = new Factor_detail();
            $detail->factor_id = $factor->id;
            $detail->post_id = 0;
            $detail->real_price = 0;
            $detail->price = 0;
            $detail->discount = 0;
            $detail->discount_id = 0;
            $detail->numb = $product['numb'];
            $detail->product_name = $product['name'];
            $detail->props = '';
            $detail->details = '';
            $detail->stock_id = 0;
            $detail->price_type = '';
            $detail->unit = $product['unit'];
            $detail->save();
        }

        session(['alert-success'=>'سفارش شما با موفقیت ثبت گردید.']);
        if(!auth()->check()){
            $code = rand(10000,99999);
            if(!$user = User::where('mobile',$mobile)->first()){
                $user = new User();
                $user->name = '';
                $user->family = '';
                $user->username = $mobile;
                $user->mobile = $mobile;
                $user->company = '';
                $user->company_post = '';
                $user->desc = '';
                $user->password = User::hashPass($code);

                $user->role = 2;
                $user->grade = 1;
                $user->status = 1;
                $user->importer = 0;
                $user->secure = Helpers::randString(4,6);
                $user->save();
            }
            $user_login_sms = route('user_login_sms',[$user->secure]);
            Sms::send_fast($mobile,'70618',['Code'=>$code,'Verify'=>$user_login_sms]);
            session(['app_code_register'=>[
                'code'=>$code,
                'mobile'=>$mobile,
                'numb'=>0,
                'time'=>time()
            ]]);
        }
        session(['user_add_order_temporary'=>[]]);
        return ['res'=>10];
    }

    public function confirm_mobile(){
        $app_code_register = session('app_code_register',[]);
        if(!isset($app_code_register['code']) || !isset($app_code_register['numb']))
            return redirect()->route('app');
        if($app_code_register['numb'] > 10){
            session(['app_code_register'=>[]]);
            session()->remove('app_code_register');
            session(['alert-danger'=>'تعداد دفعات ثبت کد به پایان رسیده.']);
            return redirect()->route('app');
        }
        return $this->make_view('app.confirm_mobile',[]);

    }

    public function confirm_mobile_save(Request $request){
        $app_code_register = session('app_code_register',[]);
        if(!isset($app_code_register['code']) || !isset($app_code_register['numb']))
            return ['res'=>9];
        if($app_code_register['numb'] > 10){
            session(['app_code_register'=>[]]);
            session()->remove('app_code_register');
            session(['alert-danger'=>'تعداد دفعات ثبت کد به پایان رسیده.']);
            return ['res'=>9];
        }
        $r = $request->all();
        if(!isset($r['code']) || strlen($r['code']) < 1){
            return ['res'=>1,'message'=>'وارد کردن کد تایید الزامیست'];
        }
        if($app_code_register['code'] != fa2la($request->code)){
            $app_code_register['numb'] += 1;
            session(['app_code_register'=>$app_code_register]);
            return ['res'=>1,'message'=>'کد وارد شده صحیح نمیباشد'];
        }
        if(!$user = User::where('mobile',$app_code_register['mobile'])->first()){
            $user = new User();
            $user->name = '';
            $user->family = '';
            $user->email = '';
            $user->mobile = $app_code_register['mobile'];
            $user->role = 2;
            $user->grade = 1;
            $user->importer = 0;
        }
        $user->mobile_active = 1;
        $user->status = 1;
        $user->username = $app_code_register['mobile'];
        $user->password = User::hashPass($app_code_register['code']);
        $user->save();

        Auth::loginUsingId($user->id,true);
        session(['alert-success'=>'حساب کاربری شما با موفقیت فعال گردید']);
        session(['app_code_register'=>[]]);
        session()->remove('app_code_register');

        $user_token = $this->user_token();
        if($fff = Factor::where([['user_token',$user_token],['user_id',0]])->get()){
            foreach ($fff as $ff){
                $ff->user_id = $user->id;
                $ff->save();
            }
        }

        if(strlen($user->name)>0)
            return ['res'=>9];
        return ['res'=>10];

    }

    public function profile(){
        if(!\auth()->check())
            return redirect()->route('app_login');
        return $this->make_view('app.edit_profile',[]);

    }

    public function profile_save(Request $request){
        if(!\auth()->check())
            return ['res'=>9];
        $user = User::where('id',\auth()->user()->id)->first();
        $user->name = $request->name;
        $user->company = $request->company;
        $user->company_post = $request->company_post;
        $user->tel = $request->tel;
        $user->address = $request->address;
        $user->save();
        return ['res'=>10,'message'=>'تغییرات با موفقیت ذخیره گردید'];

    }

    public function user_login_sms($secure){
        if($user = User::where('secure',$secure)->first()){
            Auth::loginUsingId($user->id);
        }
        return redirect()->route('app');
    }

    public function user_order_sms($id,$secure){
        if($user = User::where('secure',$secure)->first()){
            Auth::loginUsingId($user->id);
            if($order = Factor::where('id',$id)->first()){
                return redirect()->route('app_user_order_details',[$id,$order->status]);
            }
        }
        return redirect()->route('app');
    }

    public function about(){
        return $this->make_view('app.about',[]);
    }

    public function contact(){
        return $this->make_view('app.contact',[]);
    }

    public function register_admin(){
        return $this->make_view('app.register_admin',[]);
    }

    public function register_admin_save(Request $request){
        $mobile = fa2la($request->mobile);
        if(strlen($mobile)<2)
            return ['res'=>1,'message'=>'لطفا موبایل  را وارد نمایید.'];
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
            return ['res'=>1,'message'=>'لطفا موبایل وارد شده صحیح نیست.'];

        $pass = strlen($request->pass)>3 ? fa2la($request->pass) : rand(10000,99999);
        if($user = User::where('mobile',$mobile)->first()) {

            $user->secure = Helpers::randString(4,6);
            $user->save();

            $user_login_sms = route('user_login_sms',[$user->secure]);
            Sms::send_fast($mobile,'70661',['Username'=>$mobile,'Pass'=>$pass,'Verify'=>$user_login_sms]);
            return ['res' => 10, 'message' => 'کاربر قبلا در سایت عضو شده'];
        }
        if(strlen($mobile)<5)
            return ['res'=>1,'message'=>'موبایل نامعتبر است'];

        $user = new User();
        $user->name = $request->name;
        $user->family = $request->family;
        $user->username = $mobile;
        $user->mobile = $mobile;
        $user->company = $request->company;
        $user->company_post = $request->company_post;
        $user->desc = $request->desc;
        $user->importer = \auth()->check() ? \auth()->user()->id : 0;
        $user->password = User::hashPass($pass);

        $user->role = 2;
        $user->grade = 1;
        $user->status = 1;

        $user->secure = Helpers::randString(4,6);
        $user->save();
        $user_login_sms = route('user_login_sms',[$user->secure]);
        Sms::send_fast($mobile,'70661',['Username'=>$mobile,'Pass'=>$pass,'Verify'=>$user_login_sms]);
        return ['res'=>10,'message'=>'کاربر با موفقیت ذخیره گردید'];
    }

    public function login(){
        return $this->make_view('app.login',[]);
    }

    public function logined(Request $request){
        $mobile = fa2la($request->mobile);
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(strlen($mobile)<2)
            return ['res'=>1,'message'=>'وارد کردن موبایل الزامیست'];
        if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
            return ['res'=>1,'message'=>'لطفا موبایل خود را بصورت صحیح وارد نمایید.'];
        if(strlen($request->password) < 4)
            return ['res'=>1,'message'=>'لطفا رمز عبور خود را بصورت صحیح وارد نمایید.'];
        if(!$user = User::where('mobile',$mobile)->orWhere('username',$mobile)->first())
            return ['res'=>2,'message'=>'موبایل وارد شده در سیستم یافت نشد'];
        if($user->password != User::hashPass(fa2la($request->password)))
            return ['res'=>1,'message'=>'رمز عبور وارد شده صحیح نیست'];
        Auth::loginUsingId($user->id,true);
        return ['res'=>10,'message'=>'با موفقیت به حساب خود وارد شدید'];
    }

    public function register(){
        $data['titre'] = 'ثبت نام';
        return $this->make_view('app.forgot',$data);
    }

    public function forgot(){
        $data['titre'] = 'بازیابی کلمه عبور';
        return $this->make_view('app.forgot',$data);
    }

    private function ad_user(){

    }

    public function forgoted(Request $request){
        $mobile = fa2la($request->mobile);
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(strlen($mobile)<2)
            return ['res'=>1,'message'=>'وارد کردن موبایل الزامیست'];
        if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
            return ['res'=>1,'message'=>'لطفا موبایل خود را بصورت صحیح وارد نمایید.'];

        $code = rand(10000,99999);
        $user_login_sms = 'sorenamed.ir';
        if($user = User::where('mobile',$mobile)->first()){
            $user->secure = Helpers::randString(4,6);
            $user->save();
            $user_login_sms = route('user_login_sms',[$user->secure]);
        }

        Sms::send_fast($mobile,'70661',['Username'=>$mobile,'Pass'=>$code,'Verify'=> $user_login_sms]);

        session(['app_code_register'=>[
            'code'=>$code,
            'mobile'=>$mobile,
            'numb'=>0,
            'time'=>time()
        ]]);
        return ['res'=>10,'message'=>'پسورد جدید برایتان پیامک شد'];
    }

    public function logout(){
        if(\auth()->check())
            Auth::logout();
        if(\auth('admin')->check())
            Auth::guard('admin')->logout();
        return redirect()->route('app');
    }

    public function admin_orders(){
        if(!\auth()->check() || \auth()->user()->role!=1)
            return redirect()->route('app');
        return $this->make_view('app.admin_orders',[]);
    }

    public function admin_order($status){

        if(!\auth()->check() || \auth()->user()->role!=1)
            return redirect()->route('app');
        $data['titre'] = '';
        if($status == 1)
            $data['titre'] = ' در انتظار تایید';
        if($status == 3)
            $data['titre'] = ' در انتظار تایید مشتری';
        if($status == 4)
            $data['titre'] = ' تایید شده';
        if($status == -1)
            $data['titre'] = ' رد شده';
        if($status == 7)
            $data['titre'] = ' ارسال شده';
        $data['status'] = $status;
        $data['orders'] = Factor::with(['user'])->where('status',$status)->orderBy('id','DESC')->skip(0)->take(20)->get();
        return $this->make_view('app.admin_order',$data);
    }

    public function admin_order_details($id,$status){
        if(!\auth()->check() || \auth()->user()->role!=1)
            return redirect()->route('app');

        if(!$data['order']=Factor::with(['user','details'])->where('id',$id)->first()){
            session(['alert-danger'=>'سفارش مورد نظر یافت نشد']);
            return redirect()->route('app_admin_order',[$status]);
        }
        $data['status'] = $status;
        return $this->make_view('app.admin_order_details',$data);
    }

    public function admin_save_change_order(Request $request){
        if(!\auth()->check() || \auth()->user()->role!=1)
            return ['res'=>1,'message'=>'شما مجاز به انجام اینکار نیستید'];
        if($request->secure != secure($request->id))
            return ['res'=>1,'message'=>'کلید امنیتی شما منقضی شده'];

        if(!$order=Factor::with(['user','details'])->where('id',$request->id)->first()){
            return ['res'=>1,'message'=>'سفارش یافت نشد !'];
        }
        $r = $request->all();
        foreach ($order->details as $detail){
            $numb = $r['numb_'.$detail->id] ?? $detail->numb;
            $numb = fa2la($numb);
            if(!isset($r['name_' . $detail->id]) || $numb < 1){
                Factor_detail::destroy($detail->id);
            }else {
                $detail->product_name = $r['name_' . $detail->id] ?? $detail->product_name;
                $detail->numb = $numb;
                $detail->unit = $r['unit_' . $detail->id] ?? $detail->unit;
                $detail->price = fa2la($r['price_' . $detail->id]) ?? $detail->price;

                $detail->save();
            }
        }
        $order->expire = AB_lifetime_order();
        if($order->status<3)
        $order->status = 3;
        $order->company = in_array(fa2la($request->name_company),[1,2]) ? fa2la($request->name_company) : 1;
        $order->address_static = $request->address;
        $order->save();


        if(Factor_detail::where('factor_id',$order->id)->count() < 1){
            Factor::destroy($order->id);
            return ['res' => 10, 'message' => 'فاکتور از سیستم حذف گردید'];
        }else {
            $mobile = $order->user ? $order->user->mobile : $order->user_mobile;

            if ($order->user) {
                if (!$order->user->secure || strlen($order->user->secure) < 3) {
                    $order->user->secure = Helpers::randString(4, 6);
                    $order->user->save();
                }
                Sms::send_fast($mobile, 70748, ['Numb' => $order->id, 'Verify' => 'o/' . $order->id . '/' . $order->user->secure]);
            } else
                Sms::send_fast($mobile, 27314, ['Numb' => $order->id]);


            return ['res' => 10, 'message' => 'تغییرات با موفقیت اعمال شد'];
        }

    }

    public function user_orders(){
        if(!\auth()->check())
            return redirect()->route('app');
        return $this->make_view('app.user_orders',[]);
    }

    public function user_order($status){
        if(!\auth()->check())
            return redirect()->route('app');
        $data['titre'] = '';
        if($status == 1)
            $data['titre'] = ' در انتظار تایید مدیر سیستم';
        if($status == 3)
            $data['titre'] = ' در انتظار تایید من ';
        if($status == 4)
            $data['titre'] = ' تایید شده';
        if($status == -1)
            $data['titre'] = ' رد شده';
        if($status == 7)
            $data['titre'] = ' ارسال شده';
        $data['status'] = $status;
        $data['orders'] = Factor::with(['user'])->where([['status',$status],['user_id',auth()->user()->id]])->orderBy('id','DESC')->skip(0)->take(20)->get();
        return $this->make_view('app.user_order',$data);
    }

    public function user_order_details($id,$status){
        if(!\auth()->check())
            return redirect()->route('app');


        if(!$data['order']=Factor::with(['user','details'])->where('id',$id)->first()){

            session(['alert-danger'=>'سفارش مورد نظر یافت نشد']);
            return redirect()->route('app_user_order',[$status]);
        }
        if(\auth()->user()->id != $data['order']->user_id){

            session(['alert-danger'=>'سفارش متعلق به شما نمیباشد']);
            return redirect()->route('app_user_order',[$status]);
        }

        $data['status'] = $status;
        return $this->make_view('app.user_order_details',$data);
    }

    public function user_order_confirm(Request $request){
        if($request->secure != secure($request->id)){
            return ['res'=>1,'message'=>'کلید امنیتی منقضی شده'];

        }
        if(!$order=Factor::with(['user','details'])->where('id',$request->id)->first()){
            return ['res'=>1,'message'=>'سفارش یافت نشد'];

        }
        if($order->user_id != \auth()->user()->id){
            return ['res'=>1,'message'=>'سفارش متعلق به شما نیست'];

        }
        if($order->expire < time()){

            $order->status = -1;
            $order->save();
            return ['res'=>1,'message'=>'سفارش شما منقضی شده است'];
        }
        $r = $request->all();
        foreach ($order->details as $detail){
            $numb = $r['numb_'.$detail->id] ?? $detail->numb;
            $numb = fa2la($numb);
            if(!isset($r['numb_' . $detail->id]) || $numb < 1){
                Factor_detail::destroy($detail->id);
            }else {
                $detail->numb = $numb;
                $detail->save();
            }
        }
        $order->status = 4;
        $order->save();

        return ['res'=>10,'message'=>'سفارش شما با موفقیت تایید گردید'];

    }
    public function user_cancel_order($id,$secure){
        if($secure!= secure($id.'5')){
            return ['res'=>1,'message'=>'کلید امنیتی منقضی شده'];

        }
        if(!$order=Factor::with(['user','details'])->where('id',$id)->first()){
            session(['alert-danger'=>'سفارش یافت نشد']);
        }else if($order->user_id != \auth()->user()->id){
            session(['alert-danger'=>'سفارش متعلق به شما نیست']);
        }else{
            $status = $order->status;
            $order->status = -1;
            $order->save();
            session(['alert-success'=>'سفارش با موفقیت لغو گردید']);
        }

        return back();
    }

    public function user_save_change_order(Request $request){
        if(!\auth()->check() || \auth()->user()->role!=1)
            return ['res'=>1,'message'=>'شما مجاز به انجام اینکار نیستید'];
        if($request->secure != secure($request->id))
            return ['res'=>1,'message'=>'کلید امنیتی شما منقضی شده'];

        if(!$order=Factor::with(['user','details'])->where('id',$request->id)->first()){
            return ['res'=>1,'message'=>'سفارش یافت نشد !'];
        }
        $r = $request->all();
        foreach ($order->details as $detail){
            $numb = $r['numb_'.$detail->id] ?? $detail->numb;
            if($numb < 1){
                Factor_detail::destroy($detail->id);
            }else {
                $detail->product_name = $r['name_' . $detail->id] ?? $detail->product_name;
                $detail->numb = $numb;
                $detail->unit = $r['unit_' . $detail->id] ?? $detail->unit;
                $detail->price = fa2la($r['price_' . $detail->id]) ?? $detail->price;

                $detail->save();
            }
        }
        $order->expire = AB_lifetime_order();
        $order->status = 3;
        $order->save();

       return ['res'=>10,'message'=>'تغییرات با موفقیت اعمال شد'];
    }

//    public function make_view($view,$data){
//        $user_token = $this->user_token();
//        session(['user_token'=>$user_token]);
//        $response = new Response(view($view,$data));
//        return $response->withCookie(cookie('user_token', $user_token , 1000));
//    }
//
//    private function user_token(){
//        if(!$user_token = session('user_token',false)){
//            $user_token = Cookie::get('user_token',Helpers::randString(15,25));
//        }
//
//        return $user_token;
//    }


}
