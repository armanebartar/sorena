<?php

namespace App\Http\Controllers\app;


use App\Http\Controllers\Controller;
use App\Models\Factor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;


class ProfileController extends Controller{

    public function confirm_mobile(){
        $app_code_register = session('app_code_register',[]);
        if(!isset($app_code_register['code']) || !isset($app_code_register['numb']))
            return redirect()->route('app');
        if($app_code_register['numb'] > 10){
            session(['app_code_register'=>[]]);
            session()->remove('app_code_register');
            session(['alert-danger'=>'تعداد دفعات ثبت کد به پایان رسیده.']);
            return redirect()->route('app');
        }
        return $this->make_view('app.confirm_mobile',[]);

    }
    public function confirm_mobile_save(Request $request){
        $app_code_register = session('app_code_register',[]);
        if(!isset($app_code_register['code']) || !isset($app_code_register['numb']))
            return ['res'=>9];
        if($app_code_register['numb'] > 10){
            session(['app_code_register'=>[]]);
            session()->remove('app_code_register');
            session(['alert-danger'=>'تعداد دفعات ثبت کد به پایان رسیده.']);
            return ['res'=>9];
        }
        $r = $request->all();
        if(!isset($r['code']) || strlen($r['code']) < 1){
            return ['res'=>1,'message'=>'وارد کردن کد تایید الزامیست'];
        }
        if($app_code_register['code'] != fa2la($request->code)){
            $app_code_register['numb'] += 1;
            session(['app_code_register'=>$app_code_register]);
            return ['res'=>1,'message'=>'کد وارد شده صحیح نمیباشد'];
        }
        if(!$user = User::where('mobile',$app_code_register['mobile'])->first()){
            $user = new User();
            $user->name = '';
            $user->family = '';
            $user->email = '';
            $user->mobile = $app_code_register['mobile'];
            $user->role = 2;
            $user->grade = 1;
            $user->importer = 0;
            if(Cookie::get('aff',false) && $user_aff = User::where('aff_code',Cookie::get('aff','sdsd'))->first() ){

                $user->importer_aff = $user_aff->id;
            }
        }
        $user->mobile_active = 1;
        $user->status = 1;
        $user->username = $app_code_register['mobile'];
        $user->password = User::hashPass($app_code_register['code']);
        $user->save();

        Auth::loginUsingId($user->id,true);
        session(['alert-success'=>'حساب کاربری شما با موفقیت فعال گردید']);
        session(['app_code_register'=>[]]);
        session()->remove('app_code_register');

        $user_token = $this->user_token();
        if($fff = Factor::where([['user_token',$user_token],['user_id',0]])->get()){
            foreach ($fff as $ff){
                $ff->user_id = $user->id;
                $ff->save();
            }
        }

        if(strlen($user->name)>0)
            return ['res'=>9];
        return ['res'=>10];

    }

    public function profile(){
        if(!\auth()->check()) {
            session(['alert-danger'=>'لطفا ابتدا وارد حساب کاربریتان شوید']);
            return redirect()->route('app_login');
        }
        return $this->make_view('app.edit_profile',[]);

    }

    public function profile_save(Request $request){
        if(!\auth()->check())
            return ['res'=>9];
        $user = User::where('id',\auth()->user()->id)->first();
        $user->name = $request->name;
        $user->company = $request->company;
        $user->company_post = $request->company_post;
        $user->tel = $request->tel;
        $user->address = $request->address;
        $user->save();
        return ['res'=>10,'message'=>'تغییرات با موفقیت ذخیره گردید'];

    }



//    private function make_view($view,$data){
//        $app = new AppController();
//        return $app->make_view($view,$data);
//    }


}
