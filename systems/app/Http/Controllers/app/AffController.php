<?php

namespace App\Http\Controllers\app;

use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;


use App\Models\Factor;
use App\Models\Factor_detail;

use App\Models\Log_aff;
use App\Models\User;
use Faker\Extension\Helper;
use http\Exception\BadConversionException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;


class AffController extends Controller{
    private $_affilates = [
        0=>[6,'محمد دهنوی'],
        1=>[35,'محمدمهدی پویافر'],
        2=>[20,'احسان پویافر'],
        3=>[106,'منیژه علی اکبریان'],
        4=>[111,'ملیحه علی اکبریان'],
        5=>[19,'سعید پارسایی'],
        6=>[21,'علیرضا زیوری'],
        7=>[84,'یکتا لطفی'],
        8=>[82,'حمید محمدی'],
        9=>[17,'دهنوی ۲'],
//        10=>[35,''],
    ];
    public function render(){

        $id = trim(substr($_SERVER['REQUEST_URI'],strlen($_SERVER['REQUEST_URI'])-3),'/');
        $id = str_replace('a','',$id);

        if(Cookie::get('aff',false) && $user = User::where('aff_code',Cookie::get('aff','sdsd'))->first() ){
            $user->aff_count += 1;
            $user->save();
            $this->addLog($user->id,Cookie::get('user_token',100)==100,0);
        } elseif($user = User::where('id',$this->_affilates[$id])->first()){
            if(strlen($user->aff_code)<5){
                $user->aff_code = Helpers::randString(25,30);
            }
            $user->aff_count += 1;
            $user->save();
            $this->addLog($user->id,Cookie::get('user_token',100)==100,1);
        } else{
            return redirect()->route('home');
        }


        return redirect()->route('home')->withCookie(cookie('aff', $user->aff_code , 7200));
    }

    private function addLog($userId,$first,$first_aff){
        $log = new Log_aff();
        $log->url = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $log->method = $_REQUEST['REQUEST_METHOD'] ?? 'get';
        $log->ip = $_SERVER['REMOTE_ADDR'];
        $log->agent = $_SERVER['HTTP_USER_AGENT'];
        $log->user_id = \auth()->check() ? \auth()->user()->id : 0;
        $log->aff_id = $userId;
        $log->first_aff = $first_aff;
        $log->first = $first;
        $log->save();
    }

}
