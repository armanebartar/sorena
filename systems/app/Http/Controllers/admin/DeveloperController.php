<?php

namespace App\Http\Controllers\admin;

use App\Models\Option;
use App\Models\Option_groupe;
use App\Models\Permission;
use App\Models\User_permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class DeveloperController extends Controller
{
    public function index(){
        return view('panel.artisans');
    }

    public function saveController(Request $request){
        $val = strtolower($request->controller);
        $val = str_replace('\\','/',$val);
        $path = '' ;
        $nameArray = explode('/',$val);
        $count = count($nameArray) - 1;
        foreach ($nameArray as $k=>$val) {
            $path .= $k==$count ? ucfirst(str_replace('controller','',$val)) : $val.'/';
        }
        $path.='Controller';
        if(is_file(__DIR__.'/../'.$path.'.php')){
            session(['alert-danger' => 'کنترلر تکراریست.']);
        }else {
            Artisan::call('make:controller' , ['name'=>$path]);
            session(['alert-success' => 'کنترلر با موفقیت ایجاد شد.']);
        }
        return redirect()->back()->withInput();
    }

    public function saveModel(Request $request){
        $name = ucfirst(strtolower($request->model));
        if(is_file(__DIR__.'/../../../'.$name.'.php')){
            session(['alert-danger' => 'مدل تکراریست.']);
        }else {
            Artisan::call('make:model' , ['name'=>$name]);
            session(['alert-success' => 'مدل با موفقیت ایجاد شد.']);
        }
        return redirect()->back()->withInput();
    }

    public function artisanCommand($command){
        if($command == 'cacheClear') {
            session(['alert-success' => 'کش پاک شد.']);
            Artisan::call('cache:clear');
        }
        elseif($command == 'viewClear') {
            session(['alert-success' => 'ویوها پاک شدند.']);
            Artisan::call('view:clear');
        }
        elseif($command == 'configClear') {
            session(['alert-success' => 'کانفیگها پاک شدند.']);
            Artisan::call('config:clear');
        }
        elseif($command == 'configCatch') {
            session(['alert-success' => 'کانفیگها کش شدند.']);
            Artisan::call('config:cache');
        }
        elseif($command == 'optimize') {
            session(['alert-success' => 'به حالت بهینه رفت.']);
            Artisan::call('optimize');
        }
        elseif($command == 'routeCache') {
            session(['alert-success' => 'روتها کش شدند']);
            Artisan::call('route:cache');
        }
        elseif($command == 'routeClear') {
            session(['alert-success' => 'روتهای کش شده پاک شدند.']);
            Artisan::call('route:clear');
        }
        else{
            session(['alert-danger' => 'دستور یافت نشد.']);
        }
        return redirect()->back();
    }

    public function addPostType()
    {
        return view('panel.addPostType');
    }

    public function addPostTypeSave(Request $request)
    {
        $content = file_get_contents(__DIR__.'/addPostType.txt');

            $_metas="'title'=>'".$request->input('title')."',\n";
            $_metas.="'idMenuName'=>'".$request->input('idMenuName')."',\n";
            $_metas.="'nameTotal'=>'".$request->input('nameTotal')."',\n";
            $_metas.="'nameSingle'=>'".$request->input('nameSingle')."',\n";
            $_metas.="'baseRout'=>'".$request->input('baseRout')."',\n";
            $_metas.="'type'=>'".$request->input('type')."',\n";

        $cName=strtolower($request->input('controller'));


            $_permissions="'articlesShow'=>'".$cName.'Show'."',\n";
            $_permissions.="'addArticle'=>'".'add'.$cName."',\n";
            $_permissions.="'articleEdit'=>'".$cName.'Edit'."',\n";
            $_permissions.="'myArticleEdit'=>'".'my'.$cName.'Edit'."',\n";
            $_permissions.="'articleRemove'=>'".$cName.'Remove'."',\n";
            $_permissions.="'myArticleRemove'=>'".'my'.$cName.'Remove'."',\n";
            $_permissions.="'articleChangeStatus'=>'".$cName.'ChangeStatus'."',\n";


        $content = str_replace('@@@@meta',$_metas,$content);
        $content = str_replace('@@@@permission',$_permissions,$content);

        $controllerName = ucfirst($cName).'Controller';
        $content = str_replace('ArticlesController',$controllerName,$content);
        file_put_contents(__DIR__.'/'.$controllerName.'.php',$content);

        $id=$request->input('idMenuName');
        $nameTotal=$request->input('nameTotal');
        $nameSingle=$request->input('nameSingle');
        $title=$request->input('title');
        $baseRout = $request->input('baseRout');

        $menu = <<<EOS
        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="far fa-newspaper"></i>
                <span>{$nameTotal}</span>
            </a>
            <ul class="ml-menu">

                <li>
                    <a href="{{route('{$baseRout}_add')}}">افزودن {$nameSingle} جدید</a>
                </li>
                <li>
                    <a id="{$id}" href="{{route('{$baseRout}')}}">{$title}</a>
                </li>

            </ul>
        </li>

{{--@@@@menuAddPostType--}}

EOS;

        $route = <<<EOS
Route::prefix('panel/{$cName}')->group(function () {
    Route::get('/e/{sort?}/{page?}', 'panel\\{$controllerName}@index')->name("{$baseRout}")->middleware("admin");
    Route::get('add', 'panel\\{$controllerName}@add')->name("{$baseRout}_add")->middleware("admin");
    Route::post('addSave', 'panel\\{$controllerName}@addSave')->name("{$baseRout}_addSave")->middleware("admin");
    Route::get('edit/{id}', 'panel\\{$controllerName}@edit')->name("{$baseRout}_edit")->middleware("admin");
    Route::post('editSave', 'panel\\{$controllerName}@editSave')->name("{$baseRout}_editSave")->middleware("admin");
    Route::post('remove', 'panel\\{$controllerName}@remove')->name("{$baseRout}_remove")->middleware("admin");
    Route::post('activated', 'panel\\{$controllerName}@activated')->name("{$baseRout}_activated")->middleware("admin");
});

//@@@@@##

EOS;


        $menuPath = __DIR__.'/../../../../resources/views/layouts/admin.blade.php';
        $menuContent = file_get_contents($menuPath);
        $menuContent = str_replace('{{--@@@@menuAddPostType--}}',$menu,$menuContent);
        file_put_contents($menuPath,$menuContent);

        $routePath = __DIR__.'/../../../../routes/web.php';
        $routContent = file_get_contents($routePath);
        $routContent = str_replace('//@@@@@##',$route,$routContent);
        file_put_contents($routePath,$routContent);

        $permissions =[
            [$cName.'Show','مشاهده '.$request->input('nameTotal')],
            ['add'.$cName,'افزودن '.$request->input('nameSingle').' جدید'],
            [$cName.'Edit','ویرایش '.$request->input('nameTotal')],
            ['my'.$cName.'Edit','ویرایش '.$request->input('nameTotal').' خودش'],
            [$cName.'Remove','حذف '.$request->input('nameTotal')],
            ['my'.$cName.'Remove','حذف '.$request->input('nameTotal').' خودش'],
            [$cName.'ChangeStatus','تغییر وضعیت '.$request->input('nameTotal')],
            ];

        foreach ($permissions as $k=>$permission){
            $perrr=Permission::updateOrCreate(['slug'=>$permission[0]],[
                'name'=>$permission[1],
                'slug'=>$permission[0],
                'group'=>30,
                'gr_name'=>$request->input('nameTotal'),
            ]);

            $userPermission = new User_permission();
            $userPermission->user_id = Auth::guard('admin')->user()->id ;
            $userPermission->permission_id = $perrr->id;
            $userPermission->status = 1;
            $userPermission->save();
        }

        session(['alert-success' => 'پست تایپ جدید افزوده گردید.']);

return redirect()->back();
    }

    public function options(){
        $data['groups']=Option_groupe::all();
        $data['options']=Option::getOptions();

        return view('panel.dev_options',$data);
    }

    public function optionsSave(Request $request)
    {
//        dd($request->all());
        $group = $request->groupe;
        $titles=$request->title;
        $types=$request->type;
        foreach ($request->key as $kk=>$value) {
            if (strlen($value) > 3 && strlen($titles[$kk]) > 3) {

            Option::updateOrCreate(['key' => $value, 'groupe' => $group], [
                'title' => $titles[$kk],
                'type' => $types[$kk],
                'key' => $value,
                'groupe' => $group,
            ]);
        }

        }
        session(['alert-success' => 'تنظیمات ذخیره شدند.']);
        return redirect()->back();
    }

    public function groupOptionSave(Request $request)
    {
        if (strlen($request->groupName) > 2) {
            $oo = new Option_groupe();
            $oo->name = $request->groupName;
            $oo->save();
            session(['alert-success' => 'گروه جدید اضافه شد.']);
        }else{
            session(['alert-danger' => 'عملیات با خطا مواجه گردید.']);
        }
        return redirect()->back();
    }

}
