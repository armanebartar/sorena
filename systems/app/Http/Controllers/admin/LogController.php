<?php

namespace App\Http\Controllers\admin;

use App\Models\LogActivity;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogController extends Controller
{
    public function home(Request $r,$page=0){
        $data['total']=LogActivity::where('id','>','0')->count();
        $page=is_numeric($page) ? $page : 0;
        $data['page']=$page;

        $data['maxRow']=30;
        $start=$page * $data['maxRow'];
        $data['targetPage']=route('admin_logActivity_all');
        $logs=LogActivity::where('id','>','0')->skip($start)->take($data['maxRow'])
            ->select('id','created_at','user_type','user_id','subject','method')
            ->get();
        $data['tbl'] = $this->tbl($logs);

        $data['users']=User::where('id','>','0')->select('id','family','username')->get();
        $data['subTitle']='';
        return view('admin.logActivity',$data);
    }

    public function filter($userId, $start, $end,$page=0)
    {
        $start1=explode('-',$start);
        $start1=jalali_to_gregorian($start1[0],$start1[1],$start1[2]);
        $start1[1] = $start1[1]>10 ? $start1[1] : '0'.$start1[1];
        $start1[2] = $start1[2]>10 ? $start1[2] : '0'.$start1[2];
        $start1=$start1[0].'-'.$start1[1].'-'.$start1[2].' 00:00:00';

        $end1=explode('-',$end);
        $end1=jalali_to_gregorian($end1[0],$end1[1],$end1[2]);
        $timestampEnd=strtotime($end1[0].'-'.$end1[1].'-'.$end1[2]) + 86400;
        $end1=date('Y-m-d',$timestampEnd).' 00:00:00';



        $where=[['created_at','>=',$start1]];
        $where[]=['created_at','<',$end1];
        if(is_numeric($userId) && $userId>0)
            $where[]=['user_id',$userId];

        $data['total']=LogActivity::where($where)->count();
        $page=is_numeric($page) ? $page : 0;
        $data['page']=$page;

        $data['maxRow']=30;
        $start2=$page * $data['maxRow'];
        $data['targetPage']=route('admin_logActivity_filter',['userId'=>$userId,'start'=>$start,'end'=>$end]);
        $logs=LogActivity::where($where)->skip($start2)->take($data['maxRow'])
            ->select('id','created_at','user_type','user_id','subject','method')
            ->get();
        $data['tbl'] = $this->tbl($logs);

        $data['users']=User::where('id','>','0')->select('id','family','username')->get();
        $data['subTitle']='از تاریخ '.$start.' تا تاریخ '.$end;
        return view('admin.logActivity',$data);
    }

    public function activityUser(Request $r,$id,$type,$page=0){
        $data['total']=LogActivity::where([['user_id',$id],['user_type',$type]])->count();
        $page=is_numeric($page) ? $page : 0;
        $data['page']=$page;

        $data['maxRow']=20;
        $start=$page * $data['maxRow'];
        $data['targetPage']=route('admin_logActivity_user',['id'=>$id,'type'=>$type]);

//            $data['tbl']=$this->tbl(LogActivity::where([['user_id',$r->id],['user_type',$r->type]])->get());

        $logs=LogActivity::where([['user_id',$id],['user_type',$type]])->skip($start)->take($data['maxRow'])
            ->select('id','created_at','user_type','user_id','subject','method')
            ->get();
        $data['tbl'] = $this->tbl($logs);
        $data['subTitle']='';
        $data['users']=User::where('id','>','0')->select('id','family','username')->get();


        return view('admin.logActivity',$data);
    }

    public function logDetails(Request $r){
        $result['res']=1;

        if($log=LogActivity::find($r->id)){
            $result['res']=10;
            $result['subject']=$log->subject;
            $result['url']=$log->url;
            $result['method']=$log->method;
            $result['ip']=$log->ip;
            $result['agent']=$log->agent;
            $result['created_at']=$log->created_at;
            $result['type']=$log->user_type;

            $result['inputs']='';
            if ($log->inputs) {
                foreach (json_decode($log->inputs, true) as $k => $value)
                    $result['inputs'] .= "<tr><td>" . $k . "</td><td>" . $value . "</td></tr>";
            }else
                $result['inputs']='<tr><td colspan="2">هیچ داده ای برای این صفحه ارسال نکرده است.</td></tr>';
            $result['errors']='';
            if($log->errors) {
                foreach (json_decode($log->errors, true) as $k => $value)
                    $result['errors'] .= "<tr><td>" . $k . "</td><td>" . $value . "</td></tr>";
            }else
                $result['errors']='<tr><td colspan="2">هیچ خطایی در این صفحه دریافت نکرده.</td></tr>';
        }
        return json_encode($result);
    }

    public function removeLog(Request $r){
        $result['res']=1;
        if(LogActivity::where('id',$r->id)->count() > 0){
           LogActivity::destroy($r->id) ;
            $result['res']=10;
        }
        return json_encode($result);
    }

    private function tbl($logs){
        $logs=$logs->sortByDesc("created_at")->values()->all();
        $result='';

        foreach ($logs as $k=>$val){
//            $t=jDate::forge($val->created_at)->format(' H:i:s Y/m/d   ');
            $t=jdate('H:i Y-m-d',strtotime($val->created_at));
            $r=$k+1;
            $name='';
            $linkUser='';
            $lll='';
            $linkUser=route('admin_logActivity_user',['id'=>$val->user_id,'type'=>$val->user_type]);
            if($val->user_type == 'مدیر') {
                $name = isset($val->admin1->family)?$val->admin1->family:' ';
                $lll="<a href='".$linkUser."' target='_blank'>".$name."</a>";
            }
            elseif ($val->user_type == 'کاربر') {
//                $linkUser=route('admin_logActivity_all').'?type='.$val->user_type.'&id='.$val->user_id;
                $name = isset($val->user1->family)?$val->user1->family:'';
                $lll="<a href='".$linkUser."' target='_blank'>".$name."</a>";
            }

            $result.=<<<EOS
        <tr class="r_{$k}">
            <td>{$r}</td>
            <td>{$val->subject}</td>
            <td>{$lll}</td>
            <td>{$val->user_type}</td>
            <td>{$t}</td>
            <td>{$val->method}</td>
            <td class="customer_td_actions" data-id="$val->id">
            <button class="btn btn-info btn-round show" style="display: inline !important;" data-id="{$val->id}" data-name="{$name}" data-time="{$t}" >
           <i class="icon-eye"></i>
            </button>
             <a data-row="r_{$k}" data-id="{$val->id}" class="btn btn-danger btn-round remove">
           <i class="icon-trash"></i>
             </a>
            </td>
        </tr>
EOS;

        }
        return $result;
    }
}
