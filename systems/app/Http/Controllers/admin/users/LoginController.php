<?php

namespace App\Http\Controllers\admin\users;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function loginAdmin(){
        if(auth('admin')->check())
            return redirect()->route('p_dashboard');
 //       addLog('ورود به صفحه لاگین');
        return view('admin.users.login');
    }

    public function loginAdminLogin(Request $request){

        $errors = [];
        if(session('cpLoginAdmin','nn') == strtolower($request->captcha)) {
            if ($user = User::where('username', $request->username)->first()) {

                if ($user->status > 0) {
                    if (User::hashPass($request->pass) == $user->password) {

                        $roleAdmin = [1, 10, 11];
                        if (in_array($user->role, $roleAdmin)) {
                            addLog('ورود بعنوان ادمین', ['pk' => $request->username]);
                            if ($request->input('remember')) {
                                Auth::guard("admin")->loginUsingId($user->id, true);
                            }
                            else {
                                Auth::guard("admin")->loginUsingId($user->id);
                            }
                            return redirect()->route('p_dashboard');
                        } else {
                            addLog('ورود بعنوان کاربر', ['pk' => $request->username]);
                            if ($request->input('remember'))
                                Auth::loginUsingId($user->id, true);
                            else
                                Auth::loginUsingId($user->id);

                            return redirect()->route('home');
                        }

                    } else {
                        addLog('تلاش ناموفق برای لاگین', ['pk' => $request->username], 'پسورد اشتباه است');
                        $errors [] = 'پسورد صحیح نیست';
                    }
                } else {
                    addLog('تلاش ناموفق برای لاگین', ['pk' => $request->username], 'حساب کاربری مسدود است');
                    $errors [] = 'حساب کاربری شما غیرفعال شده است.';
                }
            } else {
                addLog('تلاش ناموفق برای لاگین', ['pk' => $request->username], 'نام کابری اشتباه است');
                $errors [] = 'نام کاربری صحیح نیست';
            }
        } else {
            addLog('تلاش ناموفق برای لاگین', ['pk' => $request->username], 'کد امنیتی اشتباه است');
            $errors [] = 'کد امنیتی صحیح نیست';
        }
        session(['errors' => $errors]);
        return redirect()->back()->withInput();
    }

    public function logout(){
        addLog('خروج از حساب کاربری');

        if (Auth::check())
            Auth::logout();
        if (Auth::guard('admin')->check())
            Auth::guard('admin')->logout();
        return redirect()->route('loginAdmin');

    }

}
