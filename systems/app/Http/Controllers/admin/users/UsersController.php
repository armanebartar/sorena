<?php

namespace App\Http\Controllers\admin\users;

use App\Helpers\Helpers;
use App\Models\User;
use App\Models\User_option;
use Hamcrest\Thingy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;



class UsersController extends Controller
{
    public function index(){
        if(forbidden('userShow',1))
            return redirect(route('p_dashboard'));
        addLog('مشاهده لیست اعضای سایت');
        $data['tbl'] = $this->tbl();
        return view('admin.users.users', $data);
    }

    public function activated(Request $request){

        if(!permission('userChangeStatus',1))
            return ['res' => 1, 'myAlert' => 'در حال حاضر شما مجاز به این عملیات نیستید.', 'mySuccess' => ''];
        $ppp = $request->status == 1 ? 'فعال':'غیرفعال';
        if($user = User::where([['id',$request->id],['role',2]])->select('*')->first()){
            $user->status = $request->status;
            $user->save();
            addLog($ppp.' کردن اعضا',['id'=>$user->id,'name'=>$user->name.' '.$user->family]);

            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'کاربر مورد نظر با موفقیت '.$ppp.' گردید.','tr'=>$this->tr($user,$request->row)];

        }else {
            return ['res' => 1, 'myAlert' => 'کاربر یافت نشد !', 'mySuccess' => ''];
        }


    }

    public function addSave(Request $request){
        $mode = 'edit';
        if(!$user = User::where([['id',$request->id],['role','2']])->first()){
            if(!permission('userAdd',1))
                return ['res' => 1, 'myAlert' => 'در حال حاضر مجوز اینکار را ندارید.', 'mySuccess' => ''];
            $mode = 'add';
            $user = new User();
        }else {
            if (!permission('userEdit', 1))
                return ['res' => 1, 'myAlert' => 'در حال حاضر مجوز اینکار را ندارید.', 'mySuccess' => ''];
        }


        $errors = [];
        if(strlen($request->name)<2)
            $errors [] = 'وارد کردن نام الزامیست و حداقل باید دو کارکتر داشته باشد.';
        if(strlen($request->family)<2)
            $errors [] = 'وارد کردن نام خانوادگی الزامیست و حداقل باید دو کارکتر داشته باشد.';

        if(!preg_match('/^09[0-9]{9}$/s',$request->mobile))
            $errors [] = 'موبایل وارد شده صحیح نمی‌باشد.';
        elseif($u2=User::where([['mobile',$request->mobile],['role','2']])->first()) {
            if ($mode == 'add' || $u2->id != $request->id)
                $errors [] = 'موبایل وارد شده تکراریست.';
        }


        if($mode=='add' && strlen($request->pass)<6)
            $errors [] = 'وارد کردن پسورد الزامیست و حداقل باید شش کارکتر داشته باشد.';

        if($errors != []){
            if($mode == 'add')
                addLog('افزودن ناموفق عضو جدید',$request->all(),$errors);
            else
                addLog('ویرایش ناموفق عضو',$request->all(),$errors);
            $err = '';
            foreach ($errors as $error)
                $err.=$error.'<br>';
            return ['res' => 1, 'myAlert' => $err, 'mySuccess' => ''];
        }

        if($mode == 'add')
            addLog('افزودن عضو جدید',$request->all());
        else
            addLog('ویرایش عضو',$request->all());

        $user->username = $request->mobile;
        $user->name = $request->name;
        $user->family = $request->family;
        $user->mobile = $request->mobile;
        $user->company = $request->company;
        $user->company_post = $request->postt;
        $user->tel = $request->tel;
        $user->address = $request->address;
        if(strlen($request->pass)>5) {
            if($mode == 'add' || permission('userPass'))
                $user->password = User::hashPass($request->pass);
        }
        $user->status = 1;
        $user->role = 2;
        $user->grade = 1;
        if($mode == 'add')
            $user->importer = auth('admin')->user()->id;

        $user->save();

        if ($mode=='edit')
            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'تغییرات با موفقیت ذخیره گردید.','tr'=>$this->tr($user,$request->row)];

        $r = session('final_id_user');
        $r++;
        return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'کاربر جدید با موفقیت ذخیره گردید.','tr'=>$this->tr($user,$r)];
    }

    public function remove(Request $request){
        if(!permission('userRemove',1))
            return ['res' => 1, 'myAlert' => 'در حال حاضر شما مجاز به حذف کاربر نیستید.', 'mySuccess' => ''];

        if($uu=User::where([['id',$request->id],['role',2]])->first()){
            addLog('حذف عضو',['id'=>$uu->id,'name'=>$uu->name.' '.$uu->family,'mobile'=>$uu->mobile]);
            User::destroy($request->input('id'));
            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'کاربر با موفقیت حذف گردید.'];
        }else {
            addLog('حذف ناموفق عضو',$request->all(),'آیدی نامعتبر');
            return ['res' => 1, 'myAlert' => 'حذف با خطا مواجه گردید.', 'mySuccess' => ''];
        }

    }

    private function tbl(){
        $result = '';
        $users = User::with(['adviser1','adviser2'])->where('role', 2)->select('*')->get();
        foreach ($users as $k=>$user){
            $result .=$this->tr($user,$k+1);
        }
        return $result;
    }

    private  function  tr($user,$r){
        session(['final_id_user'=>$r]);
        if(permission('userChangeStatus')) {
            $activeCode = "<span data-placement=\"top\"  data-toggle=\"tooltip\" title='برای فعال کردن کلیک کنید' data-status='1' data-st='فعال' class='btn activated btn-outline-danger btn-border-radius'>غیرفعال</span>";
            if ($user->status == 1)
                $activeCode = "<span data-placement=\"top\"  data-toggle=\"tooltip\" title='برای غیرفعال کردن کلیک کنید' data-status='0' data-st='غیر فعال' class='btn activated btn-outline-success btn-border-radius'>فعال</span>";
        }else{
            $activeCode = $user->status == 1 ? '<b style="color:green">فعال</b>' : '<b style="color: red;">غیرفعال</b>';
        }

//            dd(User::imagePath('/').$user->image);
//        if(is_file(User::imagePath('/').$user->image)) {
//            $thumbnail = url(User::imagePath('/') . User::imageSizes()[0][0] . '_' . $user->image);
//            $image = url(User::imagePath('/').$user->image);
//        }else{
//            $thumbnail = url(User::thumbnail());
//            $image = url(User::defaultImage());
//        }
        $date = jdate('Y-m-d',strtotime($user->created_at));
        $adviser = '--';
        if($user->adviser1)
            $adviser = $user->adviser1->name.' '.$user->adviser1->family;
        elseif($user->adviser2)
            $adviser = $user->adviser2->name.' '.$user->adviser2->family;
        $result = <<<EOS
            <tr data-id="{$user->id}" data-name="{$user->name} {$user->family}"
                            data-fname="{$user->name}"
                            data-lname="{$user->family}"
                            data-mobile="{$user->mobile}"
                            data-company="{$user->company}"
                            data-postt="{$user->company_post}"
                            data-tel="{$user->tel}"
                            data-address="{$user->address}"
                            data-row="{$r}"

                            >
                 <td class="center-align">{$r}</td>
                 <td>{$user->name} {$user->family}</td>
                 <td>{$user->company}</td>
                 <td>{$user->company_post}</td>
                 <td class="center-align">{$user->mobile}</td>
                 <td class="center-align">{$user->tel}</td>
                 <td class="center-align actShow">{$activeCode}</td>
                 <td class="center-align">{$date}</td>

                 <td>$adviser </td>
                 <td class="actions">

EOS;
        if(permission('userEdit'))
            $result .= <<<EOS
                     <a data-toggle="tooltip" data-placement="top" href="#" title="ویرایش کاربر" type="button" class="btn btn-info">
                         <i class="material-icons">create</i>
                     </a>
EOS;
        if(permission('userRemove'))
            $result .= <<<EOS
                     <a  data-toggle="tooltip" data-placement="top" href="#" title="حذف کاربر" type="button" class="btn btn-danger">
                         <i class="material-icons"> clear</i>
                     </a>
EOS;
        $result .= <<<EOS
                 </td>
             </tr>
EOS;
        return $result;
    }

}
