<?php

namespace App\Http\Controllers\admin\users;

use App\Helpers\Helpers;
use App\Models\User;
use App\Models\User_option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function edit(){
        addLog('ورود به صفحه پروفایل');
        $data['level'] =session('editLevelProfile',0);
        $data['tab'] = 'profile';
        session(['editLevelProfile'=>0]);

        return view('panel.users.editProfile',$data);
    }

    public function editSave(Request $request){
        $userId = Auth::guard('admin')->user()->id;

        $errors = [];
        if(strlen($request->name)<2)
            $errors [] = 'وارد کردن نام الزامیست و حداقل باید دو کارکتر داشته باشد.';
        if(strlen($request->family)<2)
            $errors [] = 'وارد کردن نام خانوادگی ازامیست و حداقل باید دو کارکتر داشته باشد.';


        if(!preg_match('/^09[0-9]{9}$/s',$request->mobile))
            $errors [] = 'موبایل وارد شده صحیح نمی‌باشد.';
        elseif($u2=User::where([['mobile',$request->mobile],['id','<>',$userId],['role','1']])->first())
            $errors [] = 'موبایل وارد شده تکراریست.';


        if($errors != []){
            addLog('ویرایش ناموفق اطلاعات کاربری',$request->all(),$errors);
            session(['errors' => $errors,'editLevelProfile'=>1]);
            return redirect()->route('p_profile_edit')->withInput();
        }
        addLog('ویرایش اطلاعات کاربری',$request->all(),$errors);

        $user=User::find($userId);
        $imageIndex = Helpers::checkAndSaveImage($request,'image',User::imagePath(),User::imageSizes(),User::imageMaxValue(),$user->image);
        if(is_file(User::imagePath('/').$imageIndex))
            $user->image = $imageIndex;
        if($imageIndex == 'notImage'){
            session(['errors' => ['فایل ارسالی یک عکس معتبر نمی‌باشد.']]);
        }

        foreach (['name','family','email','tel','address','mobile'] as $vvv) {
            $rq = $vvv;
            if ($user->$rq != $request->$vvv && !is_null($user->$rq)) {
                $uuu = new User_option();
                $uuu->user_id = $userId;
                $uuu->key = 'old_'.$vvv;
                $uuu->value = $user->$rq;
                $uuu->save();
            }
        }


        $user->name = $request->name;
        $user->family = $request->family;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->tel = $request->tel;
        $user->address = $request->address;
        $user->save();
        session(['alert-success' => 'اطلاعات با موفقیت ذخیره گردید.']);
        User::userOptions(Auth::guard('admin')->user()->id,true);
        return redirect()->route('p_profile_edit');
    }

    public function pass(){
        addLog('ورود به صفحه تغییر پسورد');
        $data['level'] =session('editLevelProfile',0);
        $data['tab'] ='pass';
        session(['editLevelProfile'=>0]);

        return view('panel.users.editProfile',$data);
    }

    public function changePass(Request $request)
    {

       if(strlen($request->pass) > 5){
           if($request->pass == $request->pass2){
               $user = User::find(Auth::guard('admin')->user()->id);
               $user->password = User::hashPass($request->pass);
               $user->save();
               addLog('تغییر پسورد');

               session(['suc_changePass'=>'پسورد جدید با موفقیت جایگزین گردید.']);
           }else {
               addLog('تغییر ناموفق پسورد',$request->all(),'پسوردها یکسان نیستند');

               session(['err_changePass' => 'پسوردها با هم یکسان نیستند.']);
           }
       }else {
           addLog('تغییر ناموفق پسورد',$request->all(),'پسورد کوتاه است');

           session(['err_changePass' => 'پسورد باید حداقل شش کارکتر داشته باشد.']);
       }
       return redirect()->route('p_profile_pass');
    }

}
