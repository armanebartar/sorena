<?php

namespace App\Http\Controllers\admin\users;

use App\Helpers\Helpers;
use App\Models\Permission;
use App\Models\User;
use App\Models\User_option;
use App\Models\User_permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;



class StaffController extends Controller{

    public function index(){
        if(forbidden('staffList',1))
            return redirect(route('p_dashboard'));
        addLog('مشاهده لیست پرسنل سایت');
        $data['tbl'] = $this->tbl();
        return view('admin.users.staff', $data);
    }

    public function activated(Request $request){

        if(!permission('staffChangeStatus',1))
            return ['res' => 1, 'myAlert' => 'در حال حاضر شما مجاز به این عملیات نیستید.', 'mySuccess' => ''];
        $ppp = $request->status == 1 ? 'فعال':'غیرفعال';
        if($user = User::where([['id',$request->id],['role',1]])->select('*')->first()){
            $user->status = $request->status;
            $user->save();
            addLog($ppp.' کردن پرسنل',['id'=>$user->id,'name'=>$user->name.' '.$user->family]);

            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'کاربر مورد نظر با موفقیت '.$ppp.' گردید.','tr'=>$this->tr($user,$request->row)];

        }else {
            return ['res' => 1, 'myAlert' => 'کاربر یافت نشد !', 'mySuccess' => ''];
        }


    }

    public function addSave(Request $request){
        $mode = 'edit';
        if(!$user = User::where([['id',$request->id],['role','1']])->first()){
            if(!permission('staffAdd',1))
                return ['res' => 1, 'myAlert' => 'در حال حاضر مجوز اینکار را ندارید.', 'mySuccess' => ''];
            $mode = 'add';
            $user = new User();
        }else {
            if (!permission('staffEdit', 1))
                return ['res' => 1, 'myAlert' => 'در حال حاضر مجوز اینکار را ندارید.', 'mySuccess' => ''];
        }


        $errors = [];
        if(strlen($request->name)<2)
            $errors [] = 'وارد کردن نام الزامیست و حداقل باید دو کارکتر داشته باشد.';
        if(strlen($request->family)<2)
            $errors [] = 'وارد کردن نام خانوادگی الزامیست و حداقل باید دو کارکتر داشته باشد.';

        if(!preg_match('/^09[0-9]{9}$/s',$request->mobile))
            $errors [] = 'موبایل وارد شده صحیح نمی‌باشد.';
        elseif($u2=User::where([['mobile',$request->mobile],['role','1']])->first()) {
            if ($mode == 'add' || $u2->id != $request->id)
                $errors [] = 'موبایل وارد شده تکراریست.';
        }


        if($mode=='add' && strlen($request->pass)<6)
            $errors [] = 'وارد کردن پسورد الزامیست و حداقل باید شش کارکتر داشته باشد.';

        if($errors != []){
            if($mode == 'add')
                addLog('افزودن ناموفق پرسنل جدید',$request->all(),$errors);
            else
                addLog('ویرایش ناموفق پرسنل',$request->all(),$errors);
            $err = '';
            foreach ($errors as $error)
                $err.=$error.'<br>';
            return ['res' => 1, 'myAlert' => $err, 'mySuccess' => ''];
        }

        if($mode == 'add')
            addLog('افزودن پرسنل جدید',$request->all());
        else
            addLog('ویرایش پرسنل',$request->all());

        $user->username = $request->mobile;
        $user->name = $request->name;
        $user->family = $request->family;
        $user->mobile = $request->mobile;
        $user->tel = $request->tel;
        $user->address = $request->address;
        if(strlen($request->pass)>5) {
            if($mode == 'add' || permission('staffPass'))
                $user->password = User::hashPass($request->pass);
        }
        $user->status = 1;
        $user->role = 1;
        $user->grade = 1;
        $user->importer = auth('admin')->user()->id;

        $user->save();

        if ($mode=='edit')
            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'تغییرات با موفقیت ذخیره گردید.','tr'=>$this->tr($user,$request->row)];

        $r = session('final_id_user');
        $r++;
        return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'کاربر جدید با موفقیت ذخیره گردید.','tr'=>$this->tr($user,$r)];
    }

    public function remove(Request $request){
        if(!permission('staffRemove',1))
            return ['res' => 1, 'myAlert' => 'در حال حاضر شما مجاز به حذف کاربر نیستید.', 'mySuccess' => ''];

        if($uu=User::where([['id',$request->id],['role',1]])->first()){
            addLog('حذف پرسنل',['id'=>$uu->id,'name'=>$uu->name.' '.$uu->family,'mobile'=>$uu->mobile]);
            User::destroy($request->input('id'));
            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'کاربر با موفقیت حذف گردید.'];
        }else {
            addLog('حذف ناموفق عضو',$request->all(),'آیدی نامعتبر');
            return ['res' => 1, 'myAlert' => 'حذف با خطا مواجه گردید.', 'mySuccess' => ''];
        }

    }

    public function permissions($userId)
    {
        $uu = User::find($userId);
        if(!(Auth::guard('admin')->user()->grade == 10 || Auth::guard('admin')->user()->grade == 11)) {
            if($uu)
                addLog('ورود غیر مجاز به صفحه سطوح دسترسی',['id'=>$uu->id,'name'=>$uu->name.' '.$uu->family,'mobile'=>$uu->mobile,'pk'=>$uu->pk]);
            else
                addLog('ورود غیر مجاز به صفحه سطوح دسترسی',['id'=>$userId,'name'=>'آیدی معتبر نبود']);

            session(['alert-danger'=>'شما مجوز تغییر سطح دسترسی را ندارید.']);
            return redirect(route('p_dashboard'));
        }
        if(!$data['user'] = User::where('id',$userId)->first()) {
            session(['alert-danger'=>'کاربر مورد نظر یافت نشد.']);
            addLog('ورود ناموفق به صفحه سطوح دسترسی',['id'=>$userId],'آیدی نامعتبر');

            return redirect()->back();
        }
        $data['userId']=$userId;
        addLog('ورود به صفحه سطوح دسترسی',['id'=>$uu->id,'name'=>$uu->name.' '.$uu->family,'mobile'=>$uu->mobile,'pk'=>$uu->pk]);
        $data['permissions']=Permission::getPermissionsGrouped();
        $data['userPer']=User::getAllPermissions($userId,1);

        return view('admin.users.staff_permissions',$data);
    }

    public function permissionsSave(Request $request)
    {
        if(!$uu = User::find($request->input('id'))){
            addLog('تغییر ناموفق سطوح دسترسی',['id'=>$request->input('id')]);
        }
        if(!(Auth::guard('admin')->user()->grade == 10 || Auth::guard('admin')->user()->grade == 11)) {
            session(['alert-danger'=>'شما مجوز تغییر سطح دسترسی را ندارید.']);
            addLog('ورود غیر مجاز به صفحه سطوح دسترسی',['id'=>$uu->id,'name'=>$uu->name.' '.$uu->family,'mobile'=>$uu->mobile,'pk'=>$uu->pk]);
            return redirect(route('p_dashboard'));
        }
        User_permission::where('user_id', $request->input('id'))->delete();
        foreach ($request->all() as  $key=>$value){
            if(is_numeric($key)){
                $usrPer=new User_permission();
                $usrPer->user_id = $request->input('id');
                $usrPer->permission_id = $key;
                $usrPer->status = 1;
                $usrPer->save();
            }
        }
        $ddd = array_merge(['id'=>$uu->id,'name'=>$uu->name.' '.$uu->family,'mobile'=>$uu->mobile,'pk'=>$uu->pk],$request->all());
        addLog('تغییر سطوح دسترسی پرسنل',$ddd);

        session(['alert-success'=>'سطوح دسترسی با موفقیت ذخیره گردید.']);
        return redirect()->back();

    }




    private function tbl(){
        $result = '';
        $users = User::where([['role', 1]])->select('*')->get();
        foreach ($users as $k=>$user){
            $result .=$this->tr($user,$k+1);
        }
        return $result;
    }

    private  function  tr($user,$r){
        session(['final_id_user'=>$r]);
        if(permission('staffChangeStatus')) {
            $activeCode = "<span data-placement=\"top\"  data-toggle=\"tooltip\" title='برای فعال کردن کلیک کنید' data-status='1' data-st='فعال' class='btn activated btn-outline-danger btn-border-radius'>غیرفعال</span>";
            if ($user->status == 1)
                $activeCode = "<span data-placement=\"top\"  data-toggle=\"tooltip\" title='برای غیرفعال کردن کلیک کنید' data-status='0' data-st='غیر فعال' class='btn activated btn-outline-success btn-border-radius'>فعال</span>";
        }else{
            $activeCode = $user->status == 1 ? '<b style="color:green">فعال</b>' : '<b style="color: red;">غیرفعال</b>';
        }

//            dd(User::imagePath('/').$user->image);
        if(is_file(User::imagePath('/').$user->image)) {
            $thumbnail = url(User::imagePath('/') . User::imageSizes()[0][0] . '_' . $user->image);
            $image = url(User::imagePath('/').$user->image);
        }else{
            $thumbnail = url(User::thumbnail());
            $image = url(User::defaultImage());
        }
        $date = jdate('Y-m-d',strtotime($user->created_at));
        $linkPermission = route('p_staff_permissions',['userId'=>$user->id]);
        $result = <<<EOS
            <tr data-id="{$user->id}" data-name="{$user->name} {$user->family}"
                            data-fname="{$user->name}"
                            data-lname="{$user->family}"
                            data-mobile="{$user->mobile}"
                            data-tel="{$user->tel}"
                            data-address="{$user->address}"
                            data-row="{$r}"

                            >
                 <td class="center-align">{$r}</td>
                 <td>{$user->name} {$user->family}</td>
                 <td class="center-align">{$user->mobile}</td>
                 <td class="center-align">{$user->mobile}</td>
                 <td class="center-align">{$user->tel}</td>
                 <td class="center-align actShow">{$activeCode}</td>
                 <td class="center-align">{$date}</td>

                 <td><img data-source="{$image}" src="{$thumbnail}" /> </td>
                 <td class="actions">

EOS;
        if(Auth::guard('admin')->user()->grade == 10 || Auth::guard('admin')->user()->grade == 11)
            $result .= <<<EOS
                     <a data-toggle="tooltip" data-placement="top" href="{$linkPermission}" title="تعیین سطوح دسترسی" type="button" class="btn btn-warning ">
                         <i class="material-icons"> lock </i>
                     </a>
EOS;
        if(permission('staffEdit'))
            $result .= <<<EOS
                     <a data-toggle="tooltip" data-placement="top" href="#" title="ویرایش کاربر" type="button" class="btn btn-info">
                         <i class="material-icons">create</i>
                     </a>
EOS;
        if(permission('staffRemove'))
            $result .= <<<EOS
                     <a  data-toggle="tooltip" data-placement="top" href="#" title="حذف کاربر" type="button" class="btn btn-danger">
                         <i class="material-icons"> clear</i>
                     </a>
EOS;
        $result .= <<<EOS
                 </td>
             </tr>
EOS;
        return $result;
    }

}
