<?php

namespace App\Http\Controllers\admin;

use App\Models\Analytic;
use App\Models\Comment;
use App\Helpers\Helpers;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class DashboardController extends Controller
{
    public function index(){
        addLog('مشاهده داشبورد');
        $data = [];

        return view('admin.dashboard',$data);
    }

    private function getAnaliticsDay($beforDay,$mode = 'all'){
        $result = '';
        if($mode=='all')
            for ($i=$beforDay;$i>=0;$i--){
//                dd(date('Y-m-d',time() - (86400*$i)));
                $result .= Analytic::where([['day',date('Y-m-d',time() - (86400*$i))]])->count()*4 .',';
            }
        elseif ($mode == 'electronic'){
            for ($i=$beforDay;$i>=0;$i--){
//                dd(date('Y-m-d',time() - (86400*$i)));
                $result .= Service_analytic::where([['day',date('Y-m-d',time() - (86400*$i))],['mode','all']])->count()*4 .',';
            }
        }
        elseif ($mode == 'staff'){
            for ($i=$beforDay;$i>=0;$i--){
//                dd(date('Y-m-d',time() - (86400*$i)));
                $result .= Service_analytic::where([['day',date('Y-m-d',time() - (86400*$i))],['mode','staff']])->count()*4 .',';
            }
        }

        return trim($result,',');
    }

    public function p_upload_image(Request $request)
    {
        dd($request->all());
        $res = Helpers::checkAndSaveImage($request,'image','files/uploads',[[200,200]],2000);
        $error = '';
        $result = '';
        $resCode = 1;
        if($res = 'notFileFound') $error = 'فایل بدرستی آپلود نشد.';
        elseif($res = 'notImage') $error = 'فایل ارسالی یک تصویر معتبر نیست.';
        elseif(!is_file('files/uploads/'.$res)) $error = 'عملیات آپلود با خطا مواجه گردید.';
        elseif($res = 'notFileFound') $error = 'فایل بدرستی آپلود نشد.';
        else{
            $error = 'آپلود با موفقیت انجام شد.';
            $result = 'files/uploads/'.$res;
            $resCode = 10;
        }

        return ['result'=>$resCode,'alert'=>$error,'res'=>$result];
    }


    public function ftpHome()
    {
        return view('admin/ftpHome');
    }

    public function ftpSave(Request $request)
    {
//        request()->file('user-file')->store('files', 'ftp');

        $type = substr($request->file('userFile')->getClientOriginalName(),strrpos($request->file('user-file')->getClientOriginalName(),'.'));
        $fileName = 'upl_'.time().'_'.rand(1,10000).$type;
        $res = Storage::disk('ftp')->put($fileName, fopen($request->file('userFile'), "r+"));
        echo $fileName;
        dd($res);


        return back();
    }


    public function uploadImage(Request $request){
        if(forbidden('uploadFtpFile',1))
            return "عملیات با خطا مواجه گردید.";
        $inputs = $request->all();
        $output_dir = "files/uploads/images/";
        $res = Helpers::checkAndSaveImage($request,'myfile',$output_dir,[[200,200]],20000);
        if($res == 'notFileFound')
            return "عملیات با خطا مواجه گردید.";
        elseif($res == 'notImage')
            return "فایل انتخابی یک تصویر معتبر نیست.";
        elseif($res == 'isLong')
            return "حجم تصویر انتخابی زیاد است.";
        elseif(is_file($output_dir.$res)) {
            $post = new Post();
            $post->title = $inputs['alt']?$inputs['alt']:$res;
            $post->slug = $res;
            $post->author = Auth::guard('admin')->user()->id;
            $post->content = $inputs['desc']?$inputs['desc']:'image';
            $post->status = 'publish';
            $post->comment = 'deactive';
            $post->parent = 0;
            $post->type = 'upload';
            $post->mime_type = 'image';
            $post->path = $output_dir;
            $post->save();
            return "<b>شورتکد</b> : "."[upload_".$post->id."]"."<br><small>".url($output_dir . $res)."</small>";
        }
        return "عملیات با خطا مواجه گردید.";
    }

    public function uploadImageByUrl(Request $request){
        if(forbidden('uploadFtpFile',1))
            return "عملیات با خطا مواجه گردید.";

        if(@$fileContent = file_get_contents($request->myfile)) {
            $type = substr($request->myfile, strrpos($request->myfile, '.'));
            $res = 'upl_' . time() . '_' . rand(1, 10000) . $type;

            $inputs = $request->all();
            $output_dir = "files/uploads/images/";
            file_put_contents($output_dir . $res, $fileContent);
            if (is_file($output_dir . $res)) {
                if (substr(mime_content_type($output_dir . $res), 0, 6) == 'image/') {
                    Image::make($output_dir . $res)->resize(200, 200)->save($output_dir . '200_' . $res);
                    $post = new Post();
                    $post->title = $inputs['alt']?$inputs['alt']:$res;
                    $post->slug = $res;
                    $post->author = Auth::guard('admin')->user()->id;
                    $post->content = $inputs['desc']?$inputs['desc']:$res;
                    $post->status = 'publish';
                    $post->comment = 'deactive';
                    $post->parent = 0;
                    $post->type = 'upload';
                    $post->mime_type = 'image';
                    $post->path = $output_dir;
                    $post->save();
                    return "شورتکد : " . "[upload_" . $post->id . "]" . "<br>" . url($output_dir . $res) . "<hr>";
                } else {
                    unlink($output_dir . $res, $fileContent);
                    return 'فایل مورد نظر یک عکس معتبر نیست.';
                }
            }
        }

        return "عملیات با خطا مواجه گردید.";
    }

    public function uploadFile(Request $request){
        if(forbidden('uploadFtpFile',1))
            return "عملیات با خطا مواجه گردید.";
        $output_dir = "files/uploads/files/";
        $inputs = $request->all();
        $res = Helpers::checkAndSaveFile($inputs['myfile'],$output_dir,['image','pdf','mpeg','mp3','mp4','excel','ppt','doc','csv','json','rar','txt','xml','zip'],50000);
        if($res == 'notFileValid')
            return "عملیات با خطا مواجه گردید.";
        elseif(is_array($res) && isset($res[0]) && $res[0] == 'mimeNotValid')
            return "فرمت فایل مناسب نیست.";
        elseif(is_array($res) && isset($res[0]) && $res[0] == 'isLongFile')
            return "حجم فایل بیش از حد است.";
        elseif(is_array($res) && isset($res[0]) && is_file($output_dir.$res[0]))
            return url($output_dir.$res[0]);
        return "عملیات با خطا مواجه گردید.";
    }

    public static function getPermission($userId,$permissionName='',$removeCatch=false){
        $permissions=User::getAllPermissions($userId,$removeCatch);
        return isset($permissions[$permissionName])?$permissions[$permissionName]:[0,'ناشناخته'];
    }

    public function getNotify()
    {
        $comment = Comment::where([['visited',0],['post_id','>','0']])->count();
        $contact = Comment::where([['visited',0],['post_id','0']])->count();
        return ['comment'=>$comment , 'contact'=>$contact];
    }
}
