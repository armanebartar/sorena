<?php

namespace App\Http\Controllers\admin\settings;


use App\Helpers\Helpers;

use App\Models\Banner;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{

    public function home($lang =DEFAULT_LANG[0]){

        if(forbidden('banners',1))
            return redirect(route('p_dashboard'));

//        addLog('ورود به صفحه اسلایدشوها');
        $lang = $lang==DEFAULT_LANG[0] || array_key_exists($lang,LANGUAGES) ? $lang : DEFAULT_LANG[0];
        if($lang !=DEFAULT_LANG[0]){
            foreach (Banner::where('lang',DEFAULT_LANG[0])->get() as $bb){
                if(Banner::where([['lang',$lang],['parent',$bb->id]])->count() < 1){
                    $ban = new Banner();
                    $ban->page = $bb->page;
                    $ban->location = $bb->location;
                    $ban->image = $bb->image;
                    $ban->path = $bb->path;
                    $ban->title = $bb->title;
                    $ban->link = $bb->link;
                    $ban->size = $bb->size;
                    $ban->lang = $lang;
                    $ban->parent = $bb->id;
                    $ban->save();
                }
            }
        }

        $data['banners']=Banner::getBanners($lang);

        $data['title'] = $lang!=DEFAULT_LANG[0] && array_key_exists($lang,LANGUAGES) ? '('.LANGUAGES[$lang][1].')' : '';
        $data['lang'] = $lang;
        $data['default'] = session('groupBannerSave',1);


//        session(['groupBannerSave'=>$data['banners'][0]->page]);

        return view('admin.settings.banners',$data);
   }

    public function edit(Request $request){
        if(secure($request->id) != $request->secure)
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید.','mySuccess'=>''];
        if(!$banner = Banner::where('id',$request->id)->first())
            return ['res'=>1,'myAlert'=>'بنر یافت نشد.','mySuccess'=>''];

        $image_sizes = [[64,64],explode('*',$banner->size)];


        $image = Helpers::checkAndSaveImage($request,'image',Banner::imagePath(),$image_sizes,Banner::imageMaxValue());
        if( $image == 'notImage')
            return ['res'=>1,'myAlert'=>'تصویر انتخابی نامعتبر است.','mySuccess'=>''];
        elseif($image == 'isLong')
            return ['res'=>1,'myAlert'=>'تصویر انتخابی حجیم است.','mySuccess'=>''];


        $banner->title = $request->title ;
        $banner->link = $request->link;
        $banner->path = Banner::imagePath();
        if(is_file(ROOT.$banner->path.'/'.$image))
            $banner->image = $image ;
        $banner->save();

        Banner::getByPage($banner->page,1,$banner->lang);
        return [
            'res'=>10,
            'myAlert'=>'',
            'mySuccess'=>'تغییرات با موفقیت ذخیره گردید.',
            'tbl'=>self::tbl(Banner::where([['page',$banner->page],['lang',$banner->lang]])->get())
        ];

    }

    public static function tbl($banners){
        $result = '';
        $kk=0;
        foreach ($banners as $slide){
            $kk++;
            $image = url($slide->path.'/64_64_'.$slide->image);
            $image1 = url($slide->path.'/'.$slide->image);
            $secure = secure($slide->id);
            $result .= <<<EOS
                <tr>
                    <td>{$kk}</td>
                    <td class="desc">{$slide->location}</td>
                    <td class="title">{$slide->title}</td>
                    <td><img class="imagee" data-img="{$image1}" src="{$image}" > </td>
                    <td class="actions" data-id="{$slide->id}" data-size="{$slide->size}" data-secure="{$secure}" data-link="{$slide->link}" data-title="{$slide->title}">
                        <a data-placement="top" data-toggle="tooltip" target="_blank" href="#" title="ویرایش "  class="btn btn-info edited">
                            <i class="material-icons">create</i>
                        </a>
                        <a data-placement="top"  data-toggle="tooltip" title="حذف "  class="btn btn-danger removed">
                            <i class="material-icons"> clear</i>
                        </a>
                    </td>
                </tr>
EOS;

        }
        return $result;
    }
}
