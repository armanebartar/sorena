<?php

namespace App\Http\Controllers\admin\settings;

use App\Helpers\Helpers;
use App\Models\Option;
use App\Models\Option_groupe;
use App\Models\Option_value;
use App\Models\Post;
use App\Models\Slider;
use App\Models\Slider_groupe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{

    public function slider($lang =DEFAULT_LANG[0]){
//        dd(Slider::getSliderByGroupId(1));

        if(forbidden('slider',1))
            return redirect(route('p_dashboard'));
//        addLog('ورود به صفحه اسلایدشوها');
        $lang = $lang==DEFAULT_LANG[0] || array_key_exists($lang,LANGUAGES) ? $lang : DEFAULT_LANG[0];
        $data['groups']=Slider_groupe::all();
        $data['slides']=Slider::getSliders($lang,$data['groups']);

        $data['title'] = $lang!=DEFAULT_LANG[0] && array_key_exists($lang,LANGUAGES) ? '('.LANGUAGES[$lang][1].')' : '';
        $data['lang'] = $lang;
        $data['default'] = session('groupOptionSave',1);

        session(['groupSlideSave'=>1]);

        return view('admin.settings.sliders',$data);
   }

    public function addSlider(Request $request){

        if (!permission('slider', 1))
            return ['res'=>1,'myAlert'=>'در حال حاضر مجاز به انجام این عملیات نیستید.','mySuccess'=>''];

        if(Post::secure([$request->lang,$request->group]) != $request->secure)
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید.','mySuccess'=>''];
        if(!$group = Slider_groupe::where('id',$request->group)->first())
            return ['res'=>1,'myAlert'=>'گروه یافت نشد.','mySuccess'=>''];
        $image_sizes = [];
        foreach (explode('**',$group->image_sizes) as $val)
            $image_sizes[]=explode('*',$val);

        $image = Helpers::checkAndSaveImage($request,'image',Slider::imagePath(),$image_sizes,Slider::imageMaxValue());
        if($image == 'notFileFound')
            return ['res'=>1,'myAlert'=>'انتخاب تصویر اسلایدشو الزامیست.','mySuccess'=>''];
        elseif($image == 'notImage')
            return ['res'=>1,'myAlert'=>'تصویر انتخابی نامعتبر است.','mySuccess'=>''];
        elseif($image == 'isLong')
            return ['res'=>1,'myAlert'=>'تصویر انتخابی حجیم است.','mySuccess'=>''];

        $slide = new Slider();
        $slide->group = $request->group ;
        $slide->title = $request->title ;
        $slide->desc = $request->desc ;
        $slide->link = $request->link ;
        $slide->image = $image ;
        $slide->path = Slider::imagePath() ;
        $slide->lang = $request->lang ;
        $slide->link_btn = $request->link_btn ;
        $slide->titre = $request->titre ;
        $slide->sort = is_numeric($request->sort) && $request->sort>=0 ? $request->sort : 1;
        $slide->save();
        Slider::getSliderByGroupId($request->group,$request->lang,1);
        return ['res'=>10,'myAlert'=>'','mySuccess'=>'اسلاید جدید با موفقیت ذخیره گردید.','tbl'=>self::tbl(Slider::where([['group',$request->group],['lang',$request->lang]])->get())];

    }

    public function editSlider(Request $request){
        if(Post::secure([$request->id]) != $request->secure)
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید.','mySuccess'=>''];
        if(!$slide = Slider::where('id',$request->id)->first())
            return ['res'=>1,'myAlert'=>'اسلاید یافت نشد.','mySuccess'=>''];
       $group = Slider_groupe::where('id',$slide->group)->first();
        $image_sizes = [];
        foreach (explode('**',$group->image_sizes) as $val)
            $image_sizes[]=explode('*',$val);

        $image = Helpers::checkAndSaveImage($request,'image',$slide->path,$image_sizes,Slider::imageMaxValue());
        if($image == 'notImage')
            return ['res'=>1,'myAlert'=>'تصویر انتخابی نامعتبر است.','mySuccess'=>''];
        elseif($image == 'isLong')
            return ['res'=>1,'myAlert'=>'تصویر انتخابی حجیم است.','mySuccess'=>''];


        $slide->title = $request->title ;
        $slide->desc = $request->desc ;
        $slide->link = $request->link ;
        $slide->link_btn = $request->link_btn ;
        $slide->titre = $request->titre ;
        $slide->sort = is_numeric($request->sort) && $request->sort>=0 ? $request->sort : 1;
        if(is_file(ROOT.$slide->path.'/'.$image))
            $slide->image = $image ;
        $slide->save();

        return ['res'=>10,'myAlert'=>'','mySuccess'=>'تغییرات با موفقیت ذخیره گردید.','tbl'=>self::tbl(Slider::where([['group',$group->id],['lang',$slide->lang]])->get())];

    }

    public function removeSlider(Request $request)
    {
        if(Post::secure([$request->id]) != $request->secure)
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید.','mySuccess'=>''];
        if(!$slide = Slider::where('id',$request->id)->first())
            return ['res'=>1,'myAlert'=>'اسلاید یافت نشد.','mySuccess'=>''];
        $groupp = $slide->group;


        $group = Slider_groupe::where('id',$slide->group)->first();
        $image_sizes = [];
        foreach (explode('**',$group->image_sizes) as $val)
            $image_sizes[]=explode('*',$val);
        foreach ($image_sizes as $image_size){
            if(is_file(ROOT.$slide->path.'/'.$image_size[0].'_'.$image_size[1].'_'.$slide->image))
                unlink(ROOT.$slide->path.'/'.$image_size[0].'_'.$image_size[1].'_'.$slide->image);
        }
        if(is_file(ROOT.$slide->path.'/'.$slide->image))
            unlink(ROOT.$slide->path.'/'.$slide->image);
        $lang = $slide->lang;
        Slider::destroy($slide->id);
        Slider::getSliderByGroupId($groupp,$lang,1);
        return ['res'=>10,'myAlert'=>'','mySuccess'=>'اسلاید با موفقیت حذف گردید.','tbl'=>self::tbl(Slider::where([['group',$group->id],['lang',$lang]])->get())];

    }

    public static function tbl($slides){
        $result = '';
        $kk=0;

        foreach ($slides as $slide){
            $kk++;
            $image = url($slide->path.'/'.$slide->image);
            $secure = Post::secure($slide->id);
            $result .= <<<EOS
                <tr>
                    <td>{$kk}</td>
                    <td class="title">{$slide->title}</td>
                    <td class="desc">{$slide->desc}</td>
                    <td><img src="{$image}" > </td>
                    <td class="actions" data-id="{$slide->id}" data-titre="{$slide->titre}" data-btn="{$slide->link_btn}" data-sort="{$slide->sort}" data-secure="{$secure}" data-link="{$slide->link}">
                        <a data-placement="top" data-toggle="tooltip" target="_blank" href="#" title="ویرایش "  class="btn btn-info edited">
                            <i class="icon-pencil"></i>
                        </a>
                        <a data-placement="top"  data-toggle="tooltip" title="حذف "  class="btn btn-danger removed">
                            <i class="icon-trash"> </i>
                        </a>
                    </td>
                </tr>
EOS;

        }
        return $result;
    }
}
