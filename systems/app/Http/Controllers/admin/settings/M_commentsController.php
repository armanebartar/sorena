<?php

namespace App\Http\Controllers\admin\settings;

use App\Helpers\Helpers;
use App\Models\M_comment;
use App\Models\Option;
use App\Models\Option_groupe;
use App\Models\Option_value;
use App\Models\Post;
use App\Models\Slider;
use App\Models\Slider_groupe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class M_commentsController extends Controller
{

    public function list($lang =DEFAULT_LANG[0]){
        $data['tbl'] = $this->tbl();
        return view('admin.settings.comments',$data);
   }

    public function add(Request $request){
        if (!permission('m_comments', 1))
            return ['res'=>1,'myAlert'=>'در حال حاضر مجاز به انجام این عملیات نیستید.','mySuccess'=>''];

        $message = 'تغییرات با موفقیت ذخیره شد';
        if($request->id==0 || !$slide = M_comment::where('id',$request->id)->first()) {
            $slide = new M_comment();
            $message = 'نظر جدید با موفقیت ذخیره گردید.';
        }
        $slide->name = $request->name ;
        $slide->company = $request->company ;
        $slide->desc = $request->desc ;

        $image = Helpers::checkAndSaveImage($request,'image',Slider::imagePath());
        if($image != 'notFileFound' && $image != 'notImage' && $image != 'isLong')
            $slide->image = Slider::imagePath('/').$image ;

        $slide->sort = is_numeric($request->sort) && $request->sort>=0 ? $request->sort : 1;
        $slide->save();

        return ['res'=>10,'myAlert'=>'','mySuccess'=>$message,'tbl'=>$this->tbl()];

    }

    public function remove(Request $request)
    {

        if(!$slide = M_comment::where('id',$request->id)->first())
            return ['res'=>1,'myAlert'=>'نظر یافت نشد.','mySuccess'=>''];


        M_comment::destroy($slide->id);
        return ['res'=>10,'myAlert'=>'','mySuccess'=>'نظر با موفقیت حذف گردید.','tbl'=>$this::tbl()];

    }

    public  function tbl(){
        $comments = M_comment::all();
        $result = '';
        foreach ($comments as $k=>$item){

            $image = is_file($item->image) ? "<img style='width:100px' src=\"".url($item->image)."\" >"  : '--';
            $r=$k+1;
            $result .= <<<EOS
                <tr>
                    <td>{$r}</td>
                    <td class="title">{$item->name}</td>
                    <td class="company">{$item->company}</td>
                    <td class="desc">{$item->desc}</td>
                    <td>{$image} </td>
                    <td class="actions" data-id="{$item->id}" data-name="{$item->name}" data-desc="{$item->desc}" data-sort="{$item->sort}"  data-company="{$item->company}">
                        <button type="button" data-placement="top" data-toggle="tooltip" target="_blank" href="#" title="ویرایش "  class="btn btn-info edited">
                            <i class="icon-pencil"></i>
                        </button>
                        <button type="button" data-placement="top"  data-toggle="tooltip" title="حذف "  class="btn btn-danger removed">
                            <i class="icon-trash"> </i>
                        </a>
                    </td>
                </tr>
EOS;

        }
        return $result;
    }
}
