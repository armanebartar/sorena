<?php

namespace App\Http\Controllers\admin\settings;

use App\Helpers\Helpers;
use App\Models\Category;
use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{

    public function index($lang='fa',$group=1){


        addLog('مشاهده منوها '.$lang);
        $data = $this->tbl_cat($lang,$group);
        $data['lang'] = $lang;
        $data['group'] = $group;
        return view('admin.settings.menus',$data);
    }

    public function addSave(Request $request,$lang='fa',$group=1){

        if(forbidden('menu',1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        if(!isset($request->name) || strlen($request->name) < 2 ) {
            addLog('ذخیره ناموفق منو جدید '.$lang.'_'.$group,$request->all(),'نام وجود ندارد یا کوتاه است');
            return ['res'=>1,'myAlert'=>'عنوان وارد شده نامعتبر است.','mySuccess'=>''];
        }

        $menu = new  Menu();
        $menu->name = $request->name;
        $menu->link =  $request->link;
        $menu->path = Menu::imagePath();
        if($request->hasFile('image')) {
            $image = Helpers::checkAndSaveImage($request, 'image', Menu::imagePath(), Menu::imageSizes(), Menu::imageMaxVolume());
            if (!is_file(Menu::imagePath('/') . $image)) {
                addLog('ذخیره ناموفق منو جدید ' . $lang.'_'.$group, $request->all(), 'تصویر نامعتبر');
                return ['res' => 1, 'myAlert' => 'تصویر انتخابی نامعتبر است.', 'mySuccess' => ''];
            }else {
                $menu->image =  $image;

            }

        }
        $menu->parent = $request->parent;
        $menu->sort = is_numeric($request->sort) && $request->sort > 0 ? $request->sort : 1;
        $menu->group = $group;
        $menu->lang = $lang;
        $menu->save();
        $tbl = $this->tbl_cat($lang,$group);

        addLog('ذخیره منو جدید '.$lang.'_'.$group,$request->all());

        return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'منو جدید با موفقیت ذخیره گردید.','tbl'=>$tbl['tbl'],'parentsTbl'=>$tbl['parentsTbl']];
    }

    public function remove(Request $request,$lang,$group){

        if(forbidden('menu',1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];


        if($cat=Menu::where('id',$request->input('id'))->first()){
            if($cat->group != $group){
                return ['res' => 1, 'myAlert' => 'منو مربوط به این دسته نمی‌باشد.', 'mySuccess' => ''];
            }
            if(Menu::where('parent',$cat->id)->count() > 0){
                return ['res' => 1, 'myAlert' => 'این منو دارای زیر منو است و نمیتوانید حذف کنید.', 'mySuccess' => ''];
            }
            addLog('حذف منو '.$lang.'_'.$group,['id'=>$cat->id,'name'=>$cat->name]);
            if(is_file(ROOT.$cat->path.'/'.$cat->image)){
                unlink(ROOT.$cat->path.'/'.$cat->image);
                foreach (Menu::imageSizes() as $meta){
                    if(isset($meta[0]) && is_file($cat->path.$meta[0].'_'.$cat->image))
                        unlink($cat->path.$meta[0].'_'.$cat->image);
                }
            }
            Menu::destroy($request->input('id'));



            $tbl = $this->tbl_cat($lang,$group);
            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'حذف منو با موفقیت انجام گردید.','tbl'=>$tbl['tbl'],'parentsTbl'=>$tbl['parentsTbl']];
        }else{
            addLog('حذف ناموفق منو',$request->all(),'آیدی گروه نامعتبر است .');
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید.', 'mySuccess' => ''];
        }

    }

    public function editSave(Request $request,$lang='fa',$group=1){

        if(forbidden('menu',1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];

        if($cat=Menu::where('id',$request->input('id'))->first()){
            if($cat->group != $group){
                return ['res' => 1, 'myAlert' => 'منو مربوط به این دسته نمی‌باشد.', 'mySuccess' => ''];
            }
            addLog('ویرایش منو '.$lang.'_'.$group,$request->all());

            $cat->name = $request->input('name');
            $cat->sort = is_numeric($request->sort) && $request->sort > 0 ? $request->sort : 1;
            $cat->link = $request->link;
            if(Menu::where('parent',$cat->id)->count() < 1)
                $cat->parent = $request->input('parent');

            $image = Helpers::checkAndSaveImage($request, 'image', $cat->path, Menu::imageSizes(), Menu::imageMaxVolume());
            if(is_file(ROOT.$cat->path.'/'.$image) ) {
                $cat->image = $image;
            }

            $cat->save();
            $tbl = $this->tbl_cat($lang,$group);
            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'ویرایش با موفقیت انجام گردید.','tbl'=>$tbl['tbl'],'parentsTbl'=>$tbl['parentsTbl']];
        }else
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید', 'mySuccess' => ''];


    }

    public function addLang(Request $request,$type){

        $this->setProperties($type);
        if(forbidden('menu',1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];

        if($cat=Category::where('id',$request->input('id'))->first()){
            if($cat->group != $this->_metas['id']){
                return ['res' => 1, 'myAlert' => 'گروه مربوط به این دسته نمی‌باشد.', 'mySuccess' => ''];
            }
            if(strlen($request->name) < 2){
                return ['res' => 1, 'myAlert' => 'نام الزامیست و باید حداقل دو کارکتر داشته باشد.', 'mySuccess' => ''];
            }
            addLog('ویرایش ترجمه '.$this->_metas['nameSingle'],$request->all());

            if(!$lang = Category::where([['parent_lang',$cat->id],['lang',$request->input('lang')]])->first()){
                $lang = new Category();
                $lang->group = $cat->group;
                $lang->parent_lang = $cat->id;
                $lang->lang = $request->input('lang');

            }
            $lang->name = $request->input('name');
            $slug = isset($request->slug) && strlen($request->slug) > 2 ? $request->slug : $request->input('name');
            $slug = Category::slugGenerate($slug,$request->input('id'));

            $lang->slug = $slug;
            $lang->save();

            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'ویرایش ترجمه با موفقیت انجام گردید.'];
        }else
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید', 'mySuccess' => ''];


    }


    private function tbl_cat($lang,$group){

        $cats = Menu::getByGroup($lang,$group,1);


        $result = '';
        $r=0;
        foreach ($cats as $k => $cat) {
            $result .= $this->make_row_cat($cat,++$r,'',count($cat->childes));
            if(count($cat->childes))
                foreach ($cat->childes as $childe)
                    $result .= $this->make_row_cat($childe,++$r,' --- ');


        }

        $parentsTbl = "<option value='0'>بدون والد</option>";
        foreach ($cats as $parent){
            $parentsTbl .= "<option value='{$parent->id}'>{$parent->name}</option>";
        }
        return ['tbl'=>$result,'parentsTbl'=>$parentsTbl];

    }

    private function make_row_cat($cat,$r,$beforeName = '',$childes=0){
        $image = '-';
//        if($cat->id > 1)
//        dd($cat,ROOT.$cat->path.'/'.$cat->image);
        if(is_file(ROOT.$cat->path.'/'.$cat->image))
            $image = "<img  src='".url($cat->path.'/'.$cat->image)."' style='max-width:100%'>";


        $name =  $cat->name;
        $result = <<<EOS
        <tr>
            <td class="center-align">{$r}</td>
            <td>{$beforeName}{$name}</td>
            <td>{$cat->link}</td>
            <td>{$image}</td>
            <td class="actions" data-childes="{$childes}" data-id="{$cat->id}" data-name="{$cat->name}" data-link="{$cat->link}" data-sort="{$cat->sort}" data-parent="{$cat->parentId}">

                <a data-toggle="tooltip" data-placement="top" href="#" title="ویرایش " type="button" class="btn btn-info edited">
                    <i class="material-icons">create</i>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="حذف " type="button" class="btn btn-danger removed ">
                    <i class="material-icons"> clear</i>
                </a>
            </td>
        </tr>
EOS;



        return $result;
    }
}
