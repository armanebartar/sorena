<?php

namespace App\Http\Controllers\admin\settings;

use App\Helpers\Helpers;
use App\Models\Option;
use App\Models\Option_groupe;
use App\Models\Option_value;
use App\Models\Permission;
use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OptionsController extends Controller
{
    public function index($lang=DEFAULT_LANG[0]){

        if(forbidden('showOptions',1))
            return redirect(route('p_dashboard'));
        addLog('ورود به صفحه تنظیمات سایت');
        $lang = $lang==DEFAULT_LANG[0] || array_key_exists($lang,LANGUAGES) ? $lang : DEFAULT_LANG[0];

        $data['groups']=Option_groupe::all();
        $this->checkPermissionSave($data['groups']);
        $data['options']=Option::getOptions($lang,$data['groups']);

        $data['title'] = $lang!=DEFAULT_LANG[0] && array_key_exists($lang,LANGUAGES) ? '('.LANGUAGES[$lang][1].')' : '';
        $data['lang'] = $lang;

        $data['default'] = session('groupOptionSave',1);
        session(['groupOptionSave'=>1]);
        return view('admin.settings.options',$data);
   }

   private function checkPermissionSave($groups){
        foreach ($groups as $group){
            if($group->status == 0 && Permission::where('slug','editOptions'.$group->id)->count() < 1){
                $per = new Permission();
                $per->slug = 'editOptions'.$group->id;
                $per->name = 'ویرایش تنظیمات '.$group->name;
                $per->group = 3;
                $per->save();
                Option_groupe::where('id',$group->id)->update(['status'=>1]);
            }
        }
   }

    public function save(Request $request){

        if(!permission('editOptions'.$request->groupId,1))
            return ['res'=>1,'myAlert'=>'در حال حاضر مجوز اینکار را ندارید','mySuccess'=>''];
        $req = $request->all();

        $options = Option::where('group',$request->groupId)->get();
        addLog('اعمال تنظیمات گروه '.$request->groupId,$req);

        $errors = [];
        foreach ($options as $val){
            $value = '';
            if(isset($req['value_'.$val->id])){

                if($val->type == 'text' || $val->type == 'textarea' || $val->type == 'ckeditor'){
                    if(strlen($req['value_'.$val->id]) > $val->size)
                        $errors[]="فیلد ".$val->title.' حداکثر میتواند '.$val->size . ' کارکتر داشته باشد.';
                    $value = $req['value_'.$val->id];
                }elseif ($val->type == 'image'){
                    if(isset($req['value_'.$val->id]) && is_file(ROOT.$req['value_'.$val->id])) {
                        $value = $req['value_'.$val->id];
                    }else
                        $errors [] = 'هیچ عکسی برای '.$val->title.' انتخاب نکرده اید.';
                }
            }
            if($value == '')
                continue;
            if(!$op = Option_value::where([['lang',$request->lang],['option_id',$val->id]])->first()){
                $op = new Option_value();
                $op->option_id = $val->id;
                $op->lang = $request->lang;
            }
            $op->value = $value;
            $op->save();

        }

        if($errors != []){
            $err = '';
            foreach ($errors as $error)
                $err .= "<p>{$error}</p>";
            return ['res'=>1,'myAlert'=>$err,'mySuccess'=>'بقیه موارد ذخیره شدند'];
        }
        Option::generateFileOptions();

        return ['res'=>10,'myAlert'=>'','mySuccess'=>'تمامی موارد با موفقیت ذخیره شدند'];
   }

    public function save_image(Request $request){
            $path = Option::imagePath('/');
            $size = 2 ;
            foreach ($request->all() as $key=>$item){
                if($request->hasFile($key)){
                    return Helpers::checkImageAjax($request,$key,$path,$size);
                }
            }
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک تصویر انتخاب نمایید'];


   }

}
