<?php

namespace App\Http\Controllers\admin\orders;


use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Models\Factor;
use App\Models\Factor_detail;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class FactorsController extends Controller{

    public function index($status='all'){
        if(forbidden('factorsShow'))
            return redirect(route('p_dashboard'));

        addLog('مشاهده لیست فاکتورها');

        $status = $status=='all'||in_array($status,[-1,0,1,2,3,4]) ? $status : 'all';
        $data['status'] = $status;

        if($status == 'all')
            $data['tbl']=$this->tbl(Factor::with(['user'])->where([['status','<>','0'],['id','>','1']])->orderBy('updated_at','DESC')->get());
        else
            $data['tbl']=$this->tbl(Factor::with(['user'])->where([['status',$status],['id','>','1']])->orderBy('updated_at','DESC')->get());
        $data['filter'] = '<strong>لیست</strong> فاکتورها';
        return view('admin.orders.factors',$data);
    }
    public function add(){
        addLog('ورود به صفحه افزودن فاکتور');
        return view('admin.orders.addFactor');
    }
    public function getFactorDetails(Request $request){
        if(!permission('factorsShow'))
            return ['res'=>2];
        if(isset($request->id) && $factor = Factor::with(['details','user'])->where('id',$request->id)->first()){
            $name = $factor->user ?  $factor->user->name.' '.$factor->user->family : '--';
            $mobile = $factor->user ? $factor->user->mobile : $factor->user_mobile;
            $company = $factor->user && $factor->user->company ? $factor->user->company :'--';

            addLog('مشاهده جزئیات فاکتور شماره '.$request->id,[$name,$mobile,$company]);

            $tbl = '';
            $total_all = 0;
            $discount_all = 0;
            foreach ($factor->details as $k=>$val){
                $r = $k+1;
                $total = ($val->price*$val->numb) - $val->discount;
                $total_all += $val->price*$val->numb;
                $discount_all += $val->discount;
                $tbl .= <<<EOS
                    <tr>
                         <td style="padding: 5px">{$r}</td>
                         <td style="padding: 5px">{$val->product_name}</td>
                         <td  style="padding: 5px">{$val->unit}</td>
                         <td class="number" style="padding: 5px">{$val->price}</td>
                         <td style="padding: 5px;text-align: center">{$val->numb}</td>
                         <td style="padding: 5px;text-align: center">{$val->discount}</td>
                         <td class="number" style="padding: 5px">{$total}</td>
                     </tr>
EOS;
            }

            $address = strlen($factor->address) > 3 ? $factor->address : '---';

            return [
                'res'=>10,
                'tbl'=>$tbl,
                'date'=> jdate('Y/m/d',$factor->time),
                'numbId'=> $factor->id,
                'name'=>$name ,
                'mobile'=>$mobile ,
                'company'=> $company,
                'address'=>$address ,
                'total_all'=> $total_all,
                'final'=> $total_all-$discount_all,
                'discount_all'=> $discount_all,

                'desc'=> $factor->desc && strlen($factor->desc) > 2 ? $factor->desc : '',
            ];
        }
        addLog('مشاهده ناموفق جزئیات فاکتور شماره '.$request->id);
        return ['res'=>1];
    }

    public function get_user_data(Request $request){
        if(!$user = User::where('id',$request->user_id)->first())
            return ['res'=>1];
        $tbl = "
        <div class=\"row\">
                <div class=\"col-4\">
                    <b>نام : </b>
                    <span class=\"name\">{$user->name} {$user->family}</span>
                </div>
                <div class=\"col-4\">
                    <b>شرکت : </b>
                    <span class=\"company\">{$user->company_post} {$user->company}</span>
                </div>
                <div class=\"col-4\">
                    <b>تلفن تماس : </b>
                    <span class=\"mobile\">{$user->mobile}</span>
                </div>
                <div class=\"col-12\" style='padding-top: 20px;'>
                    <b>آدرس : </b>
                    <span class=\"address\"><input type='text' name='address' class='form-control' value='{$user->address}'></span>
                </div>
            </div>
        ";

        return ['res'=>10,'tbl'=>$tbl];
    }

    public function await(Request $request){
        if(!permission('factorsSmsNotStock'))
            return json_encode(['res'=>1,'myAlert'=>'در حال حاضر مجاز به این کار نیستید.','mySuccess'=>'']);
        if(strlen($request->product) < 2)
            return json_encode(['res'=>1,'myAlert'=>'وارد کردن نام محصول الزامیست.','mySuccess'=>'']);
        if($request->mobile == '11111')
            return json_encode(['res'=>1,'myAlert'=>'متاسفانه موبایل کاربر در دسترس نیست.','mySuccess'=>'']);
        $name = strlen($request->name) > 2 ? $request->name : 'کاربر';
        $parameters = ['Name'=>$name,'Numb'=>$request->numb,'Product'=>$request->product];
        Sms::send_fast($request->mobile,'30385',$parameters);
        $parameters['mobile'] = $request->mobile;
        addLog('ارسال پیامک عدم موجودی',$parameters);
        return ['res'=>10,'myAlert'=>'','mySuccess'=>'پیامک برای کاربر ارسال شد.'];
    }

    public function getFactorDetailsEdit(Request $request){
        if(!permission('factorConfirmFirst') && !permission('factorConfirmLast') && !permission('factorTemporary'))
            return ['res'=>3];
        if(isset($request->id) && $factor = Factor::with(['user','details'])->where('id',$request->id)->first()){

            $name = $factor->user ? $factor->user->name.' '.$factor->user->family : '--';
            $mobile = $factor->user ? $factor->user->mobile : $factor->user_mobile;
            $company = $factor->user ? $factor->user->company:'--';

            addLog('مشاهده جزئیات فاکتور برای ویرایش شماره '.$request->id,[$name,$mobile,$company]);
            $tbl = '';
            $validPrice = true;
            $total = 0;
            $discount_all = 0;
            foreach ($factor->details as $k=>$val){
                $r = $k+1;
                if($val->price){
                    $amount = ($val->price - $val->discount) * $val->numb;
                    $price = $val->price;
                    $disc = $val->discount ;
                    $total += $val->price * $val->numb ;
                    $discount_all += $disc;
                }else{
                    $price = '0';
                    $disc = '0';
                    $amount='0';
                    $validPrice = false;
                }

                $nnn = preg_replace('/<br.{0,2}>/is',"",$val->product_name);
                $tbl .= <<<EOS
            <tr class="cl_{$val->id}">
                 <td class="td_row">{$r}<i class="far fa-times-circle remove_row_edited"></i></td>
                 <td class="prodNameEdit">
                 <textarea class="form-control inp" name="name_{$val->id}">{$nnn}</textarea>
                 <i data-id="{$val->id}" class="fas fa-search"></i>
                 </td>
                 <td class="unitField" style="padding: 5px"><input type="text" class="form-control" style="max-width: 77%" name="unit_{$val->id}" value="{$val->unit}"></td>
                 <td class="priceField" style="padding: 5px"><input type="text" class="form-control inp justNumber" name="price_{$val->id}" value="{$price}"></td>
                 <td class="numbField" style="padding: 5px;text-align: center"><input class="form-control inp" type="text" name="numb_{$val->id}" value="{$val->numb}"></td>
                 <td class="discField" style="padding: 5px;text-align: center"><input class="form-control inp" type="text" name="disc_{$val->id}" value="{$disc}"></td>
                 <td class="tot number" style="padding: 5px">{$amount}</td>
             </tr>
EOS;

            }

            $transs = $factor->transport_cost;
            if(!$validPrice) {
                $total = ' -----';
                $final = '-----';
                $discount = '0';
                $trans = '0';
            }else{
                $discount = $factor->discount;

                $final = $total - $factor->discount + $transs - $discount_all;
            }


            $address = strlen($factor->address_static) > 3 ? $factor->address_static : $factor->user->address ?? '--';


            return [
                'res'=>10,
                'tbl'=>$tbl,
                'date'=> jdate('Y/m/d',$factor->time),
                'numbId'=> $factor->id,
                'name'=>$name ,
                'mobile'=>$mobile ,
                'company'=> $company,

                'total'=> $total,
                'final'=> $final,
                'discount'=> $discount,
                'transs'=> $transs,
                'address'=>$address ,

                'factId'=> $factor->id,
                'factStatus'=> $factor->status,
                'desc'=> $factor->desc && strlen($factor->desc) > 2 ? $factor->desc : '',
            ];
        }
        addLog('مشاهده ناموفق جزئیات فاکتور برای ویرایش شماره '.$request->id);
        return ['res'=>1];
    }

    public function editSaveInterim(Request $request){

        if(!permission('factorTemporary')){
            return ['res'=>10,'myAlert'=>'در حال حاضر مجاز به ذخیره موقت نیستید.','mySuccess'=>''];
        }


        return $this->save_change($request);


    }

    private function save_change($request,$type='interim'){
        $all = $request->all();

        if(isset($request->factor_id) && $factor = Factor::where('id',$request->factor_id)->first()){
//            if($factor->status > 3)
//                return ['res'=>1,'myAlert'=>'سفارش در وضعیت تایید قرار ندارد','mySuccess'=>''];
            if($type!='interim' && $factor->status==3 && !permission('factorConfirmLast')){
                return ['res'=>1,'myAlert'=>'در حال حاضر مجاز به تایید نهایی سفارش نمی‌باشید.','mySuccess'=>''];

            }

            if($type=='interim')
                addLog('ویرایش موقت فاکتور شماره '.$request->id,$request->all());
            else
                addLog('ویرایش فاکتور شماره '.$request->id,$request->all());
            $total = 0;
            $discount_all = 0;
            foreach (Factor_detail::where('factor_id',$factor->id)->get() as $k=>$val){
                $numb = isset($all['numb_'.$val->id]) ? (int) fa2la($all['numb_'.$val->id]) ?? 0 : 0;
                if($numb<1 || !isset($all['name_'.$val->id])){
                    Factor_detail::destroy($val->id);
                    continue;
                }
                $price = (int) fa2la($all['price_'.$val->id]);
                $unit = $all['unit_'.$val->id] ?? '-';
                $disc = (int) fa2la($all['disc_'.$val->id]);
                $detail = Factor_detail::where('id',$val->id)->first();
                $detail->price =  $price;
                $detail->numb =  $numb;
                $detail->discount =  $disc;
                $detail->product_name =  $all['name_'.$val->id];
                $detail->unit = $unit;
                $detail->save();
                $total += ($numb * $price);
                $discount_all += $disc * $numb;
            }
            if(isset($all['namee'])&&is_array($all['namee'])&&count($all['namee'])>0){
                foreach ($all['namee'] as $kk=>$item){
                    $price = isset($all['pricee'])&&isset($all['pricee'][$kk]) ? (int) fa2la($all['pricee'][$kk]) : 0 ;
                    $disc = isset($all['discc'])&&isset($all['discc'][$kk]) ? (int) fa2la($all['discc'][$kk]) : 0 ;
                    $numb = isset($all['numbb'])&&isset($all['numbb'][$kk]) ? (int) fa2la($all['numbb'][$kk]) : 0 ;
                    $unit = isset($all['unitt'])&&isset($all['unitt'][$kk]) ? $all['unitt'][$kk] : 0 ;

                    if($numb<1)
                        continue;
                    $det = new Factor_detail();
                    $det->factor_id = $factor->id;
                    $det->post_id = 0;
                    $det->real_price = $price;
                    $det->price = $price;
                    $det->discount = $disc;
                    $det->discount_id = 0;
                    $det->numb = $numb;
                    $det->product_name = $item;
                    $det->unit = $unit;
                    $det->save();
                    $total += ($numb * $price);
                    $discount_all += $disc * $numb;
                }
            }


            $discount = (int) fa2la($all['discount']);
            $transs = (int) fa2la($all['transs']);
            $factor->total = $total;
            $factor->discount = $discount;
            $factor->transport_cost = $transs;
            $factor->expire = AB_lifetime_invoice();
            $factor->address_static = $request->address;
            $factor->desc = $request->desc;
            $factor->final = ($total - $discount + $transs -$discount_all) ;
            if($type!='interim'&&permission('factorConfirmLast')){
                $factor->status = 3;
//                $this->send_sms_confirm($factor);
            }
            $factor->save();
            if($type=='interim')
                return ['res'=>10,'myAlert'=>'','mySuccess'=>'تغییرات بصورت موقت ذخیره گردید'];
            return ['res'=>10,'myAlert'=>'','mySuccess'=>'تغییرات  ذخیره گردید'];

        }else {
            if($type=='interim')
                addLog('ویرایش موقت ناموفق فاکتور شماره '.$request->id,$request->all(),['آیدی فاکتور نامعتبر']);
            else
                addLog('ویرایش ناموفق فاکتور شماره '.$request->id,$request->all(),['آیدی فاکتور نامعتبر']);
            return ['res'=>1,'myAlert'=>'فاکتور مورد نظر یافت نشد!','mySuccess'=>''];
        }
    }

    private function send_sms_confirm($factor){
        $mobile = $factor->user ? $factor->user->mobile : $factor->user_mobile;
        if ($factor->user) {
            if (!$factor->user->secure || strlen($factor->user->secure) < 3) {
                $factor->user->secure = Helpers::randString(4, 6);
                $factor->user->save();
            }
            Sms::send_fast($mobile, 70748, ['Numb' => $factor->id, 'Verify' => 'o/' . $factor->id . '/' . $factor->user->secure]);
        } else
            Sms::send_fast($mobile, 27314, ['Numb' => $factor->id]);
    }


    public function editSave(Request $request){
        if(!permission('factorConfirmFirst') && !permission('factorConfirmLast')){
            return ['res'=>1,'myAlert'=>'در حال حاضر مجاز به تایید نیستید.','mySuccess'=>''];

        }
        return $this->save_change($request,'final');

    }

    public function addSave(Request $request){

        if(!$user = User::where('id',$request->user_id)->first())
            return ['res'=>1,'myAlert'=>'لطفا مشتری را انتخاب نمایید ','mySuccess'=>''];
        if(!in_array($request->comp,[1,2]))
            return ['res'=>1,'myAlert'=>'لطفا سربرگ فاکتور را انتخاب نمایید ','mySuccess'=>''];
        if(!isset($request->address) || strlen($request->address)<2)
            return ['res'=>1,'myAlert'=>'وارد کردن آدرس الزامیست','mySuccess'=>''];
        if(!isset($request->name) || !is_array($request->name) || count($request->name)<1)
            return ['res'=>1,'myAlert'=>'وارد کردن حداقل یک محصول الزامیست','mySuccess'=>''];
        $r = $request->all();
        foreach ($request->name as $k=>$val){
            $row = $k+1;
            if(strlen($val)<2)
                return ['res'=>1,'myAlert'=>'نام محصول ردیف '.$row.' نامعتبر است','mySuccess'=>''];
            if (!is_numeric(fa2la($r['price'][$k])))
                return ['res'=>1,'myAlert'=>'قیمت محصول ردیف '.$row.' نامعتبر است','mySuccess'=>''];
            if (!is_numeric(fa2la($r['numb'][$k])))
                return ['res'=>1,'myAlert'=>'تعداد محصول ردیف '.$row.' نامعتبر است','mySuccess'=>''];
            if (!is_numeric(fa2la($r['disc'][$k])))
                return ['res'=>1,'myAlert'=>'تخفیف محصول ردیف '.$row.' نامعتبر است','mySuccess'=>''];
        }

        if(!$user->secure || strlen($user->secure)<3){
            if(strlen($user->address) < 3)
                $user->address = $request->address;
            $user->secure = Helpers::randString(4,6);
            $user->save();
        }elseif(strlen($user->address) < 3 && strlen($request->address)>1){
            $user->address = $request->address;
            $user->save();
        }

        $factor = new Factor();
        $factor->user_id = $user->id;
        $factor->user_token = $user->secure;
        $factor->user_mobile = $user->mobile;
        $factor->importer = \auth('admin')->user()->id;
        $factor->status = AB_perm('app_admin_confirm_order') ? 3 : 1;
//        $factor->total = ;
//        $factor->discount = ;
//        $factor->final = ;
//        $factor->discount_id = ;
        $factor->secret_key = Helpers::randString(20,30);
//        $factor->desc = '';
        $factor->expire = AB_lifetime_invoice();
        $factor->company = $request->comp;
        $factor->address_static = $request->address;
//        $factor->transport_cost = ;
        $factor->save();

        foreach ($request->name as $k=>$val){




            $detail = new Factor_detail();
            $detail->factor_id = $factor->id;
            $detail->post_id = 0;
            $detail->real_price = 0;

                $detail->price = fa2la($r['price'][$k]);
            $detail->discount = fa2la($r['disc'][$k]);
            $detail->discount_id = 0;
            $detail->numb = fa2la($r['numb'][$k]);
            $detail->product_name = $val;
            $detail->props = '';
            $detail->details = '';
            $detail->stock_id = 0;
            $detail->price_type = '';
            $detail->unit = $r['unit'][$k] ?? 'عدد';
            $detail->save();
        }

        if(AB_perm('app_admin_confirm_order'))
            Sms::send_fast($user->mobile,70748,['Numb'=>$factor->id,'Verify'=>'o/'.$factor->id.'/'.$user->secure]);

        return ['res'=>10,'myAlert'=>'','mySuccess'=>'سفارش با موفقیت ثبت گردید'];
    }

    public function remove(Request $request){
        if(!permission('factorsRemove')){
            return ['res'=>1,'myAlert'=>'مجاز به حذف نیستید.','mySuccess'=>''];

        }
        if($factor = Factor::where('id',$request->id)->first()){
            if($factor->status > 3)
                return ['res'=>1,'myAlert'=>'فاکتور در مرحله حذف قرار ندارد','mySuccess'=>''];
            addLog('حذف فاکتور شماره '.$request->id);

            Factor::destroy($request->id);

            return ['res'=>10,'myAlert'=>'','mySuccess'=>'فاکتور با موفقیت حذف گردید.','tr'=>''];

        }else
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید','mySuccess'=>''];


    }




    public function tbl($factors,$showName=true,$name='',$mobile='',$company=''){
        $result = '';
//        $payMents = Pay_method::getAll();
        $remove = '';
        if(permission('factorsRemove')){
            $remove = "
                <a data-placement=\"top\" data-toggle=\"tooltip\" title=\"حذف\" class=\"btn btn-danger btn-circle waves-effect waves-circle waves-float\">
                            <i class=\"material-icons\"> clear</i>
                        </a>
                ";
        }
        $namee = $name;
        $tdName='';
        foreach ($factors as $k=>$val){
            if($showName) {
                $user = 'not found';
                $company = '';
                $namee = 'کاربر';
                $mobile = '11111';
                if ($u = $val->user) {
                    $user = $u->name . ' ' . $u->family;
                    $user .= strlen($u->company) > 3 ? '<br>(' . $u->company . ')' : '';
                    $namee = $u->name . ' ' . $u->family;
                    $mobile = $u->mobile;
                    $company = $u->company;
                }
                $userLink = route('p_users_show',[$val->user_id]);
                $tdName = "<td><a target=\"_blank\" href=\"{$userLink}\">{$user}</a></td>";
            }

            $status = Factor::statuses_short($val->status);
            $row = $k+1;
            $date1 = jdate('Y-m-d H:i',strtotime($val->updated_at));
            $date = jdate('Y-m-d H:i',strtotime($val->created_at));

            $result .= <<<EOS
                <tr class="cl_{$val->id} user_{$val->user_id}" data-id="{$val->id}" data-date="{$date}" data-amount="{$val->final}" data-user="{$val->user_id}" data-trans="{$val->trans_id}" data-name="{$namee}" data-company="{$company}" data-mobile="{$mobile}">
                     <td class="d-none">{$row}</td>
                     <td>{$val->id}</td>
                     {$tdName}

                     <td class="status_user"><span style="display: none">{$val->status}</span>{$status}</td>
                     <td class="ltr">{$date1}</td>
                     <td class="ltr">{$date}</td>
                     <td class="actions" data-id="{$val->id}">
                        <a data-id="{$val->id}" data-placement="top" data-toggle="tooltip"  title="جزئیات خرید" class="btn btn-success btn-circle waves-effect waves-circle waves-float showed" >
                            <i class="material-icons">remove_red_eye</i>
                        </a>

EOS;
            if($val->status < 3) {
                if($val->status < 3 && permission('factorsSmsNotStock'))
                    $result .= <<<EOS
                        <a data-placement="top" data-status="{$val->status}" data-name="{$namee}" data-mobile="{$mobile}" data-toggle="tooltip" title="عدم موجودی" class="btn btn-warning await btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">remove_shopping_cart</i>
                        </a>
EOS;
            }
                if(permission('factorConfirmLast') || ($val->status<2 && permission('factorConfirmFirst')))
                    $result .= <<<EOS
                        <a data-placement="top" data-status="{$val->status}" data-toggle="tooltip" title="ویرایش / تایید" class="btn btn-info btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">create</i>
                        </a>
EOS;

                $result .= $val->status<4 ? $remove : '';


            $result .= <<<EOS

                    </td>
                </tr>
EOS;

        }
        return $result;
    }
}
