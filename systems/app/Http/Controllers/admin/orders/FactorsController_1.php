<?php

namespace App\Http\Controllers\admin\orders;


use App\Helpers\Helpers;
use App\Models\Factor;
use App\Models\Factor_detail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class FactorsController extends Controller
{
    public function index(){
        if(forbidden('factorsShow'))
            return redirect(route('p_dashboard'));

        addLog('مشاهده لیست فاکتورها');

        session(['filter_order_start'=>1592560287]);
        session(['filter_order_end'=>0]);
        session(['filter_order_pay'=>0]);
        session(['filter_order_status'=>-1]);

        $data['tbl']=$this->tbl(Factor::where([['status','>=','0'],['id','>','1']])->orderBy('updated_at','DESC')->get());
        $data['filter'] = '<strong>لیست</strong> فاکتورها';
        return view('admin.orders.factors',$data);
    }

    public function getFactorDetails(Request $request){
        if(!permission('factorsShow'))
            return ['res'=>2];
        if(isset($request->id) && $factor = Factor::find($request->id)){
            $name = '-';
            $mobile = '-';
            $company = '--';
            $address = '-';
            if($user = User::find($factor->user_id)){
                $name = $user->name.' '.$user->family;
                $mobile = $user->mobile;
                $company = $user->company?$user->company:'--';
            }

            addLog('مشاهده جزئیات فاکتور شماره '.$request->id,[$name,$mobile,$company]);

            $tbl = '';

            $total = 0;
            foreach (Factor_detail::where('factor_id',$factor->id)->get() as $k=>$val){
                $validPrice = true;
                $r = $k+1;
                if($val->price){
                    $amount = $val->price * $val->numb;
                    $price = $val->price;
                    $total += $amount;
                }else{
                    $price = '--';
                    $amount='---';
                    $validPrice = false;
                }

                $tbl .= <<<EOS
            <tr>
                 <td style="padding: 5px">{$r}</td>
                 <td style="padding: 5px">{$val->product_name}</td>
                 <td class="number" style="padding: 5px">{$price}</td>
                 <td style="padding: 5px;text-align: center">{$val->numb}</td>
                 <td class="number" style="padding: 5px">{$amount}</td>
             </tr>
EOS;

            }
            if(!$validPrice) {
                $total = ' -----';
                $final = '-----';
                $discount = '-----';
                $transs = '-----';
            }else{
                $discount = $factor->discount;
                $transs = $factor->transport_cost;
                $final = $total - $factor->discount + $transs;
            }
            if($factor->transport=='person'){
                $trans_methos = 'از شرکت تحویل میگیرد';
            }elseif ($factor->transport=='send')
                $trans_methos = 'باید ارسال شود';
            else
                $trans_methos = 'نامعلوم';
            $address = strlen($factor->address) > 3 ? $factor->address : '---';

            return [
                'res'=>10,
                'tbl'=>$tbl,
                'date'=> jdate('Y/m/d',$factor->time),
                'numbId'=> $factor->id,
                'name'=>$name ,
                'mobile'=>$mobile ,
                'company'=> $company,
                'address'=>$address ,
                'trans_method'=> $trans_methos,
                'total'=> $total,
                'final'=> $final,
                'discount'=> $discount,
                'transs'=> $factor->transport_cost,

                'desc'=> $factor->desc && strlen($factor->desc) > 2 ? $factor->desc : '',
                'image'=> is_file(ROOT.$factor->image) ? url($factor->image) : 'noImage'
            ];
        }
        addLog('مشاهده ناموفق جزئیات فاکتور شماره '.$request->id);
        return ['res'=>1];
    }

    public function getFactorDetailsEdit(Request $request){
        if(!permission('factorConfirmFirst') && !permission('factorConfirmLast') && !permission('factorTemporary'))
            return ['res'=>3];
        if(isset($request->id) && $factor = Factor::find($request->id)){
            $name = '-';
            $mobile = '-';
            $company = '--';
            $address = '-';
            if($user = User::find($factor->user_id)){
                $name = $user->name.' '.$user->family;
                $mobile = $user->mobile;
                $company = $user->company?$user->company:'--';
            }
            addLog('مشاهده جزئیات فاکتور برای ویرایش شماره '.$request->id,[$name,$mobile,$company]);
            $tbl = '';
            $validPrice = true;
            $total = 0;
            foreach (Factor_detail::where('factor_id',$factor->id)->get() as $k=>$val){
                $r = $k+1;
                if($val->price){
                    $amount = $val->price * $val->numb;
                    $price = $val->price;
                    $total += $amount;
                }else{
                    $price = '0';
                    $amount='0';
                    $validPrice = false;
                }

                $nnn = preg_replace('/<br.{0,2}>/is',"",$val->product_name);
                $tbl .= <<<EOS
            <tr class="cl_{$val->id}">
                 <td style="padding: 5px">{$r}</td>
                 <td class="prodNameEdit">
                 <textarea class="form-control inp" name="name_{$val->id}">{$nnn}</textarea>
                 <i data-id="{$val->id}" class="fas fa-search"></i>
                 </td>
                 <td class="priceField" style="padding: 5px"><input type="text" class="form-control inp justNumber" name="price_{$val->id}" value="{$price}"></td>
                 <td class="numbField" style="padding: 5px;text-align: center"><input class="form-control inp" type="text" name="numb_{$val->id}" value="{$val->numb}"></td>
                 <td class="tot number" style="padding: 5px">{$amount}</td>
             </tr>
EOS;

            }

            $transs = $factor->transport_cost;
            if(!$validPrice) {
                $total = ' -----';
                $final = '-----';
                $discount = '0';
                $trans = '0';
            }else{
                $discount = $factor->discount;

                $final = $total - $factor->discount + $transs;
            }
            if($factor->transport=='person'){
                $trans_methos = 'از شرکت تحویل میگیرد';
            }elseif ($factor->transport=='send')
                $trans_methos = 'باید ارسال شود';
            else
                $trans_methos = 'نامعلوم';

            $address = strlen($factor->address) > 3 ? $factor->address : '---';


            return [
                'res'=>10,
                'tbl'=>$tbl,
                'date'=> jdate('Y/m/d',$factor->time),
                'numbId'=> $factor->id,
                'name'=>$name ,
                'mobile'=>$mobile ,
                'company'=> $company,

                'total'=> $total,
                'final'=> $final,
                'discount'=> $discount,
                'transs'=> $transs,
                'address'=>$address ,
                'trans_method'=> $trans_methos,

                'factId'=> $factor->id,
                'factStatus'=> $factor->status,
                'desc'=> $factor->desc && strlen($factor->desc) > 2 ? $factor->desc : '',
                'image'=> is_file(ROOT.$factor->image) ? url($factor->image) : 'noImage'
            ];
        }
        addLog('مشاهده ناموفق جزئیات فاکتور برای ویرایش شماره '.$request->id);
        return ['res'=>1];
    }



    public function editSave(Request $request){
        if(!permission('factorConfirmFirst') && !permission('factorConfirmLast')){
            return ['res'=>1,'myAlert'=>'در حال حاضر مجاز به تایید نیستید.','mySuccess'=>''];

        }


        $all = $request->all();
        if(isset($request->factor_id) && $factor = Factor::find($request->factor_id)){
            if($factor->status != 0 && $factor->status != 8){
                \session(['alert-danger'=>'فاکتور در وضعیت ویرایش یا تایید قرار ندارد.']);
                return back();
            }
            if($factor->status == 8 && !permission('factorConfirmLast')){
                \session(['alert-danger'=>'در حال حاضر مجاز به تایید نهایی سفارش نمی‌باشید.']);
                return back();
            }
            addLog('ویرایش فاکتور شماره '.$request->id,$request->all());
            $total = 0;
            foreach (Factor_detail::where('factor_id',$factor->id)->get() as $k=>$val){
                $price = (int) $all['price_'.$val->id];
                $numb = (int) $all['numb_'.$val->id];
                $detail = Factor_detail::where('id',$val->id)->first();
                $detail->price =  $price;
                $detail->numb =  $numb;
                $detail->product_name =  $all['name_'.$val->id];
                $detail->save();
                $total += ($numb * $price);
            }

            if(isset($all['new_name']) && count($all['new_name'])){
                foreach ($all['new_name'] as $kk=>$vv){
                    $new  = new Factor_detail();
                    $new->factor_id = $factor->id;
                    $new->price = isset($all['new_price'][$kk]) && is_numeric($all['new_price'][$kk]) ? $all['new_price'][$kk] : 1;;
                    $new->numb = isset($all['new_numb'][$kk]) && is_numeric($all['new_numb'][$kk]) ? $all['new_numb'][$kk] : 1;
                    $new->product_name = $vv;
                    $new->save();
                }
            }

            $discount = (int) $all['discount'];
            $transs = (int) $all['transs'];
            $factor->total = $total;
            $sendSms = false;
            if($factor->status < 1) {
                $factor->status = 8;
            }elseif ($factor->status == 8){
                $sendSms = true;
                $factor->status = 1;
            }
            $factor->discount = $discount;
            $factor->transport_cost = $transs;
            $factor->timeout = Factor::timeOut();
            $factor->final = ($total - $discount + $transs) ;
            $factor->save();

            if($sendSms && $user = User::where('id',$factor->user_id)->first()) {
                Smsirlaravel::ultraFastSend(['Numb'=>$factor->id],'27314',$user->mobile);
            }
            $factors=[];
            $factors[]=$factor;
            return ['res'=>10,'myAlert'=>'','mySuccess'=>'تغییرات با موفقیت ذخیره گردید.','tr'=>$this->tbl($factors)];



        }else {
            addLog('ویرایش ناموفق فاکتور شماره '.$request->id,$request->all(),['آیدی فاکتور نامعتبر']);
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید.','mySuccess'=>''];


        }

    }




//-----------    not used




    public function filter(Request $request){
        if(forbidden('factorsShow'))
            return redirect(route('p_dashboard'));
        $start = explode('/',fa2la($request->start));
        if(is_array($start) && count($start) == 3){
            $st = jalali_to_gregorian($start[0],$start[1],$start[2],'/');
            $start = strtotime($st);
        }else{
            $start = 1592560287;
        }


        $end = explode('/',fa2la($request->end));
        if(is_array($end) && count($end) == 3){
            $st = jalali_to_gregorian($end[0],$end[1],$end[2],'/');
            $end = strtotime($st);
        }else{
            $end = time();
        }




        if ($request->pay == 1)
            $pay = ['trans_id','>','0'];
        elseif ($request->pay == 2)
            $pay = ['trans_id','0'];
        else
            $pay = ['trans_id','>=','0'];

        session(['filter_order_start'=>$start]);
        session(['filter_order_end'=>$end]);
        session(['filter_order_pay'=>$request->pay]);
        session(['filter_order_status'=>$request->status]);

        addLog('مشاهده لیست فاکتورها با فیلتر',['شروع : '.jdate('d-m-Y',$start),'خاتمه : '.jdate('d-m-Y',$end),'وضعیت : '.$request->status]);
        if($request->status < 0){
            $data['filter'] = "لیست فاکتورها از تاریخ ".jdate('d-m-Y',$start).' لغایت '.jdate('d-m-Y',$end);
            $data['tbl']=$this->tbl(Factor::where([['status','>=','0'],['time','>=',$start],['time','<=',$end],$pay])->orderBy('updated_at','DESC')->get());
            return view('admin.orders.factors',$data);
        }
        $data['filter'] = "لیست فاکتورها از تاریخ ".jdate('d-m-Y',$start).' لغایت '.jdate('d-m-Y',$end)." با وضعیت ".Factor::statuses_short($request->status);
        $data['tbl']=$this->tbl(Factor::where([['status',$request->status],['time','>=',$start],['time','<=',$end],$pay])->orderBy('updated_at','DESC')->get());
        return view('admin.orders.factors',$data);

    }

    public function filter2(Request $request){
        if(forbidden('factorsShow'))
            return redirect(route('p_dashboard'));


        addLog('مشاهده لیست فاکتورها با فیلتر',[]);
        $data['filter'] = "لیست فاکتورها با محصول :   ".$request->product;
        $ides = [];
//        dd($request->product,Factor_detail::where('product_name',$request->product)->get());
        foreach (Factor_detail::where('product_name','like','%'.$request->product.'%')->get() as $value){
            $ides[]=$value->factor_id;
        }
        if($request->status < 0){
            $data['tbl']=$this->tbl(Factor::whereIn('id',$ides)->orderBy('updated_at','DESC')->get());
            return view('admin.orders.factors',$data);
        }
        $data['tbl']=$this->tbl(Factor::whereIn('id',$ides)->where('status',$request->status)->orderBy('updated_at','DESC')->get());



        return view('admin.orders.factors',$data);
    }

    public function checkOrder(){
        if(forbidden('factorsShow'))
            ['res'=>1];

        $start = session('filter_order_start',1592560287);
        $end = session('filter_order_end',time());
        $status = session('filter_order_status',-1);
        $end = !is_numeric($end) || $end < $start ? time() : $end ;
        $payMethos = session('filter_order_pay',0);
        if ($payMethos == 1)
            $pay = ['trans_id','>','0'];
        elseif ($payMethos == 2)
            $pay = ['trans_id','0'];
        else
            $pay = ['trans_id','>=','0'];
        $time = session('lastTimeGetOrderCount',0);
        if(Factor::where([['status','0'],['time','>',$time]])->count() > 0){
            session(['lastTimeGetOrderCount'=>time()]);
            if($status < 0){
                $filter = "لیست فاکتورها از تاریخ ".jdate('d-m-Y',$start).' لغایت '.jdate('d-m-Y',$end);
                $tbl = $this->tbl(Factor::where([['status','>=','0'],['time','>=',$start],['time','<=',$end],$pay])->orderBy('updated_at','DESC')->get());
            }else{
                $filter = "لیست فاکتورها از تاریخ ".jdate('d-m-Y',$start).' لغایت '.jdate('d-m-Y',$end)." با وضعیت ".Factor::statuses_short($request->status);
                $tbl = $this->tbl(Factor::where([['status',$request->status],['time','>=',$start],['time','<=',$end],$pay])->orderBy('updated_at','DESC')->get());
            }
            return ['res'=>10,'filter'=>$filter,'tbl'=>$tbl];
        }
        return ['res'=>1];

    }

    public function getRemind(Request $request){
        return Invoice::remained($request->id);

    }

    public function getMessages(Request $request){
        if(!$factor = Factor::where('id',$request->id)->first()){
            return json_encode(['message'=>1]);
        }
        $factor->new_msg_user = 0;
        $factor->save();

        return json_encode(['message'=>$this->getChat($request->id)]);
    }

    private function getChat($factid){
        $outputMsg = "";
        $value = Factor_option::where([["factor_id", $factid],['key','messages']])->get();
        if(count($value)>0){
            foreach($value as $key =>$message){
                $message = json_decode($message->value,true);
                foreach($message as $item) {
                    $time = jdate('Y/m/d - h:m' ,$item['time']);
                    if ($item['role'] != 'user') {
                        $outputMsg .= "<div class=\"outgoing_msg\">
                                        <div class=\"sent_msg\">
                                            <p>{$item['msg']}</p>
                                            <span class=\"time_date\">{$time}</span> </div>
                                    </div>";
                    } else {
                        $outputMsg .= "<div class=\"incoming_msg\">
                                        <div class=\"received_msg\">
                                            <div class=\"received_withd_msg\">
                                                <p>{$item['msg']}</p>
                                                 <span class=\"time_date\">{$time}</span> </div>
                                        </div>
                                    </div>";
                    }
                }
            }
        }else
            $outputMsg = "<div class='notMessageFound'><span>هنوز مکالمه ای با این کاربر انجام نشده.</span></div>";

        return $outputMsg;
    }

    public function sendMessage(Request $request){
        if($factor = Factor::where('id',$request->id)->first()){

            $factor->new_msg_admin = 1;
            $factor->save();

            if($message = Factor_option::where([['factor_id', $request->id],['key','messages']])->first()){
                $oldMesg = json_decode($message->value, true);
            }else {
                $message = new Factor_option();
                $oldMesg =[];
            }

            $oldMesg[]=['user_id'=>auth()->guard('admin')->user()->id,'role'=>'admin','msg'=>$request->msg,'time'=>time()];
            $newMesg = json_encode($oldMesg);
            $message->value = $newMesg;
            $message->factor_id = $request->id;
            $message->key = 'messages';
            $message->save();
            $messages = 'no';
            if($factor->new_msg_user){
                $messages = $this->getChat($factor->id);
            }

            return json_encode(['res'=>'success','msg'=>'پیام مورد نظر با موفقیت ثبت شد','messages'=>$messages]);

        }else
            return json_encode(['res'=>'error','msg'=>'فاکتور مورد نظر یافت نشد']);

    }

    public function getNewMessages()
    {
        $result = [];
        if($nn = Factor::where('new_msg_user',1)->select('id')->get()){
            foreach ($nn as $value)
                $result[]=$value->id;

        }
//        return [1,2];
        return $result;
    }



    public function editSaveInterim(Request $request){

        if(!permission('factorTemporary')){
            return ['res'=>10,'myAlert'=>'در حال حاضر مجاز به ذخیره موقت نیستید.','mySuccess'=>''];
        }

        $all = $request->all();
        if(isset($request->factor_id) && $factor = Factor::find($request->factor_id)){
            addLog('ویرایش موقت فاکتور شماره '.$request->id,$request->all());
            $total = 0;
            foreach (Factor_detail::where('factor_id',$factor->id)->get() as $k=>$val){
                $price = (int) $all['price_'.$val->id];
                $numb = (int) $all['numb_'.$val->id];
                $detail = Factor_detail::where('id',$val->id)->first();
                $detail->price =  $price;
                $detail->numb =  $numb;
                $detail->product_name =  $all['name_'.$val->id];
                $detail->save();
                $total += ($numb * $price);
            }
            if(isset($all['new_name']) && count($all['new_name'])){
                foreach ($all['new_name'] as $kk=>$vv){
                    $new  = new Factor_detail();
                    $new->factor_id = $factor->id;
                    $new->price = isset($all['new_price'][$kk]) && is_numeric($all['new_price'][$kk]) ? $all['new_price'][$kk] : 1;;
                    $new->numb = isset($all['new_numb'][$kk]) && is_numeric($all['new_numb'][$kk]) ? $all['new_numb'][$kk] : 1;
                    $new->product_name = $vv;
                    $new->save();
                }
            }

            $discount = (int) $all['discount'];
            $transs = (int) $all['transs'];
            $factor->total = $total;

            $factor->discount = $discount;
            $factor->transport_cost = $transs;
            $factor->timeout = Factor::timeOut();
            $factor->final = ($total - $discount + $transs) ;
            $factor->save();

            return ['res'=>10,'myAlert'=>'','mySuccess'=>'تغییرات بصورت موقت ذخیره گردید','total'=>$factor->final];


        }else {
            addLog('ویرایش موقت ناموفق فاکتور شماره '.$request->id,$request->all(),['آیدی فاکتور نامعتبر']);

            return ['res'=>1,'myAlert'=>'فاکتور مورد نظر یافت نشد!','mySuccess'=>''];
        }


    }

    public function remove(Request $request){
        if(!permission('factorsRemove')){
            return ['res'=>1,'myAlert'=>'مجاز به حذف نیستید.','mySuccess'=>''];
//            \session(['alert-danger'=>'مجاز به حذف نیستید.']);
//            return  back();
        }
        if($factor = Factor::find($request->id)){
            $det = [];
            if($details = Factor_detail::where('factor_id',$factor->id)->get())
                foreach ($details as $detail)
                    $det[]=[$detail->product_name,'تعداد : '.$detail->numb,'قیمت : '.$detail->price];
            addLog('حذف فاکتور شماره '.$request->id,['کاربر شماره : '.$factor->user_id,$det]);
            Factor_detail::where('factor_id',$factor->id)->delete();
            Factor::destroy($request->id);
            if($inv = Invoice::where([['type','factor'],['type_id',$request->id]])->first()){
                Invoice::destroy($inv->id);
            }
//            session(['alert-success'=>'فاکتور با موفقیت حذف گردید.']);
            return ['res'=>10,'myAlert'=>'','mySuccess'=>'فاکتور با موفقیت حذف گردید.','tr'=>''];

        }else
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید','mySuccess'=>''];

        return redirect()->back();
    }

    public function packaging(Request $request,$id=0){
        if(!permission('factorPackage')){
            return ['res'=>1,'myAlert'=>'مجاز به این کار نیستید.','mySuccess'=>''];
        }
        $id = $id==0 ? $request->id : $id;
        if($factor = Factor::where('id',$id)->first()){
            $all = $request->all();

            addLog('تغییر وضعیت فاکتور به در حال بسته بندی شماره '.$id);
            $factor->status = 4;
            $factor->save();
            if(isset($all['sms']) && $all['sms'] == 'on'){
                if($uu = User::where('id',$factor->user_id)->first()) {
                    Smsirlaravel::ultraFastSend(['Numb' => $factor->id], '27315',$uu->mobile);
                }
            }
            $factors = [];
            $factors[]=$factor;
            return ['res'=>10,'myAlert'=>'','mySuccess'=>'فاکتور با موفقیت تغییر وضعیت داد.','tr'=>$this->tbl($factors)];


        }else {
            addLog('تغییر وضعیت فاکتور ناموفق به در حال بسته بندی شماره '.$id,[],['آیدی فاکتور نامعتبر']);

            return ['res'=>10,'myAlert'=>'عملیات با خطا مواجه گردید.','mySuccess'=>''];

        }
        return back();
    }

    public function saveBijak(Request $request){
        if(!permission('factorTransport')){
            return ['res'=>1,'myAlert'=>'مجاز به این کار نیستید.','mySuccess'=>''];
        }
        $innn = $request->all();

        if($factor = Factor::where('id',$request->id)->first()){
            $bijak = new Bijak();
            $bijak->factor_id = $factor->id;
            $bijak->user_id = $factor->user_id;
            $bijak->name = $request->name;
            $bijak->type = $request->type;
            $bijak->tel = $request->tel;
            $bijak->mobile = $request->mobile;
            $bijak->description = $request->desc;
            $bijak->time = $request->time;
            $image = Helpers::checkAndSaveImage($request,'image','files/bijaks',[[100,100]],2000);
            if(is_file('files/bijaks/'.$image))
                $bijak->image = 'files/bijaks/'.$image;
            $bijak->save();
            $factor->status = 5;
            $factor->bijak_id = $bijak->id;
            $factor->save();

            if(isset($innn['sendSms']) && $innn['sendSms']=='on' && $user = User::where('id',$factor->user_id)->first()) {
                Smsirlaravel::send('سفارش شما تحویل '.$request->type.' گردید.' . "\n" . 'شماره سفارش  : ' . $factor->id, $user->mobile);
            }

            addLog('ذخیره بیجک برای فاکتور شماره '.$request->id,$request->all());
            $factors=[];
            $factors[]=$factor;
            return ['res'=>10,'myAlert'=>'','mySuccess'=>'بیجک با موفقیت ذخیره گردید.','tr'=>$this->tbl($factors)];

        }else {
            addLog('ذخیره ناموفق بیجک برای فاکتور شماره '.$request->id,$request->all(),['آیدی فاکتور نامعتبر']);
            return ['res'=>1,'myAlert'=>'ثبت بیجک با خطا مواجه گردید.','mySuccess'=>''];

        }
        return back();
    }

    public function await(Request $request){
        if(!permission('factorsSmsNotStock'))
            return json_encode(['res'=>1,'myAlert'=>'در حال حاضر مجاز به این کار نیستید.','mySuccess'=>'']);
        if(strlen($request->product) < 2)
            return json_encode(['res'=>1,'myAlert'=>'وارد کردن نام محصول الزامیست.','mySuccess'=>'']);
        if($request->mobile == '11111')
            return json_encode(['res'=>1,'myAlert'=>'متاسفانه موبایل کاربر در دسترس نیست.','mySuccess'=>'']);
        $name = strlen($request->name) > 2 ? $request->name : 'کاربر';

        $parameters = ['Name'=>$name,'Numb'=>$request->numb,'Product'=>$request->product];
        $res = Smsirlaravel::ultraFastSend($parameters,'30385',$request->mobile);
        $parameters['mobile'] = $request->mobile;

        addLog('ارسال پیامک عدم موجودی',$parameters);

        return ['res'=>10,'myAlert'=>'','mySuccess'=>'پیامک برای کاربر ارسال شد.'];

        //dd($request->all());
    }

    public function change_checkout(Request $request){
        if(!permission('factorsCheckout'))
            return ['res'=>1,'myAlert'=>'در حال حاضر مجوز اینکار را ندارید','mySuccess'=>''];
        if(!$factor=Factor::where('id',$request->factorId)->first())
            return ['res'=>1,'myAlert'=>'فاکتور مورد نظر یافت نشد!','mySuccess'=>''];
        $factor->checkout = $request->newCheckout=='credit'?'credit':'cash';
        $factor->save();

        $pay = $request->newCheckout=='credit'?'اعتباری':'نقدی';
        addLog('تغییر وضعیت پرداخت به '.$pay.' فاکتور شماره '.$request->factorId);
        return ['res'=>10,'myAlert'=>'','mySuccess'=>'تغییر وضعیت با موفقیت انجام شد'];


    }

    public function delivery(Request $request){
        if(!permission('factorDelivery'))
            return ['res'=>1,'myAlert'=>'در حال حاضر مجوز اینکار را ندارید','mySuccess'=>''];
        if(!$factor=Factor::where('id',$request->factorId)->first())
            return ['res'=>1,'myAlert'=>'فاکتور مورد نظر یافت نشد!','mySuccess'=>''];
        $factor->status = 6;
        $factor->save();
        addLog('تغییر وضعیت به '.' تحویل مشتری '.' فاکتور شماره '.$request->factorId);
        return ['res'=>10,'myAlert'=>'','mySuccess'=>'تغییر وضعیت با موفقیت انجام شد'];

    }



//    public function edit($id){
//        $data['factor'] = Factor::with(['rel_user','rel_details'])->where('id',$id)->first();
////        dd($data);
//        return view('admin.orders.editFactor',$data);
//    }

//    public function getMessages(Request $request){
//        if(!$factor = Factor::where('id',$request->id)->first()){
//            return json_encode(['message'=>1]);
//        }
//        $factor->new_msg_user = 0;
//        $factor->save();
//
//        return json_encode(['message'=>$this->getChat($request->id)]);
//    }
//
//    private function getChat($factid){
//        $outputMsg = "";
//        $value = Factor_option::where([["factor_id", $factid],['key','messages']])->get();
//        if(count($value)>0){
//            foreach($value as $key =>$message){
//                $message = json_decode($message->value,true);
//                foreach($message as $item) {
//                    $time = jdate('Y/m/d - h:m' ,$item['time']);
//                    if ($item['role'] != 'user') {
//                        $outputMsg .= "<div class=\"outgoing_msg\">
//                                        <div class=\"sent_msg\">
//                                            <p>{$item['msg']}</p>
//                                            <span class=\"time_date\">{$time}</span> </div>
//                                    </div>";
//                    } else {
//                        $outputMsg .= "<div class=\"incoming_msg\">
//                                        <div class=\"received_msg\">
//                                            <div class=\"received_withd_msg\">
//                                                <p>{$item['msg']}</p>
//                                                 <span class=\"time_date\">{$time}</span> </div>
//                                        </div>
//                                    </div>";
//                    }
//                }
//            }
//        }else
//            $outputMsg = "<div class='notMessageFound'><span>هنوز مکالمه ای با این کاربر انجام نشده.</span></div>";
//
//        return $outputMsg;
//    }
//
//    public function sendMessage(Request $request){
//        if($factor = Factor::where('id',$request->id)->first()){
//
//            $factor->new_msg_admin = 1;
//            $factor->save();
//
//            if($message = Factor_option::where([['factor_id', $request->id],['key','messages']])->first()){
//                $oldMesg = json_decode($message->value, true);
//            }else {
//                $message = new Factor_option();
//                $oldMesg =[];
//            }
//
//            $oldMesg[]=['user_id'=>auth()->guard('admin')->user()->id,'role'=>'admin','msg'=>$request->msg,'time'=>time()];
//            $newMesg = json_encode($oldMesg);
//            $message->value = $newMesg;
//            $message->factor_id = $request->id;
//            $message->key = 'messages';
//            $message->save();
//            $messages = 'no';
//            if($factor->new_msg_user){
//                $messages = $this->getChat($factor->id);
//            }
//
//            return json_encode(['res'=>'success','msg'=>'پیام مورد نظر با موفقیت ثبت شد','messages'=>$messages]);
//
//        }else
//            return json_encode(['res'=>'error','msg'=>'فاکتور مورد نظر یافت نشد']);
//
//    }
//
//    public function getNewMessages()
//    {
//        $result = [];
//        if($nn = Factor::where('new_msg_user',1)->select('id')->get()){
//            foreach ($nn as $value)
//                $result[]=$value->id;
//
//        }
////        return [1,2];
//        return $result;
//    }


    public function add(){
        addLog('ورود به صفحه افزودن فاکتور');
        return view('admin.orders.addFactor');
    }

    public function addSave(Request $request)
    {
//        dd($request->all());

        $discount = fa2la($request->discount);

        $transs = 0;
        $factor = new Factor();
        $factor->user_id = $request->user_id;
        $factor->importer = Auth::guard('admin')->user()->id;
        $factor->status = 8;
        $factor->discount = $discount  ;
        $factor->transport_cost = $transs  ;
        $factor->discount_id = 0 ;
        $factor->time = time();
        $factor->timeout = Factor::timeOut();
        $factor->invoice = 'yes';
        $factor->save() ;

        $all = $request->all();
        $total = 0;
        foreach ($request->productId as $k=>$value){
            $numb = isset($all['numb'][$k]) && $all['numb'][$k] ? fa2la($request->discount) : 0;

            if(!$numb || !$product = Product::find($value))
                continue;
            $code = isset($all['codes'][$k]) && $all['codes'][$k] ? $all['codes'][$k] : 0;
            $price = isset($all['price'][$k]) && $all['price'][$k] ? fa2la($request->discount)  : 0;
            $t = $price * $numb;
            $total += $t;
            $productName = isset($all['productName'][$k]) && $all['productName'][$k] ? nl2br($all['productName'][$k]) : 'not found';

            $detail = new Factor_detail();
            $detail->post_id = $product->post_id;
            $detail->product_id = $value;
            $detail->code_id = $code ;
            $detail->factor_id = $factor->id;
            $detail->price = $price;
            $detail->numb = $numb;
            $detail->product_name = $productName;
            $detail->price_type = 'ریال';
            $detail->irc = 0;
            if($code && $c = Code::find($code))
                $detail->irc = $c->irc;
            $detail->save();

        }

        $factor->total = $total;
        $factor->final = ($total - $discount + $transs);
        $factor->save();

        addLog('افزودن فاکتور',$all);
        session(['alert-success'=>'فاکتور با موفقیت ایجاد گردید.']);
        return redirect()->back();
    }

    public function clearPrice($price){
        return ;
    }

    public function getCustomers(){
        $result = [];
        foreach (User::where('role',2)->get() as $value){
            $company = $value->company;
            $result [] = [
                'id'=>$value->id,
                'value'=>$value->name.' '.$value->family.' - '.$value->company,
                'name'=>$value->name.' '.$value->family,
                'company'=>$company,
                'mobile'=>$value->mobile,
                'address'=>'not found'
            ];
        }
        return $result;
    }

    public function getProducts(){
        if(is_file('assets/products.json')){
            return json_decode(file_get_contents('assets/products.json'),true);
        }
        $result = [];
//        $products = DB::select("select products.id,title from posts,products where posts.id = products.post_id and posts.type = 'product'");
        $rr = 0;
        foreach (Post::where('type','product')->select('id')->get() as $value){
            $post = Post::getOneProduct($value->id);
            if(!(strpos($post->title,'دستکش') === false))
                continue;
            $agency = $post->agencies ? ' - '.$post->agencies->name : '';

            $brand = $post->brand ? ' - '.$post->brand : '';
            $brand = '';
            if($post->codes){

                foreach ($post->codes as $code) {
                    $rr++;
                    $result [] = [
                        'id' => $post->productId,
                        'value' => $post->title . ' - ' . $code->object . $agency . $brand,
                        'name' => $post->title,
                        'subName' => $code->object,
                        'code' => $code->id,
                        'irc' => $code->irc,
                        'price' => $code->price1,
                        'rr' => 'l_'.$rr
                    ];

                }

            }else{
                $rr++;
                $result [] = [
                    'id'=>$post->productId,
                    'value'=>$post->title.$agency.$brand,
                    'name'=>$post->title,
                    'subName'=>' - ',
                    'code'=>0,
                    'irc'=>0,
                    'price'=>0,
                    'rr'=>$rr
                ];
            }

        }
        file_put_contents('assets/products.json',json_encode($result));
        return $result;

    }

    public function getBijak(Request $request){
        if($bijak = Bijak::where('id',$request->id)->first()){
            $result = "<div class='row' >";
            $result .= "<div class='col-8'>";
            $result .=  "<p><b>نام ".$bijak->type." :</b> ".$bijak->name."</p>";
            $result .=  "<p><b>تلفن ".$bijak->type." :</b> ".$bijak->tel."</p>";
            $result .=  "<p><b>موبایل ".$bijak->type." :</b> ".$bijak->mobile."</p>";
            $result .=  "<p>".$bijak->description."</p>";
            $result .= "</div><div class='col-4'>";
            if(is_file($bijak->image)) {
                $image = url($bijak->image);
                $result .= "<a target='_blank' href='{$image}'><img src='{$image}' ></a>";
            }
            $result .= "</div>";
        }else{
            $result = "<h4>هیچ بیجکی برای این فاکتور یافت نشد.</h4>";
        }
        return ['bijak'=>$result];
    }


    public function getTrans(Request $request){

        if($trans = Transaction::with('rel_method')->where('id',$request->id)->first()){
//            dd($trans);
            return [
                'code'=>$trans->id ,
                'price'=>$trans->price ,
                'tracking'=>$trans->tracking_code ,
                'date1'=>$trans->time ,
                'date2'=>jdate('Y-m-d H:i',strtotime($trans->created_at)) ,
                'method'=>is_object($trans->rel_method) ? $trans->rel_method->name : '--',
                'stat'=> Transaction::getStatusMsg($trans->status) ,
            ];
        }
        return [
            'code'=>'--',
            'price'=>'--',
            'tracking'=>'--',
            'date1'=>'--',
            'date2'=>'--',
            'method'=>'--',
            'stat'=>'--',
        ];
    }

    public function getUserTrans(Request $request){
        $trans = Transaction::where([['status','1'],['user_id',$request->id]])->orderBy('id','DESC')->get();
        if(count($trans) > 0){
            $tbody = '';
            foreach ($trans as $tran){
                $time = jdate('Y-m-d',strtotime($tran->created_at));
                $tbody .= "<tr data-id='{$tran->id}'><td>{$tran->id}</td><td class='number'>{$tran->price}</td><td>{$time}</td><td>{$tran->factor_id}</td></tr>";

            }
        }else
            $tbody = "<tr><td colspan='4'>این کاربر هیچ تراکنش تایید شده ای ندارد</td></tr>";

        return ['tbody'=>$tbody];

    }

    public function addTransToFactor(Request $request){
        if($request->trans < 1){
            return ['res'=>1,'myAlert'=>'لطفا یکی از تراکنشها را انتخاب کنید','mySuccess'=>''];
        }
        if(!$trans = Transaction::where('id',$request->trans)->first())
            return ['res'=>1,'myAlert'=>'تراکنش یافت نشد !','mySuccess'=>''];
        if($trans->status != 1)
            return ['res'=>1,'myAlert'=>'تراکنش تایید شده نیست. لطفا ابتدا تراکنش را تایید نمایید.','mySuccess'=>''];
        if(!$factor = Factor::where('id',$request->factor)->first())
            return ['res'=>1,'myAlert'=>'فاکتور یافت نشد !','mySuccess'=>''];

        addLog('نسبت دادن تراکنش به فاکتور',$request->all());
        $factor->trans_id = $request->trans;
        $factor->save();

        $trans->factor_id = $request->factor;
        $trans->save();

        return ['res'=>10,'myAlert'=>'','mySuccess'=>'تراکنش با موفقیت به فاکتور نسبت داده شد.','transId'=>$request->trans];

    }

    public function addTrans_andToFactor(Request $request){
//        dd($request->all());
        if(!$factor = Factor::where('id',$request->factor)->first())
            return ['res'=>1,'myAlert'=>'فاکتور یافت نشد !','mySuccess'=>''];
        if($factor->user_id != $request->user)
            return ['res'=>1,'myAlert'=>'فاکتور مربوط به این کاربر نیست','mySuccess'=>''];
        $price = fa2la($request->price);
        if(!is_numeric($price) || $price < 1)
            return ['res'=>1,'myAlert'=>'لطفا مبلغ را بصورت صحیح وارد نمایید.','mySuccess'=>''];
        if(strlen($request->code) < 2)
            return ['res'=>1,'myAlert'=>'لطفا کد پیگیری را بصورت صحیح وارد نمایید.','mySuccess'=>''];

        if(! Pay_method::where('id',$request->method_way)->first())
            return ['res'=>1,'myAlert'=>'لطفا روش پرداخت را انتخاب نمایید.','mySuccess'=>''];

        addLog('افزودن تراکنش و نسبت دادن به فاکتور',$request->all());

        $trans = new Transaction();
        $trans->user_id = $factor->user_id ;
        $trans->factor_id = $factor->id;
        $trans->status = 1;
        $trans->price = $price;
        $trans->payment_way = $request->method_way;
        $trans->tracking_code = $request->code;
        $trans->description = $request->details;
        $trans->number_cart = '';
        $trans->importer = Auth::guard('admin')->user()->id;

        $trans->time = $request->time_pay;
        $trans->save();

        $factor->trans_id = $trans->id;
        $factor->save();

        $invoice = new Invoice();

        $date = new \DateTime();
        $timestamp = time();
        $invoice->time =   $date->setTimestamp($timestamp);
        $invoice->total = $trans->price;
        $invoice->type = 'transaction';
        $invoice->user_id = $trans->user_id;
        $invoice->type_id = $trans->id;
        $invoice->save();

        return ['res'=>10,'myAlert'=>'','mySuccess'=>'تراکنش با موفقیت ثبت شد و به فاکتور نسبت داده شد.','transId'=>$trans->id];
    }





    public function tbl($factors,$showName=true,$name='',$mobile='',$company=''){
        $result = '';
//        $payMents = Pay_method::getAll();
        $remove = '';
        if(permission('factorsRemove')){
            $remove = "
                <a data-placement=\"top\" data-toggle=\"tooltip\" title=\"حذف\" class=\"btn btn-danger btn-circle waves-effect waves-circle waves-float\">
                            <i class=\"material-icons\"> clear</i>
                        </a>
                ";
        }
        $namee = $name;
        $tdName='';
        foreach ($factors as $k=>$val){
            if($showName) {
                $user = 'not found';
                $company = '';
                $namee = 'کاربر';
                $mobile = '11111';
                if (!is_null($u = $val->rel_user)) {
                    $user = $u->name . ' ' . $u->family;
                    $user .= strlen($u->company) > 3 ? '<br>(' . $u->company . ')' : '';
                    $namee = $u->name . ' ' . $u->family;
                    $mobile = $u->mobile;
                    $company = $u->company;
                }
                $userLink = route('p_users_show',[$val->user_id]);
                $tdName = "<td><a target=\"_blank\" href=\"{$userLink}\">{$user}</a></td>";
            }

            $status = Factor::statuses_short($val->status);
            $row = $k+1;
            $date1 = jdate('Y-m-d H:i',strtotime($val->updated_at));
            $date = jdate('Y-m-d H:i',strtotime($val->created_at));


            if($val->trans_id > 0){
                $trans = "<button class='btn btn-success paid'>پرداخت شده</button>";
            }else
                $trans = "<button class='btn btn-danger unpaid'>پرداخت نشده</button>";


                if (($val->status < 3 || $val->status == 8) && permission('factorsCheckout')) {
                    if ($val->checkout == 'credit')
                        $checkout = "<button class='btn btn-warning credit'>اعتباری</button>";
                    else
                        $checkout = "<button class='btn btn-success cash'>نقدی</button>";
                } else {
                    if ($val->checkout == 'credit')
                        $checkout = "<span class='text-warning'>اعتباری</span>";
                    else
                        $checkout = "<span class='text-success'>نقدی</span>";
                }


            $result .= <<<EOS
                <tr class="cl_{$val->id} user_{$val->user_id}" data-id="{$val->id}" data-date="{$date}" data-amount="{$val->final}" data-user="{$val->user_id}" data-trans="{$val->trans_id}" data-name="{$namee}" data-company="{$company}" data-mobile="{$mobile}">
                     <td class="d-none">{$row}</td>
                     <td>{$val->id}</td>
                     {$tdName}

                     <td class="number total">{$val->final}</td>
                     <td class="status_user">{$status}</td>
                     <td class="ltr">{$date1}</td>
                     <td class="ltr">{$date}</td>
                     <td class="checkout">{$checkout}</td>
                     <td class="btn-pay">
                        {$trans}
                     </td>
                     <td class="actions" data-id="{$val->id}">
                        <a data-id="{$val->id}" data-placement="top" data-toggle="tooltip"  title="جزئیات خرید" class="btn btn-success btn-circle waves-effect waves-circle waves-float showed" >
                            <i class="material-icons">remove_red_eye</i>
                        </a>

EOS;
            if($val->status < 2 || $val->status == 8) {
                if(($val->status == 0 || $val->status == 8) && permission('factorsSmsNotStock'))
                    $result .= <<<EOS
                        <a data-placement="top" data-status="{$val->status}" data-name="{$namee}" data-mobile="{$mobile}" data-toggle="tooltip" title="عدم موجودی" class="btn btn-warning await btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">remove_shopping_cart</i>
                        </a>
EOS;

                if(($val->status==0 && permission('factorConfirmFirst')) ||
                    ($val->status==8 && permission('factorConfirmLast')))
                $result .= <<<EOS
                        <a data-placement="top" data-status="{$val->status}" data-toggle="tooltip" title="ویرایش / تایید" class="btn btn-info btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">create</i>
                        </a>
EOS;
            }elseif($val->status < 6){
                if(Auth::guard('admin')->user()->grade > 9)
                    $result .= <<<EOS
                        <a data-placement="top" data-status="{$val->status}" data-toggle="tooltip" title="ویرایش / تایید" class="btn btn-info btn-circle waves-effect waves-circle waves-float">
                            <i class="material-icons">create</i>
                        </a>
EOS;

                if(($val->status == 2 || $val->status == 3) && permission('factorPackage') ) {
                    $packagingUrl = route('p_factors_packaging',[$val->id]);
                    $result .= <<<EOS
                <a href="{$packagingUrl}" data-placement="top" data-toggle="tooltip" title="در حال بسته بندی" class="btn btn-primary btn-circle waves-effect waves-circle waves-float">
                     <i class="material-icons">shopping_basket</i>
                </a>
EOS;
                }elseif ($val->status == 4&& permission('factorTransport')){
                    $result .= <<<EOS
                <a href="" data-placement="top" data-toggle="tooltip" title="تحویل به باربری" class="btn btn-warning goToPeyk btn-circle waves-effect waves-circle waves-float">
                     <i class="material-icons">local_shipping</i>
                </a>
EOS;
                }
                if (permission('factorDelivery'))
                $result .= <<<EOS
                <a href="" style="background-color: darkcyan !important;" data-placement="top" data-toggle="tooltip" title="تحویل به مشتری" class="btn btn-success delivery btn-circle waves-effect waves-circle waves-float">
                     <i class="material-icons">redeem</i>
                </a>
EOS;

            }


            $result .= <<<EOS
                          {$remove}
                    </td>
                </tr>
EOS;

        }
        return $result;
    }
}
