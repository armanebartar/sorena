<?php

namespace App\Http\Controllers\admin\comments;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactsController extends Controller
{



    public function index($page = 1){
        if(forbidden('ContactsShow',1))
            return redirect(route('p_dashboard'));

        addLog('مشاهده کامنتهای تماس با ما ');
        $page=is_numeric($page) ? $page : 1;
        $data['page']=$page;
        $page--;
        $maxRow = 15;
        $data['maxRow']=$maxRow;
        $start=$page * $data['maxRow'];
        $data['targetPage']=route('p_comments');
        $data['comments'] = Comment::where([['post_id',0]])->orderBy('id','DESC')->get();


        $data['title'] = "کامنتهای دریافتی از بخش تماس با ما";

        $data['tbl'] = $this->tbl($data['comments']);

        return view('admin.comments.contacts',$data);
    }

    public function userComments($userId,$type='all'){
        if(!$user = User::find($userId)){
            addLog('ورود ناموفق به بخش کامنتهای کاربر',['type'=>$type,'userId'=>$userId],'آیدی کاربر نامعتبر');
            session(['alert-danger'=>'آدرس url وارد شده نامعتبر است!']);
            return redirect()->back();
        }
        if(forbidden($type.'Comments',1))
            return redirect(route('p_dashboard'));
        $commentGroups = $this->_groups;
        if(array_key_exists($commentGroups[$type],$commentGroups)){
            addLog('ورود ناموفق به بخش کامنتهای کاربر',['type'=>$type,'userId'=>$userId],'تایپ نامعتبر');
            session(['alert-danger'=>'آدرس url وارد شده نامعتبر است!']);
            return redirect()->back();
        }
        addLog('مشاهده '.$commentGroups[$type].' '.$user->name.' '.$user->family);

        $data['maxRow']=25;

        if($type == 'all') {
            $data['comments'] = DB::select("select  `posts`.`type`,`comments`.`email`,`comments`.`visited`,`post_id`,`comments`.`status`,  `comments`.`user_id`,  `comments`.`content`,  `comments`.`created_at`,  `comments`.`id`,  `comments`.`name`,  `posts`.`title`   from `comments`,`posts` where `posts`.`id` = `comments`.`post_id`  and `comments`.`user_id`='$userId' order by `comments`.`id` DESC ");
//            $data['total'] = DB::select("select  count(*) as `count` from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` ")[0]->count;
        }
        else {
            $data['comments'] = DB::select("select `posts`.`type`,`comments`.`email`,`comments`.`visited`,`post_id`,`comments`.`status`,  `comments`.`user_id`,  `comments`.`content`,  `comments`.`created_at`,  `comments`.`id`,  `comments`.`name`,  `posts`.`title`  from `comments`,`posts` where `posts`.`id` = `comments`.`post_id`  and `comments`.`user_id`='$userId' order by `comments`.`id` DESC ");
//            $data['total'] = DB::select("select  count(*) as `count` from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` and `posts`.`type` = '$type' ")[0]->count;
        }

        $data['title'] = $commentGroups[$type].' از '.$user->name.' '.$user->family;
        $data['type'] = $type;
        $data['target'] = 'user';


        $data['tbl'] = $this->tbl($data['comments'],'user');

        return view('admin.comments.comments',$data);
    }

    public function seen(Request $request){
        if(forbidden('ContactsShow',1))
            return "مجاز به مشاهده کامنت نیستید.";
        $res = 'کامنت یافت نشد' ;
        if($comment = Comment::find($request->id)){

            if($comment->visited < 1){
                $comment->visited = 1;
                $comment->save();
            }
            $res = $comment->content;
        }
        return $res;
    }

    public function activated(Request $request){
        if($commentParent = Comment::find($request->id)) {

            if(!permission('changeStatusContacts')){
                session(['alert-danger'=>'تغییر وضعیت این کامنت برای شما میسر نمیباشد.']);
            }

            $commentParent->status = $request->status == 'publish' ? 1 : 0;
            $commentParent->save();
            session(['alert-success'=>'تغییر وضعیت کامنت با موفقیت انجام گردید.']);
        }else
            session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
        return redirect()->back();
    }

    public function remove(Request $request){
        if($commentParent = Comment::find($request->id)) {


            if(!permission('removeContact')){
                session(['alert-danger'=>'حذف این کامنت برای شما میسر نمیباشد.']);
            }

            Comment::destroy($request->id);
            session(['alert-success'=>'کامنت با موفقیت حذف گردید.']);
        }else
            session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
        return redirect()->back();
    }

    private function tbl($comments,$target = 'all'){
        $result = '';
        foreach ($comments as $k=>$comment){
            if(permission('changeStatusContacts')) {
                $activeCode = "<span data-placement=\"top\"  data-toggle=\"tooltip\" title='برای فعال کردن کلیک کنید' class='btn activated btn-outline-danger btn-border-radius'>غیرفعال</span>";
                if ($comment->status == 1)
                    $activeCode = "<span data-placement=\"top\"  data-toggle=\"tooltip\" title='برای غیرفعال کردن کلیک کنید' class='btn deActivated btn-outline-success btn-border-radius'>فعال</span>";
            }else{
                $activeCode = $comment->status  ? '<b style="color:green">فعال</b>' : '<b style="color: red;">غیرفعال</b>';
            }

            $classVisited = $comment->visited ? '' : 'notVisited';

            $r = $k+1;
            $stt = $comment->user_id ? '<a href="'.route('p_comments_userComments',['userId'=>$comment->user_id]).'">کاربر</a>':'میهمان';
            $commentContent = strlen($comment->content) < 150 ? $comment->content : mb_substr($comment->content,0,150).' ...';
            $title = $comment->subject;

            $time = jdate('Y-m-d H:i',strtotime($comment->created_at));
            $nameData = $comment->user_id ? "<a href='".route('p_comments_userComments',['userId'=>$comment->user_id])."'>".$comment->name.'</a>':$comment->name;
            $result .=<<<EOS
            <tr class="{$classVisited}" data-id="{$comment->id}"
                data-content="{$comment->content}"
                data-sender="{$nameData}"
                data-email="{$comment->email}"
                data-title="{$title}"
                data-type=""
                 >
                <td class="center-align ">{$r}</td>
                <td>{$comment->name}({$stt})</td>
                <td>{$title}</td>
                <td >{$commentContent}</td>
                <td class="center-align actShow">{$activeCode}</td>
                <td class="center-align">{$time}</td>
                <td class="actions">
                    <a data-placement="top"  data-toggle="tooltip" href="#" title="مشاهده جزئیات"  class="btn btn-success btn-circle waves-effect waves-circle waves-float showComment">
                        <i class="material-icons">remove_red_eye</i>
                    </a>
EOS;
            if( permission('removeContact'))
                $result .=<<<EOS
                    <a data-placement="top"  data-toggle="tooltip" title="حذف کامنت"  class="btn btn-danger btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons"> clear</i>
                    </a>
EOS;
            $result .= <<<EOS
                </td>
            </tr>
EOS;

        }

        return $result;
    }


}
