<?php

namespace App\Http\Controllers\admin\comments;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommentsController extends Controller
{
    private $_groups =[
        'all'=>'کامنتها',
        'post'=>'کامنتهای بخش اخبار',
        'gallery'=>'کامنتهای بخش گالری تصاویر',
        'tourism'=>'کامنتهای بخش نیشابورگردی',
        'law'=>'کامنتهای بخش قوانین',
        'project'=>'کامنتهای بخش پروژه ها',
        'page'=>'کامنتهای بخش صفحات',
        'recall'=>'کامنتهای بخش فراخوانها',
        'memories'=>'کامنتهای بخش آثار',
    ];
    private $_groups2 =[
        'post'=>'اخبار',
        'gallery'=>'گالری تصاویر',
        'tourism'=>'نیشابورگردی',
        'law'=>'قوانین',
        'project'=>'پروژه ها',
        'page'=>'صفحات',
        'recall'=>' فراخوانها',
        'memories'=>'آثار',

    ];

    public function index($type='all',$page = 1){
        if(forbidden($type.'Comments',1))
            return redirect(route('p_dashboard'));
        $commentGroups = $this->_groups;
        if(in_array($type,$commentGroups)){
            addLog('ورود ناموفق به بخش کامنتها',['type'=>$type],'تایپ نامعتبر');
            session(['alert-danger'=>'آدرس url وارد شده نامعتبر است!']);
            return redirect()->back();
        }
        addLog('مشاهده '.$commentGroups[$type]);
        $page=is_numeric($page) ? $page : 1;
        $data['page']=$page;
        $page--;
        $maxRow = 15;
        $data['maxRow']=$maxRow;
        $start=$page * $data['maxRow'];
        $data['targetPage']=route('p_comments',['type'=>$type]);
        if($type == 'all') {
            $data['comments'] = DB::select("select  `posts`.`type`,`comments`.`email`,`comments`.`visited`,`post_id`,`comments`.`status`,  `comments`.`user_id`,  `comments`.`content`,  `comments`.`created_at`,  `comments`.`id`,  `comments`.`name`,  `posts`.`title`   from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` order by `comments`.`id` DESC limit $start,$maxRow;");
            $data['total'] = DB::select("select  count(*) as `count` from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` ")[0]->count;
        }
        else {
            $data['comments'] = DB::select("select `posts`.`type`,`comments`.`email`,`comments`.`visited`,`post_id`,`comments`.`status`,  `comments`.`user_id`,  `comments`.`content`,  `comments`.`created_at`,  `comments`.`id`,  `comments`.`name`,  `posts`.`title`  from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` and `posts`.`type` = '$type' order by `comments`.`id` DESC limit $start,$maxRow;");
            $data['total'] = DB::select("select  count(*) as `count` from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` and `posts`.`type` = '$type' ")[0]->count;
        }

        $data['title'] = $commentGroups[$type];
        $data['type'] = $type;
        $data['target'] = 'all';


        $data['tbl'] = $this->tbl($data['comments']);

        return view('admin.comments.comments',$data);
    }

    public function userComments($userId,$type='all'){
        if(!$user = User::find($userId)){
            addLog('ورود ناموفق به بخش کامنتهای کاربر',['type'=>$type,'userId'=>$userId],'آیدی کاربر نامعتبر');
            session(['alert-danger'=>'آدرس url وارد شده نامعتبر است!']);
            return redirect()->back();
        }
        if(forbidden($type.'Comments',1))
            return redirect(route('p_dashboard'));
        $commentGroups = $this->_groups;
        if(!in_array($type,$commentGroups)){
            addLog('ورود ناموفق به بخش کامنتهای کاربر',['type'=>$type,'userId'=>$userId],'تایپ نامعتبر');
            session(['alert-danger'=>'آدرس url وارد شده نامعتبر است!']);
            return redirect()->back();
        }
        addLog('مشاهده '.$commentGroups[$type].' '.$user->name.' '.$user->family);

        $data['maxRow']=25;

        if($type == 'all') {
            $data['comments'] = DB::select("select  `posts`.`type`,`comments`.`email`,`comments`.`visited`,`post_id`,`comments`.`status`,  `comments`.`user_id`,  `comments`.`content`,  `comments`.`created_at`,  `comments`.`id`,  `comments`.`name`,  `posts`.`title`   from `comments`,`posts` where `posts`.`id` = `comments`.`post_id`  and `comments`.`user_id`='$userId' order by `comments`.`id` DESC ");
//            $data['total'] = DB::select("select  count(*) as `count` from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` ")[0]->count;
        }
        else {
            $data['comments'] = DB::select("select `posts`.`type`,`comments`.`email`,`comments`.`visited`,`post_id`,`comments`.`status`,  `comments`.`user_id`,  `comments`.`content`,  `comments`.`created_at`,  `comments`.`id`,  `comments`.`name`,  `posts`.`title`  from `comments`,`posts` where `posts`.`id` = `comments`.`post_id`  and `comments`.`user_id`='$userId' order by `comments`.`id` DESC ");
//            $data['total'] = DB::select("select  count(*) as `count` from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` and `posts`.`type` = '$type' ")[0]->count;
        }

        $data['title'] = $commentGroups[$type].' از '.$user->name.' '.$user->family;
        $data['type'] = $type;
        $data['target'] = 'user';


        $data['tbl'] = $this->tbl($data['comments'],'user');

        return view('admin.comments.comments',$data);
    }

    public function postComments($postId,$type='all'){
        if(!$post = Post::find($postId)){
            addLog('ورود ناموفق به بخش کامنتهای پست',['type'=>$type,'postId'=>$postId],'آیدی پست نامعتبر');
            session(['alert-danger'=>'آدرس url وارد شده نامعتبر است!']);
            return redirect()->back();
        }
        $type = $post->type;
        $commentGroups = $this->_groups;

        if (forbidden($type . 'Comments', 1))
            return redirect(route('p_dashboard'));


        if (!in_array($type, $commentGroups)) {
            addLog('ورود ناموفق به بخش کامنتهای پست', ['type' => $type, 'postId' => $postId], 'تایپ نامعتبر');
            session(['alert-danger' => 'آدرس url وارد شده نامعتبر است!']);
            return redirect()->back();
        }



        addLog('مشاهده '.$commentGroups[$type].' '.$post->title);
        $data['maxRow']=25;

        if($type == 'all') {
            $data['comments'] = DB::select("select  `posts`.`type`,`comments`.`email`,`comments`.`visited`,`post_id`,`comments`.`status`,  `comments`.`user_id`,  `comments`.`content`,  `comments`.`created_at`,  `comments`.`id`,  `comments`.`name`,  `posts`.`title`   from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` and `comments`.`post_id`='$postId' order by `comments`.`id` DESC ");
//            $data['total'] = DB::select("select  count(*) as `count` from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` ")[0]->count;
        }
        else {
            $data['comments'] = DB::select("select `posts`.`type`,`comments`.`email`,`comments`.`visited`,`post_id`,`comments`.`status`,  `comments`.`user_id`,  `comments`.`content`,  `comments`.`created_at`,  `comments`.`id`,  `comments`.`name`,  `posts`.`title`  from `comments`,`posts` where `posts`.`id` = `comments`.`post_id`  and `comments`.`post_id`='$postId' order by `comments`.`id` DESC ");
//            $data['total'] = DB::select("select  count(*) as `count` from `comments`,`posts` where `posts`.`id` = `comments`.`post_id` and `posts`.`type` = '$type' ")[0]->count;
        }

        $data['title'] = $commentGroups[$type].' از '.$post->title;
        $data['type'] = $type;
        $data['target'] = 'post';


        $data['tbl'] = $this->tbl($data['comments'],'post');

        return view('admin.comments.comments',$data);
    }

//    public function contact(){
//
//
//            if (forbidden('contactMessage', 1))
//                return redirect(route('p_dashboard'));
//
//
//
//
//        addLog('مشاهده پیامهای بخش ارتباط با ما');
//        $data['maxRow']=25;
//
//        $data['comments'] = Comment::where('post_id',0)->get();
//
//        $data['title'] = 'پیامهای بخش ارتباط با ما';
//
//        $data['target'] = 'post';
//
//        $data['tbl'] = $this->tbl2($data['comments'],'contact');
//
//        return view('panel.comments.comments',$data);
//    }

    public function seen(Request $request)
    {

        $res = 'کامنت یافت نشد' ;
        if($comment = Comment::find($request->id)){

            if(!$post = Post::find($comment->post_id)) {
                return 'not1';
            }
            $commentGroups = $this->_groups;

            if (!in_array($post->type, $commentGroups) || forbidden($post->type . 'Comments', 1))
                return 'not2';

            if($comment->visited < 1){
                $comment->visited = 1;
                $comment->save();
            }
            $res = $comment->content;
        }
        return $res;
    }

    public function answer(Request $request)
    {
        if($commentParent = Comment::find($request->id)) {
            if(!$post = Post::find($commentParent->post_id)) {
                session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
            }
            $commentGroups = $this->_groups;

                if (!in_array($post->type, $commentGroups) || forbidden($post->type . 'Comments', 1))
                    session(['alert-danger'=>'پاسخ به این کامنت برای شما میسر نمیباشد.']);

            $commentParent->status = 1;
            $commentParent->visited = 1;
            $commentParent->save();
            $avatar = "files/images/avatar.png";
            if(is_file(User::imagePath('/').Auth::guard('admin')->user()->image))
                $avatar = User::imagePath('/').Auth::guard('admin')->user()->image;
            $comment = new Comment();
            $comment->user_id = Auth::guard('admin')->user()->id;
            $comment->user_role = 1;
            $comment->post_id = $commentParent->post_id;
            $comment->name = Auth::guard('admin')->user()->name.' '.Auth::guard('admin')->user()->family;
            $comment->email = '';
            $comment->content = $request->answer;
            $comment->parent = $commentParent->parent?$commentParent->parent:$commentParent->id;
            $comment->status = 1;
            $comment->avatar = $avatar;
            $comment->visited = 1;
            $comment->save();
            session(['alert-success'=>'پاسخ با موفقیت ذخیره گردید.']);
        }else
            session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
        return redirect()->back();
    }

    public function activated(Request $request)
    {
        if($commentParent = Comment::find($request->id)) {
            if(!$post = Post::find($commentParent->post_id)) {
                session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
                return redirect()->back();
            }
            $commentGroups = $this->_groups;
//            dd($commentGroups[$post->type], $commentGroups);


            if (!in_array($commentGroups[$post->type], $commentGroups) || forbidden($post->type . 'Comments', 1) || forbidden($post->type .'changeStatusComment')) {
                session(['alert-danger' => 'تغییر وضعیت 2این کامنت برای شما میسر نمیباشد.']);
                return redirect()->back();
            }

            $commentParent->status = $request->status == 'publish' ? 1 : 0;
            $commentParent->save();
            session(['alert-success'=>'تغییر وضعیت کامنت با موفقیت انجام گردید.']);
        }else
            session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
        return redirect()->back();
    }

    public function editSave(Request $request)
    {
        if($commentParent = Comment::find($request->id)) {
            if(!$post = Post::find($commentParent->post_id)) {
                session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
            }
            $commentGroups = $this->_groups;

                if (!in_array($post->type, $commentGroups) || forbidden($post->type . 'Comments', 1) || forbidden($post->type .'editComment'))
                    session(['alert-danger'=>'ویرایش این کامنت برای شما میسر نمیباشد.']);

            $commentParent->content = $request->comment;
            $commentParent->status = 1;
            $commentParent->visited = 1;
            $commentParent->save();
            session(['alert-success'=>'تغییر با موفقیت ذخیره گردید.']);
        }else
            session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
        return redirect()->back();
    }

    public function remove(Request $request){
        if($commentParent = Comment::find($request->id)) {
            if(!$post = Post::find($commentParent->post_id)) {
                session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
            }
            $commentGroups = $this->_groups;

                if (!in_array($post->type, $commentGroups) || forbidden($post->type . 'Comments', 1) || forbidden($post->type.'removeComment'))
                    session(['alert-danger'=>'حذف این کامنت برای شما میسر نمیباشد.']);

            Comment::destroy($request->id);
            session(['alert-success'=>'کامنت با موفقیت حذف گردید.']);
        }else
            session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
        return redirect()->back();
    }

    private function tbl($comments,$target = 'all'){
        $result = '';
        $groups = $this->_groups2;
        foreach ($comments as $k=>$comment){
            $type = $comment->post_type;
            if(permission($type.'changeStatusComment')) {
                $activeCode = "<span data-placement=\"top\"  data-toggle=\"tooltip\" title='برای فعال کردن کلیک کنید' class='btn activated btn-outline-danger btn-border-radius'>غیرفعال</span>";
                if ($comment->status == 1)
                    $activeCode = "<span data-placement=\"top\"  data-toggle=\"tooltip\" title='برای غیرفعال کردن کلیک کنید' class='btn deActivated btn-outline-success btn-border-radius'>فعال</span>";
            }else{
                $activeCode = $comment->status  ? '<b style="color:green">فعال</b>' : '<b style="color: red;">غیرفعال</b>';
            }

            $classVisited = $comment->visited ? '' : 'notVisited';
            $type = $groups[$comment->type];
            $r = $k+1;
            $stt = $comment->user_id ? '<a href="'.route('p_comments_userComments',['userId'=>$comment->user_id]).'">کاربر</a>':'میهمان';
            $commentContent = strlen($comment->content) < 150 ? $comment->content : mb_substr($comment->content,0,150).' ...';
            $title = "<a target='_blank' href='".route('singleArticle',['id'=>$comment->post_id])."'>".$comment->title."</a>";
            $title2 = $title;
//            $title2 = "<a target='_blank' href='".route('p_comments_postComments',['postId'=>$comment->post_id,'type'=>$type])."'>".$comment->title."</a>";
            $time = jdate('Y-m-d H:i',strtotime($comment->created_at));
            $nameData = $comment->user_id ? "<a href='".route('p_comments_userComments',['userId'=>$comment->user_id])."'>".$comment->name.'</a>':$comment->name;
            $result .=<<<EOS
            <tr class="{$classVisited}" data-id="{$comment->id}"
                data-content="{$comment->content}"
                data-sender="{$nameData}"
                data-email="{$comment->email}"
                data-title="{$title2}"
                data-type="{$type}"
                 >
                <td class="center-align ">{$r}</td>
EOS;
            if($target == 'all' || $target == 'post')
            $result .=<<<EOS
                <td>{$comment->name}({$stt})</td>
EOS;
            if($target == 'all' || $target == 'user')
                $result .=<<<EOS
                <td>{$title}</td>

EOS;
            $result .=<<<EOS
                <td >{$commentContent}</td>
                <td class="center-align actShow">{$activeCode}</td>
                <td class="center-align">{$time}</td>
                <td class="actions">
                    <a data-placement="top"  data-toggle="tooltip" href="#" title="مشاهده جزئیات"  class="btn btn-success btn-circle waves-effect waves-circle waves-float showComment">
                        <i class="material-icons">remove_red_eye</i>
                    </a>
EOS;
            if(permission($type.'editComment'))
                $result .=<<<EOS
                    <a data-placement="top"  data-toggle="tooltip" href="#" title="ویرایش کامنت"  class="btn btn-info btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons">create</i>
                    </a>
EOS;
            if( permission($type.'removeComment'))
                $result .=<<<EOS
                    <a data-placement="top"  data-toggle="tooltip" title="حذف کامنت"  class="btn btn-danger btn-circle waves-effect waves-circle waves-float">
                        <i class="material-icons"> clear</i>
                    </a>
EOS;
            $result .= <<<EOS
                </td>
            </tr>
EOS;

        }

        return $result;
    }

//    private function tbl2($comments,$target = 'all'){
//        $result = '';
//        $groups = $this->_groups2;
//        foreach ($comments as $k=>$comment){
//
//
//            $classVisited = $comment->visited ? '' : 'notVisited';
//
//            $r = $k+1;
//            $commentContent = strlen($comment->content) < 150 ? $comment->content : mb_substr($comment->content,0,150).' ...';
//            $title = "<a target='_blank' href='".route('singleArticle',['id'=>$comment->post_id])."'>".$comment->subject."</a>";
//            $time = jdate('Y-m-d H:i',strtotime($comment->created_at));
//            $nameData = $comment->user_id ? "<a href='".route('p_comments_userComments',['userId'=>$comment->user_id])."'>".$comment->name.'</a>':$comment->name;
//            $result .=<<<EOS
//            <tr class="{$classVisited}" data-id="{$comment->id}"
//                data-content="{$comment->content}"
//                data-sender="{$nameData}"
//                data-email="{$comment->email}"
//                data-title="{$title}"
//                data-type="تماس با ما"
//                 >
//                <td class="center-align ">{$r}</td>
//EOS;
//            if($target == 'all' || $target == 'post')
//                $result .=<<<EOS
//                <td>{$comment->name}</td>
//EOS;
//
//                $result .=<<<EOS
//                <td>{$title}</td>
//
//EOS;
//            $result .=<<<EOS
//                <td >{$commentContent}</td>
//
//                <td class="center-align">{$time}</td>
//                <td class="actions">
//                    <a data-placement="top"  data-toggle="tooltip" href="#" title="مشاهده جزئیات"  class="btn btn-success btn-circle waves-effect waves-circle waves-float showComment">
//                        <i class="material-icons">remove_red_eye</i>
//                    </a>
//EOS;
//
//            if( permission('removeComment'))
//                $result .=<<<EOS
//                    <a data-placement="top"  data-toggle="tooltip" title="حذف کامنت"  class="btn btn-danger btn-circle waves-effect waves-circle waves-float">
//                        <i class="material-icons"> clear</i>
//                    </a>
//EOS;
//            $result .= <<<EOS
//                </td>
//            </tr>
//EOS;
//
//        }
//
//        return $result;
//    }

}
