<?php

namespace App\Http\Controllers\admin\posts;

use App\Helpers\Helpers;
use App\Models\Brand;
use App\Models\Category;
use App\Models\File;
use App\Models\Post;
use App\Models\Post_cat;
use App\Models\Post_option;
use App\Models\Post_prop;
use App\Models\Post_tag;
use App\Models\Post_type;
use App\Models\S_commission;
use App\Models\S_discount;
use App\Models\S_money;
use App\Models\S_product;
use App\Models\S_prop;
use App\Models\S_prop_value;
use App\Models\S_stock;
use App\Models\Tag;
use App\Models\User;
use App\Models\User_option;
use App\Models\Post_attribute;
use Faker\Provider\Image;
use Hamcrest\Thingy;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Exception;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller{
    private $_options = ['seo_title','seo_desc','seo_keyword'];
    private $_other_options = [
        'product'=>['method_use_title','method_use','proposal_title','proposal']
    ];
    private $_options_product = ['method_use_title','method_use','proposal_title','proposal'];
//    private $_melk_options ;

//    public function __construct(){
//        $this->_melk_options = Post::$_melk_options;
//    }

    public function index($type,$sort='id',$page=1){
        $this->setProperties($type);

        if(forbidden($this->_permissions['articlesShow'],1))
            return redirect(route('p_dashboard'));
        addLog('مشاهده لیست '.$this->_metas['nameTotal']);
        $page=is_numeric($page) ? $page : 1;
        $data['page']=$page;
        $page--;
        $data['maxRow']=2500;
        $start=$page * $data['maxRow'];
        $data['targetPage']=route($this->_metas['baseRout'],[$type,$sort]);
        $data['sort']=$sort;


        $meta = $this->_metas;

        $where = [['type', $type],['parent',0]];

        if($sort == 'active'){
            $where[]=['status','publish'];
            $where[]=['status','<>','draft'];
            $data['titre']='فعال';
        }elseif ($sort == 'deactive'){
            $where[]=['status','unpublish'];
            $where[]=['status','<>','draft'];
            $data['titre']='غیرفعال';
        } elseif ($sort == 'myPost'){
            $where[]=['author',Auth::guard('admin')->user()->id];
            $where[]=['status','<>','draft'];
            $data['titre']='من';
        } elseif ($sort == 'draft'){
            $where[]=['status','draft'];
            $data['titre']='پیش نویس';
        }else {
            $where[]=['status','<>','draft'];
            $data['titre']='';
        }
        $data['total'] = Post::where($where)->count();
        $posts = Post::with(['cat','thumb','options'])->where($where)->skip($start)->take(2000)->select('*')->get();
        $data['meta']=$meta;

        $data['tbl'] = $this->tbl($posts);
        return view('admin.posts.posts', $data);
    }

    public function add($type, $id = 0, $lang = ''){
        $this->setProperties($type);
        $lang = $lang==''?session('lang',DEFAULT_LANG[0]):$lang;
        if($id == 0 || !$data['post'] = Post::with(['cats','tags','thumb','attaches','options','attrs','props','s_commission','s_money','s_product','s_discount'])->where([['id',$id],['type',$type]])->first()){
            if (forbidden($this->_permissions['addArticle'], 1))
                return redirect(route('p_dashboard'));
            addLog('ورود به صفحه افزودن ' . $this->_metas['nameSingle']);
            $id = Post::make_draft($type);
            return redirect()->route('p_posts_add',['type'=>$type,'id'=>$id,'lang'=>$lang]);
        }

        if(DEFAULT_LANG[0]!=$lang && !isset(LANGUAGES[$lang])){
            session(['alert-danger'=>'زبان مورد نظر یافت نشد']);
            return redirect()->route('p_posts',[$type]);
        }
        $data['is_translate'] = '';
        if(isset(LANGUAGES[$lang])){
            $data['is_translate'] = ' ترجمه '.LANGUAGES[$lang][1].' ';
        }
        $data['not_translate'] = DEFAULT_LANG[0]==$lang;

        $data['options']=[];
        foreach ($data['post']->options as $op)
            $data['options'][$op->key]=$op->value;

        $userId = Auth::guard('admin')->user()->id;
        $data['mode'] = 'add';
        if($data['post']->status != 'draft') {
            if (!permission($this->_permissions['articleEdit']) && !($data['post']->author == $userId && permission($this->_permissions['myArticleEdit']))) {
                session(['alert-danger' => 'شما مجاز به ویرایش این ' . $this->_metas['nameSingle'] . ' نمی‌باشید.']);
                return redirect(route('p_dashboard'));
            }
            addLog('رفتن به صفحه ویرایش ' . $this->_metas['nameSingle'] , [ 'postId'=>$id, 'title'=>$data['post']->title ]);
            $data['mode'] = 'edit';
        }

        $data['oldGroups']=[];
        foreach ($data['post']->cats as $gg)
            $data['oldGroups'][]=$gg->id;

        $data['oldTags']=[];
        foreach ($data['post']->tags as $tt)
            $data['oldTags'][]=$tt->id;

        $data['attaches'] = [];

        foreach ($data['post']->attaches as $attach){
            if($attach->type == 'image')
                $data['attaches'][]=$attach;
        }
       // $data['options'] = [];
        $data['attrs'] = $this->_metas['by_attr'] ? $this->attrs($data['post']->attrs) : '';
        $data['props'] = '';
        $data['stock_props'] = '';
        $data['stock_tbl'] = '';
        if($this->_metas['is_product']){
            $props = $this->props($data['post']->cat_id,$data['post']->id,$data['post']->props);
            $data['props'] = $props[0];
            $data['stock_props'] = $props[1];
            $data['stock_tbl'] = $this->stock_get($data['post']->id);
            foreach ($this->_options_product as $opti)
                $data['options'][$opti] = $data['options'][$opti]??'';
        }


        if($this->_metas['status_category']) $data['cats'] = Category::getCatsByGroup($this->_metas['id'],1);
        if($this->_metas['status_tag']) $data['tags'] = Tag::getAll();
        if($this->_metas['status_brand']) $data['brands'] = Brand::getAll();


        $options = $this->_other_options[$type] ?? [];
        $options = array_merge($options,$this->_options);
        foreach ($options as $opti)
            $data['options'][$opti] = $data['options'][$opti] ?? '';



        $data['meta']=$this->_metas;
        $data['lang']=$lang;
        $data['discounts']=$this->discount_get($data['post']->id);

        return view('admin.posts.addPost',$data);
    }

    private function props($cat_id,$post_id,$this_props=null){

        $this_props = is_null($this_props) ? Post_prop::where('post_id',$post_id)->get() : $this_props;
        $old_props = [];
        foreach ($this_props as $item)
            $old_props[$item->prop_id.'_'.$item->prop_val_id] = $item;

        $res = '';
        $stock_props = '';
        if($props = S_prop::where('cat_id',$cat_id)->get()){
            $res .= "<div class=\"row\">";
            foreach($props as $prop){
                $stock_props.="<div class='col-md-3'><p>{$prop->title}</p><select name='stock_props_{$prop->id}'><option value='0'>بی اثر</option>";
                $res .= " <div class=\"col-sm-3\">";
                $res .= "<h6>{$prop->title}</h6>";
                foreach(S_prop_value::where('prop_id',$prop->id)->get() as $vvv){
                    $stock_props.="<option value='".$prop->id.'_'.$vvv->id."'>{$vvv->value}</option>";
                    $checked = isset($old_props[$prop->id.'_'.$vvv->id]) ? "checked='checked'" : '';
                    $res .= "
                    <label>
                        <input type=\"checkbox\"  name='props[]' {$checked} value='{$prop->id}_{$vvv->id}' />
                        <span>{$vvv->value}</span>
                     </label>
                     ";
                }
                $stock_props.="</select></div>";
                $res .= "</div>";
            }
            $res .= "</div>";
        }
          return [$res,$stock_props];
    }

    public function get_attrs(Request $request){
        if($cat = Category::where('id',$request->id)->first()){
            $old_attrs = [];
            foreach (Post_attribute::where('post_id',$request->post_id)->get() as $item)
                $old_attrs[trim($item->title)] = $item->desc;
            $details = json_decode($cat->details,1);
            $attrs = $details['attr'] ?? [];
            foreach ($attrs as $attr ){
                if(!isset($old_attrs[$attr])){
                    $old_attrs[$attr] = '';
                }
            }
            $res_attrs = '';

            foreach ($old_attrs as $k=>$attr ){
                $res_attrs .= "
                <tr>
                    <td class=\"myInput\"><input value='{$k}' type=\"text\" name=\"attrTitle[]\" class=\"form-control\"></td>
                    <td class=\"myInput\"><input value='{$attr}' type=\"text\" name=\"attrDesc[]\" class=\"form-control\"></td>
                    <td>
                        <i class=\"fa fa-trash-alt text-danger\" style='cursor: pointer'></i>
                    </td>
                </tr>
                ";
            }
            $props = $this->props($cat->id,$request->post_id);
            return ['res'=>10,'attrs'=>$res_attrs,'props'=>$props[0],'stock_props'=>$props[1]];
        }
        return ['res'=>1];
    }

    private function attrs($attrs){
        $res = '';
        foreach ($attrs as $attr){
            $res .= "
                <tr>
                    <td class=\"myInput\"><input value='{$attr->title}' type=\"text\" name=\"attrTitle[]\" class=\"form-control\"></td>
                    <td class=\"myInput\"><input value='{$attr->desc}' type=\"text\" name=\"attrDesc[]\" class=\"form-control\"></td>
                    <td>
                        <i class=\"fa fa-trash-alt text-danger\" style='cursor: pointer'></i>
                    </td>
                </tr>
                ";
        }
        return $res;
    }

    public function addSave(Request $request,$type){
        $this->setProperties($type);
        $is_translate = false;
        if($request->id != 0 && $post = Post::where([['id',$request->id],['type',$type]])->first()){
            if($post->status=='draft'){
                $msg = 'ذخیره '.$this->_metas['nameSingle'].' جدید';
            }else {
                $msg = 'ویرایش ' . $this->_metas['nameSingle'];
            }
            $mode = 'edit';
            $is_translate = $post->lang != 'no' && $post->parent > 0 ;
            if($is_translate)
                $msg = 'ویرایش ترجمه '.$this->_metas['nameSingle'];

            if($is_translate && !permission($this->_permissions['addTranslate']))
                return ['res'=>1,'myAlert'=>"در حال حاضر مجاز به انجام اینکار نیستید.",'mySuccess'=>''];
            if(!permission($this->_permissions['articleEdit']))
                return ['res'=>1,'myAlert'=>"در حال حاضر مجاز به انجام اینکار نیستید.",'mySuccess'=>''];
        }
//        else{
//            $mode = 'add';
//            $msg = 'ذخیره '.$this->_metas['nameSingle'].' جدید';
//            $post = new Post();
//            $post->author = Auth::guard('admin')->user()->id;
//
//            if(!permission($this->_permissions['addArticle']))
//                return ['res'=>1,'myAlert'=>"در حال حاضر مجاز به انجام اینکار نیستید.",'mySuccess'=>''];
//        }

        $errors = [];
        if(strlen($request->title) < 3)
            $errors [] = "عنوان " . $this->_metas['nameSingle'] . " الزامیست و باید حداقل ۳ کارکتر داشته باشد.";
       if(!$request->group_index || Category::where('id',$request->group_index)->count() < 1)
        if(!$is_translate && $this->_metas['status_category'] && (!isset($request->groups) || !is_array($request->groups) || count($request->groups)<1))
            $errors [] =  "انتخاب دسته ".$this->_metas['nameSingle'].' الزامیست.';
        if(!$is_translate && $this->_metas['status_tag'] && (!isset($request->tags) || !is_array($request->tags) || count($request->tags)<1))
            $errors [] =  "انتخاب برچسب ".$this->_metas['nameSingle'].' الزامیست.';
        if(!$is_translate && $this->_metas['status_brand'] && (!isset($request->brand) ))
            $errors [] =  "انتخاب برند ".$this->_metas['nameSingle'].' الزامیست.';

        if(!is_file($request->path.$request->image) && !is_file($post->path.$post->image))
            $errors [] = 'انتخاب تصویر شاخص الزامیست.';

        if(strlen($request->contentPost) < 20)
            $errors [] = "متن " . $this->_metas['nameSingle'] . " الزامیست و باید حداقل ۲۰ کارکتر داشته باشد.";
        if($this->_metas['is_product']){
            if(S_money::where('id',$request->money_type)->count() < 1)
                $errors [] = 'نوع قیمتگذاری را انتخاب نمایید.';
            if(!is_numeric(fa2la($request->stock)))
                $errors [] = 'موجودی محصول را وارد نمایید';
            if(!is_numeric(fa2la($request->real_price)))
                $errors [] = 'قیمت محصول را وارد نمایید';
            if(!is_numeric(fa2la($request->price1)))
                $errors [] = 'قیمت پلن اول محصول را وارد نمایید';
            if(!in_array($request->type_commission,['fix','percent']))
                $errors [] = 'نوع کیسیون بازاریاب را انتخاب نمایید.';
            if(!is_numeric(fa2la($request->commission1)))
                $errors [] = 'کمیسیون بازاریاب را وارد نمایید';
        }
        if($errors != []){
            $myAlert = '';
            foreach ($errors as $error)
                $myAlert .= $error.'<br>';
            return ['res'=>1,'myAlert'=>$myAlert,'mySuccess'=>''];
        }


        $minContent = strlen($request->minContent) > 5 ?
            $request->minContent :
            substr(preg_replace('/[\n \t \n\t \t\n]{2,}/is',' ',strip_tags($request->contentPost)),0,50);
        // save post

        if(is_file($request->path.$request->image)) {
            $post->image = $request->image;
            $post->path = $request->path;
        }
        $post->title = $request->title;
        $post->slug = Post::slugGenerate($request->title,0) ;
        $post->content = $request->contentPost ;
        if($mode=='add' || $post->status=='draft')
            $post->status = $this->_permissions['articleChangeStatus'] ? 'publish' : 'unpublish';
        $post->comment = $this->_metas['status_comment'] ? 'active' : 'deactive';

        $post->date = $request->date;
        $post->type = $this->_metas['type'];
        $post->subTitle = $request->subTitle;
        if($this->_metas['status_brand'])
            $post->brand = $request->brand;
        $post->minContent = $minContent;
        $post->cat_id = $request->group_index;
        $post->save();

        addLog($msg,$request->all());
        $childes = [];
        if($mode == 'edit'){
            Post_option::where('post_id',$post->id)->delete();
            if($this->_metas['by_attr'])
                Post_attribute::where('post_id',$post->id)->delete();
            if($this->_metas['is_product'])
                Post_prop::where('post_id',$post->id)->delete();

            if(!$is_translate) {
                if ($this->_metas['status_tag'])
                    Post_tag::where('post_id', $post->id)->delete();
                if ($this->_metas['status_category'])
                    Post_cat::where('post_id', $post->id)->delete();

                if($childes = Post::where('parent',$post->id)->pluck('id')){
                    foreach ($childes as $child){
                        if ($this->_metas['status_tag'])
                            Post_tag::where('post_id', $child)->delete();
                        if ($this->_metas['status_category'])
                            Post_cat::where('post_id', $child)->delete();
                    }
                }
            }

            foreach (File::where('post_id',$post->id)->get() as $item11){

                $item11->post_id = 0;
                $item11->save();
            }
        }


        $optes = $this->_other_options[$post->type] ?? [];
        $optes = array_merge($optes,$this->_options);
        $requestArr = $request->all();
        foreach ($optes as $vv){
            $val = $request->$vv;
            if($vv == 'seo_keyword'){
                $val = preg_replace('/[\n \r \n\r ,]+/is',',',$val);
            }
            if(strlen($val) < 3) {
                if ($vv == 'seo_title')
                    $val = $post->title;
                elseif ($vv == 'seo_desc')
                    $val = $minContent;
//                elseif ($vv == 'seo_keyword')
//                    $val = implode(',',$request->tags);
            }
            $opt = new Post_option();
            $opt->post_id = $post->id;
            $opt->key = $vv;
            $opt->value = $val;
            $opt->save();
        }


        // save tags
        if(!$is_translate&&$this->_metas['status_tag'] && isset($request->tags) && is_array($request->tags))
        foreach ($request->tags as $tagValue){
            $tag = new Post_tag();
            $tag->tag_id = $tagValue ;
            $tag->post_id = $post->id;
            $tag->save();

            foreach ($childes as $child){
                $tag = new Post_tag();
                $tag->tag_id = $tagValue ;
                $tag->post_id = $child;
                $tag->save();
            }

        }
        // save brand
        if(!$is_translate&&$this->_metas['status_brand'] && isset($request->brand) && $childes = Post::where('parent',$post->id)->get())
        foreach ($childes as $child){
            $child->brand = $post->brand;
            $child->save();

        }

        // save categories
        if(!$is_translate&&$this->_metas['status_category'] && isset($request->groups) && is_array($request->groups))
        foreach ($request->groups as $catValue){
            $cat = new Post_cat();
            $cat->cat_id = $catValue ;
            $cat->post_id = $post->id;
            $cat->save();

            foreach ($childes as $child){
                $cat = new Post_cat();
                $cat->cat_id = $catValue ;
                $cat->post_id = $child->id;
                $cat->save();
            }
        }

        if(isset($request->attach) && is_array($request->attach))
            foreach ($request->attach as $attach){
                if($fff = File::where('id',$attach)->first()){
                    $fff->post_id = $post->id;
                    $fff->title = $requestArr['att_img_desc_'.$fff->id] ?? $post->title;
                    $fff->save();
                }

            }
        if($this->_metas['by_attr']){
            $attrTitle = $request->attrTitle ?? [];
            foreach ($attrTitle as $k=>$v){
                $desc = $request->attrDesc[$k] ?? '';
                if(strlen($v) > 0 && strlen($desc) > 0) {
                    $attr = new Post_attribute();
                    $attr->post_id = $post->id;
                    $attr->title = $v;
                    $attr->desc = $desc;
                    $attr->save();
                }
            }
        }
        if($this->_metas['is_product']){
            $props = $request->props ?? [];
            foreach ($props as $k=>$v){
                $prop = explode('_',$v);
                if(count($prop) == 2) {
                    $attr = new Post_prop();
                    $attr->post_id = $post->id;
                    $attr->prop_id = $prop[0];
                    $attr->prop_val_id = $prop[1];
                    $attr->save();
                }
            }

            if(!$product = S_product::where('post_id',$post->id)->first()){
                $product = new S_product();
                $product->post_id = $post->id;
            }
            $product->real_price = fa2la($request->real_price);
            $product->price1 = fa2la($request->price1);
            $product->price2 = is_numeric(fa2la($request->price2)) ? fa2la($request->price2) : fa2la($request->price1);
            $product->price3 = is_numeric(fa2la($request->price3)) ? fa2la($request->price3) : fa2la($request->price1);
            $product->money_id = $request->money_type;
            $product->type = $request->product_type;
            $product->sell_rate = is_numeric(fa2la($request->sell_rate)) ? fa2la($request->sell_rate) : 0;
            $product->transport = is_numeric(fa2la($request->transport)) ? fa2la($request->transport) : 0;
            $product->unit = $request->unit;
            $product->weight = is_numeric(fa2la($request->weight)) ? fa2la($request->weight) : 1;
            $product->stock = fa2la($request->stock);
            $product->stock_show = is_numeric(fa2la($request->stock_show)) ? fa2la($request->stock_show) : 10;
            $product->stock_report = is_numeric(fa2la($request->stock_report)) ? fa2la($request->stock_report) : 5;

            $product->save();

            if(!$commission = S_commission::where('post_id',$post->id)->first()){
                $commission = new S_commission();
                $commission->post_id = $post->id;
            }
            $commission->type = $request->type_commission;
            $commission->commission1 = fa2la($request->commission1) ;
            $commission->commission2 = is_numeric(fa2la($request->commission2)) ? fa2la($request->commission2) : fa2la($request->commission1);
            $commission->commission3 = is_numeric(fa2la($request->commission3)) ? fa2la($request->commission3) : fa2la($request->commission1);
            $commission->save();
        }

        session(['alert-success' => $this->_metas['nameSingle'].' با موفقیت ذخیره شد']);


        return ['res'=>10,'myAlert'=>'','mySuccess'=>$this->_metas['nameSingle'].' با موفقیت ذخیره شد'];
    }

    public function addTranslate($type,$id,$lang,$secret){

        $langes = LANGUAGES;
        if(!isset($langes[$lang])) {
            session(['alert-danger'=>'زبان انتخابی نامعتبر است.']);
            return back();
        }

        if($secret != Post::secure([$type,$id,$lang])){
            session(['alert-danger'=>'عملیات با خطا مواجه گردید.']);
            return back();
        }

        $this->setProperties($type);

        if(forbidden($this->_permissions['addTranslate'],1)){
            session(['alert-danger'=>'در حال حاضر شما مجوز افزودن ترجمه را ندارید.']);
            return back();
        }

        if(!$post = Post::with(['thumb','attaches'])->where([['id',$id],['type',$type]])->first()) {
            session(['alert-danger'=>'پست موردنظر یافت نشد!']);
            return back();
        }
        if(!$trans = Post::where([['parent',$post->id],['lang',$lang]])->first()){

            $post->translates = trim( $post->translates,',').','.$lang;
            $post->save();

            $trans = new Post();
            $trans->commentCount = 0;
            $trans->parent = $post->id;
            $trans->type = $type;
            $trans->lang = $lang;
            $trans->visited = 0;
            $trans->status = $post->status;
            $trans->comment = $post->comment;
            $trans->image = $post->image;
            $trans->path = $post->path;
            $trans->date = $post->date;
            $trans->title = $post->title;
            $trans->slug = $post->slug;
            $trans->author = auth('admin')->user()->id;
            $trans->content = $post->content;
            $trans->subTitle = $post->subTitle;
            $trans->minContent = $post->minContent;
            $trans->save();

            foreach (Post_option::where('post_id',$post->id)->get() as $item){
                $opt = new Post_option();
                $opt->post_id = $trans->id;
                $opt->key = $item->key;
                $opt->value = $item->value;
                $opt->save();
            }

            if($files = File::where('post_id',$post->id)->get())
                foreach ($files as $attach){
                    $fff = new File();
                    $fff->post_id = $trans->id;
                    $fff->title = $attach->title;
                    $fff->importer = $attach->importer;
                    $fff->file = $attach->file;
                    $fff->type = $attach->type;
                    $fff->path = $attach->path;
                    $fff->save();
                }

            if($this->_metas['status_tag'] && $tags = Post_tag::where('post_id',$post->id)->get())
                foreach ($tags as $tagValue){
                    $tag = new Post_tag();
                    $tag->tag_id = $tagValue->tag_id ;
                    $tag->post_id = $trans->id;
                    $tag->save();
                }

            if($this->_metas['status_category'] && $cats = Post_cat::where('post_id',$post->id)->get())
                foreach ($cats as $catValue){
                    $cat = new Post_cat();
                    $cat->cat_id = $catValue->cat_id ;
                    $cat->post_id = $trans->id;
                    $cat->save();
                }

        }
        return redirect()->route('p_posts_edit',[$type,$trans->id,$lang]);
    }

    public function remove(Request $request,$type){
        $this->setProperties($type);

        if(!$post = Post::where([['id',$request->id],['type',$type]])->first()){
            addLog('حذف ناموفق '.$this->_metas['nameSingle'],$request->all(),'آیدی  نامعتبر است');
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید.','mySuccess'=>''];
        }

        $userId=Auth::guard('admin')->user()->id;

        if(!permission($this->_permissions['articleRemove']) && !($post->author == $userId && permission($this->_permissions['myArticleRemove']))){
            return ['res'=>1,'myAlert'=>'در حال حاضر شما دسترسی لازم جهت حذف '.$this->_metas['nameSingle'].' را ندارید.','mySuccess'=>''];

        }

        addLog('حذف '.$this->_metas['nameSingle'],$post->title);

//        Post_tag::where('post_id',$post->id)->delete();
//        Post_cat::where('post_id',$post->id)->delete();
        Post::where('parent',$post->id)->delete();

        Post::destroy($request->id);

        return ['res'=>10,'myAlert'=>'','mySuccess'=>$this->_metas['nameSingle'].' موردنظر با موفقیت حذف گردید.'];
    }

    public function activated(Request $request,$type){
        $this->setProperties($type);
        if(forbidden($this->_permissions['articleChangeStatus'],1)){
            addLog('تغییر وضعیت ناموفق '.$this->_metas['nameSingle'],$request->all(),'مجوز لازم را ندارد');
            return ['res'=>1,'myAlert'=>'در حال حاضر شما دسترسی لازم جهت تغییر وضعیت '.$this->_metas['nameSingle'].' را ندارید.','mySuccess'=>''];
        }

        $ppp = $request->status == 'publish' ? 'فعال' : 'غیرفعال';

        if($post = Post::where([['id',$request->id],['type',$type]])->select('id','status','title')->first()){

            $post->status = $request->status=='publish' ? 'publish' : 'unpublish';
            $post->save();
            if($request->status == 'publish')
                $newBtn = "<span data-placement=\"top\" data-titre='غیر فعال' data-new='unpublish'  data-toggle=\"tooltip\" title='برای غیرفعال کردن کلیک کنید' class='btn activated btn-outline-success btn-border-radius'>فعال</span>";
            else
                $newBtn = "<span data-placement=\"top\" data-titre='فعال' data-new='publish'  data-toggle=\"tooltip\" title='برای فعال کردن کلیک کنید' class='btn activated btn-outline-danger btn-border-radius'>غیرفعال</span>";
            addLog($ppp.' کردن '.$this->_metas['nameSingle'],['id'=>$post->id,'title'=>$post->title]);

            return ['res'=>10,'myAlert'=>'','mySuccess'=>$this->_metas['nameSingle'].' موردنظر با موفقیت '.$ppp. ' گردید.','newBtn'=>$newBtn];
        }else {
            addLog($ppp.' کردن ناموفق '.$this->_metas['nameSingle'],$request->all(),'آیدی و پست تایپ با یکدیگر همخوانی ندارد.');
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید. ','mySuccess'=>''];
        }

    }

    public function saveThumb(Request $request){
        $this->setProperties($request->type);
        if (!permission($this->_permissions['addArticle']) && permission($this->_permissions['articleEdit']))
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'در حال حاضر مجاز به اینکار نیستید.'];
        $path =  'files/posts/' ;
        $size = 2 ;
        foreach ($request->all() as $key=>$item){
            if($request->hasFile($key)){
                return Helpers::checkImageAjax($request,$key,$path,$size);
            }
        }
        return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک تصویر انتخاب نمایید'];

    }

    public function saveImageAtt(Request $request){
        $this->setProperties($request->type);
        if (!permission($this->_permissions['addArticle']) && permission($this->_permissions['articleEdit']))
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'در حال حاضر مجاز به اینکار نیستید.'];
        try {
            $path = 'files/posts/images/' ;
            $size = 2 ;
            foreach ($request->all() as $key=>$item){
                if($request->hasFile($key)){
                    $res = Helpers::checkImageAjax($request,$key,$path,$size);
                    if($res['res'] == 10) {
                        $file = new File();
                        $file->importer = auth('admin')->check() ? auth('admin')->user()->id : auth()->user()->id;
                        $file->file = $res['fileName'];
                        $file->path = $res['path'];
                        $file->type = 'image';
                        $file->post_id = $request->post_id;
                        $file->save();
                        $res['id'] = $file->id;
                    }
                    return  $res;
                }
            }
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک تصویر انتخاب نمایید'];

        }catch (Exception $exception){
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'عملیات با خطا مواجه گردید'];
        }
    }

    public function saveFileAtt(Request $request){
        $this->setProperties($request->type);
//        dd($_FILES);
        if (!permission($this->_permissions['addArticle']) && permission($this->_permissions['articleEdit']))
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'در حال حاضر مجاز به اینکار نیستید.'];
        try {
            $path = 'files/posts/files/' ;

            foreach ($_FILES as $FILE){
                $res = Helpers::checkFileAjax($FILE,$path,2,['pdf','excel']);
                if($res['res'] == 10) {
                    $file = new File();
                    $file->importer = auth('admin')->check() ? auth('admin')->user()->id : auth()->user()->id;
                    $file->file = $res['fileName'];
                    $file->path = $path;
                    $file->type = $res['mime'];
                    $file->post_id = $request->post_id;
                    $file->save();
                    $res['id'] = $file->id;
                }
                return $res;
            }

            return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک فایل انتخاب نمایید'];

        }catch (Exception $exception){
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'عملیات با خطا مواجه گردید'];
        }
    }

    public function saveAtt(Request $request){
        if(!auth()->check() && !\auth('admin')->check())
            return ['res'=>1,'mySuccess'=>'','myAlert'=>'شما مجاز نیستید'];
        $path = auth('admin')->check() ? 'dist/images/posts/' : 'dist/images/estate/';
        $size = 2 ;
        $res =  Helpers::checkImageAjax($request,'thumb',$path,$size);
        if($res['res'] != 10)
            return $res;

        $file = new File();
        $file->importer = auth('admin')->check() ? auth('admin')->user()->id : auth()->user()->id;
        $file->file = $res['file'];
        $file->save();
        $res['file_id'] = $file->id;
        $rm = url('assets/images/remove.png');
        $res['tags'] = "<div>
                         <a href=\"{$res['url']}\" target=\"_blank\">
                             <img class=\"img_b\" src=\"{$res['url']}\">
                         </a>
                         <img data-div=\"att_20\" class=\"rm_file_att_icon\" src=\"{$rm}\">

                         <input type=\"hidden\" name=\"attach[]\" value=\"{$file->id}\">
                     </div>";

        return $res;
    }

    public function saveProperty(Request $request){
        if(strlen($request->name)<2)
            return ['res'=>1,'myAlert'=>'نام کوتاه است.'];
        $this->setProperties($request->type);
        $options = "";
        $parentLang = 0;
        if($request->group == 'category'){
            if(Category::where([['name',$request->name],['group',$this->_metas['id']],['lang',$request->lang]])->count() < 1){

                if($request->lang != DEFAULT_LANG[0]){
                    $newCatParent = new Category();
                    $newCatParent->name = $request->name;
                    $newCatParent->slug = Category::slugGenerate($request->name);
                    $newCatParent->parent = 0;
                    $newCatParent->parent_lang = 0;
                    $newCatParent->group = $this->_metas['id'];
                    $newCatParent->lang = DEFAULT_LANG[0];
                    $newCatParent->save();
                    $parentLang = $newCatParent->id;
                }
                $newCat = new Category();
                $newCat->name = $request->name;
                $newCat->slug = Category::slugGenerate($request->name);
                $newCat->parent = 0;
                $newCat->parent_lang = $parentLang;
                $newCat->group = $this->_metas['id'];
                $newCat->lang = $request->lang;
                $newCat->save();

                foreach (Category::getCatsByGroup($this->_metas['id'],1) as $cat) {
                    $cc = $cat->parentId==0?$cat->name:'_'.$cat->name;
                    $selected = $cat->id==$newCat->id ? "selected='selected'" : '';
                    $options .= "<option {$selected} value='{$cat->id}'>{$cc}</option>";
                }
                return ['res'=>10,'myAlert'=>'','mySuccess'=>'','options'=>$options];
            }else
                return ['res'=>9];
        } elseif($request->group == 'tag'){
            if(Tag::where([['name',$request->name],['lang',$request->lang]])->count() < 1){
                if($request->lang != DEFAULT_LANG[0]){
                    $newTagParent = new Tag();
                    $newTagParent->name = $request->name;
                    $newTagParent->slug = Tag::slugGenerate($request->name);
                    $newTagParent->parent_lang = 0;
                    $newTagParent->lang = DEFAULT_LANG[0];
                    $newTagParent->save();
                    $parentLang = $newTagParent->id;
                }

                $newTag = new Tag();
                $newTag->name = $request->name;
                $newTag->slug = Tag::slugGenerate($request->name);
                $newTag->parent_lang = $parentLang;
                $newTag->lang = $request->lang;
                $newTag->save();

                foreach (Tag::where('lang',$request->lang)->get() as $tag) {
                    $selected = $tag->id==$newTag->id ? "selected='selected'" : '';
                    $options .= "<option {$selected} value='{$tag->id}'>{$tag->name}</option>";
                }
                return ['res'=>10,'myAlert'=>'','options'=>$options];
            }else
                return ['res'=>9];
        } elseif($request->group == 'brand'){
            if(Brand::where([['name',$request->name],['lang',$request->lang]])->count() < 1){
                if($request->lang != DEFAULT_LANG[0]){
                    $newBrandParent = new Brand();
                    $newBrandParent->name = $request->name;
                    $newBrandParent->slug = Tag::slugGenerate($request->name);
                    $newBrandParent->parent_lang = 0;
                    $newBrandParent->lang = DEFAULT_LANG[0];
                    $newBrandParent->save();
                    $parentLang = $newBrandParent->id;
                }

                $newBrand = new Brand();
                $newBrand->name = $request->name;
                $newBrand->slug = Brand::slugGenerate($request->name);
                $newBrand->parent_lang = $parentLang;
                $newBrand->lang = $request->lang;
                $newBrand->save();

                foreach (Brand::where('lang',$request->lang)->get() as $brand) {
                    $selected = $brand->id==$newBrand->id ? "selected='selected'" : '';
                    $options .= "<option {$selected} value='{$brand->id}'>{$brand->name}</option>";
                }
                return ['res'=>10,'myAlert'=>'','options'=>$options];
            }else
                return ['res'=>9];
        }
        return ['res'=>1,'myAlert'=>'خطای نامعلوم!!'];
    }

    private function discount_get($post_id){
        $plans = S_product::user_plans();
        $result = '';
        foreach(S_discount::where('post_id',$post_id)->orderBy('id','DESC')->get() as $diss) {
            $type = $diss->type == 'fix' ? 'ثابت' : 'درصد';
            $discount = sep($diss->discount);
            $time = $diss->limit_time ? jdate('Y-m-d H:i:m', $diss->start) . '<br>' . jdate('Y-m-d H:i:m', $diss->end) : 'نامحدود';
            $target = '';
            if ($diss->user_id) {
                if ($userrrr = User::where('id', $diss->user_id)->first())
                    $target = $userrrr->name . ' ' . $userrrr->family;
            } elseif ($diss->users) {
                foreach (User::whereIn('id', json_decode($diss->users))->select(['name', 'family'])->get() as $userr)
                    $target .= "<span style = \"border: 1px solid #999;display: inline-block\" >{$userr->name} {$userr->family}</span>";
            } elseif ($diss->user_grade)
                $target = $plans[$diss->user_grade][0];
             else
                $target = "همه کاربران";
             if($diss->status == 'active')
                 $status = "<button data-id='{$diss->id}' class='btn btn-outline-info deactive_discount' >فعال</button>";
             else
                 $status = "<span class='text-danger'>غیرفعال</span>";


            $result .= "<tr>
                            <td>{$type}</td>
                            <td>{$discount}</td>
                            <td style=\"direction: ltr\"> {$time}</td>
                            <td>{$target}</td>
                            <td>{$status}</td>
                        </tr>
                            ";

        }
        return $result;
    }

    public function discount_deactive(Request $request){
        if(secure($request->post) != $request->secure)
            return ['res'=>1,'myAlert'=>'کلید امنیتی نامعتبر است','mySuccess'=>''];
        if(!$discount = S_discount::where('id',$request->id)->first())
            return ['res'=>1,'myAlert'=>'تخفیف یافت نشد!','mySuccess'=>''];
        if($discount->post_id != $request->post)
            return ['res'=>1,'myAlert'=>'تخفیف  مربوط به این پست نیست!','mySuccess'=>''];
        $discount->status = 'deactive';
        $discount->save();
        return ['res'=>10,'myAlert'=>'','mySuccess'=>'تخفیف با موفقیت غیر فعال گردید','tbl'=>$this->discount_get($request->post)];
    }

    public function discount_set(Request $request){

        if(!in_array($request->discount_type,['fix','percent']))
            return ['res'=>1,'message'=>'نوع تخفیف را مشخص نمایید.'];
        if(!is_numeric(fa2la($request->discount_val)))
            return ['res'=>1,'message'=>'میزان تخفیف را وارد نمایید.'];
        if(!in_array($request->discount_target,['all','users','plan']))
            return ['res'=>1,'message'=>'گروه هدف را مشخص نمایید.'];
        if($request->discount_target=='users' && (!isset($request->discount_users) || !is_array($request->discount_users) || count($request->discount_users)<1))
            return ['res'=>1,'message'=>'اعضای هدف را انتخاب نمایید'];
        if($request->discount_target=='plan' && (!isset($request->discount_plan) || !in_array($request->discount_plan,[1,2,3])))
            return ['res'=>1,'message'=>'پلن هدف را انتخاب نمایید'];


        if($request->discount_limit == 'on') {
            $start = $request->discount_start ? $this->change_time($request->discount_start)  : 0;
            $end = $request->discount_end ? $this->change_time($request->discount_end)  : 0;
            if (!$start )
                return ['res' => 1, 'message' => 'تاریخ شروع نامعتبر است'];

            if (!$end )
                return ['res' => 1, 'message' => 'تاریخ پایان نامعتبر است'];
            if($end <= $start)
                return ['res' => 1, 'message' => 'تاریخ پایان باید از تاریخ شروع بزرگتر باشد'];
        }else{
            $start =  0;
            $end =  0;
        }
        $dis = new S_discount();
        $dis->admin_id = auth('admin')->user()->id;
        $dis->type = $request->discount_type;
        $dis->discount = fa2la($request->discount_val);
        $dis->status = 'active';
        $dis->limit_time = $request->discount_limit == 'on' ? 1 : 0;
        $dis->start = $start;
        $dis->end = $end;
        if($request->discount_target=='users'){
            if(count($request->discount_users)==1)
                $dis->user_id = $request->discount_users[0];
            else
                $dis->users = json_encode($request->discount_users);
        } else if($request->discount_target=='plan'){
            $dis->user_grade = $request->discount_plan;
        }

        $dis->post_id = $request->id;
        $dis->save();

        return ['res' => 10, 'message' => 'تغییرات با موفقیت ذخیره گردید','tbl'=>$this->discount_get($request->id)];
    }

    public function stock_set(Request $request){
        if(!is_numeric(fa2la($request->this_stock)))
            return ['res'=>1,'message'=>'موجودی باید یک عدد معتبر باشد'];
        $new_props = [];
        foreach ($request->all() as $k=>$item){
            if(substr($k,0,12) == 'stock_props_' && $item!=0)
                $new_props[]=$item;
        }
        if($new_props == [])
            return ['res'=>1,'message'=>'انتخاب حداقل یک ویژگی الزامیست'];



//        dd($request->all(),$new_props);
        $not_duplicate = true;
       foreach (S_stock::where('post_id',$request->id)->get() as $item){
           $old_props = json_decode($item->props,1);
           $ok = true;
           foreach ($new_props as $new_prop){
               if(!in_array($new_prop,$old_props))
                   $ok = false;
           }
           if($ok){
               $item->stock = fa2la($request->this_stock);
               $item->desc = $request->this_stock_desc;
               $item->real_price = fa2la($request->var_real_price);
               $item->price1 = fa2la($request->var_price1);
               $item->price2 = fa2la($request->var_price2);
               $item->price3 = fa2la($request->var_price3);
               $item->save();
               $not_duplicate = false;
           }

       }
       if($not_duplicate){
           $st = new S_stock();
           $st->post_id = $request->id;
           $st->stock = fa2la($request->this_stock);
           $st->desc = $request->this_stock_desc;
           $st->props = json_encode($new_props);
           $st->importer = auth('admin')->user()->id;
           $st->real_price = fa2la($request->var_real_price);
           $st->price1 = fa2la($request->var_price1);
           $st->price2 = fa2la($request->var_price2);
           $st->price3 = fa2la($request->var_price3);
           $st->save();
       }
        return ['res' => 10, 'message' => 'تغییرات با موفقیت ذخیره گردید','tbl'=>$this->stock_get($request->id)];
    }

    private function stock_get($post_id){

        $result = '';
        foreach(S_stock::where('post_id',$post_id)->orderBy('id','DESC')->get() as $stock) {
//            dd(json_decode($stock,1));
            $props = '';
            foreach (json_decode($stock->props,1) as $item){
                $item = explode('_',$item);
                if(isset($item[1]) ){
                    $p_key = S_prop::where('id',$item[0])->first();
                    $p_val = S_prop_value::where('id',$item[1])->first();
                    if($p_key && $p_val ){
//                        dd($p_key,$p_val,$item);
                        $props.= $p_key->title .' = '.$p_val->value.'<br>';
                    }
                }
            }
            $props = trim($props,'<br>');
            $status = "<button type=\"button\" class=\"btn btn-outline-danger rm_stock\" data-id=\"{$stock->id}\">حذف</button>";
            $result .= "<tr>
                            <td>{$props}</td>
                            <td>{$stock->stock}</td>
                            <td>".sep($stock->real_price)."</td>"."
                            <td>
                                <p>".sep($stock->price1)."</p>"."
                                <p>".sep($stock->price2)."</p>"."
                                <p>".sep($stock->price3)."</p>"."

                            </td>
                            <td>{$stock->desc}</td>
                            <td>{$status}</td>
                        </tr>
                            ";

        }
        return $result;
    }

    public function stock_remove(Request $request){
            if(secure($request->post,'mobile') != $request->secure)
                return ['res'=>1,'myAlert'=>'کلید امنیتی نامعتبر است','mySuccess'=>''];
            if(!$stock = S_stock::where('id',$request->id)->first())
                return ['res'=>1,'myAlert'=>'موجودی یافت نشد!','mySuccess'=>''];
            if($stock->post_id != $request->post)
                return ['res'=>1,'myAlert'=>'موجودی  مربوط به این پست نیست!','mySuccess'=>''];
            S_stock::destroy($stock->id);
            return ['res'=>10,'myAlert'=>'','mySuccess'=>'حذف با موفقیت انجام شد','tbl'=>$this->stock_get($request->post)];
        }

    private function change_time($time){
        $time = explode(' ',fa2la($time));
//        dd($time);
        if(!isset($time[0]))
            return false;
        $date = explode('/',$time[0]);
//        dd($date);
        if(!isset($date[2]))
            return false;
        $time = isset($time[1]) ? explode(':',$time[1]) : [0,0,0];
        $time = isset($time[2]) ? ($time[2] + ($time[1]*60) + ($time[0]*3600)) : 0;
        return strtotime(jalali_to_gregorian($date[0],$date[1],$date[2],'-')) + $time;

    }

    private function tbl($posts,$removeCatch=0){
        $result = '';
        $multiLang = count(LANGUAGES) > 0;

        $type = $this->_metas['type'];
        foreach ($posts as $k=>$post){

            $category = $post->cat->name ?? '';


            if($post->status == 'draft'){
                $activeCode = "پیش نویس";
            }else {
                if (permission($this->_permissions['articleChangeStatus'])) {
                    $activeCode = "<span data-placement=\"top\" data-titre='فعال' data-new='publish'  data-toggle=\"tooltip\" title='برای فعال کردن کلیک کنید' class='btn activated btn-outline-danger btn-border-radius'>غیرفعال</span>";
                    if ($post->status == 'publish')
                        $activeCode = "<span data-placement=\"top\" data-titre='غیر فعال' data-new='unpublish'  data-toggle=\"tooltip\" title='برای غیرفعال کردن کلیک کنید' class='btn activated btn-outline-success btn-border-radius'>فعال</span>";
                } else {
                    $activeCode = $post->status == 'publish' ? '<b style="color:green">فعال</b>' : '<b style="color: red;">غیرفعال</b>';
                }
            }


            $image = is_file($post->path.$post->image) ?
                "<img class='thumb' data-source='".url($post->path.$post->image)."' src='".url($post->path.'64_64_'.$post->image)."' />" :
                '---';

            $hrefEdit = route($this->_metas['baseRout'].'_edit',[$this->_metas['type'],$post->id]);
            $r = $k+1;
            $userId=Auth::guard('admin')->user()->id;
            $thisLang = explode(',',$post->translates);
            $showNews = AB_route_post($type,$post->id,$post->slug);
            $result .=<<<EOS
            <tr data-id="{$post->id}" data-title="{$post->title}" id="tr_{$post->id}">
                <td class="center-align">{$r}</td>
                <td>{$post->title}</td>
                <td class="center-align">{$category}</td>
                <td class="center-align actShow">{$activeCode}</td>
EOS;
            if($multiLang){
                $result .="<td class='lang'>";
                foreach (LANGUAGES as $ll){
                    if(in_array($ll[0],$thisLang)){
                        $result .= "<a href='".route('p_translate_add',[$type,$post->id,$ll[0],Post::secure([$type,$post->id,$ll[0]])])."' class='btn btn-primary'>".$ll[0].'</a>';
                    }else{
                        $result .= "<a href='".route('p_translate_add',[$type,$post->id,$ll[0],Post::secure([$type,$post->id,$ll[0]])])."' class='btn btn-info'>".$ll[0].'</a> ';
                        $result .= " <a class='text-warning' href='".route('p_translate_byGoogle',[$post->id,$ll[0],$type,Post::secure([$post->id,$ll[0],$type])])."'>ترجمه با گوگل</a>";
                    }
                    $result.="<hr>";
                }

                $result .= "</td>";

            }else
                $result .= "<td class='center-align'>{$post->date}</td>";
            $result .=<<<EOS
                <td>{$image}</td>
                <td class="actions">
                    <a data-placement="top" target="_blank" data-toggle="tooltip" href="{$showNews}" title="نمایش در سایت"  class="btn btn-success">
                         <i class="icon-eye"></i>
                    </a>
EOS;
            if(permission($this->_permissions['articleEdit']) || ($post->author == $userId && permission($this->_permissions['myArticleEdit'])))
                $result .=<<<EOS
                    <a data-placement="top"  data-toggle="tooltip" target="_blank" href="{$hrefEdit}" title="ویرایش "  class="btn btn-info">
                       <i class="icon-pencil"></i>
                    </a>
EOS;
            if( permission($this->_permissions['articleRemove']) || ($post->author == $userId && permission($this->_permissions['myArticleRemove'])))
                $result .=<<<EOS
                    <a data-placement="top"  data-toggle="tooltip" title="حذف "  class="btn btn-danger">
                       <i class="icon-trash"></i>
                    </a>
EOS;



            $result .= <<<EOS
                </td>
            </tr>
EOS;

        }

        return $result;
    }
}
