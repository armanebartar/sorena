<?php

namespace App\Http\Controllers\admin\posts;

use App\Helpers\Helpers;
use App\Models\Tag;
use App\Models\Tag_option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagsController extends Controller
{
    public function index(){
        if(forbidden('tags'))
            return redirect()->route('p_dashboard');
        $this->setProperties('news');
        addLog('ورود به صفحه برچسب‌ها');
        $data['tbl']=$this->tbl(Tag::getAll(1));
        $data['meta'] = $this->_metas;
        return view('admin.posts.tags',$data);
    }

    public function addSave(Request $request){
        $this->setProperties('news');

        if (!isset($request->name) || strlen($request->name) < 2) {
            return ['res' => 1, 'myAlert' => 'نام برچسب کوتاه است','mySuccess'=>''];
        }

        $slug = isset($request->slug) && strlen($request->slug) > 2 ? $request->slug : $request->name;
        $slug = Tag::slugGenerate($slug,$request->id??0);
        if(!$request->id || !$tag = Tag::where('id',$request->id)->first()) {
            if(!permission('addTag',1))
                return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!','mySuccess'=>''];
            $tag = new  Tag();
            $tag->lang = DEFAULT_LANG[0];
            addLog('افزودن  برچسب', $request->all());
        }else{
            if(!permission('editTag',1))
                return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!','mySuccess'=>''];
            addLog('ویرایش  برچسب', $request->all());
        }
        $tag->name = $request->name;
        $tag->slug = $slug;

        $tag->save();


        return ['res'=>10,'myAlert' => '','mySuccess'=>'عملیات با موفقیت انجام شد','tbl'=>$this->tbl(Tag::getAll())];
    }

    public function remove(Request $request){
        $tbl = '';
        if(!permission('removeTag',1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!','mySuccess'=>''];


        if ($request->id > 0 && Tag::where('id',$request->input('id'))->count() > 0) {
            Tag::destroy($request->input('id'));
            addLog('حذف برچسب',$request->all());
            return ['res'=>10,'myAlert' => '','mySuccess'=>'عملیات با موفقیت انجام شد','tbl'=>$this->tbl(Tag::getAll())];

        }
        return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید','mySuccess'=>''];
    }

    public function addLang(Request $request){

        if(!permission('addLangTag',1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];

        if($cat=Tag::where('id',$request->input('id'))->first()){

            if(strlen($request->name) < 2){
                return ['res' => 1, 'myAlert' => 'نام الزامیست و باید حداقل دو کارکتر داشته باشد.', 'mySuccess' => ''];
            }
//            addLog('ویرایش ترجمه برچسب'.$request->all());

            if(!$lang = Tag::where([['parent_lang',$cat->id],['lang',$request->input('lang')]])->first()){
                $lang = new Tag();
                $lang->parent_lang = $cat->id;
                $lang->lang = $request->input('lang');

            }
            $lang->name = $request->input('name');
            $slug = isset($request->slug) && strlen($request->slug) > 2 ? $request->slug : $request->input('name');
            $slug = Tag::slugGenerate($slug,$request->input('id'));

            $lang->slug = $slug;
            $lang->save();

            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'ویرایش ترجمه با موفقیت انجام گردید.'];
        }else
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید', 'mySuccess' => ''];


    }

    public function edit_lang($id,$lang){

        if(!isset(LANGUAGES[$lang])){
            session(['alert-danger'=>'ترجمه مورد نظر در دسترس نیست']);
            return back();
        }
        if (!$tag = Tag::with(['options'])->where('id', $id)->first()){
            session(['alert-danger'=>'دسته مورد نظر یافت نشد']);
            return back();
        }
        if(!permission('addLangTag')){
            session(['alert-danger'=>'شما مجاز به انجام این عملیات نیستید']);
            return back();
        }

        if(!$trans = Tag::where([['parent_lang',$tag->id],['lang',$lang]])->first()){
            $trans = new Tag();
            $trans->parent_lang = $tag->id;
            $trans->lang = $lang;
            $trans->name = $tag->name;
            $trans->slug = $tag->slug;
            $trans->image = $tag->image;
            $trans->path = $tag->path;
            $trans->save();
        }
        return $this->edit_complete($trans->id);
    }

    public function edit_complete($id=0){

        if (!$tag = Tag::with(['options'])->where('id', $id)->first()){
            session(['alert-danger'=>'برچسب مورد نظر یافت نشد']);
            return back();
        }

        $this->setProperties('news');
        if(!permission('editTag')){
            session(['alert-danger'=>'شما مجاز به انجام این عملیات نیستید']);
            return back();
        }

        $data['tag'] = Tag::get_by_options($tag->id,$tag);
        return view('admin.posts.tag_edit',$data);
    }

    public function edit_complete_save(Request $request){

        if (!$tag = Tag::with(['options'])->where('id', $request->id)->first()){
            return ['res' => 1, 'myAlert' => 'برچسب یافت نشد', 'mySuccess' => ''];
        }

        $this->setProperties('news');
        if(!permission('editTag'))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        if(strlen($request->name) < 5)
            return ['res' => 1, 'myAlert' => 'عنوان دسته خیلی کوتاه است', 'mySuccess' => ''];
        $tag->name = $request->name;
        $tag->slug = strlen($request->slug)>3 ? Tag::slugGenerate($request->slug) : Tag::slugGenerate($request->name);
        $tag->image = $request->image;
        $tag->path = $request->path;
        $tag->save();
        $options = ['minContent','desc','seo_title','seo_desc','seo_keyword'];
        Tag_option::where('tag_id',$tag->id)->delete();
        foreach ($options as $option){
            $opt = new Tag_option();
            $opt->tag_id = $tag->id;
            $opt->key = $option;
            $opt->value = $request->{$option} ?? '';
            $opt->save();
        }
        return ['res' => 10, 'mySuccess' => 'تغییرات با موفقیت ذخیره گردید', 'myAlert' => ''];
    }

    public function saveThumb(Request $request){
        $this->setProperties('news');
        if(!permission('editTag'))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        $path =  'files/tags/' ;
        $size = 2 ;
        foreach ($request->all() as $key=>$item){
            if($request->hasFile($key)){
                return Helpers::checkImageAjax($request,$key,$path,$size);
            }
        }
        return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک تصویر انتخاب نمایید'];
    }

    private function tbl($tags){
        $result = '';

        $multiLang = count(LANGUAGES) > 0;
        $r=0;
        foreach ($tags as $k=>$tag){
            $r++;
            $result .=<<<EOS
            <tr>
                <td class="center-align">{$r}</td>
                <td>{$tag->name}</td>
EOS;
            if(permission('addLangTag') && $multiLang){
                $langes = (array)$tag->langs;
                $result .="<td class='lang'>";
                foreach (LANGUAGES as $ll){
                    $class =  'btn-info';
                    $lang_title = '';
                    $lang_slug = '';
                    if(array_key_exists($ll[0],$langes)){
                        $class =  'btn-primary';
                        $lang_title = $langes[$ll[0]][0];
                        $lang_slug = $langes[$ll[0]][1];
                    }

                    $result .= "<button type='button' data-title='{$tag->name}' data-slug='{$tag->slug}' data-title2='{$lang_title}' data-slug2='{$lang_slug}' data-lang='".$ll[0]."' data-id='{$tag->id}'  class='btn {$class}'>".$ll[0].'</button>';
//                $result.="<hr>";
                }

                $result .= "</td>";

            }else
                $result .= "<td>{$tag->slug}</td>";

            $result .= <<<EOS
                <td class="actions" data-id="{$tag->id}" data-name="{$tag->name}" data-slug="{$tag->slug}">
                    <a data-toggle="tooltip" data-placement="top" href="#" title="نمایش برچسب در سایت" type="button" class="btn btn-success ">
                       <i class="icon-eye"></i>
                    </a>
EOS;
            if(permission('editTag'))
                $result .= <<<EOS
                    <a data-toggle="tooltip" data-placement="top" href="#" title="ویرایش برچسب" type="button" class="btn btn-info edited">
                         <i class="icon-pencil"></i>
                    </a>
EOS;
            if(permission('removeTag'))
                $result .= <<<EOS
                    <a data-toggle="tooltip" data-placement="top" title="حذف برچسب" type="button" class="btn btn-danger removed ">
                         <i class="icon-trash"></i>
                    </a>
EOS;
            $result .= <<<EOS
                </td>
            </tr>
EOS;

        }

        return $result;
    }

}
