<?php

namespace App\Http\Controllers\admin\posts;

use App\Helpers\Helpers;
use App\Models\Category;
use App\Models\Post;
use App\Models\Special;
use App\Models\Special_post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class SpecialController extends Controller
{
    private $_pathImage = 'files/special/';

    public function index($type,$id,$secure){
        if(secure($type.$id) != $secure)
            return back();
        $this->setProperties($type);
        if(!$special = Special::with(['posts','type'])->where([['id',$id],['post_type',$type]])->first()){
            session(['alert-danger'=>'مطلب مورد نظر یافت نشد.']);
            return back();
        }

        addLog('مشاهده  '.$special->name.' '.$this->_metas['title']);
        $data['tbl'] = $this->tbl(Special_post::getAll($special->id));
        $data['meta'] = $this->_metas;
        $data['special'] = $special;

        return view('admin.posts.specials',$data);
    }

    public function saveGroup(Request $request){
        $this->setProperties($request->type);
        if(!$sp = Special::where('id',$request->id)->first())
            return ['res'=>1,'myAlert'=>'عملیات با خطا مواجه گردید','mySuccess'=>''];
        $sp->name = $request->name;
        $sp->link = $request->link;
        $path = $this->_pathImage;
        $sp->path =  $path;
        $image_sizes = [];
        foreach (explode('**',$sp->image_sizes) as $val)
            $image_sizes[]=explode('*',$val);
        $image_sizes = count($image_sizes)>0 ? $image_sizes : $this->_metas['image_sizes'];
        if($request->hasFile('image')) {
            $image = Helpers::checkAndSaveImage($request, 'image', $path, $image_sizes, $this->_metas['image_maxValue']);

            if (is_file($path. $image)) {
                $sp->image =  $image;
            }
        }
        $sp->save();
        return ['res'=>10,'myAlert'=>'','mySuccess'=>'تغییرات با موفقیت ذخیره گردید'];
    }
    public function posts($type){
        $this->setProperties($type);
        $result = [];
        foreach (Post::where([['type',$type],['parent',0]])->get() as $value)
            $result [] = ['id'=>$value->id, 'value'=>$value->title, 'name'=>$value->title,];

        return $result;
    }

    public function addSaveGroup(Request $request,$type){
        $this->setProperties($type);
        if(!permission($this->_permissions['special'],1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];

        if(!$sp = Special::where('id',$request->special)->first())
            return ['res' => 1, 'myAlert' => 'گروه مورد نظر یافت نشد !', 'mySuccess' => ''];





        if(!isset($request->name) || strlen($request->name) < 2 ) {
            return ['res'=>1,'myAlert'=>'عنوان وارد شده نامعتبر است.','mySuccess'=>''];
        }


        $image_sizes = [];
        foreach (explode('**',$sp->image_sizes) as $val)
            $image_sizes[]=explode('*',$val);
        $image_sizes = count($image_sizes)>0 ? $image_sizes : $this->_metas['image_sizes'];
        $path = $this->_pathImage;
        $mode = 'edit';
        $message_log = 'ویرایش پست  جدید برای '.$sp->name;
        $message = 'تغییرات با موفقیت ذخیره شدند.';
        if(!$request->id || !$spec = Special_post::where('id',$request->id)->first()) {
            if(!$post = Post::where('id',$request->post_id)->first())
                return ['res' => 1, 'myAlert' => 'پست انتخابی نامعتبر است.', 'mySuccess' => ''];
            if(Special_post::where([['post_id',$request->post_id],['special_id',$sp->id]])->count() > 0) {
                return ['res'=>1,'myAlert'=>'این پست قبلا به لیست اضافه شده است.','mySuccess'=>''];
            }
            if($post->type != $type)
                return ['res' => 1, 'myAlert' => 'پست انتخابی در این دسته نیست !!', 'mySuccess' => ''];
            $spec = new  Special_post();
            $spec->post_id = $request->post_id;
            $spec->special_id = $sp->id;
            $mode = 'add';
            $message_log = 'ذخیره پست  جدید برای '.$sp->name;
            $message = 'پست جدید با موفقیت به لیست افزوده شد.';
        }

        $spec->title = $request->name;
        $spec->path = $path;
        if($request->hasFile('image')) {
            $image = Helpers::checkAndSaveImage($request, 'image', $path, $image_sizes, $this->_metas['image_maxValue']);

            if ($mode=='add' && !is_file($path. $image)) {
                return ['res' => 1, 'myAlert' => 'تصویر انتخابی نامعتبر است.', 'mySuccess' => ''];
            }
            $spec->image =  $image;

        }elseif($mode == 'add'){
            copy($post->path.$post->image, $path.$post->image);
            $spec->image =  $post->image;

            foreach ($image_sizes as $size){
                Image::make($path.$post->image)->resize($size[0], $size[1])->save($path . $size[0] . '_' . $size[1] . '_' . $post->image);
            }
        }

        $spec->save();
        $tbl = $this->tbl(Special_post::getAll($sp->id));

        addLog($message_log,$request->all());

        return ['res' => 10, 'myAlert' => '', 'mySuccess' => $message,'tbl'=>$tbl];
    }

    public function removeGroup(Request $request,$type){
        $this->setProperties($type);
        if(forbidden($this->_permissions['special'],1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        if(Post::secure([$request->id,$request->sp]) != $request->secure)
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید.', 'mySuccess' => ''];

        if(!$sp = Special::where('id',$request->sp)->first())
            return ['res' => 1, 'myAlert' => 'گروه مورد نظر یافت نشد !', 'mySuccess' => ''];
        if(!$cat=Special_post::where('id',$request->input('id'))->first())
            return ['res' => 1, 'myAlert' => 'گروه مورد نظر یافت نشد !', 'mySuccess' => ''];
        $image_sizes = [];
        foreach (explode('**',$sp->image_sizes) as $val)
            $image_sizes[]=explode('*',$val);

        $path = $this->_pathImage;
        $image_sizes = count($image_sizes)>0 ? $image_sizes : $this->_metas['image_sizes'];
        if(is_file($cat->path.$cat->image))
            unlink($cat->path.$cat->image);
        foreach ($image_sizes as $size){
            if(is_file($cat->path . $size[0] . '_' . $size[1] . '_' . $cat->image))
                unlink($cat->path . $size[0] . '_' . $size[1] . '_' . $cat->image);
        }
        addLog('حذف پست ویژه '.$this->_metas['nameSingle'],['id'=>$cat->id,'name'=>$cat->title]);
        Special_post::destroy($request->input('id'));
        Special_post::where('parent',$request->input('id'))->delete();
        $tbl = $this->tbl(Special_post::getAll($sp->id));
        return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'حذف گروه با موفقیت انجام گردید.','tbl'=>$tbl];


    }



    public function addLang(Request $request,$type){

        $this->setProperties($type);

        if(!permission($this->_permissions['special'],1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        if(Post::secure([$request->id,$request->lang,$request->sp]) != $request->secure)
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید.', 'mySuccess' => ''];

        if(!$sp = Special::where('id',$request->sp)->first())
            return ['res' => 1, 'myAlert' => 'گروه مورد نظر یافت نشد !', 'mySuccess' => ''];
        if(strlen($request->name) < 2)
            return ['res' => 1, 'myAlert' => 'عنوان الزامیست و باید حداقل ۲ کارکتر داشته باشد.', 'mySuccess' => ''];


        if($cat=Special_post::where('id',$request->input('id'))->first()){

            addLog('ویرایش ترجمه '.$this->_metas['nameSingle'],$request->all());

            if(!$lang = Special_post::where([['parent',$cat->id],['lang',$request->input('lang')]])->first()){
                $lang = new Special_post();
                $lang->special_id = $cat->special_id;
                $lang->post_id = $cat->post_id;
                $lang->lang = $request->input('lang');
                $lang->image = $cat->image;
                $lang->path = $cat->path;
                $lang->parent = $cat->id;

            }
            $lang->title = $request->input('name');
//            dd($request->all());

            if($request->hasFile('image')){

                $image_sizes = [];
                foreach (explode('**',$sp->image_sizes) as $val)
                    $image_sizes[]=explode('*',$val);

                $image = Helpers::checkAndSaveImage($request, 'image', $cat->path, $image_sizes, $this->_metas['image_maxValue']);
//                dd(url($cat->path.'/'.$image));
                if(is_file($cat->path.$image) ) {
                    $lang->image = $image;
                }
            }


            $lang->save();

            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'ویرایش ترجمه با موفقیت انجام گردید.'];
        }else
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید', 'mySuccess' => ''];


    }


    private function tbl($posts){

        $result = '';
        $multiLang = count(LANGUAGES) > 0;
        $r=0;
        if($posts)
        foreach ($posts as $k=>$post){
//            if($post->parent > 0)
//                continue;
            $r++;
            $image = '-';
            if(is_file(ROOT.$post->path.$post->image))
                $image = "<img  src='".url($post->path.$post->image)."' style='max-width:100%'>";
            $result .=<<<EOS
            <tr>
                <td class="center-align">{$r}</td>
                <td>{$post->title}</td>
EOS;
            if(permission($this->_permissions['special']) && $multiLang){
                $langes = (array)$post->langs;
                $result .="<td class='lang'>";
                foreach (LANGUAGES as $ll){
                    $class =  'btn-info';
                    $lang_title = '';
                    if(array_key_exists($ll[0],$langes)){
                        $class =  'btn-primary';
                        $lang_title = $langes[$ll[0]][0];
                    }

                    $sec = Post::secure([$post->id,$ll[0],$post->special_id]);
                    $result .= "<button type='button' data-sp='{$post->special_id}' data-secure='{$sec}' data-title='{$post->title}' data-title2='{$lang_title}'  data-lang='".$ll[0]."' data-id='{$post->id}'  class='btn {$class}'>".$ll[0].'</button>';
//                $result.="<hr>";
                }

                $result .= "</td>";

            }

            $secure = Post::secure([$post->id,$post->special_id]);
            $result .= <<<EOS
                <td>{$image}</td>
                <td class="actions" data-sp="{$post->special_id}" data-id="{$post->id}" data-secure="{$secure}" data-name="{$post->title}">
                    <a data-toggle="tooltip" data-placement="top" href="#" title="نمایش  در سایت" type="button" class="btn btn-success ">
                        <i class="material-icons"> remove_red_eye</i>
                    </a>
EOS;
            if(permission($this->_permissions['special']))
                $result .= <<<EOS
                    <a data-toggle="tooltip" data-placement="top" href="#" title="ویرایش " type="button" class="btn btn-info edited">
                        <i class="material-icons">create</i>
                    </a>
EOS;
            if(permission($this->_permissions['special']))
                $result .= <<<EOS
                    <a data-toggle="tooltip" data-placement="top" title="حذف " type="button" class="btn btn-danger removed ">
                        <i class="material-icons"> clear</i>
                    </a>
EOS;
            $result .= <<<EOS
                </td>
            </tr>
EOS;

        }

        return $result;

    }

}
