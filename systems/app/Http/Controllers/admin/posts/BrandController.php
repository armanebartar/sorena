<?php

namespace App\Http\Controllers\admin\posts;

use App\Models\Brand;
use App\Models\Brand_option;
use App\Helpers\Helpers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class BrandController extends Controller
{

    public function index($type){
        $this->setProperties($type);

        addLog('مشاهده برندهای '.$this->_metas['title']);
        $data['tbl'] = $this->tbl();
        $data['meta'] = $this->_metas;

//        dd($data);
        return view('admin.posts.brands',$data);
    }

    public function addSave(Request $request,$type){
        $this->setProperties($type);
        if(!isset($request->name) || strlen($request->name) < 2 ) {
            addLog('ذخیره ناموفق برند '.$this->_metas['nameSingle'].' جدید',$request->all(),'نام وجود ندارد یا کوتاه است');
            return ['res'=>1,'myAlert'=>'عنوان وارد شده نامعتبر است.','mySuccess'=>''];
        }

        $slug = isset($request->slug) && strlen($request->slug) > 2 ? $request->slug : $request->name;
        $slug = Brand::slugGenerate($slug,$request->id ?? 0);
        if(!$request->id || !$brand = Brand::where([['id',$request->id],['group',$this->_metas['id']]])->first()) {
            if(!permission($this->_permissions['addBrand']))
                return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
            $brand = new  Brand();
            $titreLog = 'ذخیره برند '.$this->_metas['nameSingle'].' جدید';
            $message = 'برند جدید با موفقیت ذخیره گردید.';

            $brand->lang = DEFAULT_LANG[0];

        }else{
            if(!permission($this->_permissions['editBrand']))
                return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
            $titreLog = 'ویرایش برند '.$this->_metas['nameSingle'];
            $message = 'تغییرات با موفقیت ذخیره گردید.';

        }

        $brand->name = $request->name;
        $brand->slug = $slug;
        $brand->path = Brand::imagePath('/');
        if($request->hasFile('image')) {
            $image = Helpers::checkAndSaveImage($request, 'image', Brand::imagePath('/'), $this->_metas['image_sizes_brand'], Brand::imageMaxSize());
            if (!is_file(Brand::imagePath('/') . $image)) {
                return ['res' => 1, 'myAlert' => 'تصویر انتخابی نامعتبر است.', 'mySuccess' => ''];
            }else {
                $brand->image =  $image;

            }

        }

        $brand->group = $this->_metas['id'];
        $brand->save();
        $tbl = $this->tbl();
        Cache::forget('BrandPostByGroup'.$this->_metas['id']);
        addLog($titreLog,$request->all());

        return ['res' => 10, 'myAlert' => '', 'mySuccess' => $message,'tbl'=>$tbl];
    }

    public function remove(Request $request,$type){
        $this->setProperties($type);
        if(!permission($this->_permissions['removeBrand'],1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];

        if($firstGroup = Brand::where('group',$this->_metas['id'])->first())
            if($firstGroup->id == $request->input('id'))
                return ['res' => 1, 'myAlert' => 'این برند اصلی است و قابل حذف نیست.', 'mySuccess' => ''];

        if($brand=Brand::where('id',$request->input('id'))->first()){
            if($brand->group != $this->_metas['id']){
                return ['res' => 1, 'myAlert' => 'برند مربوط به این دسته نمی‌باشد.', 'mySuccess' => ''];
            }

            addLog('حذف برند '.$this->_metas['nameSingle'],['id'=>$brand->id,'name'=>$brand->name]);
            if(is_file(ROOT.$brand->path.'/'.$brand->image)){
                unlink(ROOT.$brand->path.'/'.$brand->image);
                foreach ($this->_metas['image_sizes_brand'] as $meta){
                    if(isset($meta[0]) && is_file($brand->path.'/'.$meta[0].'_'.$meta[1].'_'.$brand->image))
                        unlink($brand->path.'/'.$meta[0].'_'.$meta[1].'_'.$brand->image);
                }
            }
            Brand::destroy($request->input('id'));

            Cache::forget('BrandPostByGroup'.$this->_metas['id']);

            $tbl = $this->tbl();
            return ['res' => 10,
                'myAlert' => '',
                'mySuccess' => 'حذف برند با موفقیت انجام گردید.',
                'tbl'=>$tbl
            ];
        }else{
            addLog('حذف ناموفق برند خبری',$request->all(),'آیدی برند نامعتبر است یا برابر یک است');
            return ['res' => 1,
                'myAlert' => 'عملیات با خطا مواجه گردید.',
                'mySuccess' => ''
            ];
        }

    }

    public function addLang(Request $request,$type){
        $this->setProperties($type);
        if(!permission('addLangBrand',1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];

        if($brand=Brand::where('id',$request->input('id'))->first()){
            if($brand->group != $this->_metas['id']){
                return ['res' => 1, 'myAlert' => 'برند مربوط به این دسته نمی‌باشد.', 'mySuccess' => ''];
            }
            if(strlen($request->name) < 2){
                return ['res' => 1, 'myAlert' => 'نام الزامیست و باید حداقل دو کارکتر داشته باشد.', 'mySuccess' => ''];
            }
            addLog('ویرایش ترجمه برند '.$this->_metas['nameSingle'],$request->all());

            if(!$lang = Brand::where([['parent_lang',$brand->id],['lang',$request->input('lang')]])->first()){
                $lang = new Brand();
                $lang->group = $brand->group;
                $lang->parent_lang = $brand->id;
                $lang->lang = $request->input('lang');

            }
            $lang->name = $request->input('name');
            $slug = isset($request->slug) && strlen($request->slug) > 2 ? $request->slug : $request->input('name');
            $slug = Brand::slugGenerate($slug,$request->input('id'));

            $lang->slug = $slug;
            $lang->save();

            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'ویرایش ترجمه با موفقیت انجام گردید.'];
        }else
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید', 'mySuccess' => ''];


    }

    public function edit_lang($id,$lang){

        if(!isset(LANGUAGES[$lang])){
            session(['alert-danger'=>'ترجمه مورد نظر در دسترس نیست']);
            return back();
        }
        if (!$brand = Brand::with(['type','options'])->where('id', $id)->first()){
            session(['alert-danger'=>'برند مورد نظر یافت نشد']);
            return back();
        }
        if(!is_object($brand->type)){
            session(['alert-danger'=>'برند مورد نظر نا مشخص است']);
            return back();
        }
        $this->setProperties($brand->type->type);
        if(!permission($this->_permissions['addLangBrand'])){
            session(['alert-danger'=>'شما مجاز به انجام این عملیات نیستید']);
            return back();
        }

        if(!$trans = Brand::where([['parent_lang',$brand->id],['lang',$lang]])->first()){
            $trans = new Brand();
            $trans->parent_lang = $brand->id;
            $trans->lang = $lang;
            $trans->name = $brand->name;
            $trans->slug = $brand->slug;

            $trans->group = $brand->group;
            $trans->image = $brand->image;
            $trans->path = $brand->path;
            $trans->save();
        }
        return $this->edit_complete($trans->id);
    }

    public function edit_complete($id=0){

        if (!$brand = Brand::with(['type','options'])->where('id', $id)->first()){
            session(['alert-danger'=>'برند مورد نظر یافت نشد']);
            return back();
        }
        if(!is_object($brand->type)){
            session(['alert-danger'=>'برند مورد نظر نا مشخص است']);
            return back();
        }
        $this->setProperties($brand->type->type);
        if(!permission($this->_permissions['editBrand'])){
            session(['alert-danger'=>'شما مجاز به انجام این عملیات نیستید']);
            return back();
        }

        $data['brand'] = Brand::get_by_options($brand->id,$brand);
        return view('admin.posts.brand_edit',$data);
    }

    public function edit_complete_save(Request $request){

        if (!$brand = Brand::with(['type','options'])->where('id', $request->id)->first()){
            return ['res' => 1, 'myAlert' => 'برند یافت نشد', 'mySuccess' => ''];
        }
        if(!is_object($brand->type)){
            return ['res' => 1, 'myAlert' => 'برند مورد نظر نا مشخص است', 'mySuccess' => ''];

        }
        $this->setProperties($brand->type->type);
        if(!permission($this->_permissions['editBrand']))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        if(strlen($request->name) < 5)
            return ['res' => 1, 'myAlert' => 'عنوان برند خیلی کوتاه است', 'mySuccess' => ''];
        $brand->name = $request->name;
        $brand->slug = strlen($request->slug)>3 ? Brand::slugGenerate($request->slug) : Brand::slugGenerate($request->name);
        $brand->image = $request->image;
        $brand->path = $request->path;
        $brand->save();
        $options = ['minContent','desc','seo_title','seo_desc','seo_keyword'];
        Brand_option::where('brand_id',$brand->id)->delete();
        foreach ($options as $option){
            $opt = new Brand_option();
            $opt->brand_id = $brand->id;
            $opt->key = $option;
            $opt->value = $request->{$option} ?? '';
            $opt->save();
        }
        return ['res' => 10, 'mySuccess' => 'تغییرات با موفقیت ذخیره گردید', 'myAlert' => ''];
    }

    public function saveThumb(Request $request){
        $this->setProperties($request->type);
        if(!permission($this->_permissions['editBrand']))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        $path =  Brand::imagePath('/') ;
        $size = 2 ;
        foreach ($request->all() as $key=>$item){
            if($request->hasFile($key)){
                return Helpers::checkImageAjax($request,$key,$path,$size);
            }
        }
        return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک تصویر انتخاب نمایید'];
    }

    private function tbl(){
        $brands = Brand::getBrandsByGroup($this->_metas['id'],1);
        $result = '';
        $r=0;
        foreach ($brands as $k => $brand) {
            $result .= $this->make_row_brand($brand,++$r);
        }


        return $result;
    }

    private function make_row_brand($brand,$r){
        $imageSize = $this->_metas['image_sizes_brand'][0][0].'_'.$this->_metas['image_sizes_brand'][0][1].'_';

        $image = '-';
        $multiLang = count(LANGUAGES) > 0;
        if(is_file(ROOT.$brand->path.'/'.$imageSize.$brand->image))
            $image = "<img  src='".url($brand->path.'/'.$imageSize.$brand->image)."' style='max-width:100%'>";
        elseif(is_file(ROOT.$brand->path.'/'.$brand->image))
            $image = "<img  src='".url($brand->path.'/'.$brand->image)."' style='max-width:100%'>";

        $result = <<<EOS
        <tr>
            <td class="center-align">{$r}</td>
            <td>{$brand->name}</td>
            <td>{$brand->slug}</td>
EOS;
        if($multiLang){
            $langes = (array)$brand->langs;
            $result .="<td class='lang'>";
            foreach (LANGUAGES as $ll){
                $class =  'btn-info';
                $lang_title = '';
                $lang_slug = '';
                if(array_key_exists($ll[0],$langes)){
                    $class =  'btn-primary';
                    $lang_title = $langes[$ll[0]][0];
                    $lang_slug = $langes[$ll[0]][1];
                }

                $result .= "<button type='button' data-title='{$brand->name}' data-slug='{$brand->slug}' data-title2='{$lang_title}' data-slug2='{$lang_slug}' data-lang='".$ll[0]."' data-id='{$brand->id}'  class='btn {$class}'>".$ll[0].'</button>';

            }
            $result .= "</td>";
        }
        $result .= <<<EOS
            <td>{$image}</td>
            <td class="actions"  data-id="{$brand->id}" data-name="{$brand->name}" data-slug="{$brand->slug}" >
                <a data-toggle="tooltip" data-placement="top" href="#" title="نمایش  در سایت" type="button" class="btn btn-success">
                     <i class="icon-eye"></i>
                </a>
EOS;
        if(permission($this->_permissions['editBrand']))
            $result .= <<<EOS
                <a data-toggle="tooltip" data-placement="top" href="#" title="ویرایش " type="button" class="btn btn-info edited">
                    <i class="icon-pencil"></i>
                </a>
EOS;
        if($brand->id != 1 && permission($this->_permissions['removeBrand']))
            $result .= <<<EOS
                <a data-toggle="tooltip" data-placement="top" title="حذف " type="button" class="btn btn-danger removed ">
                    <i class="icon-trash"></i>
                </a>
EOS;
        $result .= "</td></tr>";
        return $result;
    }

}
