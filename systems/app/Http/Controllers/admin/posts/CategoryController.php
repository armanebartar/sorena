<?php

namespace App\Http\Controllers\admin\posts;

use App\Models\Cat_option;
use App\Models\Category;
use App\Helpers\Helpers;
use App\Models\S_prop;
use App\Models\S_prop_value;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class CategoryController extends Controller
{

    public function categories($type){
        $this->setProperties($type);

        addLog('مشاهده دسته بندیهای '.$this->_metas['title']);
        $data = $this->tbl_cat();
        $data['meta'] = $this->_metas;

        return view('admin.posts.categories',$data);
    }

    public function addSaveGroup(Request $request,$type){
        $this->setProperties($type);
        if(!isset($request->name) || strlen($request->name) < 2 ) {
            addLog('ذخیره ناموفق گروه '.$this->_metas['nameSingle'].' جدید',$request->all(),'نام وجود ندارد یا کوتاه است');
            return ['res'=>1,'myAlert'=>'عنوان وارد شده نامعتبر است.','mySuccess'=>''];
        }

        $slug = isset($request->slug) && strlen($request->slug) > 2 ? $request->slug : $request->name;
        $slug = Category::slugGenerate($slug,$request->id ?? 0);
        if(!$request->id || !$cat = Category::where([['id',$request->id],['group',$this->_metas['id']]])->first()) {
            if(!permission($this->_permissions['addCategory']))
                return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
            $cat = new  Category();
            $titreLog = 'ذخیره گروه '.$this->_metas['nameSingle'].' جدید';
            $message = 'گروه جدید با موفقیت ذخیره گردید.';
            $cat->parent = $request->parent;
            $cat->lang = DEFAULT_LANG[0];

        }else{
            if(!permission($this->_permissions['editCategory']))
                return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
            $titreLog = 'ویرایش گروه '.$this->_metas['nameSingle'];
            $message = 'تغییرات با موفقیت ذخیره گردید.';
            if(  $request->parent != $cat->id)
                $cat->parent = $request->parent;
        }

        $cat->name = $request->name;
        $cat->slug = $slug;
        $cat->path = Category::imagePath();
        if($request->hasFile('image')) {
            $image = Helpers::checkAndSaveImage($request, 'image', Category::imagePath(), $this->_metas['image_sizes_cat'], Category::imageMaxSize());
            if (!is_file(Category::imagePath('/') . $image)) {
                return ['res' => 1, 'myAlert' => 'تصویر انتخابی نامعتبر است.', 'mySuccess' => ''];
            }else {
                $cat->image =  $image;

            }

        }

        $cat->group = $this->_metas['id'];
        $cat->save();
        Cache::forget('CatPostByGroup'.$this->_metas['id']);
        $tbl = $this->tbl_cat();
        addLog($titreLog,$request->all());

        return ['res' => 10, 'myAlert' => '', 'mySuccess' => $message,'tbl'=>$tbl['tbl'],'parentsTbl'=>$tbl['parentsTbl']];
    }

    public function removeGroup(Request $request,$type){
        $this->setProperties($type);
        if(!permission($this->_permissions['removeCategory'],1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];

        if($firstGroup = Category::where('group',$this->_metas['id'])->first())
            if($firstGroup->id == $request->input('id'))
                return ['res' => 1, 'myAlert' => 'این گروه اصلی است و قابل حذف نیست.', 'mySuccess' => ''];

        if($cat=Category::where('id',$request->input('id'))->first()){
            if($cat->group != $this->_metas['id']){
                return ['res' => 1, 'myAlert' => 'گروه مربوط به این دسته نمی‌باشد.', 'mySuccess' => ''];
            }
            if(Category::where('parent',$cat->id)->count() > 0){
                return ['res' => 1, 'myAlert' => 'این گروه دارای زیر گروه است و نمیتوانید حذف کنید.', 'mySuccess' => ''];
            }
            addLog('حذف گروه '.$this->_metas['nameSingle'],['id'=>$cat->id,'name'=>$cat->name]);
            if(is_file(ROOT.$cat->path.'/'.$cat->image)){
                unlink(ROOT.$cat->path.'/'.$cat->image);
                foreach ($this->_metas['image_sizes_cat'] as $meta){
                    if(isset($meta[0]) && is_file($cat->path.'/'.$meta[0].'_'.$meta[1].'_'.$cat->image))
                        unlink($cat->path.'/'.$meta[0].'_'.$meta[1].'_'.$cat->image);
                }
            }
            Category::destroy($request->input('id'));

            Cache::forget('CatPostByGroup'.$this->_metas['id']);

            $tbl = $this->tbl_cat();
            return ['res' => 10,
                'myAlert' => '',
                'mySuccess' => 'حذف گروه با موفقیت انجام گردید.',
                'tbl'=>$tbl['tbl'],
                'parentsTbl'=>$tbl['parentsTbl']
            ];
        }else{
            addLog('حذف ناموفق گروه خبری',$request->all(),'آیدی گروه نامعتبر است یا برابر یک است');
            return ['res' => 1,
                'myAlert' => 'عملیات با خطا مواجه گردید.',
                'mySuccess' => ''
            ];
        }

    }

    public function addLang(Request $request,$type){
        $this->setProperties($type);
        if(!permission('addLangCategory',1))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];

        if($cat=Category::where('id',$request->input('id'))->first()){
            if($cat->group != $this->_metas['id']){
                return ['res' => 1, 'myAlert' => 'گروه مربوط به این دسته نمی‌باشد.', 'mySuccess' => ''];
            }
            if(strlen($request->name) < 2){
                return ['res' => 1, 'myAlert' => 'نام الزامیست و باید حداقل دو کارکتر داشته باشد.', 'mySuccess' => ''];
            }
            addLog('ویرایش ترجمه '.$this->_metas['nameSingle'],$request->all());

            if(!$lang = Category::where([['parent_lang',$cat->id],['lang',$request->input('lang')]])->first()){
                $lang = new Category();
                $lang->group = $cat->group;
                $lang->parent_lang = $cat->id;
                $lang->lang = $request->input('lang');

            }
            $lang->name = $request->input('name');
            $slug = isset($request->slug) && strlen($request->slug) > 2 ? $request->slug : $request->input('name');
            $slug = Category::slugGenerate($slug,$request->input('id'));

            $lang->slug = $slug;
            $lang->save();

            return ['res' => 10, 'myAlert' => '', 'mySuccess' => 'ویرایش ترجمه با موفقیت انجام گردید.'];
        }else
            return ['res' => 1, 'myAlert' => 'عملیات با خطا مواجه گردید', 'mySuccess' => ''];


    }

    public function edit_lang($id,$lang){

        if(!isset(LANGUAGES[$lang])){
            session(['alert-danger'=>'ترجمه مورد نظر در دسترس نیست']);
            return back();
        }
        if (!$category = Category::with(['type','options'])->where('id', $id)->first()){
            session(['alert-danger'=>'دسته مورد نظر یافت نشد']);
            return back();
        }
        if(!is_object($category->type)){
            session(['alert-danger'=>'دسته مورد نظر نا مشخص است']);
            return back();
        }
        $this->setProperties($category->type->type);
        if(!permission($this->_permissions['addLangCategory'])){
            session(['alert-danger'=>'شما مجاز به انجام این عملیات نیستید']);
            return back();
        }

        if(!$trans = Category::where([['parent_lang',$category->id],['lang',$lang]])->first()){
            $trans = new Category();
            $trans->parent_lang = $category->id;
            $trans->lang = $lang;
            $trans->name = $category->name;
            $trans->slug = $category->slug;
            $trans->parent = $category->parent;
            $trans->group = $category->group;
            $trans->image = $category->image;
            $trans->path = $category->path;
            $trans->save();
        }
        return $this->edit_complete($trans->id);
    }

    public function edit_complete($id=0){

        if (!$category = Category::with(['type','options'])->where('id', $id)->first()){
            session(['alert-danger'=>'دسته مورد نظر یافت نشد']);
            return back();
        }
        if(!is_object($category->type)){
            session(['alert-danger'=>'دسته مورد نظر نا مشخص است']);
            return back();
        }
        $this->setProperties($category->type->type);
        if(!permission($this->_permissions['editCategory'])){
            session(['alert-danger'=>'شما مجاز به انجام این عملیات نیستید']);
            return back();
        }

        $data['cat'] = Category::get_by_options($category->id,$category);
        $details = json_decode($category->details,1);
        $data['properties'] = S_prop::with('values')->where('cat_id',$id)->get();


        $data['attrs'] = $details['attr'] ?? [];
        $data['comments'] = $details['comment'] ?? [];
        $data['meta'] = $this->_metas ?? [];
        return view('admin.posts.cat_edit',$data);
    }

    public function edit_complete_save(Request $request){

        if (!$category = Category::with(['type','options'])->where('id', $request->id)->first()){
            return ['res' => 1, 'myAlert' => 'گروه یافت نشد', 'mySuccess' => ''];
        }
        if(!is_object($category->type)){
            return ['res' => 1, 'myAlert' => 'دسته مورد نظر نا مشخص است', 'mySuccess' => ''];

        }
        $this->setProperties($category->type->type);
        if(!permission($this->_permissions['editCategory']))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        if(strlen($request->name) < 5)
            return ['res' => 1, 'myAlert' => 'عنوان دسته خیلی کوتاه است', 'mySuccess' => ''];
        $category->name = $request->name;
        $category->slug = strlen($request->slug)>3 ? Category::slugGenerate($request->slug) : Category::slugGenerate($request->name);
        $category->image = $request->image;
        $category->path = $request->path;
        $category->parent = $request->parent ?? 0;
        $category->details = json_encode($this->props($request->all(),$category));
        $category->save();
        $options = ['minContent','desc','seo_title','seo_desc','seo_keyword'];
        Cat_option::where('cat_id',$category->id)->delete();
        foreach ($options as $option){
            $opt = new Cat_option();
            $opt->cat_id = $category->id;
            $opt->key = $option;
            $opt->value = $request->{$option} ?? '';
            $opt->save();
        }
        return ['res' => 10, 'mySuccess' => 'تغییرات با موفقیت ذخیره گردید', 'myAlert' => ''];
    }

    private function props($request,$category){
        if(isset($request['values'])&&is_array($request['values'])){

            foreach ($request['values'] as $value){
                if(isset($request['prop'.$value])&&isset($request['propTitle'.$value])&&is_array($request['propTitle'.$value])){
                    if(!(isset($request['catId'.$value]) && $request['catId'.$value]!=0 && $property = S_prop::where('id',$request['catId'.$value])->first()))
                        $property = new S_prop();
                    elseif($property->cat_id != $category->id)
                        $property = new S_prop();
                    $property->cat_id = $category->id;
                    $property->title = $request['prop'.$value];
                    $property->save();

//                    dd($request);
                    foreach ($request['propTitle'.$value] as $k=>$item){
                        if(!(isset($request['propId'.$value][$k]) && $request['propId'.$value][$k]!=0 && $val = S_prop_value::where('id',$request['propId'.$value][$k])->first()))
                            $val = new S_prop_value();
                        elseif($val->prop_id != $property->id)
                            $val = new S_prop_value();
                        $val->value = $item;
//                        $val->desc = $request['propDesc'.$value][$k];
                        $val->prop_id = $property->id;
                        $val->save();
                    }
                }
            }
        }

        $result = [];


        if(isset($request['comment'])){
            foreach ($request['comment'] as $item){
                $result['comment'][]= $item;
            }
        }

        if(isset($request['attr'])){
            foreach ($request['attr'] as $item){
                $result['attr'][]= $item;
            }
        }

        return $result;
    }

    public function saveThumb(Request $request){
        $this->setProperties($request->type);
        if(!permission($this->_permissions['editCategory']))
            return ['res' => 1, 'myAlert' => 'شما مجاز به انجام این عملیات نیستید!', 'mySuccess' => ''];
        $path =  'files/categories/' ;
        $size = 2 ;
        foreach ($request->all() as $key=>$item){
            if($request->hasFile($key)){
                return Helpers::checkImageAjax($request,$key,$path,$size);
            }
        }
        return ['res'=>1,'mySuccess'=>'','myAlert'=>'لطفا یک تصویر انتخاب نمایید'];
    }

    private function tbl_cat(){
        $cats = Category::getCatsByGroup($this->_metas['id'],1);
        $result = '';
        $r=0;
        foreach ($cats as $k => $cat) {
            $result .= $this->make_row_cat($cat,++$r,'',count($cat->childes));
            if(count($cat->childes))
                foreach ($cat->childes as $childe)
                    $result .= $this->make_row_cat($childe,++$r,' --- ');
        }

        $parentsTbl = "<option value='0'>بدون والد</option>";
        foreach ($cats as $parent){
            $parentsTbl .= "<option value='{$parent->id}'>{$parent->name}</option>";
        }
        return ['tbl'=>$result,'parentsTbl'=>$parentsTbl];
    }

    private function make_row_cat($cat,$r,$beforeName = '',$childes=0){
        $imageSize = $this->_metas['image_sizes_cat'][0][0].'_'.$this->_metas['image_sizes_cat'][0][1].'_';

        $image = '-';
        $multiLang = count(LANGUAGES) > 0;
        if(is_file(ROOT.$cat->path.'/'.$imageSize.$cat->image))
            $image = "<img  src='".url($cat->path.'/'.$imageSize.$cat->image)."' style='max-width:100%'>";
        elseif(is_file(ROOT.$cat->path.'/'.$cat->image))
            $image = "<img  src='".url($cat->path.'/'.$cat->image)."' style='max-width:100%'>";

        $result = <<<EOS
        <tr>
            <td class="center-align">{$r}</td>
            <td>{$beforeName}{$cat->name}</td>
            <td>{$cat->slug}</td>
EOS;

        if($multiLang){
            $langes = (array)$cat->langs;
            $result .="<td class='lang'>";
            foreach (LANGUAGES as $ll){
                $class =  'btn-info';
                $lang_title = '';
                $lang_slug = '';
                if(array_key_exists($ll[0],$langes)){
                    $class =  'btn-primary';
                    $lang_title = $langes[$ll[0]][0];
                    $lang_slug = $langes[$ll[0]][1];
                }

                $result .= "<button type='button' data-title='{$cat->name}' data-slug='{$cat->slug}' data-title2='{$lang_title}' data-slug2='{$lang_slug}' data-lang='".$ll[0]."' data-id='{$cat->id}'  class='btn {$class}'>".$ll[0].'</button>';

            }
            $result .= "</td>";
        }
        $link_show = AB_route_cat($this->_metas['id'],$cat->id,$cat->slug);
        $result .= <<<EOS
            <td>{$image}</td>
            <td class="actions" data-childes="{$childes}" data-id="{$cat->id}" data-name="{$cat->name}" data-slug="{$cat->slug}" data-parent="{$cat->parentId}">
                <a data-toggle="tooltip" data-placement="top" target="_blank" href="{$link_show}" title="نمایش  در سایت" type="button" class="btn btn-success">
                     <i class="icon-eye"></i>
                </a>
EOS;
        if(permission($this->_permissions['editCategory']))
            $result .= <<<EOS
                <a data-toggle="tooltip" data-placement="top" href="#" title="ویرایش " type="button" class="btn btn-info edited">
                    <i class="icon-pencil"></i>
                </a>
EOS;
        if($cat->id != 1 && permission($this->_permissions['removeCategory']))
            $result .= <<<EOS
                <a data-toggle="tooltip" data-placement="top" title="حذف " type="button" class="btn btn-danger removed ">
                    <i class="icon-trash"></i>
                </a>
EOS;
        $result .= "</td></tr>";
        return $result;
    }

}
