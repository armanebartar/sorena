<?php
namespace App\Http\Controllers\profile;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Factor;
use App\Models\Factor_history;
use App\Models\Favorite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class ProfileController extends Controller {


    public function profile($status = 3){
        $data['status'] = !in_array($status,[1,2,3,4,5]) ? 3 : $status;
        $data['orders'] = Factor::with(['user'])->where([['user_id',\auth()->user()->id],['status',$data['status']]])->orderBy('created_at','DESC')->get();
        $data['titre'] = "سفارشات در انتظار تایید من";
        if($data['status']==1 || $data['status']==2) $data['titre'] = "در انتظار تایید واحد فروش";
        if($data['status']==4) $data['titre'] = "سفارشات تایید شده";
        if($data['status']< 1 ) $data['titre'] = "سفارشات رد شده";
        return  view('profile.profile',$data);
    }

    public function profile_order($id, $secure){
        if($secure!=secure($id,'mobile')){
            session(['alert-danger'=>'کلید امنیتی شما منقضی شده']);
            return back();
        }
        if(!$data['order'] = Factor::with(['details'])->where('id',$id)->first()){
            session(['alert-danger'=>'سفارش مورد نظر یافت نشد']);
            return back();
        }
        if($data['order']->user_id != auth()->user()->id){
            session(['alert-danger'=>'سفارش متعلق به شما نمی‌باشد']);
            return back();
        }
        $data['status'] = $data['order']->status;
        return view('profile.order_details',$data);
    }


}
