<?php

namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\Post_type;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cookie;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected  $_metas;
    protected $_permissions;

    protected function setProperties($type){
        if($postType = Post_type::where('type',$type)->first()){
            $uc = ucfirst($type);
            $image_sizes = [];
            foreach (explode('**',$postType->image_sizes) as $val)
                $image_sizes[]=explode('*',$val);
            $image_sizes_cat = [];
            foreach (explode('**',$postType->image_sizes_cat) as $val)
                $image_sizes_cat[]=explode('*',$val);
            $image_sizes_tag = [];
            foreach (explode('**',$postType->image_sizes_tag) as $val)
                $image_sizes_tag[]=explode('*',$val);
            $image_sizes_brand = [];
            foreach (explode('**',$postType->image_sizes_brand) as $val)
                $image_sizes_brand[]=explode('*',$val);
            $this->_metas = [
                'id'=>$postType->id,
                'title'=>'لیست '.$postType->nameTotal,
                'idMenuName'=>'menuItems'.$uc,
                'nameTotal'=>$postType->nameTotal,
                'nameSingle'=>$postType->nameSingle,
                'baseRout'=>'p_posts',
                'type'=>$type,
                'files'=>$postType->files,
                'image_sizes'=>$image_sizes,
                'image_sizes_cat'=>$image_sizes_cat,
                'image_sizes_tag'=>$image_sizes_tag,
                'image_sizes_brand'=>$image_sizes_brand,
                'image_maxValue'=>$postType->image_maxValue,
                'file_maxValue'=>$postType->file_maxValue,
                'status_comment'=>$postType->status_comment,
                'status_category'=>$postType->status_category,
                'status_tag'=>$postType->status_tag,
                'status_brand'=>$postType->status_brand,
                'by_attr'=>$postType->by_attr,
                'is_product'=>$postType->is_product,
            ];

            $this->_permissions=[
                'articlesShow'=>$type.'sShow',
                'addArticle'=>'add'.$uc,
                'articleEdit'=>$type.'Edit',
                'myArticleEdit'=>'my'.$uc.'Edit',
                'articleRemove'=>$type.'Remove',
                'myArticleRemove'=>'my'.$uc.'Remove',
                'articleChangeStatus'=>$type.'ChangeStatus',
                'addTranslate'=>$type.'AddTranslate',
                'addMyTranslate'=>$type.'AddMyTranslate',

                'addCategory'=>'addCategory'.$postType->id,
                'editCategory'=>'editCategory'.$postType->id,
                'removeCategory'=>'removeCategory'.$postType->id,
                'addLangCategory'=>'addLangCategory'.$postType->id,

                'addBrand'=>'addBrand'.$postType->id,
                'editBrand'=>'editBrand'.$postType->id,
                'removeBrand'=>'removeBrand'.$postType->id,
                'addLangBrand'=>'addLangBrand'.$postType->id,

                'addTag'=>'addTag',
                'editTag'=>'editTag',
                'removeTag'=>'removeTag',
                'addLangTag'=>'addLangTag',

                'special'=>'special'.$postType->id,

            ];
        }else
            die('post type not found!');
    }

    public function make_view($view,$data){
        $user_token = $this->user_token();
        session(['user_token'=>$user_token]);
        $response = new Response(view($view,$data));
        return $response->withCookie(cookie('user_token', $user_token , 1000));
    }

    protected function user_token(){
        if(!$user_token = session('user_token',false)){
            $user_token = Cookie::get('user_token',Helpers::randString(15,25));
        }

        return $user_token;
    }



}
