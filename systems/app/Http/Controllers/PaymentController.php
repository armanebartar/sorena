<?php

namespace App\Http\Controllers;


use App\Models\Factor;
use App\Helpers\Helpers;
use App\Models\Factor_history;
use App\Models\Payment;
use Illuminate\Http\Request;




class PaymentController extends Controller{


    public function basket_payment(Request $request){
        $data = AB_basket_update();
        if(count($data['details'])<1)
            return redirect()->route('basket_show');
        $data['opt'] = AB_get_options();

        return view('basket_payment',$data);
    }

    public function payment(Request $request){
        $r = $request->all();
        if(!auth()->check()){
            session(['alert-danger'=>'ابتدا باید به سایت وارد شوید.']);
            session(['backLogin'=>route('basket_address')]);
            return redirect()->route('login');
        }
        if(!isset($r['factor_id_pay'])) {
            $data = AB_basket_update();
            $this_factor = $data['factor'];
            if (!$this_factor->address_id) {
                session(['alert->danger' => 'آدرس ارسال را مشخص نمایید']);
                return redirect()->route('basket_address');
            }
            $send_method = $r['send_method'];
        }else{
            if(!$this_factor = Factor::where('id',$r['factor_id_pay'])->first()){
                session(['alert-danger'=>'فاکتور یافت نشد!']);
                return  back();
            }
            if($this_factor->user_id != auth()->user()->id){
                session(['alert-danger'=>'فاکتور متعلق به شما نیست!']);
                return  back();
            }
            if($this_factor->status!=0 && $this_factor->status!=3&&$this_factor->status!=4){
                session(['alert-danger'=>'فاکتور در مرحله پرداخت قرار ندارد!']);
                return  back();
            }
            $send_method = AB_default_send();
        }


        if(strlen($send_method)<2 || !$send_method = AB_send_methos($send_method)){
            session(['alert->danger'=>'روش ارسال را مشخص نمایید']);
            return redirect()->route('basket_address');
        }

        if(count($this_factor->details)<1){
            return redirect()->route('basket_show');
        }
        if($this_factor->status < 4) {
            $this_factor->status = 4;
            $this_factor->expire = AB_lifetime_invoice();
            $this_factor->time = time();
            Factor_history::new_history($this_factor->id,4);
        }
        $this_factor->send_method = $send_method['name'];
        $this_factor->send_price = $send_method['price'];
        $this_factor->save();


        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->factor_id = $this_factor->id;
        $payment->status = 1;
        $payment->amount = $this_factor->final - $this_factor->discount + $this_factor->send_price;
        $payment->code = Helpers::db_secure_key('payment',45,60);
        $payment->payment_way = 1;
        $payment->time = time();
        $payment->save();

        $callback = route('payment_callback',['gateway'=>'zarin', 'code'=>$payment->code, 'secure'=>secure($payment->code)]);
        $desc = "پرداخت فاکتور شماره ".$this_factor->id;
        $res = zarin_request($payment->amount,$desc,$callback,auth()->user()->mobile,\auth()->user()->name.' '.\auth()->user()->family);


        $this_factor->status = 0;
        $this_factor->save();
        session(['alert_pay'=>$res]);
        return  redirect()->route('basket_address');

    }


    public function payment_callback($gateway,$code,$secure){
        $data['res'] = 0;
        $data['message'] = '';
        $data['ref'] = '';
        if(secure($code) == $secure){
            if($payment = Payment::where('code',$code)->first()){
                if($gateway == 'zarin'){
                    $result = zarin_verify($payment->amount);
                    if($result['res']==10){
                        $payment->status = 3;
                        $payment->tracking_code = $result['RefID'];
                        $factor = Factor::where('id', $payment->factor_id)->first();
                        $final = $factor->final - $factor->discount + $factor->send_price;
                        $data['res'] = 10;
                        $data['ref'] = $result['RefID'];
                        if ($payment->amount == $final) {
                            $factor->status = 6;
                            $factor->save();
                            Factor_history::new_history($factor->id,6);
                        }
                    }else{
                        $data['res'] = 1;
                        $payment->status = 0;
                    }
                    $payment->description = $result['message'];
                    $payment->traceNo = $result['Authority'];
                    $payment->gatway_data = json_encode($result['result']);
                    $payment->save();
                    $data['message'] = $result['message'];
                }
            }else
                $data['message'] = 'تراکنش یافت نشد';

        }else
            $data['message'] = 'کلید امنیتی تراکنش منقضی شده';

        $data['opt'] = AB_get_options();

        return view('pay_result',$data);
    }

}
