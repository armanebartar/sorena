<?php

namespace App\Http\Controllers\Auth;


use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function index(){
        if(Auth::check())
            return redirect()->route('profile');

        addLog('ورود به ثبت نام');
        $data['opt'] = AB_get_options();
        return view('Auth.register',$data);
    }

    public function save(Request $request){
        if(Auth::check())
            return redirect()->route('profile');
        if(!isset($request->roles) || $request->roles != 'on'){
                session(['errorsRegister'=>['فقط در صورت موافقت با شرایط و قوانین سایت میتوانید عضو شوید','تیک موافقت با شرایط و قوانین سایت را بزنید.']]);
                return back()->withInput();

        }
        if(isset(CAPTCHA['register']) && CAPTCHA['register'][0]){
            if($request->captcha != session('capRegister','sddjfdjfbdfbshdbfdfsdfsdfsdfsdjhsbdfhsd')){
                session(['errorsRegister'=>['کد امنیتی وارد شده نامعتبر است.']]);
                return back()->withInput();
            }
        }
        $errors = [];

        if(strlen($request->name)<1)
            $errors [] = 'وارد کردن نام و نام خانوادگی الزامیست.';
        elseif(strlen($request->name)<4)
            $errors [] = 'نام حداقل باید چهار کارکتر داشته باشد.';
        elseif(strlen($request->name)>50)
            $errors [] = 'نام حداکثر میتواند ۵۰ کارکتر داشته باشد.';

//        if(strlen($request->family)<1)
//            $errors [] = 'وارد کردن نام خانوادگی الزامیست.';
//        elseif(strlen($request->family)<2)
//            $errors [] = 'نام خانوادگی حداقل باید دو کارکتر داشته باشد.';
//        elseif(strlen($request->family)>25)
//            $errors [] = 'نام خانوادگی حداکثر میتواند ۲۵ کارکتر داشته باشد.';

//        $username = strtolower(fa2la($request->username));
//        if(strlen($username)<1)
//            $errors [] = 'وارد کردن نام کاربری الزامیست';
//        elseif(strlen($username)<4)
//            $errors [] = 'نام کاربری باید حداقل ۴ کارکتر داشته باشد.';
//        elseif(strlen($username)>15)
//            $errors [] = 'نام کاربری حداکثر میتواند ۱۵ کارکتر داشته باشد.';
//        elseif(!preg_match('/^[0-9a-z]{4,15}$/s',$username))
//            $errors [] = 'نام کاربری وارد شده صحیح نمی‌باشد.';
//        elseif(\App\Models\User::where('username',$username)->count() > 0)
//            $errors [] = 'نام کاربری وارد شده تکراریست.';

        $mobile = fa2la(strtolower($request->mobile));
        if(strlen($mobile)<1)
            $errors [] = 'وارد کردن موبایل الزامیست';


        elseif(!preg_match('/^09[0-9]{9}$/s',$mobile)
//            && preg_match('/[a-z0-9\.\-\_]{1,25}\@[a-z0-9\.\-\_]{1,25}\.[a-z0-9\.\-\_]{1,10}]/s',$mobile)
        )
            $errors [] = 'موبایل / ایمیل وارد شده صحیح نمی‌باشد.';
        elseif(User::where('mobile',$mobile)->count() > 0)
            $errors [] = 'موبایل وارد شده تکراریست.';
//        elseif(User::where('email',$mobile)->count() > 0)
//            $errors [] = 'ایمیل وارد شده تکراریست.';

        if(strlen($request->pass)<1)
            $errors [] = 'پسوورد الزامیست .';
        elseif(strlen($request->pass)<6)
            $errors [] = 'پسوورد باید حداقل شش کارکتر داشته باشد.';
        elseif(strlen($request->pass)>30)
            $errors [] = 'پسوورد میتواند حداکثر ۳۰ کارکتر داشته باشد.';
//        elseif($request->password != $request->password2)
//            $errors [] = 'پسووردها با هم یکسان نیستند.';

        if($errors != []){
            addLog('ثبت نام ناموفق',$request->all(),$errors);
            session(['errorsRegister' => $errors]);
            return redirect()->back()->withInput();
        }




        $code = rand(1000,99999);

        $thisData = [
            'name'=>$request->name ,
            'family'=>$request->family ,
            'mobile'=>$mobile ,
            'username'=>$mobile ,
            'tel'=>$request->tel ,
            'address'=>$request->address ,
            'password'=>$request->password ];

        $this->saveUser($thisData);
        return redirect()->route('profile');

//        if(is_numeric($mobile)) {
            session(['alert-success'=>'کد فعالسازی به موبایلتان ارسال گردید.']);
            Helpers::sendSms(' جهت تکمیل ثبت نام، کد زیر را در فیلد مربوطه وارد نمایید :' . $code,$mobile);
//            Smsirlaravel::send(' جهت تکمیل ثبت نام، کد زیر را در فیلد مربوطه وارد نمایید :' . $code, $mobile);
//        }
//        else{
//
//            $email = $mobile;
//
//            $vv = new User_verify();
//            $vv->code = Helpers::randString(30,45);
//            $vv->data = serialize($thisData);
//            $vv->save();
//
//            $opt = $GLOBALS['myData'.session('lang','')];
//            Mail::send('Emails.confirm_register',
//                array(
//                    'Token' => $vv->code,
//                    'Code' => $code,
//
//                ),
//                function ($message) use ($email,$opt) {
//                    $message->from($opt['email'], $opt['site_name']);
//                    $message->sender($opt['email'], $opt['site_name']);
//                    $message->subject("تاییدیه ثبت نام هلند کالا");
//                    $message->to($email,$opt['site_name'].' - '."تاییدیه ثبت نام هلند کالا");
//                });
//
//            session(['alert-success'=>'کد فعالسازی به ایمیلتان ارسال گردید.']);
//        }

        session([
            'register_verify_level'=>2,
            'register_verify_code'=>$code,
            'register_verify_start'=>time(),
            'register_verify_numb'=>0,
            'register_verify_values'=>$thisData
        ]);

        return redirect()->route('register_verify');


    }

    public function confirm($code){
        if($user = User_verify::where('code',$code)->first()){


            $data = unserialize($user->data);

            $this->saveUser($data);
            User_verify::destroy($user->id);
            if(session('backLogin',false) && (strpos(session('backLogin',''),'shop/cart') || strpos(session('backLogin',''),'shop_single')))
                return redirect(session('backLogin'));

            return redirect()->route('profile');

        }

        session(['alert-danger'=>'']);
        return  redirect()->route('home');
    }

    public function verify(){
        if(session('register_verify_level') != 2 || strlen(session('register_verify_code',1)<4) ){
            session()->forget(['register_verify_level','register_verify_code','register_verify_start','register_verify_numb','register_verify_values']);
            return redirect()->route('register');
        }
        $data['opt'] = AB_get_options();
        return view('Auth.register_verify',$data);
    }

    public function verify_save(Request $request){
        if(session('register_verify_level') != 2 || strlen(session('register_verify_code',1)<4) ){
            session()->forget(['register_verify_level','register_verify_code','register_verify_start','register_verify_numb','register_verify_values']);
            return redirect()->route('register');
        }

        if(isset(CAPTCHA['register_verify']) && CAPTCHA['register_verify'][0]){
            if($request->captcha != session('capRegister_verify','sddjfdjfbdfbshdbfdfsdfsdfsdfsdjhsbdfhsd')){
                session(['alert-danger'=>'کد امنیتی وارد شده نامعتبر است.']);
                return back()->withInput();
            }
        }

        $code = $request->code;
        if(strlen($code) < 4 || $code != session('register_verify_code','sddjfdjfbdfbshdbfdfsdfsdfsdfsdjhsbdfhsd')){
            $numb = session('register_verify_numb',1);
            $numb++;
            if($numb > 5){
                session(['alert-danger'=>'تعداد دفعات مجاز شما به پایان رسید.']);
                session()->forget(['register_verify_level','register_verify_code','register_verify_start','register_verify_numb','register_verify_values']);
                return redirect()->route('register');
            }
            session([
                'alert-danger'=>'کد وارد شده صحیح نمی‌باشد.',
                'register_verify_numb'=>$numb
            ]);
            return back()->withInput();
        }

        $fields = session('register_verify_values',[]);
        session()->forget(['register_verify_level','register_verify_code','register_verify_start','register_verify_numb','register_verify_values']);

        if(!isset($fields['name']) || !isset($fields['family'])) {
            session(['alert-danger'=>'ثبت نام با خطا مواجه گردید.']);
            return redirect()->route('register');
        }

       $this->saveUser($fields);

        if(session('backLogin',false) && (strpos(session('backLogin',''),'shop/cart') || strpos(session('backLogin',''),'shop_single')))
            return redirect(session('backLogin'));

        return redirect()->route('profile');

    }

    private function saveUser($fields){
        $user = new User();
        $user->username = $fields['username'];
        $user->name = $fields['name'];
//        $user->family = $fields['family'];
//        if(is_numeric($fields['mobile'])) {
            $user->mobile = $fields['mobile'];
            $user->mobile_active = 1;
//        } else {
//            $user->email = $fields['mobile'];
//            $user->email_active = 1;
//        }
//        $user->tel = isset($fields['tel']) ? $fields['tel'] : '';
//        $user->address = isset($fields['address']) ? $fields['address'] : '';
        $user->password = User::hashPass($fields['password']);
        $user->status = 1;
        $user->role = 2;
        $user->grade = 1;
        $user->importer = 0;
        $user->aff = 0 ;
        if($aff = Cookie::get('aff',false)){
            if(User::where('id',$aff)->count() > 0)
                $user->aff = $aff;
        }
        $user->save();

//        if($user->aff != 0 ){
//            if($affRate = Award_rate::where('slug','aff_register')->first() && $affUser = User::where('id',$user->aff)->first()){
//                if($affRate->rate > 0){
//                    $rate = new User_rate();
//                    $rate->user_id = $user->aff;
//                    $rate->type = 'register';
//                    $rate->type_id = $user->id;
//                    $rate->desc = 'ثبت نام مشتری جدید - '.$user->name.' '.$user->family.'_'.$user->mobile;
//                    $rate->numb = $affRate->rate;
//                    $rate->save();
//
//                    $affUser->rate += $affRate->rate;
//                    $affUser->save();
//                }
//            }
//        }

        addLog(' ثبت نام کاربر جدید',['id'=>$user->id,'name'=>$user->name.' '.$user->family,'mobile'=>$user->mobile]);

        Auth::loginUsingId($user->id);

        session([
            'user_id'=>$user->id,
            ['user'=>[
                'name'=>$user->name,
                'family'=>$user->family,
                'mobile'=>$user->mobile,
            ]
            ]
        ]);

        session(['alert-success' => 'ثبت نام شما با موفقیت انجام شد.']);
    }

    public function resend(){
        if(session('register_verify_level') != 2 || strlen(session('register_verify_code',1)<4) ){
            session()->forget(['register_verify_level','register_verify_code','register_verify_start','register_verify_numb','register_verify_values']);
            return redirect()->route('register');
        }
        $lastSend = session('register_verify_start','1000000000000000000');
        if(time() - $lastSend < 60){
            session(['alert-danger'=>'شما در هر ۶۰ ثانیه یکبار میتوانید کد دریافت کنید.']);
            return back();
        }
        $fields = session('register_verify_values',[]);
        if(!isset($fields['mobile']) || !preg_match('/^09[0-9]{9}$/s',$fields['mobile'])){
            session()->forget(['register_verify_level','register_verify_code','register_verify_start','register_verify_numb','register_verify_values']);
            return redirect()->route('register');
        }
        $code = rand(1000,99999);
        Smsirlaravel::send(' جهت تکمیل ثبت نام، کد زیر را در فیلد مربوطه وارد نمایی :'.$code,$fields['mobile']);
        session([
            'register_verify_level'=>2,
            'register_verify_code'=>$code,
            'register_verify_start'=>time(),
            'register_verify_numb'=>0,
        ]);

        return redirect()->route('register_verify');
    }

}
