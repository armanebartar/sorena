<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Helpers;
use App\Helpers\Sms;
use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ForgotController extends Controller {

    public function index(){
        if(Auth::check())
            return redirect()->route('profile');
       $data['opt'] = AB_get_options();
        return view('Auth.forgot',$data);
    }

    public function checkMobile(Request $request){
//        dd($request->all());

        if(isset(CAPTCHA['forgot']) && CAPTCHA['forgot'][0]){
            if($request->captcha != session('capForgot','sddjfdjfbdfbshdbfdfsdfsdfsdfsdjhsbdfhsd')){
                session(['alert-danger-forgot'=>'کد امنیتی وارد شده نامعتبر است.']);
                return back()->withInput();
            }
        }
        $mobile = fa2la($request->username);
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(strlen($mobile)<2){
            session(['alert-danger'=>'وارد کردن موبایل  الزامیست.']);
            return back()->withInput();
        }
        if(!is_numeric($mobile) || strlen($mobile)!=11 || !str_starts_with($mobile, '09')){
            session(['alert-danger-forgot'=>'موبایل وارد شده نامعتبر است.']);
            return back()->withInput();
        }




        if(!$user = User::where([['mobile',$mobile],['role','2']])->first()){
            session(['alert-danger-forgot'=>'موبایل وارد شده در سیستم وجود ندارد.']);
            return back()->withInput();
        }
        $code = rand(10000,99999);

        $res = Sms::send_fast($mobile,'20746',['Pass'=>$code]);


//            if($res != "ارسال با موفقیت انجام گردید"){
//                session(['alert-danger'=>'ارسال پیامک با خطا مواجه گردید. لطفا دقایقی بعد مجددا تلاش نمایید.']);
//                return back()->withInput();
//            }
        session([
            'forgot_verify_level'=>2,
            'forgot_verify_code'=>$code,
            'forgot_verify_start'=>time(),
            'forgot_verify_numb'=>0,
            'forgot_verify_username'=>$user->mobile,
            'forgot_verify_titre'=>'پسورد جدید به موبایلتان ارسال گردید',
        ]);
        return redirect()->route('forgot_verify');
    }

    public function verifyPage(Request $request){
       if(session('forgot_verify_level') != 2 || strlen(session('forgot_verify_code',1)<4) ){
            session()->forget(['forgot_verify_level','forgot_verify_titre','forgot_verify_code','forgot_verify_start','forgot_verify_numb','forgot_verify_username']);
            return redirect()->route('forgot');


        }
        $data['opt'] = AB_get_options();
        return view('Auth.forgot_verify',$data);


    }


    public function checkCode(Request $request){
        if(session('forgot_verify_level') != 2 || strlen(session('forgot_verify_code',1)<4) ){
            session()->forget(['forgot_verify_level','forgot_verify_code','forgot_verify_titre','forgot_verify_start','forgot_verify_numb','forgot_verify_username']);
            return redirect()->route('forgot');
        }

        if(isset(CAPTCHA['forgot_verify']) && CAPTCHA['forgot_verify'][0]){
            if($request->captcha != session('capForgot_verify','sddjfdjfbdfbshdbfdfsdfsdfsdfsdjhsbdfhsd')){
                session(['alert-danger-forgot2'=>'کد امنیتی وارد شده نامعتبر است.']);
                return back()->withInput();
            }
        }

        $code = fa2la($request->code);
        if(strlen($code) < 4 || $code != session('forgot_verify_code','sddjfdjfbdfbshdbfdfsdfsdfsdfsdjhsbdfhsd')){
            $numb = session('forgot_verify_numb',1);
            $numb++;
            if($numb > 5){
                session(['alert-danger-forgot'=>'تعداد دفعات مجاز شما به پایان رسید.']);
                session()->forget(['forgot_verify_level','forgot_verify_titre','forgot_verify_code','forgot_verify_start','forgot_verify_numb','forgot_verify_username']);
                return redirect()->route('forgot');
            }
            session([
                'alert-danger-forgot2'=>'کد وارد شده صحیح نمی‌باشد.',
                'forgot_verify_numb'=>$numb
            ]);
            return back()->withInput();
        }

        $username = session('forgot_verify_username','sdsadasds4s21354asd6413as21d');
        session()->forget(['forgot_verify_level','forgot_verify_titre','forgot_verify_code','forgot_verify_start','forgot_verify_numb','forgot_verify_username']);

        if(!$user = User::where([['username',$username],['role','2']])->orwhere([['mobile',$username],['role','2']])->first()){
            session(['alert-danger-forgot2'=>'بازیابی پسورد با خطا مواجه گردید.']);
            return redirect()->route('forgot');
        }
        $user->remember_token = Helpers::randString(25,40);
        $user->password = User::hashPass($code);
        $user->save();

        session([
            'user_id'=>$user->id,
            ['user'=>[
                'name'=>$user->name,
                'family'=>$user->family,
                'mobile'=>$user->mobile,
            ]
            ]
        ]);

        session(['alert-success'=>'پسورد جدید با موفقیت ذخیره گردید.']);

        Auth::loginUsingId($user->id,true);

        if(session('backLogin',false) && (strpos(session('backLogin',''),'shop/cart') || strpos(session('backLogin',''),'shop_single')))
            return redirect(session('backLogin'));

        return redirect()->route('profile');


    }




}
