<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Helpers;
use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Cookie;

class LoginController extends Controller {

    public function index(){
        if(Auth::check())
            return redirect()->route('profile');
        session(['backLogin'=>back()->getTargetUrl()]);
        $data['opt'] = AB_get_options();
//        dd($data);
        return view('Auth.login',$data);
    }

    public function login(Request $request){


        if(Auth::check())
            return redirect()->route('profile');

        if(isset(CAPTCHA['login']) && CAPTCHA['login'][0]){
            if($request->captcha != session('capLogin','sddjfdjfbdfbshdbfdfsdfsdfsdfsdjhsbdfhsd')){
                session(['alert-danger-login'=>'کد امنیتی وارد شده نامعتبر است.']);
                return back()->withInput();
            }
        }
        $mobile = fa2la($request->username);
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(!str_starts_with($mobile, '09') || strlen($mobile)!=11 || !is_numeric($mobile)){
            session(['alert-danger-login'=>'موبایل وارد شده نامعتبر است.']);
            return back()->withInput();
        }

        if(strlen($request->password) < 4 ){
            session(['alert-danger-login'=>'پسورد وارد شده نامعتبر است.']);
            return back()->withInput();
        }


        $pass = User::hashPass(fa2la($request->password));

        if(!$user = User::where([['username',$mobile],['role','2']])->orwhere([['mobile',$mobile],['role','2']])->first()){
            session(['alert-danger-login'=>' موبایل وارد شده ناصحیح است.']);
            return back()->withInput();
        }

//        if(!$user = User::where([['username',$mobile],['role','2']])->orwhere([['mobile',$mobile],['role','2']])->first()){
//            session(['alert-danger-login'=>'نام کاربری / موبایل وارد شده ناصحیح است.']);
//            return back()->withInput();
//        }
        if($user->password != $pass){
            session(['alert-danger-login'=>'پسورد وارد شده ناصحیح است.']);
            return back()->withInput();
        }

        if(!$user->status){
            session(['alert-danger-login'=>'حساب کاربری شما غیر فعال است.']);
            return back()->withInput();
        }

        session([
            'user_id'=>$user->id,
            ['user'=>[
                'name'=>$user->name,
                'family'=>$user->family,
                'mobile'=>$user->mobile,
                ]
            ]
        ]);


//        if($request->remember == 'on')
            Auth::loginUsingId($user->id,true);
//        else
//            Auth::loginUsingId($user->id);

        if(session('backLogin',false))
            return redirect(session('backLogin'));

        return redirect()->route('profile');
    }



    public function logout(){

        addLog('خروج از حساب کاربری');

        if (Auth::check())
            Auth::logout();
        if (Auth::guard('admin')->check())
            Auth::guard('admin')->logout();
        session()->forget(['user_id','user']);

        return redirect()->route('home');

    }



}
