<?php

namespace App\Http\Controllers;


use App\Helpers\Sms;
use App\Models\Brand;
use App\Models\Category;
use App\Models\City;
use App\Models\Factor_detail;
use App\Models\Favorite;
use App\Helpers\Helpers;
use App\Models\Post;
use App\Models\Post_cat;
use App\Models\S_money;

use App\Models\S_product;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;




class HomeController extends Controller{

    public function captcha($captchaName,$count=5,$level=3){
        Helpers::captcha($captchaName,$count,$level);
    }

    public function home($lang=''){
        $useragent=isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
            return redirect()->route('app');
        $lang = AB_set_lang($lang);
        $data['opt'] = AB_get_options();
        $data['moneys'] = S_money::getAll();
        $data['money_default'] = S_money::where('is_default',1)->first();
//        dd(AB_special(1));
        return view('home',$data);
    }

    public function send_verify_app(Request $request){
        $mobile = fa2la($request->mobile);
        $mobile = strlen($mobile)==10 ? '0'.$mobile : $mobile;
        if(!is_numeric($mobile) || strlen($mobile) != 11 || !str_starts_with($mobile, '09'))
            return ['res'=>1,'message'=>'لطفا موبایل خود را بصورت صحیح وارد نمایید.'];
        $code = rand(10000,99999);
        if(!$user = User::where('mobile',$mobile)->first()){
            $user = new User();
            $user->name = '';
            $user->family = '';
            $user->username = $mobile;
            $user->mobile = $mobile;
            $user->company = '';
            $user->company_post = '';
            $user->desc = '';
            $user->password = User::hashPass($code);

            $user->role = 2;
            $user->grade = 1;
            $user->status = 1;
            $user->importer = 0;
            $user->secure = Helpers::randString(4,6);
            $user->save();
        }
        $user_login_sms = route('user_login_sms',[$user->secure]);
        Sms::send_fast($mobile,'70661',['Username'=>$mobile,'Pass'=>$code,'Verify'=>$user_login_sms]);
        return ['res'=>10,'message'=>'اطلاعات ورود به موبایلتان ارسال گردید'];
    }

    public function addToFavorite(Request $request){
        if(auth()->check() && Post::where('id',$request->id)->count() > 0){
            if($ff = Favorite::where([['user_id',\auth()->user()->id],['post_id',$request->id]])->first()){
                Favorite::destroy($ff->id);
                return  ['res'=>1];
            }else{
                $ff = new  Favorite();
                $ff->user_id = Auth::user()->id;
                $ff->post_id = $request->id;

                $ff->save();
                return  ['res'=>10];
            }
        }
        return  ['res'=>0];
    }

    public function getCities(Request $request){
        $result = "<option value='0'>انتخاب شهر</option>";

        if($states = State::with('cities')->where('name',$request->state)->first()){
            if($cities = City::where('state_id',$states->id)->orderBy('name','ASC')->get()){
                foreach ($cities as $city)
                    $result.="<option value='{$city->name}'>{$city->name}</option>";
            }
        }
        return $result;
    }

    public function search(Request $request,$page=1){
        $data['ads'] = $GLOBALS['ad'];
        $data['page'] = is_numeric($page)&&$page>0?$page:1;
        $data['limit'] = 10;
        $skip = ($data['page']-1) * $data['limit'];
        $data['posts'] =  Post::where('title','like','%'.$request->s.'%')->where([['status','publish']])->orderBy('id','DESC')->skip($skip)->take($data['limit'])->get();
        $data['total'] = Post::where('title','like','%'.$request->s.'%')->where([['status','publish']])->count();
        $data['targetPage'] = route('search2',[$request->s]);
        $data['opt'] = $GLOBALS['myData'.session('lang','')];
        $data['search'] = $request->s;
        return view('search',$data);
    }

    public function search2($search,$page=1){
        $data['ads'] = $GLOBALS['ad'];
        $data['page'] = is_numeric($page)&&$page>0?$page:1;
        $data['limit'] = 10;
        $skip = ($data['page']-1) * $data['limit'];
        $data['posts'] =  Post::where('title','like','%'.$search.'%')->where([['status','publish']])->orderBy('id','DESC')->skip($skip)->take($data['limit'])->get();
        $data['total'] = Post::where('title','like','%'.$search.'%')->where([['status','publish']])->count();
        $data['targetPage'] = route('search2',[$search]);
        $data['opt'] = $GLOBALS['myData'.session('lang','')];
        $data['search'] = $search;

        return view('search',$data);
    }

    public function contact_us($search,$page=1){
        $data['ads'] = $GLOBALS['ad'];
        $data['opt'] = $GLOBALS['myData'.session('lang','')];


        return view('contact',$data);
    }

    public function about_us($search,$page=1){
        $data['ads'] = $GLOBALS['ad'];
        $data['opt'] = $GLOBALS['myData'.session('lang','')];


        return view('about_us',$data);
    }



}
