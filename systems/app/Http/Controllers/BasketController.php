<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Category;
use App\Models\City;
use App\Models\Factor_detail;
use App\Models\Favorite;
use App\Helpers\Helpers;
use App\Models\Post;
use App\Models\Post_cat;
use App\Models\S_money;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;




class BasketController extends Controller{


    public function add_to_basket(Request $request){
        $numb = is_numeric(fa2la($request->quantity))&&fa2la($request->quantity)>0 ? fa2la($request->quantity) : 1;
        $post = Post::where('id',$request->id)->first();
        $factor = AB_add_to_basket($post,json_encode($request->props),$numb);

        return redirect()->back()->cookie('myFactor',$factor->secret_key,time() + (86400*10),'/');
    }
    public function add_to_basket_ajax(Request $request){

//        $post = Post::where('id',$request->id)->first();
//        $factor = AB_add_to_basket($post,json_encode($request->props),$numb);
//
//        return redirect()->back()->cookie('myFactor',$factor->secret_key,time() + (86400*10),'/');



        $numb = is_numeric(fa2la($request->numb))&&fa2la($request->numb)>0 ? fa2la($request->numb) : 1;
        $post_id = $request->id;
        $unit = $request->unit ?? '';
        $product_name = $request->name;
        $props = '';
        $factor = AB_get_this_basket();

        if(!$detail = \App\Models\Factor_detail::where([['post_id',$post_id],['props',$props],['factor_id',$factor->id],['product_name',$product_name]])->first()){
            $detail = new \App\Models\Factor_detail();
            $detail->factor_id = $factor->id;
            $detail->props = $props;
            $detail->post_id = $post_id;
            $detail->numb = $numb;
            $detail->product_name = $product_name;
            $detail->unit = $unit;
        }else{
            $detail->numb += $numb;
        }

        $zero_price = [
            'real_price'=>0,
            'price'=>0,
            'discount'=>0,
            'dis_id'=>0,
            'stock_id'=>0,
        ];
        $prices = $post_id ? AB_get_post_prop($post_id,json_decode($props)) : $zero_price;
        $type = 'buy';
        $money = \App\Models\S_money::where('is_default',1)->first();
        $detail->real_price = $prices['real_price'];
        $detail->price = $prices['price'];
        $detail->discount = $prices['discount'];
        $detail->discount_id = $prices['dis_id'];
        $detail->details = AB_prop_values(json_decode($props,1));
        $detail->stock_id = $prices['stock_id'];
        $detail->price_type = $money->name;
        $detail->type = $type;
        if($type=='reserve')
            $detail->payable = 10;
        elseif ($type == 'invoice')
            $detail->payable = 0;
        else
            $detail->payable = 100;
        $detail->save();

        session(['myFactor'=>$factor->secret_key]);

        return $this->get_basket_details_2($factor);
    }

    public function update_basket_ajax(Request $request){
        $factor = AB_get_this_basket();
        $details = Factor_detail::where('factor_id',$factor->id)->get();

        $r = $request->all();
//        dd($r);
        foreach ($details as $detail){
            if(isset($r['det_'.$detail->id])){
                if(fa2la($r['det_'.$detail->id]) != $detail->numb){
                    if($r['det_'.$detail->id]>0) {
                        $detail->numb = fa2la($r['det_'.$detail->id]);
                        $detail->save();
                    }else
                        Factor_detail::destroy($detail->id);
                }
            }else{
                Factor_detail::destroy($detail->id);
            }
        }
        return array_merge(['res'=>10],$this->get_basket_details_2($factor));
    }

    public function get_basket_details_ajax(){
        $factor = AB_get_this_basket();
        return $this->get_basket_details_2($factor);
    }

    public function get_basket_details_2($factor){
        $details = Factor_detail::where('factor_id',$factor->id)->get();

        $result = '';
        $count_price = 0;
        foreach ($details as $detail){
            $this_price = $detail->price - $detail->discount;
            $count_price+= $this_price * $detail->numb;

            $this_price = $this_price ?  "<span>قیمت : </span>".sep($this_price).' تومان' : "";


            $link = $detail->post_id&&$detail->post ?  AB_route_post($detail->post->type,$detail->post->id,$detail->post->slug) : '#';
            $result .= "
                    <li class=\"product-box-contain\">
                        <div class=\"drop-cart ggg\">

                            <div class=\"drop-contain\">
                                <a href=\"{$link}\">
                                    <h5>{$detail->product_name}</h5>
                                </a>
                                <ul class='details_basket'>{$detail->details}</ul>
                                <h6> {$this_price} </h6>
                                <div class='numb_change'>
                                تعداد :‌
                                <input type='text' name='det_{$detail->id}' value='{$detail->numb}'>
                                {$detail->unit}
</div>
                                <button class=\"close-button close_button\">
                                    <i class=\"fa-solid fa-xmark\"></i>
                                </button>
                            </div>
                        </div>
                    </li>
               ";
        }
        return ['list'=>$result,'count_product'=>count($details),'count_price'=>sep($count_price)];
    }
    public function get_basket_details(){
        $factor = AB_get_this_basket();
        $details = $factor->details ?? [];

        $result = '';
        $count_price = 0;
        foreach ($details as $detail){
            $this_price = $detail->price - $detail->discount;
            $count_price+= $this_price * $detail->numb;
            $this_price = sep($this_price);
            $image = url($detail->post->path.'64_64_'.$detail->post->image);
            $link = AB_route_post($detail->post->type,$detail->post->id,$detail->post->slug);
            $result .= "
                    <li class=\"product-box-contain\">
                        <div class=\"drop-cart\">
                            <a href=\"{$link}\" class=\"drop-image\">
                                <img src='{$image}' alt='{$detail->product_name}' class=\"blur-up lazyload\" >
                            </a>
                            <div class=\"drop-contain\">
                                <a href=\"{$link}\">
                                    <h5>{$detail->product_name}</h5>
                                </a>
                                <ul class='details_basket'>{$detail->details}</ul>
                                <h6><span>{$detail->numb} x</span> {$this_price} تومان</h6>
                                <button class=\"close-button close_button\">
                                    <i class=\"fa-solid fa-xmark\"></i>
                                </button>
                            </div>
                        </div>
                    </li>


               ";
        }
        return ['list'=>$result,'count_product'=>count($details),'count_price'=>sep($count_price)];
    }



    public function basket_show(){
        $data = AB_basket_update();
        $data['opt'] = AB_get_options();

        return view('basket_show',$data);
    }


}
