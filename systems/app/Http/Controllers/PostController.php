<?php

namespace App\Http\Controllers;


use App\Models\Category;
use App\Models\City;
use App\Models\Factor_detail;
use App\Models\Favorite;
use App\Helpers\Helpers;
use App\Models\Post;
use App\Models\Post_cat;
use App\Models\S_money;

use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;




class PostController extends Controller{

    public function shop_show($id,$slug,$lang=''){

        return $this->show_post('product',$id,$slug,$lang='');
    }
    public function academy_show($id,$slug,$lang=''){
        return $this->show_post('learn',$id,$slug,$lang='');
    }
    public function news_show($id,$slug,$lang=''){
        return $this->show_post('news',$id,$slug,$lang='');
    }
    public function show_post($type,$id,$slug,$lang=''){
        $lang = AB_set_lang($lang);

//        dd($type,$id,$slug,$lang);
        if(!$data['post'] = AB_get_post($id,['type'=>$type,'lang'=>$lang],true,true)){
            session(['alert-danger'=>'پست مورد نظر یافت نشد']);
            return redirect()->route('home');
        }
        $data['opt'] = AB_get_options();
        $data['categories'] = array_reverse(Category::getCatsByParents($data['post']->post->cat));

        $data['related'] = $this->post_related(Category::where('id',$data['post']->cat_id)->first(),$type,0,6,false);

        if($data['post']->type == 'product') {
            $data['props'] = AB_get_props($data['post']->id);
            $data['product'] = $data['post']->post->s_product;
            $data['moneys'] = S_money::getAll();

            return view('show_product', $data);
        }

        return view('show_post',$data);
    }

    public function get_post_stock(Request $request){
        $post = AB_get_post($request->id,['type'=>'product'],false);
        $result = AB_get_post_prop($request->id,json_decode($request->props));
        $result['price_show'] = AB_price_show($post->post->S_product,'','',$result);
        return $result;

    }




    public function academy($id,$slug='',$page=1){
        return $this->category('learn',$id,$slug='',$page=1);
    }

    public function shop($id,$slug='',$page=1){
        return $this->category('product',$id,$slug='',$page=1);
    }
    public function news($id,$slug='',$page=1){
        return $this->category('news',$id,$slug='',$page=1);
    }

    private function post_related($category,$type,$skip,$limit,$by_total=true){
        $cat_ides = [];
        foreach (Category::getCatsByChildes($category) as $childe)
            $cat_ides[]=$childe->id;
        $ides = array_merge(Post_cat::whereIn('cat_id',$cat_ides)->pluck('post_id')->toArray(),Post::whereIn('cat_id',$cat_ides)->pluck('id')->toArray());
        if($type=='product')
            $data['posts'] =  Post::with(['s_product'])->whereIn('id',$ides)->where([['status','publish']])->orderBy('id','DESC')->skip($skip)->take($limit)->get();
        else
            $data['posts'] =  Post::whereIn('id',$ides)->where([['status','publish']])->orderBy('id','DESC')->skip($skip)->take($limit)->get();

        if($by_total) {
            $data['total'] = Post::whereIn('id', $ides)->where([['status', 'publish']])->count();
            return $data;
        }
        return $data['posts'] ?? [];
    }

    public function category($type,$id,$slug='',$page=1){
        $data['page'] = is_numeric($page)&&$page>0?$page:1;
        $data['category'] = Category::with('type')->where('id',$id)->first();

//        $cat_ides = [];
//        foreach (Category::getCatsByChildes($data['category']) as $childe)
//            $cat_ides[]=$childe->id;


        $data['type_id'] = $data['category']->type->id;
        $data['type_slug'] = $data['category']->type->type;
        $data['limit'] = 12;
        $skip = ($data['page']-1) * $data['limit'];

//        $ides = array_merge(Post_cat::whereIn('cat_id',$cat_ides)->pluck('post_id')->toArray(),Post::whereIn('cat_id',$cat_ides)->pluck('id')->toArray());
//        if($data['type_slug']=='product')
//            $data['posts'] =  Post::with(['s_product'])->whereIn('id',$ides)->where([['status','publish']])->orderBy('id','DESC')->skip($skip)->take($data['limit'])->get();
//        else
//            $data['posts'] =  Post::whereIn('id',$ides)->where([['status','publish']])->orderBy('id','DESC')->skip($skip)->take($data['limit'])->get();
//
//        $data['total'] = Post::whereIn('id',$ides)->where([['status','publish']])->count();

        $data = array_merge($data,$this->post_related($data['category'],$data['type_slug'],$skip,$data['limit']));

        $data['targetPage'] = route('category',[$type,$id,$data['category']->slug]);
        $data['opt'] = AB_get_options();
        $data['categories'] = array_reverse(Category::getCatsByParents($data['category']));
        if($data['type_slug']=='product'){

            return view('category-product',$data);
        }

        return view('category',$data);
    }


    public function search_product(Request $request){
        $dives = '';
        if(mb_strlen($request->this_val)<2)
            return ['dives'=>$dives];
        $products = Post::where('title','like','%'.$request->this_val.'%')->skip(0)->take(50)->get();
        $this_val = "
        <div style='background-color: #0baf9a' data-id='0'>
                <p>{$request->this_val}</p>
                <img src='".url('b.png')."'>
                <img src='".url('e.png')."'>
            </div>
        ";
//        $this_val = '';
        foreach ($products as $product){
            if($request->this_val == $product->title)
                $this_val = '';
            $dives .= "
             <div data-id='{$product->id}'>
                <p>{$product->title}</p>
                <img src='".url('b.png')."'>
                <img src='".url('e.png')."'>
            </div>
            ";
        }
        return ['dives'=>$this_val.$dives];

    }




}
