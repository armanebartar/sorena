<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Comment_option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function comment_save(Request $request){
        if(strlen($request->name)<3) {
            return ['res'=>1,'myAlert'=>'نام نمیتواند خالی باشد','mySuccess'=>''];

        }
        if(strlen($request->email)<3) {
            return ['res'=>1,'myAlert'=>'ایمیل/موبایل نمیتواند خالی باشد.','mySuccess'=>''];
        }
//        if(strlen($request->subject)<3) {
//            return ['res'=>1,'myAlert'=>'موضوع نمیتواند خالی باشد.','mySuccess'=>''];
//        }
        if(strlen($request->message)<3) {
            return ['res'=>1,''=>'متن پیام نمیتواند خالی باشد','mySuccess'=>''];

        }

        $avatar = "files/images/avatar.png";
        $comment = new Comment();
        $comment->name = $this->cleanInput($request->name);
        $comment->email =  $this->cleanInput($request->email) ;
        $comment->user_id = 0;
        $comment->user_role = 0;
        $comment->post_id = $request->post_id ?? 0;
        $comment->subject = $request->subject ?? '';

        $comment->content = $this->cleanInput($request->message);
        $comment->parent = $request->comment_parent ?? 0;
        $comment->status = 0;
        $comment->avatar = $avatar;
        $comment->website = $request->website ? $this->cleanInput($request->website) :'';
        $comment->save();

        Comment_option::create(['comment_id' => $comment->id,'key'=>'ip','value'=>$_SERVER['REMOTE_ADDR']]);
        Comment_option::create(['comment_id' => $comment->id,'key'=>'agent','value'=>$_SERVER['HTTP_USER_AGENT']]);
        Comment_option::create(['comment_id' => $comment->id,'key'=>'comment_primitive','value'=>$request->message]);
        return ['res'=>1,'myAlert'=>'','mySuccess'=>'پیام شما با موفقیت دریافت گردید.'];
    }

    public function checkCaptcha(Request $request)
    {
        if(Auth::guard('admin')->check() || Auth::check())
            return ['res'=>1];

        if($request->captcha == session('myCaptcha2','sdasdsdjhsdjbasdgd')){
            return ['res'=>1];
        }
        return ['res'=>0];

    }

    public function contactSave(Request $request){
        if(session('myCaptcha3','kdsjfkhsjdfjdbfjdsf') != $request->captcha){
            session(['alert-danger' => "کد امنیتی وارد شده صحیح نمی‌باشد."]);
            return redirect()->back()->withInput();
        }
        $okSave = false;
        $userId = 0;
        $avatar = '';
        if(!Auth::check() && !Auth::guard('admin')->check()) {
            if (strlen($request->name) < 2)
                session(['alert-danger' => "وارد کردن نام الزامیست."]);
            elseif (strlen($request->email) < 5)
                session(['alert-danger' => "وارد کردن ایمیل یا تلفن الزامیست."]);
            elseif (strlen($request->messageContent) < 5)
                session(['alert-danger' => "محتوای پیام خیلی کوتاه است."]);
            else {
                $name = $request->name;
                $email = $request->email;
                $okSave = true;

            }
        }
        else{
            if (strlen($request->messageContent) < 5)
                session(['alert-danger' => "محتوای پیام خیلی کوتاه است."]);
            else {
                if(Auth::check()) {
                    $name = Auth::user()->name . ' ' . Auth::user()->family;
                    $email = Auth::user()->email . ' - ' . Auth::user()->mobile;
                    $userId = Auth::user()->id;
                }else{
                    $name = Auth::guard('admin')->user()->name . ' ' . Auth::guard('admin')->user()->family;
                    $email = Auth::guard('admin')->user()->email . ' - ' . Auth::guard('admin')->user()->mobile;
                    $userId = Auth::guard('admin')->user()->id;
                }
                $okSave = true;

            }
        }

        if($okSave){
            $coment = new Comment();
            $coment->user_id = $userId;
            $coment->user_role = 1;
            $coment->post_id = 0;
            $coment->name = $name;
            $coment->email = $email;
            $coment->subject = $request->subject;
            $coment->content = $request->messageContent;
            $coment->parent = 0;
            $coment->status = 0;
            $coment->avatar = $avatar;
            $coment->visited = 0;
            $coment->save();
            session(['alert-success' => "پیام شما با موفقیت ذخیره گردید."]);
            return redirect()->back();
        }else
            return redirect()->back()->withInput();
    }

    private function cleanInput($data){
        $data = preg_replace('/<script.+?script>/','',$data);
        $data = strip_tags($data);
        return $data;
    }

    public function commentList($postId){
        $result = '';
        $count = 0;
        $avatar1 = [];
        foreach (Comment::where([['post_id',$postId],['parent',0],['status',1]])->select('*')->get() as $comment){
            $commentId=$comment->id;
            $count++;
            $avatar1[]= url($comment->avatar);
            $avatar = url($comment->avatar);
            $date = jdate('Y-m-d',strtotime($comment->created_at));
            $time = jdate('H:i',strtotime($comment->created_at));
            $result .=<<<EOS
                <li class="comment even thread-even depth-1 single-comment _item _person" id="li-comment-{$commentId}">
                     <div id="comment-{$commentId}" class="comment-body">
                         <div class="_item__user comment-meta ">
                             <img src="{$avatar}" width="35" height="35" alt="{$comment->name}" class="rounded-circle avatar avatar-35wp-user-avatar wp-user-avatar-35 alignnone photo avatar-default" />
                             <div class="body">
                                    <span class="_item__user--name vcard text-dark">
                                        <span class="fn">{$comment->name}</span>
                                    </span>
                                 <span class="_item__user--date text-muted">
                                        <span class="_date">
                                            |
                                            <span class="date">
                                                {$date}
                                            </span>
                                            -
                                            <span class="time">
                                                {$time}
                                            </span>
                                        </span>
                                    </span>
                             </div>
                             <div class="_item__user--like-reply float-left">

                                    <span class="_btn">
                                        <a rel='nofollow' class='comment-reply-link' href='' onclick='return addComment.moveForm("comment-{$commentId}", "{$commentId}", "respond", "{$postId}")' aria-label='dehnavi'>
                                            <i class="fa fa-reply"></i>
                                        </a>
                                    </span>

                             </div>
                             <div class="clearfix"></div>
                         </div>
                         <div class="_item__comment">
                             <div class="item_text">
                                 <p class="">
                                 {$comment->content}
</p>
                             </div>
                         </div>
                     </div>

EOS;
            if($childs=Comment::where([['parent',$comment->id],['status',1]])->select('*')->get()){
                $result .= "<ol class=\"children _item__comment__reply commentlist\">";
                foreach ($childs as $child){
                    $count++;
                    $childId=$child->id;
                    $avatar1[]= url($child->avatar);
                    $avatar = url($child->avatar);
                    $date = jdate('Y-m-d H:i',strtotime($child->created_at));
                    $time = jdate('H:i',strtotime($child->created_at));
                    $result.=<<<EOS

                    <li class="comment odd alt depth-2 single-comment _item _person" id="li-comment-{$childId}">
                        <div id="comment-{$childId}" class="comment-body">
                            <div class="_item__user comment-meta ">
                                <img src="{$avatar}" width="35" height="35" alt="{$child->name}" class="rounded-circle avatar avatar-35wp-user-avatar wp-user-avatar-35 alignnone photo avatar-default" />
                                <div class="body">
                                    <span class="_item__user--name vcard text-dark">
                                        <span class="fn">{$child->name}</span>
                                    </span>
                                    <span class="_item__user--date text-muted">
                                        <span class="_date">
                                            |
                                            <span class="date">
                                                {$date}
                                            </span>
                                            -
                                            <span class="time">
                                                {$time}
                                            </span>
                                        </span>
                                    </span>
                                </div>
                                <div class="_item__user--like-reply float-left">


                                    <span class="_btn">
                                        <a rel='nofollow' class='comment-reply-link' href='' onclick='return addComment.moveForm("comment-{$childId}", "{$commentId}", "respond", "{$postId}")' aria-label='{$child->name}'>
                                            <i class="fa fa-reply"></i>
                                        </a>
                                    </span>

                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="_item__comment">
                                <div class="item_text">
                                    <p class="">{$child->content}</p>
                                </div>
                            </div>
                        </div>
                    </li>

EOS;

                }
                $result .= '</ol>';
            }
            $result .= '<li>';
        }

        return [$result,$count];
    }

}
