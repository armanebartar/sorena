<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Option extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $guarded=[];

    public  static function imagePath($ended=''){
        return "files/options".$ended;
    }


    public static function imageSizes(){
        return [[64,64]];
    }

    public static function imageMaxValue(){
        // by kilo byte
        return 5000;
    }

    public function values(){
        return $this->hasMany(Option_value::class,'option_id','id');
    }

    public static function generateFileOptions(){
        $res = "<?php"."\n";
        $res .= '$myData = [';
        $allOpt = Option::allOptions();
        if(isset($allOpt['fa']))
        foreach ($allOpt['fa'] as $k=>$option){
            $res .= "'{$k}'=>'{$option}',";
        }
        $res .= "];";
        file_put_contents(ROOT.'myData.php',$res);

        if(is_array(LANGUAGES)){
            foreach (LANGUAGES as $LANGUAGE){
                $res = "<?php"."\n";
                $res .= '$myData'.$LANGUAGE[0].' = [';

                if(isset($allOpt[$LANGUAGE[0]]))
                    foreach ($allOpt[$LANGUAGE[0]] as $k=>$option){
                        $res .= "'{$k}'=>'{$option}',";
                    }
                $res .= "];";
                file_put_contents(ROOT.'myData'.$LANGUAGE[0].'.php',$res);
            }
        }

    }


    public static function allOptions(){
        $options = Option::with('values')->where('id','>','0')->get();
        $result = [];
        foreach ($options as $option){
            foreach ($option->values as $value){
                $result[$value->lang][$option->key] = $value->value;
            }
        }
        return $result;
    }

    public static function getOptions($lang='',$groups=null){
        $lang = $lang==''?session('lang',DEFAULT_LANG[0]):$lang;

        $groups = is_null($groups) ? Option_groupe::all() : $groups;
        $result = [];
        foreach ($groups as $group){
            $result[$group->id]=[];
        }


        $options = self::with('values')->where('id','>',0)->orderBy('sort','DESC')->get();

        foreach ($options as $k=>$val){
            $value = '';
            foreach ($val->values as $vv){
                if($vv->lang == DEFAULT_LANG[0]){
                    $value = $vv->value;
                    break;
                }
            }
            $result[$val->group][$val->key]=(object)[
                'id'=>$val->id,
                'title'=>$val->title,
                'key'=>$val->key,
                'type'=>$val->type,
                'value'=>$value,
                'desc'=>$val->desc,
            ];
        }


        if($lang != DEFAULT_LANG[0]){

            foreach ($options as $k=>$val){
                foreach ($val->values as $vv){
                    if($vv->lang == $lang){
                        $result[$val->group][$val->key]->value = $vv->value;
                        break;
                    }
                }
            }
        }
//        dd($result);

        return $result;
    }

//    public static function getOptionsByGroup($groupId,$removeCatch=false){
//        if($removeCatch)
//            Cache::forget('optionsGroup_'.session('lang','fa').$groupId);
//
//        return Cache::get('optionsGroup_'.session('lang','fa').$groupId,function () use ($groupId) {
//            $options=[];
//            foreach (Option::where('groupe',$groupId)->select('key','value','groupe')->get() as $option){
//                $options[$option->key] = session('lang','fa')=='fa' ? $option->value : $option->value_en ;
//            }
//
//            Cache::put('optionsGroup_'.session('lang','fa').$groupId, $options, 1000000);
//            return $options;
//
//        });
//    }

//    public static function getAllOptions($removeCatch=false){
//        if($removeCatch)
//            Cache::forget('allOptions_'.session('lang','fa'));
//
//        return Cache::get('allOptions_'.session('lang','fa'),function ()  {
//            $options=[];
//            foreach (Option::where('id','>',0)->select('key','value','groupe')->get() as $option){
//                $options[$option->key] = session('lang','fa')=='fa' ? $option->value : $option->value_en ;
//            }
//
//            Cache::put('allOptions_'.session('lang','fa'), $options, 1000000);
//            return $options;
//
//        });
//    }
//
//    public static function getOptionByPage($pageName,$removeCatch=true){
//       return self::getAllOptions($removeCatch);
//}




}
