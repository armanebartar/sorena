<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Slider extends Model
{
    use HasFactory;
    public  static function imagePath($ended=''){
        return "files/sliders".$ended;
    }

    public static function imageMaxValue(){
        // by kilo byte
        return 500;
    }

    public static function getSliders($lang='',$groups=null){
        $lang = $lang==''?session('lang',DEFAULT_LANG[0]):$lang;

        $groups = is_null($groups) ? Slider_groupe::all() : $groups;
        $result = [];
        foreach ($groups as $group){
            $result[$group->id]=[];
        }
        $slides = self::where('lang',$lang)->get();
        foreach ($slides as $k=>$val){
                $result[$val->group][$val->id] = (object)[
                    'id' => $val->id,
                    'title' => $val->title,
                    'desc' => $val->desc,
                    'link' => $val->link,
                    'image' => $val->image,
                    'path' => $val->path,
                    'sort' => $val->sort,
                    'link_btn' => $val->link_btn,
                    'titre' => $val->titre,
                ];
        }
        return $result;
    }

    public static function getSliderByGroupId($groupId,$lang='fa',$removeCatch=true){

        if($removeCatch) {
            Cache::forget('sliders_'.$lang.$groupId);
        }
        return Cache::get('sliders_'.$lang.$groupId,function () use ($groupId,$lang) {
            $sliders = [];
            if($result = Slider::where([['group',$groupId],['lang',$lang]])->orderBy('sort','DESC')->get()) {
                foreach ($result as $value)
                    $sliders[]=$value;
            }
            Cache::put('sliders_'.$lang.$groupId, $sliders, 1000000);
            return $sliders;
        });
    }
}
