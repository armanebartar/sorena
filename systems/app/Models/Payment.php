<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory,SoftDeletes;


public static function statuses(){

    $statuses = [
        -1=>'پرداخت رد شده',
        0=>'پرداخت ناموفق',
        1=>'در حال پرداخت',//زمانیکه به درگاه رفته و قصد پرداخت را دارد
        2=>'پرداخت انجام شده در انتظار تایید سیستمی',
        3=>'تایید سیستمی انجام شده در انتظار تایید مدیر',
        4=>'تایید نهایی توسط مدیر',


    ];
}

}
