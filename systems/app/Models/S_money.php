<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class S_money extends Model{
    use HasFactory;
    public static function getAll(){
        $result = [];
        foreach (self::all() as $item)
            $result[$item->id] = $item;
        return $result;
    }
}
