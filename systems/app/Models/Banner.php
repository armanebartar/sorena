<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Banner extends Model
{
    use HasFactory;
    private static $_all_banners = [];
    public  static function imagePath($ended=''){
        return "files/banners".$ended;
    }

    public static function imageMaxValue(){
        // by kilo byte
        return 500;
    }


    public static function getBanners($lang='',$page=null){
        $lang = $lang==''?session('lang',DEFAULT_LANG[0]):$lang;
        $result = [];
        if(is_null($page))
            foreach (Banner::where('lang',$lang)->get() as $banner)
                $result[$banner->page][$banner->id] = $banner;
        else
            foreach (Banner::where([['lang',$lang],['page',$page]])->get() as $banner)
                $result[$banner->page][$banner->id] = $banner;

        return $result;
    }

    public static function getByPage($page,$removeCatch=false,$lang='fa'){

        if($removeCatch) {
            Cache::forget('banners_'.$lang.$page);
        }
        return Cache::get('banners_'.$page,function () use ($page,$lang) {
            $banners = [];
            if($result = Banner::where([['page',$page],['lang',$lang]])->get()) {
                foreach ($result as $value)
                    $banners[$value->id]=$value;
            }
            Cache::put('banners_'.$lang.$page, $banners, 1000000);
            return $banners;
        });
    }

    public static function banners(){

        self::$_all_banners = [];
        foreach (self::where('id','>',0)->get() as $item) {
            if($item->lang==DEFAULT_LANG[0])
                self::$_all_banners[$item->lang][$item->id] = $item;
            else
                self::$_all_banners[$item->lang][$item->parent] = $item;
        }
        return self::$_all_banners;
    }

    public static function get_banner($id,$lang=''){
        if(self::$_all_banners == [])
            self::banners();
        if(isset(self::$_all_banners[$lang]) && isset(self::$_all_banners[$lang][$id]))
            return self::$_all_banners[$lang][$id];
        return self::$_all_banners[$lang][$id] ?? die('بنر مورد نظر یافت نشد!');
    }

}
