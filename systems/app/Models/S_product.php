<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class S_product extends Model
{
    use HasFactory;
    public $timestamps = false;

    public static function user_plans(){
        return [
            1=>['برنزی','bronz'],
            2=>['نقره','silver'],
            3=>['طلایی','gold']
        ];
    }
}
