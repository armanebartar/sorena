<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Price extends Model
{
    use HasFactory;
    public function val()
    {
        return $this->hasOne(Price_value::class,'id','last');
    }

}
