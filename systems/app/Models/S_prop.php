<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class S_prop extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function values(){
        return $this->hasMany(S_prop_value::class,'prop_id');
    }

}
