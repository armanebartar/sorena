<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Brand extends Model
{
    use HasFactory,SoftDeletes;
    public  static function imagePath($ended=''){
        return "files/brands".$ended;
    }

    public static function imageMaxSize(){
        // by kilo byte
        return 500;
    }

    public function type(){
        return $this->hasOne(Post_type::class,'id','group');
    }

    public function options(){
        return $this->hasMany(Brand_option::class,'brand_id','id');
    }

    public static function getAll(){

        $values=self::where('id','>',0)->get();
        $result = [];
        foreach ($values as $value){
            if($value->parent_lang > 0)
                continue;
            $lang = [];
            foreach ($values as $val){
                if($val->parent_lang == $value->id){
                    $lang[$val->lang] = [$val->name,$val->slug];
                }
            }
            $result[$value->id] = (object) ['id'=>$value->id,'name'=>$value->name,'slug'=>$value->slug,'langs'=>$lang];
        }
        $result = (object) $result;
        return $result;


    }

    public static function getBrandsByGroup($group,$remove=false){
        if($remove) {
            Cache::forget('BrandPostByGroup'.$group);
        }
        return Cache::get('BrandPostByGroup'.$group,function () use ($group){
            $result=[];
            $values=self::where([['group',$group]])->get();


            foreach ($values as $value){
                if($value->parent_lang > 0)
                    continue;
                $res1=[];
                $res1['id'] = $value->id;
                $res1['name'] = $value->name;
                $res1['slug'] = $value->slug;
                $res1['image'] = $value->image;
                $res1['path'] = $value->path;
                $res1['langs'] = [];
                foreach ($values as $ll){
                    if($ll->parent_lang == $value->id)
                        $res1['langs'][$ll->lang]=[$ll->name,$ll->slug] ;
                }
                $res1['childes'] =[];



                $result [$value->id] = (object) $res1;
            }

            Cache::put('BrandPostByGroup'.$group,$result,1000000);
            return $result;
        });
    }



    public static function get_by_options($id,$brand=false){
        if(!$brand){
            $brand = self::with(['options'])->where('id',$id)->first();
        }
        if($brand && is_object($brand->options)){
            foreach ($brand->options as $option){
                $brand->{$option->key} = $option->value;
            }
        }
        return $brand;
    }


}
