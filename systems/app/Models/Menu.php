<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Menu extends Model
{
    use HasFactory;
    public  static function imagePath($ended=''){
        return "files/menu".$ended;
    }

    public static function imageMaxVolume(){
        // by kilo byte
        return 500;
    }
    public static function imageSizes(){
        return [[64,64],[200,200]];
    }

    public static function getByGroup($lang,$group,$remove=false){
        if($remove) {
            Cache::forget('menuByGroup'.$lang.$group);
        }
        return Cache::get('menuByGroup'.$lang.$group,function () use ($lang,$group){
            $result=[];
            $values=self::where([['group',$group],['lang',$lang]])->select('*')->get();


            foreach ($values as $value){
                if($value->parent > 0 || $value->parent_lang > 0)
                    continue;
                $res1=[];
                $res1['id'] = $value->id;
                $res1['name'] = $value->name;
                $res1['link'] = $value->link;
                $res1['image'] = $value->image;
                $res1['path'] = $value->path;
                $res1['sort'] = $value->sort;
                $res1['parentId'] = $value->parent;

                $res1['childes'] =[];

                foreach ($values as $val){
                    if($val->parent == $value->id){
                        $res2=[];
                        $res2['id'] = $val->id;
                        $res2['name'] = $val->name;
                        $res2['link'] = $val->link;
                        $res2['image'] = $val->image;
                        $res2['path'] = $val->path;
                        $res2['sort'] = $val->sort;
                        $res2['parentId'] = $val->parent;

                        $res1['childes'][]=(object)$res2;
                    }
                }

                $result [$value->id] = (object) $res1;
            }

            Cache::put('menuByGroup'.$lang.$group,$result,1000000);
            return $result;
        });
    }



}
