<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Special_post extends Model
{
    use HasFactory;
    public static function getAll($special_id){
        $values=self::where('special_id',$special_id)->select('*')->get();
        $result = [];
        foreach ($values as $value){
            if($value->parent > 0)
                continue;
            $lang = [];
            foreach ($values as $val){
                if($val->parent == $value->id){
                    $lang[$val->lang] = [$val->title,$val->postId];
                }
            }
            $result[$value->id] = (object) ['id'=>$value->id,'postId'=>$value->postId,'image'=>$value->image,'path'=>$value->path,'title'=>$value->title,'special_id'=>$special_id,'langs'=>$lang];
        }
        $result = (object) $result;
        return $result;


    }
}
