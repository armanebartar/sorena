<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{

    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public  static function imagePath($endes = ''){
        return "files/images/users".$endes;
    }


    public static function imageSizes(){
        return [[64,64],[200,200]];
    }

    public static function imageMaxValue(){
        // by kilo byte
        return 500;
    }

    public static function hashPass($pass,$other=[]){
        $otherData = '';
        $other = (array) $other;
        foreach ($other as $item)
            $otherData.=$item;
        return md5("hbjhGHVGFGF^%^$%46547668^^%$".$pass."nhjMNH7678943tGVHV654#%^^*&^$#12@#".$otherData);
    }




    public static function defaultImage(){
        return 'files/users/user.png';
    }

    public static function thumbnail(){
        return 'files/users/user.png';
    }



    // relation by user_options table
    public function rel_options(){
        return $this->hasMany(User_option::class);
    }

    public function rel_aff(){
        return $this->hasMany(User::class,'aff','id');
    }
    public function wallet(){
        return $this->hasMany(Wallet::class,'user_id','id');
    }
    public function transfer(){
        return $this->hasMany(User_transfer::class,'user_id','id');
    }
    public function adviser1(){
        return $this->hasOne(User::class,'id','importer');
    }
    public function adviser2(){
        return $this->hasOne(User::class,'id','importer_aff');
    }

    public static function userOptions($userId,$removeCatch=false){
        if($removeCatch)
            Cache::forget('userOptions_'.$userId);

        return Cache::get('userOptions_'.$userId,function () use ($userId) {
            if($user = User::find($userId)){
                $result = [];
                foreach ($user->attributes as $k=>$value)
                    $result[$k] = $value;
                foreach ($user->rel_options as $k=>$value)
                    $result[$value->key] = $value->value;
                Cache::put('userOptions_'.$userId, $result, 60);
                return $result;
            }else
                return false;
        });
    }

    public static function getUserOptions($userId,$optionName){
        $options=self::userOptions($userId);
        if($options && isset($options[$optionName]))
            return $options[$optionName];
        return false;
    }


    // relation by permissions table
    public function permissions(){
        return $this->belongsToMany(Permission::class,'user_permission','user_id','permission_id');
    }

    public static function getAllPermissions($userId,$remove=false){
        if($remove) {
            Cache::forget('permissionsUser_'.$userId);
        }
        return Cache::get('permissionsUser_'.$userId,function () use ($userId) {
            $permissions=[];
            foreach(User_permission::where('user_id',$userId)->get() as $k=>$val){
                $permissions[$val->permissionName->slug] = [$val->status,$val->permissionName->name];
            }
            Cache::put('permissionsUser_'.$userId, $permissions, 1000000);
            return $permissions;

        });
    }

    public static function getPermission($userId,$permissionName='',$removeCatch=false){
        $userRole = Auth::guard('admin')->user()->grade;
        if($userRole == 10 || $userRole==11)
            return [1,'مدیر'];
        $permissions=self::getAllPermissions($userId,$removeCatch);

        if(isset($permissions[$permissionName]))
            return $permissions[$permissionName];
        else{
            if($per= Permission::where('slug',$permissionName)->first())
                return [0,$per->name];
        }
        return [0,''];
    }

    public static function checkPermissionPage($permissionName='',$removeCatch=false){
//        dd($permissionName);
        $userId = Auth::guard('admin')->user()->id;
        $userRole = Auth::guard('admin')->user()->role;
        if($userRole == 10 || $userRole==11)
            return true;

        $permission = self::getPermission($userId,$permissionName,$removeCatch);

        if($permission[1] == 'ناشناخته')
            $permission = self::getPermission($userId,$permissionName,1);
        if(!$permission[0]) {
            addLog('ورود به بخش غیرمجاز');
            session(['alert-danger'=>'دسترسی به این بخش ('.$permission[1].') برای شما میسر نمی‌باشد.']);
            if($permission[1] == 'ناشناخته')
                session(['alert-danger'=>'این دسترسی تعریف نشده است !!']);
            return false;
        }
        return true;
    }


}
