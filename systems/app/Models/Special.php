<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Special extends Model
{
    use HasFactory;
    private static $_all_sp=[];
    public function posts(){
        return $this->hasMany(Special_post::class,'special_id','id');
    }
    public function type(){
        return $this->hasOne(Post_type::class,'type','post_type');
    }

    public static function sp($lang){
        $lang = $lang==''?session('lang',DEFAULT_LANG[0]) : $lang;
        self::$_all_sp = [];
        foreach (Special::with('posts')->where('id','>',0)->get() as $sp){
            $posts = [];
            $ides = [];
            $specs = [];
            foreach ($sp->posts as $post){
                if($post->lang != $lang)
                    continue;
                $ides[]=$post->post_id;
                $specs['title'][$post->post_id]=$post->title;
                $specs['image'][$post->post_id]=$post->image;
                $specs['path'][$post->post_id]=$post->path;
            }
            foreach (Post::with(['cat','s_product'])->whereIn('id',$ides)->orderBy('id','DESC')->skip(0)->take(10)->get() as $key=>$item){
                $posts[$item->id]= $item;
                $posts[$item->id]->title= $specs['title'][$item->id] ?? $item->title;

                if(is_file(ROOT.$specs['path'][$item->id].$specs['image'][$item->id])){
                    $posts[$item->id]->path= $specs['path'][$item->id] ?? $item->path;
                    $posts[$item->id]->image= $specs['image'][$item->id] ?? $item->image;
                }

            }
            self::$_all_sp[$sp->id]['group'] = $sp;
            self::$_all_sp[$sp->id]['posts'] = $posts;

        }
        return self::$_all_sp;
    }

    public static function sp_1($lang,$id){
        if(self::$_all_sp == []){
            self::sp($lang);
        }
        return self::$_all_sp[$id] ?? [];
    }
}
