<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Fire extends Model
{

    public $timestamps = false;
    public $table = 'fire';
}
