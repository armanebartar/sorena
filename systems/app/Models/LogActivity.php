<?php

namespace App\Models;

use App\Http\Middleware\Admin;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    use HasFactory;
    protected $fillable = [

        'subject', 'url', 'method', 'ip', 'agent', 'user_id','inputs','user_type','errors'

    ];
    public function admin1()
    {
        return $this->belongsTo(Admin::class,'user_id');
    }
    public function user1()
    {
        return $this->belongsTo(User::class,'user_id');
    }


}
