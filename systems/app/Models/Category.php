<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Category extends Model
{
    use HasFactory,SoftDeletes;
    public  static function imagePath($ended=''){
        return "files/categories".$ended;
    }

    public static function imageMaxSize(){
        // by kilo byte
        return 500;
    }

    public function type(){
        return $this->hasOne(Post_type::class,'id','group');
    }

    public function options(){
        return $this->hasMany(Cat_option::class,'cat_id','id');
    }
    public function posts(){
        return $this->hasMany(Post::class,'cat_id','id');
    }


    public static function getCatsByGroup($group,$remove=false){
        if($remove) {
            Cache::forget('CatPostByGroup'.$group);
        }
        return Cache::get('CatPostByGroup'.$group,function () use ($group){
            $result=[];
            $values=self::where([['group',$group]])->get();


            foreach ($values as $value){
                if($value->parent > 0 || $value->parent_lang > 0)
                    continue;
                $res1=[];
                $res1['id'] = $value->id;
                $res1['name'] = $value->name;
                $res1['slug'] = $value->slug;
                $res1['image'] = $value->image;
                $res1['path'] = $value->path;
                $res1['parentId'] = $value->parent;
                $res1['langs'] = [];
                foreach ($values as $ll){
                    if($ll->parent_lang == $value->id)
                        $res1['langs'][$ll->lang]=[$ll->name,$ll->slug] ;
                }
                $res1['childes'] =[];

                foreach ($values as $val){
                    if($val->parent == $value->id){
                        $res2=[];
                        $res2['id'] = $val->id;
                        $res2['name'] = $val->name;
                        $res2['slug'] = $val->slug;
                        $res2['image'] = $val->image;
                        $res2['path'] = $val->path;
                        $res2['parentId'] = $val->parent;
                        $res2['langs'] = [];
                        foreach ($values as $ll2){
                            if($ll2->parent_lang == $val->id)
                                $res2['langs'][$ll2->lang]=[$ll2->name,$ll2->slug] ;
                        }
                        $res1['childes'][]=(object)$res2;
                    }
                }

                $result [$value->id] = (object) $res1;
            }

            Cache::put('CatPostByGroup'.$group,$result,1000000);
            return $result;
        });
    }

    public static function get_by_options($id,$category=false){
        if(!$category){
            $category = self::with(['options'])->where('id',$id)->first();
        }
        if($category && is_object($category->options)){
            foreach ($category->options as $option){
                $category->{$option->key} = $option->value;
            }
        }
        return $category;
    }

    public static function getCatsByParents($value,$result=[],$sep='',$step=0,$fromPost=true){
        $res['id'] = $value->id;
        $res['name'] = $value->name;
        $res['slug'] = $value->slug;
        $res['image'] = $value->seo_image;
        $res['parent'] = $value->parent;
        $res['group'] = $value->group;
        $res['sep'] = $sep;
        $step++;
        $result[]=(object)$res;
        if($parent = self::where('id',$value->parent)->first()){
            $result = self::getCatsByParents($parent,$result,$sep.' - ',$step);
        }

        return $result;
    }
    public static function getCatsByChildes($value,$result=[],$sep='',$step=0,$fromPost=true){
        $res['id'] = $value->id;
        $res['name'] = $value->name;
        $res['slug'] = $value->slug;
        $res['image'] = $value->seo_image;
        $res['parent'] = $value->parent;
        $res['group'] = $value->group;
        $res['sep'] = $sep;
        $step++;
        $result[]=(object)$res;
        if($childes = self::where('parent',$value->id)->get()){
            foreach ($childes as $childe){
                $result = self::getCatsByChildes($childe,$result,$sep.' - ',$step);
            }

        }

        return $result;
    }

}
