<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Tag extends Model
{
    use HasFactory,SoftDeletes;
    public static function getAll(){

        $values=self::where('id','>',0)->select('*')->get();
        $result = [];
        foreach ($values as $value){
            if($value->parent_lang > 0)
                continue;
            $lang = [];
            foreach ($values as $val){
                if($val->parent_lang == $value->id){
                    $lang[$val->lang] = [$val->name,$val->slug];
                }
            }
            $result[$value->id] = (object) ['id'=>$value->id,'name'=>$value->name,'slug'=>$value->slug,'langs'=>$lang];
        }
        $result = (object) $result;
        return $result;


    }

    public function options(){
        return $this->hasMany(Tag_option::class,'tag_id','id');
    }

    public static function get_by_options($id,$tag=false){
        if(!$tag){
            $tag = self::with(['options'])->where('id',$id)->first();
        }
        if($tag && is_object($tag->options)){
            foreach ($tag->options as $option){
                $tag->{$option->key} = $option->value;
            }
        }
        return $tag;
    }
}
