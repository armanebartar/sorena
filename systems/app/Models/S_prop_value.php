<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class S_prop_value extends Model
{
    use HasFactory;
    public $timestamps = false;
}
