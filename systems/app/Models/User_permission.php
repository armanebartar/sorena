<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_permission extends Model
{
    use HasFactory;
    public function permissionName(){
        return $this->hasOne(Permission::class,'id','permission_id');
    }
}
