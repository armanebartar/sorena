<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class City extends Model
{
    public $timestamps = false;

    public static function getAll($removeCatch=false){
        $catchName = "city_all";

        if($removeCatch)
            Cache::forget($catchName);

        return Cache::get($catchName,function () use ($catchName) {
            $result = [];
            foreach (self::where('id','>',0)->orderBy('name','ASC')->get() as $val)
                $result [$val->id] = $val;
            Cache::put($catchName, $result, 1000000);
            return $result;
        });
    }

    public  static function getCity($id,$removeCatch=false){
        $all = self::getAll($removeCatch);
        return isset($all[$id]) ? $all[$id] : false;
    }
    public static function getByState($stateId,$removeCatch=false){
        $result = [];
        if($stateId == 11)
            $result []= (object) ['id'=>472,'name'=>'نیشابور','state_id'=>11];
        foreach (self::getAll($removeCatch) as $value){
            if($value->state_id == $stateId)
                $result[]=$value;
        }
        return $result;
    }
}
