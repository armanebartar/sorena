<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Factor_history extends Model
{
    use HasFactory;
    public static function new_history($factor_id,$new_status){
        $desc = Factor::statuses_short($new_status);
        $h = new Factor_history();
        $h->user_id = auth()->check() ? auth()->user()->id : 0;
        $h->factor_id = $factor_id;
        $h->desc = 'تغییر وضعیت به : '.Factor::statuses_short($new_status);
        $h->new_status = $new_status;
        $h->save();
    }
}
