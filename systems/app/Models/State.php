<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class State extends Model
{
    public $timestamps = false;
    public static function getAll($removeCatch=false){
        $catchName = "state_all";

        if($removeCatch)
            Cache::forget($catchName);

        return Cache::get($catchName,function () use ($catchName) {
            $result = [];
            foreach (self::where('id','>',0)->orderBy('name','ASC')->get() as $val)
                $result [$val->id] = $val;
            Cache::put($catchName, $result, 1000000);
            return $result;
        });
    }


    public  static function getState($id,$removeCatch=false){
        $all = self::getAll($removeCatch);
        return isset($all[$id]) ? $all[$id] : false;
    }

    public function cities(){
        return $this->hasMany(City::class,'state_id','id');
    }

}
