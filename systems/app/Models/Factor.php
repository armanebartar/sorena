<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Factor extends Model
{
    use HasFactory,SoftDeletes;


public static function statuses_short($status_key='all'){
     $statuses = [
        0=>'سبد خرید',
        1=>'در انتظار تایید اولیه',
        2=>'در انتظار تایید نهایی',
        3=>'در انتظار تایید مشتری',
        4=>'در انتظار پرداخت',
        5=>'پرداخت شده',
        6=>'تایید سیستمی پرداخت',
        7=>'تایید نهایی پرداخت',
        8=>'در حال جمع آوری',
        9=>'در حال بسته بندی',
        10=>'ارسال به پست',
        11=>'پست به مشتری',
        12=>' تحویل به مشتری',
    ];
     if($status_key == 'all')
         return $statuses;
     if($status_key>3)
         return 'تایید نهایی مشتری';
     return $statuses[$status_key] ?? 'نامعلوم';
}
public static function statuses($status_key='all'){
     $statuses = [
        0=>'سبد خرید',
        1=>'پیش فاکتور و در انتظار تایید اولیه مدیر',
        2=>'در انتظار تایید نهایی مدیر',
        3=>'پیش فاکتور تایید شده توسط مدیر در انتظار تایید کاربر',
        4=>'پیش فاکتور تایید شده توسط کاربر و در انتظار پرداخت',
        5=>'پرداخت شده در انتظار تایید پرداخت',
        6=>'تایید سیستمی پرداخت',
        7=>'پرداخت توسط مدیر تایید شده و ارسال به انبار',
        8=>'در حال جمع آوری در انبار',
        9=>'در حال بسته بندی',
        10=>'ارسال به پست',
        11=>'پست به مشتری',
        12=>'تایید تحویل توسط مشتری',
    ];
     if($status_key == 'all')
         return $statuses;
     return $statuses[$status_key] ?? 'نامعلوم';
}
    public function details(){
        return $this->hasMany(Factor_detail::class,'factor_id','id');
    }
    public function address(){
        return $this->hasOne(Address::class,'id','address_id');
    }
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
