<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Factor_detail extends Model
{
    use HasFactory,SoftDeletes;
    public function post(){
        return $this->hasOne(Post::class,'id','post_id');
    }
}
