<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Post_prop extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function prop(){
        return $this->hasOne(S_prop::class,'id','prop_id');
    }
    public function val(){
        return $this->hasOne(S_prop_value::class,'id','prop_val_id');
    }

}
