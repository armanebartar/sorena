<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasFactory;
    protected $guarded=[];
    public $timestamps = false;
    private static $_user_permissions = null;

    public static function getPermissionsGrouped(){
        $result = [];
        foreach (Permission::all() as $value){
            $result [$value->group][] = $value;
        }
        return $result;
    }

    public static function user_permission($permission_key){
        if(auth()->check())
            $user_id = auth()->user()->id;

        else if(auth('admin')->check()) {
            if(auth('admin')->user()->grade > 9)
                return true;
            $user_id = auth('admin')->user()->id;
        }
        else
            return false;

        if(is_null(self::$_user_permissions)){
            $user_permissions = [];
            if($permissions = User_permission::with(['permissionName'])->where('user_id',$user_id)->get()){

                foreach ($permissions as $permission){
                    if($permission->permissionName){
                        $user_permissions[$permission->permissionName->slug] = $permission->permissionName->name;
                    }
                }
            }
            self::$_user_permissions = $user_permissions;
        }

        return isset(self::$_user_permissions[$permission_key]) ;
    }

}
