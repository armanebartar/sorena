<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post_type extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $guarded=[];

    public function specials(){
        return $this->hasMany(Special::class,'post_type','type');
    }

    public static function checkExistPermission($postType){

//        dd($postType->status,$postType);
        if($postType->status == 0){
            $type = $postType->type;
            $uc = ucfirst($type);
            $permissions = [
                [$type.'sShow','مشاهده لیست '.$postType->title],
                ['add'.$uc,'افزودن '.$postType->nameSingle],
                [$type.'Edit','ویرایش '.$postType->nameSingle],
                ['my'.$uc.'Edit','ویرایش '.$postType->nameTotal.' خودش'],
                [$type.'Remove','حذف '.$postType->nameSingle],
                ['my'.$uc.'Remove','حذف '.$postType->nameTotal.' خودش'],
                [$type.'ChangeStatus','تغییر وضعیت '.$postType->nameTotal],
                [$type.'AddTranslate','افزودن و ویرایش ترجمه  '.$postType->nameTotal],
                [$type.'AddMyTranslate','افزودن و ویرایش ترجمه  '.$postType->nameTotal.' خودش'],

                ['addCategory'.$postType->id,'افزودن دسته برای '.$postType->title],
                ['editCategory'.$postType->id,'ویرایش دسته های '.$postType->title],
                ['removeCategory'.$postType->id,'حذف دسته های '.$postType->title],
                ['addLangCategory'.$postType->id,'مدیریت ترجمه دسته های '.$postType->title],

                ['addTag','افزودن برچسب'],
                ['editTag','ویرایش برچسبها'],
                ['removeTag','حذف برچسب'],
                ['addLangTag','مدیریت ترجمه برچسبها'],

                ['special'.$postType->id,'مدیریت مطالب ویژه '.$postType->title],

                [$type.'Comments','مشاهده کامنتهای '.$postType->title],
                [$type .'changeStatusComment','تغییر وضعیت کامنتهای '.$postType->title],
                [$type .'editComment','ویرایش کامنتهای '.$postType->title],
                [$type.'removeComment','حذف کامنتهای '.$postType->title],

            ];
            $pp = Permission::where('id','>','0')->orderBy('group', 'DESC')->first();
            $group = $pp->group;
            $group++;
//            dd($permissions);
            foreach ($permissions as $permission){
                if(Permission::where('slug',$permission[0])->count() < 1){
                    $per = new Permission();
                    $per->name = $permission[1];
                    $per->slug = $permission[0];
                    $per->group = $group;
                    $per->gr_name = 'مدیریت '.$postType->title;
                    $per->save();
                }
            }
            self::where('id',$postType->id)->update(['status'=>'1']);
        }
    }

}
