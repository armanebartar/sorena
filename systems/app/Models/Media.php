<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Media extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $table = 'files';

    private static $typesValid = [
            'image'=>['jpeg','jpg','png','gif'],
            'pdf'=>['pdf'],
            'video'=>['mp4','wma'],
            'audio'=>['mp3'],
            'excel'=>['xls','excel','csv'],
            'ppt'=>['ppt'],
            'doc'=>['doc','docs'],
            'json'=>['json'],
            'rar'=>['rar'],
            'txt'=>['txt'],
            'xml'=>['xml'],
            'zip'=>['zip'],
        ];

    private static $mimeTypesValid = [
        'image'=>['image/jpeg','image/png','image/gif','image/xpng','image/bmp','image/jpg'],
        'pdf'=>['application/pdf'],
        'mpeg'=>['video/mpeg'],
        'mp3'=>['audio/mpeg'],
        'mp4'=>['video/mp4'],
        'excel'=>['application/vnd.ms-excel','application/msexcel','application/x-msexcel','application/x-ms-excel','application/x-excel','application/x-dos_ms_excel','application/xls','application/x-xls','application/vnd.ms-excel'],
        'ppt'=>['application/vnd.ms-powerpoint','application/vnd.openxmlformats-officedocument.presentationml.presentation','application/vnd.openxmlformats-officedocument.presentationml.template','application/vnd.openxmlformats-officedocument.presentationml.slideshow','application/vnd.ms-powerpoint.addin.macroEnabled.12','application/vnd.ms-powerpoint.presentation.macroEnabled.12','application/vnd.ms-powerpoint.template.macroEnabled.12','application/vnd.ms-powerpoint.slideshow.macroEnabled.12'],
        'doc'=>['application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/vnd.openxmlformats-officedocument.wordprocessingml.template','application/vnd.ms-word.document.macroEnabled.12'],
        'csv'=>['text/csv','application/csv','application/x-csv','text/comma-separated-values','text/x-comma-separated-values','text/tab-separated-values'],
        'json'=>['application/json','application/x-javascript','text/javascript','text/x-javascript','text/x-json'],
        'rar'=>['application/x-rar-compressed','application/octet-stream'],
        'txt'=>['text/plain'],
        'xml'=>['text/xml','application/xml'],
        'zip'=>['application/zip','application/x-zip-compressed','application/octet-stream','multipart/x-zip'],
    ];

    public static function getTypes($typeNames){
        $result = '';
        $typeNames = (array) $typeNames;
        $validTypes = self::$typesValid;
        foreach ($typeNames as $typeName) {
            $typeName = trim($typeName);
            if (isset($validTypes[$typeName]))
                $result .= implode(', ', $validTypes[$typeName]) . ', ';
        }

        return trim($result,', ');
    }

    public static function getMimeTypes($mimeNames){
        $result = '';
        $mimeNames = (array) $mimeNames;
        $validTypes = self::$mimeTypesValid;
        foreach ($mimeNames as $typeName)
            if(isset($validTypes[$typeName]))
                $result .= implode(', ',$validTypes[$typeName]).', ';

        return trim($result,', ');
    }

    public  static function imagePath($ended=''){
        return "files/uploads".$ended;
    }

    public static function imageMaxSize(){
        // by kilo byte
        return 500;
    }





}
