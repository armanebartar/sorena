<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    use HasFactory,SoftDeletes;
    public static $_melk_options = [
        ['melk_type','نوع ملک','combo',['آپارتمان','ویلایی','زمین','زمین کشاورزی']],
        ['melk_noy','نوع معامله','combo',['فروش','اجاره']],
        ['melk_mohajer','مناسب اخذ اقامت','radio',['بله','خیر']],
        ['melk_price','قیمت','text',''],
        ['melk_meter','متراژ','text',''],
        ['melk_zir','زیربنا','text',''],

        ['melk_price_type','واحد پول','combo',['میلیون تومان','لیر','دلار','یورو']],
        ['melk_address','آدرس','text',''],
        ['melk_khab','تعداد خواب','combo',[0,1,2,3,4,5,6,7,8,9,10]],
        ['melk_kitchen','تعداد آشپزخانه','combo',[0,1,2,3,4]],
        ['melk_garaje','پارکینگ','radio',['دارد','ندارد']],
        ['melk_options','سایر امکانات','checkbox',['پارکینگ','استخر','سونا']],
    ];

//    public  static function imagePath($ended=''){
//        return "files/images/post".$ended;
//    }


//    public static function imageSizes(){
//        return [[64,64],[200,200]];
//    }

//    public static function imageMaxValue(){
//        // by kilo byte
//        return 500;
//    }

//    public static function typeValid(){
//
//        return ['image','pdf','mpeg','mp3','mp4','excel','ppt','doc','csv','json','rar','txt','xml','zip'];
//    }

    public static function secure($data){
        $data = (array) $data;
        return substr(md5(implode('',$data).env('salt')),5,13);
    }





    // relation by post_options table
    public function options(){
        return $this->hasMany(Post_option::class,'post_id');
    }

    public function typed(){
        return $this->hasOne(Post_type::class,'type','type');
    }
    public function thumb(){
        return $this->hasOne(Media::class,'id','thumbnail');
    }

    public function attaches(){
        return $this->hasMany(File::class,'post_id','id');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'post_tags','post_id','tag_id');
    }

    public function cats(){
        return $this->belongsToMany(Category::class,'post_cats','post_id','cat_id');
    }
    public function cat(){
        return $this->hasOne(Category::class,'id','cat_id');
    }
    public function rel_author(){
        return $this->hasOne(User::class,'id','author');
    }

    public function attrs(){
        return $this->hasMany(Post_attribute::class,'post_id');
    }
    public function props(){
        return $this->hasMany(Post_prop::class,'post_id');
    }
    public function s_commission(){
        return $this->hasOne(S_commission::class,'post_id');
    }
    public function s_money(){
        return $this->hasOneThrough(
            S_money::class,
            S_product::class,
            'post_id',
            'id',
            'id',
            'money_id'
        );
    }
    public function s_product(){
        return $this->hasOne(S_product::class,'post_id');
    }
    public function s_discount(){
        return $this->hasOne(S_discount::class,'post_id');
    }

    public static function make_draft($type){
        $post = new Post();
        $post->type = $type;
        $post->author = auth('admin')->user()->id;
        $post->status = 'draft';
        $post->parent = 0;
        $post->lang = DEFAULT_LANG[0];
        $post->save();
        return $post->id;

    }


    public static function postData($post,$byCat=false,$byAttach=false){
        $res = [
            'id'=>$post->id,
            'title'=>$post->title,
            'slug'=>$post->slug,
            'content'=>$post->content,
            'commentCount'=>$post->commentCount,
            'visited'=>$post->visited,
            'type'=>$post->type,
            'subTitle'=>$post->subTitle,
            'minContent'=>$post->minContent,
            'image'=>$post->image,
            'date'=>$post->date,
            'status'=>$post->status,
        ];
        foreach ($post->options as $option)
            $res[$option->key] = $option->value;
        if($post->type == 'estate'){
            foreach (self::$_melk_options as $k=>$v){
                if(!isset($res[$v[0]]))
                    $res[$v[0]] = '';
            }
        }
        if($byCat)
            $res['cats'] = $post->cats;
        if($byAttach)
            $res['attaches'] = $post->attaches;

        return (object) $res;
    }

}
